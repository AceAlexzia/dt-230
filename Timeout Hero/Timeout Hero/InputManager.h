#pragma once
#ifndef INPUT_MANAGER
#define INPUT_MANAGER

#include "SDL.h"
#include "Game.h"
#include "ControllerManager.h"

//creates enum for 2 axis
enum Axis { HORIZONTAL, VERTICAL };

class InputManager
{
private:
	InputManager();
	static InputManager* s_Instance;
	const Uint8* m_KeyState;
	std::vector<SDL_Scancode> m_PressingKeys;
	std::vector<SDL_Scancode> m_KeyUpKeys;

	static ControllerManager s_ControllerManager;

	//private function
	int IsPressingKey(SDL_Scancode key);
	void ClearUnpressedKeys();

public:
	~InputManager() {}
	static InputManager* GetInstance();
	void Listen();

	//Keyboard Getters
	bool GetKey(SDL_Scancode key);
	bool GetKeyUp(SDL_Scancode key);
	bool GetKeyDown(SDL_Scancode key);
	int GetAxisKey(Axis axis);

	//Controller Getters
	bool GetKey(SDL_GameControllerButton button);
	bool GetKeyUp(SDL_GameControllerButton button);
	bool GetKeyDown(SDL_GameControllerButton button);
	float GetAxisController(SDL_GameControllerAxis axis);

	bool HasController();

};

#endif // !INPUT_MANAGER
