#include "PauseManager.h"
#include "TransitionManager.h"

PauseManager* PauseManager::s_Instance = nullptr;


PauseManager* PauseManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new PauseManager();
}

bool PauseManager::GetIsPause()
{
	return m_IsPause;
}


void PauseManager::Update()
{
	//if it is not in the game scene -> no need to update
	if (SceneManager::GetInstance()->scene->name != GAME_SCENE) 
	{
		return;
	}
	//if it is in the game scene
	else 
	{
		//if there is a transition
		//no need to update
		if (TransitionManager::GetInstance()->IsTransition()) 
		{
			return;
		}
	}

	if (!m_IsPause)
	{
		if (InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_ESCAPE) || InputManager::GetInstance()->GetKeyDown(SDL_CONTROLLER_BUTTON_START))
		{
			if (m_IsPause)
			{
				Resume();
			}
			else
			{
				Pause();
			}
		}
	}
	
}


void PauseManager::Pause()
{
	m_IsPause = true;
	//spawn pause ui entity
	UIManager::GetInstance()->SpawnPauseUI();
	UIManager::GetInstance()->PressPauseMenu();
}

void PauseManager::Resume()
{
	m_IsPause = false;
	//destroy pause ui entity
	UIManager::GetInstance()->RemovePauseUI();
}