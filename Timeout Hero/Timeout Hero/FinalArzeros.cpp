#include "FinalArzeros.h"
#include "LevelManager.h"
#include "TransitionManager.h"

void FinalArzeros::Initialize()
{
	//add all neccessary components
	Initialize_Component();

	//set collider box position
	SetCollider();

	//set map boundary
	GameMap* gameMap = LevelManager::GetInstance()->GetMap();
	m_RowCount = gameMap->GetRowCount();
	m_ColCount = gameMap->GetColCount();
	m_TileSize = gameMap->GetTileSize();

	//set spawning delay for each health state
	m_MonsterSpawningDelay[4] = Vector2D(3.1f, 5.1f);
	m_MonsterSpawningDelay[3] = Vector2D(2.0f, 2.6f);
	m_MonsterSpawningDelay[2] = Vector2D(1.8f, 2.6f);
	m_MonsterSpawningDelay[1] = Vector2D(1.6f, 2.5f);
}

void FinalArzeros::Update(float deltaTime)
{
	//movement updateing
	Update_Movement(deltaTime);
	//finish state updating
	Update_FinishState(deltaTime);
	//update hurt state
	Update_Hurt(deltaTime);
	//update moving direction
	Update_Moving();
	//updating collision
	Update_Collision();
	//updating animation state
	Update_Animation(deltaTime);
	//updating monster spawning
	Update_MonsterSpawning(deltaTime);
}


void FinalArzeros::Initialize_Component()
{
	//initialize component
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(0.0f, 0.0f);
		m_Transform = owner->GetComponent<Transform>();
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("arzeros_idle", 232, 180, MONSTER_LAYER);
		m_Sprite = owner->GetComponent<Sprite>();
	}
	if (!owner->HasComponent<SpriteAnimation>())
	{
		owner->AddComponent<SpriteAnimation>();
		m_Anim = owner->GetComponent<SpriteAnimation>();
	}
	if (!owner->HasComponent<Collider>())
	{
		owner->AddComponent<Collider>();
		m_Collider = owner->GetComponent<Collider>();
	}
	if (!owner->HasComponent<Rigidbody>())
	{
		owner->AddComponent<Rigidbody>();
		m_Rigidbody = owner->GetComponent<Rigidbody>();
	}

	m_Rigidbody->setGravity(0.0f);

	m_Anim->SetProps("arzeros_idle", 8, 200);

	m_Sprite->m_Angle = 90.0f;

	m_Transform->position = Vector2D(720.0f, -1.0f * m_Sprite->GetWidth());

	m_Sprite->m_Flip = SDL_FLIP_NONE;

	m_Collider->m_IsEnable = true;
}

void FinalArzeros::Update_Movement(float deltatime)
{
	//when the HP = 3
	if (m_HP == 4) 
	{
		if (m_MovingDir == DOWN)
		{
			//apply force to the lower direction
			m_Rigidbody->ApplyForceY(ARZEROS_ACCELERATION);

			//when it reaches its max lower position
			if (m_Transform->position.y >= 96.0f)
			{
				m_Transform->position.y = 96.0f;
				m_Rigidbody->m_velocity.y = 0.0f;
				m_MovingDir = UP;
			}
		}
		else 
		{
			if (m_Rigidbody->m_velocity.y != 0.0f)
			{
				m_Rigidbody->m_velocity.y = 0.0f;
				if (m_Transform->position.y >= 96.0f)
				{
					m_Transform->position.y = 96.0f;
				}
			}
		}
	}
	//when the HP = 2
	else if (m_HP == 2) 
	{
		//if Arzeros is not in the center
		if (!m_StartSweeping) 
		{
			//check on the x-axis
			if (m_Transform->position.x != 720.0f) 
			{
				if (m_Transform->position.x - 720.0f < 10.0f && m_Transform->position.x - 720.0f > -10.0f)
				{
					m_Transform->position.x = 720.0f;
					m_Rigidbody->m_velocity.x = 0.0f;
				}
				else
				{
					if (m_Transform->position.x - 720.0f < 0.0f)
					{
						m_Rigidbody->ApplyForceX(ARZEROS_ACCELERATION);
					}
					else
					{
						m_Rigidbody->ApplyForceX(-ARZEROS_ACCELERATION);
					}
				}
			}
			
			//check on the y-axis
			if (m_Transform->position.y != 48.0f) 
			{
				if (m_Transform->position.y - 48.0f < 10.0f && m_Transform->position.y - 48.0f > -10.0f)
				{
					m_Transform->position.y = 48.0f;
				}
				else
				{
					if (m_Transform->position.y - 48.0f < 0.0f)
					{
						m_Rigidbody->ApplyForceY(ARZEROS_ACCELERATION);
					}
					else
					{
						if (m_Rigidbody->m_velocity.y > 0.0f) 
						{
							m_Rigidbody->m_velocity.y = 0.0f;
						}
						m_Rigidbody->ApplyForceY(-ARZEROS_ACCELERATION);
					}
				}
			}

			//if Arzeros is in the center ->  start sweeping
			if (m_Transform->position.x == 720.0f && m_Transform->position.y == 48.0f)
			{
				m_StartSweeping = true;
			}
			
		}
		else 
		{
			if (m_Rigidbody->m_velocity.y != 0.0f)
			{
				m_Rigidbody->m_velocity.y = 0.0f;
			}

			//when the direction is not related -> time increased
			if ((m_Rigidbody->Position().x > 0.0f && m_SweepingForce > 0.0f) || (m_Rigidbody->Position().x < 0.0f && m_SweepingForce < 0.0f))
			{
				m_SweepingCounter += deltatime;
			}

			//when the time reaches its max -> invert the force direction
			if (m_SweepingCounter > m_SweepingTime)
			{
				m_SweepingCounter = 0.0f;
				m_SweepingForce *= -1.0f;
			}

			//apply the force to rigidbody
			m_Rigidbody->ApplyForceX(m_SweepingForce);
		}
		
	}
	//when the HP = 1
	else if (m_HP == 1 || m_HP == 3) 
	{
		//when the state is NOT actually started
		if (!m_StartFinal) 
		{
			//apply force to make it stay still
			if (m_Rigidbody->Position().x > 0)
			{
				m_Rigidbody->ApplyForceX(-ARZEROS_ACCELERATION);
			}
			else
			{
				m_Rigidbody->ApplyForceX(ARZEROS_ACCELERATION);
			}


			//when the force is low enough -> start the last state
			if (m_Rigidbody->m_velocity.x < 10.0f || m_Rigidbody->m_velocity.x > -10.0f)
			{
				m_StartFinal = true;
			}
		}
		//when the state is actually started
		else 
		{
			//make sure there's no movement on the x-axis
			if (m_Rigidbody->m_velocity.x != 0.0f) 
			{
				m_Rigidbody->m_velocity.x = 0.0f;
			}

			//moving up state
			if (m_MovingDir == UP)
			{
				//apply force to the upper direction
				m_Rigidbody->ApplyForceY(-ARZEROS_ACCELERATION);

				//when it reaches its max upper position
				if (m_Transform->position.y <= -m_Sprite->GetWidth())
				{
					m_Transform->position.y = -m_Sprite->GetWidth();
					m_Rigidbody->m_velocity.y = 0.0f;
					m_MovingDir = DOWN;

					m_Transform->position.x = m_DroppingPosX[(int)GetRandom(0.0f, 2.99f)];
				}
			}
			else if (m_MovingDir == DOWN)
			{
				//apply force to the lower direction
				m_Rigidbody->ApplyForceY(ARZEROS_ACCELERATION * 2.0f);

				//when it reaches its max lower position
				if (m_Transform->position.y > 192.0f)
				{
					m_Transform->position.y = 192.0f;
					m_Rigidbody->m_velocity.y = 0.0f;;
					m_MovingDir = UP;
				}
			}
		}

	}
	//dead state
	else 
	{
		//stop it movement on y-axis
		if (m_Rigidbody->m_velocity.y != 0.0f)
		{
			m_Rigidbody->m_velocity.y = 0.0f;
		}

		//stop it movement on x-axis
		if (m_Rigidbody->m_velocity.x != 0.0f)
		{
			m_Rigidbody->m_velocity.x = 0.0f;
		}
	}
}

void FinalArzeros::Update_FinishState(float deltatime)
{
	//if the state is finished
	if (m_FinishState)
	{
		if (m_IsDead)
		{

			//set offset of x and y
			float offsetX = GetRandom(-1.0f, 1.0f) * FINAL_ARZEROS_SHAKE_AMOUNT * deltatime;
			float offsetY = GetRandom(-1.0f, 1.0f) * FINAL_ARZEROS_SHAKE_AMOUNT * deltatime;

			//set position = currentPosition + offset
			m_Transform->position = Vector2D(m_ShakingPos.x + offsetX, m_ShakingPos.y + offsetY);
		}
		else
		{
			Blink(deltatime);

			//first frame
			if (m_DeadTimeCounter == 0.0f && m_HP <= 0)
			{
				LevelManager::GetInstance()->PauseTime();
			}

			//time counting before entering dead animation state
			m_DeadTimeCounter += deltatime;

			if (m_HP <= 0) 
			{
				
				if (m_DeadTimeCounter > 2.0f)
				{
					Dead();

				}
			}
			else 
			{
				if (m_DeadTimeCounter > 8.0f)
				{
					m_DeadTimeCounter = 0.0f;

					m_FinishState = false;
					LevelManager::GetInstance()->ResumeTime();
					m_Sprite->m_IsEnable = true;

				}
			}
			
		}
	}

}


void FinalArzeros::Update_Hurt(float deltatime)
{
	if (m_IsHurt)
	{
		m_HurtTime += deltatime;

		if (m_HurtTime >= FINAL_ARZEROS_HURT_TIME)
		{
			m_HurtTime = 0.0f;
			m_IsHurt = false;

			//set finish state
			m_FinishState = true; 

		}
	}
}


void FinalArzeros::Update_Moving()
{

	//limits the velocity
	//velocity X
	if (m_Rigidbody->m_velocity.x < -FINAL_ARZEROS_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.x = -FINAL_ARZEROS_MAX_VELOCITY;
	}
	else if (m_Rigidbody->m_velocity.x > FINAL_ARZEROS_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.x = FINAL_ARZEROS_MAX_VELOCITY;
	}
	//velocity Y
	if (m_Rigidbody->m_velocity.y < -FINAL_ARZEROS_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.y = -FINAL_ARZEROS_MAX_VELOCITY;
	}
	else if (m_Rigidbody->m_velocity.y > FINAL_ARZEROS_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.y = FINAL_ARZEROS_MAX_VELOCITY;
	}

	//change sprite flipping direction
	if (m_Rigidbody->m_velocity.x < -FINAL_ARZEROS_MAX_VELOCITY && m_Sprite->m_Flip == SDL_FLIP_HORIZONTAL)
	{
		m_Sprite->m_Flip = SDL_FLIP_NONE;
	}
	else if (m_Rigidbody->m_velocity.x > FINAL_ARZEROS_MAX_VELOCITY && m_Sprite->m_Flip == SDL_FLIP_NONE)
	{
		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
	}

	//set new position
	m_Transform->Translate(m_Rigidbody->Position());
	//set the collider size
	SetCollider();

}


void FinalArzeros::Update_Collision()
{
	//check whether the object's position is off the map
	if (m_Collider->Get().x + m_Collider->Get().w < 0 || m_Collider->Get().x > m_ColCount * m_TileSize || m_Collider->Get().y + m_Collider->Get().h < 0 || m_Collider->Get().y > m_RowCount * m_TileSize)
	{
		if (m_Collider->m_IsEnable) 
		{
			m_Collider->m_IsEnable = false;
		}
	}
	else 
	{
		if (!m_IsHurt && !m_FinishState && !m_Collider->m_IsEnable) 
		{
			m_Collider->m_IsEnable = true;
		}
	}

	//if player collides with this
	if (CollisionTriggerred())
	{
		//if the player is dashing
		if (m_Player->Get_IsDashing())
		{
			//set IsDashThrough of Hero
			m_Player->m_IsDashThrough = true;
			m_IsHurt = true;

			//collision is disabled
			m_Collider->m_IsEnable = false;

			//decrease hp
			m_HP--;

			//if the hp is less than 0
			if (m_HP <= 0) 
			{
				//set event
				CutsceneManager::GetInstance()->m_IsBossEvent = true;
			}
			
		}
		//if the player is not blinking
		else if (!m_Player->Get_IsBlinking())
		{
			//the player hurts
			m_Player->m_IsHurt = true;
		}
	}
}

void FinalArzeros::Update_Animation(float deltatime)
{
	//dead state
	if (m_IsDead) 
	{
		m_Anim->SetProps("arzeros_dead", 4, 250);
	}
	//is hurting state
	else if (m_IsHurt)
	{
		m_Anim->SetProps("arzeros_hurt", 2, 0);
	}
	//sprite swapping
	else if (m_HP < 4 && m_SwappingCounter > FINAL_ARZEROS_SWAPPING_TIME * m_HP)
	{
		m_Anim->SetProps("arzeros_hurt_final", 1, 0);

		m_SwappingCounter += deltatime;
		if (m_SwappingCounter - (FINAL_ARZEROS_SWAPPING_TIME * m_HP) > FINAL_ARZEROS_SWAPPING_LENGTH)
		{
			m_SwappingCounter = 0.0f;
		}
	}
	//idle state
	else
	{
		m_Anim->SetProps("arzeros_idle", 8, 200);

		m_SwappingCounter += deltatime;
	}
}

void FinalArzeros::Update_MonsterSpawning(float deltatime) 
{
	if (m_HP <= 0 || !m_IsAttacking) 
	{
		return;
	}

	m_MonsterSpawningCounter += deltatime;

	float spawningDelay = GetRandom(m_MonsterSpawningDelay[m_HP].x, m_MonsterSpawningDelay[m_HP].y);

	if (m_MonsterSpawningCounter > spawningDelay)
	{
		SpawnArchMonster();
		m_MonsterSpawningCounter = 0.0f;
	}
	
}


void FinalArzeros::SetCollider()
{
	//set the offset
	m_Collider->SetBuffer(-45, -45, 90, 30);
	//set the collider
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}

void FinalArzeros::SpawnArchMonster()
{
	//get spawning index for y-axis
	int spawnPosYindex = (int)GetRandom(0.0f, 4.0f);

	if (spawnPosYindex < 0)
	{
		spawnPosYindex = 0;
	}
	else if (spawnPosYindex > 3) 
	{
		spawnPosYindex = 3;
	}

	//get spawning index for x-axis
	float spawnPosX = GetRandom(-1.0f, 1.0f);

	if (spawnPosX >= 0.0f) 
	{
		spawnPosX = (m_ColCount * m_TileSize) + 10.0f;
	}
	else 
	{
		spawnPosX = -30.0f;
	}

	//set spawn position
	Vector2D spawnPos = Vector2D(spawnPosX, m_MonsterSpawnPosY[spawnPosYindex]);


	//initialize Normal Monster Entity
	Entity& archMonster(EntityManager::GetInstance()->AddEntity("ArchMonster"));

	//initialize components for Normal Monster entity
	//Transform Component
	archMonster.AddComponent<Transform>(spawnPos.x, spawnPos.y);
	//Sprite Component
	archMonster.AddComponent<Sprite>("arch-monster-idle", 96, 96, MONSTER_LAYER);
	//Collider Component
	archMonster.AddComponent<Collider>();
	//SpriteAnimation Component
	archMonster.AddComponent<SpriteAnimation>();
	//Rigidbody Component
	archMonster.AddComponent<Rigidbody>();

	//set moving direction
	Vector2D movingDir(m_PrincessCollider.x - spawnPos.x, m_PrincessCollider.y - spawnPos.y);
	//normalize to get the actual direction
	movingDir.Normalize();

	//ArchMonster Component
	archMonster.AddComponent<ArchMonster>(100.0f, Vector2D(movingDir.x, movingDir.y));
	//set the pointer to player
	archMonster.GetComponent<ArchMonster>()->SetPlayer(m_Player);
	//set the princess collider
	archMonster.GetComponent<ArchMonster>()->SetPrincessCollider(m_PrincessCollider);

}


void FinalArzeros::Blink(float deltatime)
{
	m_BlinkTime += deltatime;

	if (m_BlinkTime >= FINAL_ARZEROS_BLINK_LENGTH)
	{
		m_BlinkTime = 0.0f;

		m_Sprite->m_IsEnable = !m_Sprite->m_IsEnable;
	}
}


void FinalArzeros::FinishState()
{
	m_FinishState = true;
}


void FinalArzeros::Dead()
{
	m_Sprite->m_IsEnable = true;
	m_IsDead = true;
	m_ShakingPos = m_Transform->position;

	LevelManager::GetInstance()->sectionChange = 1;
	TransitionManager::GetInstance()->FadeTransition();
	CutsceneManager::GetInstance()->m_IsBossEvent = false;
}


void FinalArzeros::SetPrincessCollider(SDL_Rect princessCollider) 
{
	m_PrincessCollider = princessCollider;
}


void FinalArzeros::SetMovingDir(Direction dir)
{
	m_MovingDir = dir;
}

void FinalArzeros::StartAttacking() 
{
	m_IsAttacking = true;
}
