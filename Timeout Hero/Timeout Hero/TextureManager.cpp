#include "TextureManager.h"
#include "Camera.h"

TextureManager* TextureManager::s_Instance = nullptr;

TextureManager::TextureManager() {}

TextureManager* TextureManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new TextureManager();
}

SDL_Texture* TextureManager::LoadTexture(const char* fileName)
{
    SDL_Surface* surface = IMG_Load(fileName);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(Game::GetInstance()->m_Renderer, surface);
    SDL_FreeSurface(surface);
    return texture;
}

bool TextureManager::Load(std::string id, std::string fileName) {
	SDL_Surface* surface = IMG_Load(fileName.c_str());
	if (surface == nullptr) {
		SDL_Log("Failed to load texture: %s %s", fileName.c_str(), SDL_GetError());
		return false;
	}

	SDL_Texture* texture = SDL_CreateTextureFromSurface(Game::m_Renderer , surface);
	if (texture == nullptr) {
		SDL_Log("Failed to create texture from surface: %s", SDL_GetError());
		return false;
	}

	m_textureMap[id] = texture;
	SDL_FreeSurface(surface);

	return true;
}

void TextureManager::Draw(SDL_Texture* texture, SDL_Rect sourceRectangle, SDL_Rect destinationRectangle, SDL_RendererFlip flip, float angle) 
{
    SDL_RenderCopyEx(Game::GetInstance()->m_Renderer, texture, &sourceRectangle, &destinationRectangle, angle, nullptr, flip);
}

void TextureManager::DrawTile(std::string tilesetID, int tileSize, int x, int y, int row, int frame, SDL_RendererFlip flip) {
	//render checking has already done in TileLayer
	Vector2D cam = Camera::GetInstance()->GetPosition();
	SDL_Rect srcRect = { tileSize * frame, tileSize * (row), tileSize, tileSize };
	SDL_Rect dstRect = { x - cam.x, y - cam.y, tileSize, tileSize };
	SDL_RenderCopyEx(Game::GetInstance()->m_Renderer, m_textureMap[tilesetID], &srcRect, &dstRect, 0, 0, flip);
}

SDL_Texture* TextureManager::GetTexture(std::string id)
{
	return m_textureMap[id];
}
