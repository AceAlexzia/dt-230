#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

#include <map>
#include <string>
#include <SDL_ttf.h>
#include "TextureManager.h"
#include "FontManager.h"
#include "EntityManager.h"

class AssetManager {
    private:
		static AssetManager* s_Instance;
		AssetManager();
        std::map<std::string, SDL_Texture*> textures;
        std::map<std::string, TTF_Font*> fonts;
    public:
		static AssetManager* GetInstance();
        ~AssetManager();
        void ClearData();
        
        void AddTexture(std::string textureId, std::string filePath);
        //void AddFont(std::string fontId, std::string  filePath, int fontSize);

        SDL_Texture* GetTexture(std::string textureId);
        TTF_Font* GetFont(std::string fontId);
};

#endif
