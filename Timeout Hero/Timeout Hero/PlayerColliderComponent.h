#pragma once
#ifndef PLAYERCOLLIDERCOMPONENT_H
#define PLAYERCOLLIDERCOMPONENT_H

#define PUSH_BACK_OFFSET 0.05f

#include "ColliderComponent.h"
#include "CollisionHandler.h"

class PlayerCollider: public Collider
{
	private :
		Vector2D m_LastSafeTile = Vector2D(0, 0);
		Vector2D m_ObjectDirection = Vector2D(0, 0);

	public:
		Vector2D lastSafePos = Vector2D(0, 0);
		bool objectCollision[2]; //[0] for x-axis //[1] for y-axis

		PlayerCollider(): Collider() {}

		bool CollideInsideMap();

		bool CollideWithMap_Left();
		bool CollideWithMap_Right();
		bool CollideWithMap_Up();
		bool CollideWithMap_Down();

		bool CollideWithSpike();

		bool CollideWithSpike_Left();
		bool CollideWithSpike_Right();
		bool CollideWithSpike_Up();
		bool CollideWithSpike_Down();

		bool CollideWithObject_Left();
		bool CollideWithObject_Right();
		bool CollideWithObject_Up();
		bool CollideWithObject_Down();

		void SetObjectCollision(SDL_Rect objectHitBox, Direction dir);
		void ResetObjectCollision();
};

#endif