#pragma once
#ifndef VERTICAL_MONSTER_H
#define VERTICAL_MONSTER_H

#define V_MONSTER_SPEED 250
#define V_MONSTER_TIME_LENGTH 1.5f

#include "Monster.h"
#include "RigidbodyComponent.h"

class VerticalMonster : public Monster
{
	public:
		VerticalMonster() {}
		~VerticalMonster() {}
		void Initialize() override;
		void Update(float deltaTime) override;

	private:
		Rigidbody* m_Rb;
		Vector2D m_MovingDirection = Vector2D(0, 1);
		float m_TimeLength = 0.0f;
};

#endif