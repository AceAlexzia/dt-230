#include "PlayerColliderComponent.h"

bool PlayerCollider::CollideInsideMap()
{
	return CollisionHandler::GetInstance()->MapCollision_Inside(m_Collider);
}

bool PlayerCollider::CollideWithMap_Left()
{
	if (CollisionHandler::GetInstance()->MapCollision_Left(m_Collider, m_LastSafeTile.x))
	{
		lastSafePos.x = m_LastSafeTile.x + CollisionHandler::GetInstance()->GetTileSize() + PUSH_BACK_OFFSET + m_Buffer.x;
		return true;
	}
	else
	{
		return false;
	}
}

bool PlayerCollider::CollideWithMap_Right()
{
	if (CollisionHandler::GetInstance()->MapCollision_Right(m_Collider, m_LastSafeTile.x))
	{
		lastSafePos.x = m_LastSafeTile.x - m_Collider.w - PUSH_BACK_OFFSET + m_Buffer.x;
		return true;
	}
	else
	{
		return false;
	}
}

bool PlayerCollider::CollideWithMap_Up()
{
	if (CollisionHandler::GetInstance()->MapCollision_Up(m_Collider, m_LastSafeTile.y))
	{
		lastSafePos.y = m_LastSafeTile.y + CollisionHandler::GetInstance()->GetTileSize() + PUSH_BACK_OFFSET + m_Buffer.y;
		return true;
	}
	else
	{
		return false;
	}
}

bool PlayerCollider::CollideWithMap_Down()
{
	if (CollisionHandler::GetInstance()->MapCollision_Down(m_Collider, m_LastSafeTile.y))
	{
		lastSafePos.y = m_LastSafeTile.y - m_Collider.h - PUSH_BACK_OFFSET + m_Buffer.y;
		return true;
	}
	else
	{
		return false;
	}
}


bool PlayerCollider::CollideWithSpike()
{
	return CollisionHandler::GetInstance()->SpikeCollision_Inside(m_Collider);
}

bool PlayerCollider::CollideWithSpike_Left()
{
	return CollisionHandler::GetInstance()->SpikeCollision_Left(m_Collider);
}

bool PlayerCollider::CollideWithSpike_Right()
{
	return CollisionHandler::GetInstance()->SpikeCollision_Right(m_Collider);
}

bool PlayerCollider::CollideWithSpike_Up()
{
	return CollisionHandler::GetInstance()->SpikeCollision_Up(m_Collider);
}

bool PlayerCollider::CollideWithSpike_Down()
{
	return CollisionHandler::GetInstance()->SpikeCollision_Down(m_Collider);
}

bool PlayerCollider::CollideWithObject_Left() 
{
	if (m_ObjectDirection.x < 0.0f) 
	{
		m_ObjectDirection.x = 0;
		objectCollision[0] = true;
		return true;
	}
	else 
	{
		return false;
	}
}

bool PlayerCollider::CollideWithObject_Right() 
{
	if (m_ObjectDirection.x > 0.0f)
	{
		m_ObjectDirection.x = 0;
		objectCollision[0] = true;
		return true;
	}
	else
	{
		return false;
	}
}

bool PlayerCollider::CollideWithObject_Up() 
{
	if (m_ObjectDirection.y < 0.0f)
	{
		m_ObjectDirection.y = 0;
		objectCollision[1] = true;
		return true;
	}
	else
	{
		return false;
	}
}

bool PlayerCollider::CollideWithObject_Down() 
{
	if (m_ObjectDirection.y > 0.0f)
	{
		m_ObjectDirection.y = 0;
		objectCollision[1] = true;
		return true;
	}
	else
	{
		return false;
	}
}

void PlayerCollider::SetObjectCollision(SDL_Rect objectHitBox, Direction dir)
{

	if (dir == LEFT)
	{
		m_ObjectDirection.x = -1.0f;
		lastSafePos.x = objectHitBox.x + objectHitBox.w + m_Buffer.x;
	}
	else if (dir == RIGHT)
	{
		m_ObjectDirection.x = 1.0f;
		lastSafePos.x = objectHitBox.x - m_Collider.w + m_Buffer.x;
	}
	else if (dir == UP)
	{
		m_ObjectDirection.y = -1.0f;
		lastSafePos.y = objectHitBox.y + objectHitBox.h + m_Buffer.y;
	}
	else if (dir == DOWN)
	{
		m_ObjectDirection.y = 1.0f;
		lastSafePos.y = objectHitBox.y - m_Collider.h + m_Buffer.y;
	}
}

void PlayerCollider::ResetObjectCollision()
{
	m_ObjectDirection = Vector2D(0.0f, 0.0f);
}
