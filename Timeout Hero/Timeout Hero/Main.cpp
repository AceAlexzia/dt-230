#include <iostream>
#include "Constants.h"
#include "Game.h"
#include "Timer.h"

int main(int argc, char *args[]) 
{
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

    srand(time(NULL));

    Game::GetInstance()->Initialize();

    while (Game::GetInstance()->IsRunning())
    {
        Game::GetInstance()->Loop();
    }

	Game::GetInstance()->Destroy();

    delete Game::GetInstance();
    return 0;
}
