#pragma once
#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include <iostream>
#include "MenuScene.h"
#include "GameScene.h"
#include "SplashScene.h"

#define MENU_SCENE "MenuScene"
#define GAME_SCENE "GameScene"
#define SPLASH_SCENE "SplashScene"

//IMPORTANT NOTE
#define STARTING_SCENE MENU_SCENE	//this is a starting scene of the engine
									//change it whatever you'd like and change it back to MENU_SCENE befre merging back to development branch

class Scene;
class SceneManager
{
	public:
		Scene* scene;
		SceneManager() {};
		~SceneManager();
		void Initialize();
		void LoadScene(std::string sceneName);
		void Update(float deltaTime);
		void Render();


		static SceneManager* GetInstance();
	private:
		static SceneManager* s_Instance;
};

#endif // !SCENEMANAGER_H