#pragma once
#ifndef DIALOGUE_MANAGER
#define DIALOGUE_MANAGER

#define DIALOGUE_COUNTDOWN 0.05f
#define DIALOGUE_FONT_NAME "editundo"

#include "LevelManager.h"

#include <iostream>
#include <fstream>
#include <vector>

class DialogueManager
{
	public:
		~DialogueManager() {}

		static DialogueManager* GetInstance();
		void Update(float dt);

		void Speak(std::string cutsceneName);
		bool IsSpeaking();

		int GetDialogueLine();

		void Pause();
		void Resume();
		bool IsPausingDialogue();

		void Reset();

	private:
		static DialogueManager* s_Instance;

		DialogueManager();

		std::vector<std::string> m_DialogueVect;
		std::string m_Speaker;
		std::string m_CurrentDialogue;

		bool m_IsSpeaking = false;

		std::ifstream m_DataFile;
		float m_DialogueCounter = 0.0f;

		int m_DialogueLine = 0;

		bool m_IsPaused = false;

		//display dialogue in real time
		void Display(float dt);

		//used when displaying next dialogue
		void NextDialogue();

		//spawn dialogue box
		void SpawnDialogue(bool isTop = true);
		//destroy dialogue box
		void DestroyDialogue();
		//set dialogue panel
		void SetDialogueSpeaker();
		//set dialogue text
		void AddDialogueText(std::string textToAdd);
		//add some details to the dialogue in the final section
		void AddStaticticDialogue(std::string cutsceneName);

		std::map<std::string, std::string> m_SpeakerPanelID;

		Entity* m_DialoguePanel = nullptr;
		Entity* m_DialgoueText = nullptr;
		Entity* m_DialgoueText2 = nullptr;
		Text* m_CurrentText = nullptr;
};

#endif