#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include "Game.h"
#include "FontManager.h"

class TextureManager {
private :
	friend class FontManager;
	static TextureManager* s_Instance;
	std::map<std::string, SDL_Texture*> m_textureMap;
	TextureManager();
public:
	static TextureManager* GetInstance();
	static SDL_Texture* LoadTexture(const char* fileName);
	bool Load(std::string id, std::string fileName);
	static void Draw(SDL_Texture* texture, SDL_Rect sourceRectangle, SDL_Rect destinationRectangle, SDL_RendererFlip flip = SDL_FLIP_NONE, float angle = 0.0);
	void DrawTile(std::string tilesetID, int tileSize, int x, int y, int row, int frame, SDL_RendererFlip flip = SDL_FLIP_NONE);
	SDL_Texture* GetTexture(std::string id);
};

#endif
