#include "TransformComponent.h"

Transform::Transform(int posX, int posY)
{
    position = Vector2D(posX, posY);
}


void Transform::TranslateX(float x) 
{ 
    position.x += x; 
}

void Transform::TranslateY(float y)
{ 
    position.y += y; 
}

void Transform::Translate(Vector2D v) 
{ 
    position.x += v.x; position.y += v.y; 
}


void Transform::Log(std::string msg)
{
    std::cout << msg << "(X, Y) = (" << position.x << ", " << position.y << ")" << std::endl;
}