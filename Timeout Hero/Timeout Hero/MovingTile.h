#pragma once
#ifndef MOVING_TILE_H
#define MOVING_TILE_H

#include "LevelObject.h"
#include "RigidbodyComponent.h"

class MovingTile : public LevelObject
{
public:
    MovingTile() {}
    ~MovingTile() {}
    void Initialize() override;
    void Update(float deltaTime) override;

private:
    //Rigidbody* m_Rb;
    Vector2D m_MovingDirection = Vector2D(-1, 0);
    float m_TimeLength = 0.0f;

    void Update_Collision();
};

#endif