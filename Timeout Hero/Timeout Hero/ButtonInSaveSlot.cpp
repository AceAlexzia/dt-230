#include "ButtonInSaveSlot.h"

ButtonInSaveSlot::ButtonInSaveSlot(int x, int y, bool canClick, std::string sTexture, std::string selectTexture, int pos, bool selecting)
{
	buttonPos.x = x;
	buttonPos.y = y;
	available = canClick;
	if (selecting)
	{
		isSelecting = true;
	}
	startingTexture = sTexture;
	selectingTexture = selectTexture;
	ID = pos;
}

void ButtonInSaveSlot::Initialize()
{
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(buttonPos.x, buttonPos.y);
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("ApplyButton", 146, 38, TIME_COUNT_LAYER);
	}
	isSet = true;
}

void ButtonInSaveSlot::Update(float deltaTime)
{
	//--------------------------------- Will Opti in the future ---------------------------------
	if (isSet)
	{
		if (!isSelecting)
		{
			owner->GetComponent<Sprite>()->SetTexture(startingTexture);
		}
		else
		{
			owner->GetComponent<Sprite>()->SetTexture(selectingTexture);
		}
		isSet = false;
	}
	
}

void ButtonInSaveSlot::OnClick(int i)
{
	m_FnPtr(i);
}

void ButtonInSaveSlot::SetFunction(void (*fn)(int i))
{
	m_FnPtr = fn;
}

Entity* ButtonInSaveSlot::SelectUI(int direction)
{
	return nextUI[direction];
}
