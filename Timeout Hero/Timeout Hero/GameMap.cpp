#include "GameMap.h"

GameMap::~GameMap() {
	for (int i = 0; i < m_MapLayer.size(); i++) {
		delete m_MapLayer[i];
	}
}


void GameMap::Render() {
	for (unsigned int i = 0; i < m_MapLayer.size(); i++) {
		m_MapLayer[i]->Render();
	}
}


std::vector<TileLayer*> GameMap::GetMapLayer() 
{ 
	return m_MapLayer; 
}


int GameMap::GetRowCount() 
{
	return m_RowCount;
}

int GameMap::GetColCount()
{
	return m_ColCount;
}

int GameMap::GetTileSize() 
{
	return m_TileSize;
}
