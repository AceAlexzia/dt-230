#pragma once
#ifndef ARCH_MONSTER_H
#define ARCH_MONSTER_H

#define ARCH_MONSTER_SPEED 300

#include "Monster.h"
#include "RigidbodyComponent.h"

class ArchMonster : public Monster
{
public:
	ArchMonster(float destroyTime = 3.0f, Vector2D movingDirection = Vector2D(-1.0f, 0.0f));
	~ArchMonster() {}
	void Initialize() override;
	void Update(float deltaTime) override;

	void SetPrincessCollider(SDL_Rect princessCollider);

private:
	Rigidbody* m_Rb;
	Vector2D m_MovingDirection = Vector2D(-1, 0);
	float m_DestroyTime = 0.0f;
	float m_Lifespan = 0.0f;

	SDL_Rect m_PrincessCollider = { 0, 0, 0, 0 };

	void Update_PrincessCollision();
};

#endif