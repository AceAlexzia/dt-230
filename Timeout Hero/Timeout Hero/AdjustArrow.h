#pragma once
#ifndef ADJUSTARROW_H
#define ADJUSTARROW_H

#include "SpriteComponent.h"
#include "TransformComponent.h"
#include "Vector2D.h"
#include "UIComponent.h"
class AdjustArrow :public UIComponent
{
	public:
		AdjustArrow(int x, int y, Entity* ad,bool selecting = false);
		void Initialize() override;
		void Update(float deltaTime) override;

		bool available;
		bool isPressed = false;
		bool isSet;
		Entity* SelectUI(int direction);
		Entity* ownAdjuster;
	private:
		Vector2D arrowPos;
		Vector2D getPos;

};

#endif // !ADJUSTARROW_H