#include "Button.h"

Button::Button(int x, int y, bool canClick, std::string text, std::string sTexture, std::string selectTexture, int textSize, bool selecting)
{
	buttonPos.x = x;
	buttonPos.y = y;
	available = canClick;
	if (selecting)
	{
		isSelecting = true;
	}
	startingTexture = sTexture;
	selectingTexture = selectTexture;
	buttonText = text;
	sizeText = textSize;
}

void Button::Initialize()
{
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(buttonPos.x, buttonPos.y);
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("ApplyButton", 146, 38, TIME_COUNT_LAYER);
	}

	Entity& text(EntityManager::GetInstance()->AddEntity("ButtonText"));
	text.AddComponent<Transform>(owner->GetComponent<Transform>()->position.x, owner->GetComponent<Transform>()->position.y);
	text.AddComponent<Text>(TIME_COUNT_LAYER, "ButtonText" + owner->name, "DisposableDroidBB", sizeText, textColor, buttonText);
	textButton = &text;
	
	isSet = true;
	getPos = Camera::GetInstance()->GetPosition();

}

void Button::Update(float deltaTime)
{
	textButton->GetComponent<Transform>()->position.x = owner->GetComponent<Sprite>()->GetWidth() / 2 - (textButton->GetComponent<Text>()->GetWidth() / 2) + owner->GetComponent<Transform>()->position.x- getPos.x;
	textButton->GetComponent<Transform>()->position.y = owner->GetComponent<Sprite>()->GetHeight() / 2 - (textButton->GetComponent<Text>()->GetHeight() / 2) + owner->GetComponent<Transform>()->position.y - getPos.y;
	
	if (isSet)
	{
		if (!isSelecting)
		{
			owner->GetComponent<Sprite>()->SetTexture(startingTexture);
			textButton->GetComponent<Text>()->color = { 255,255,255,255 };
		}
		else
		{
			owner->GetComponent<Sprite>()->SetTexture(selectingTexture);
			textButton->GetComponent<Text>()->color = { 251,242,54,255 };
		}
		isSet = false;
	}
	
}

void Button::OnClick()
{
	m_FnPtr();
}

void Button::SetFunction(void (*fn)())
{
	m_FnPtr = fn;
}

Entity* Button::SelectUI(int direction)
{
	return nextUI[direction];
}
