#pragma once

#ifndef LEVEL_MANAGER
#define LEVEL_MANAGER

#include "GameMap.h"
#include "MapParser.h"
#include "Hero.h"
#include "Camera.h"
#include "VerticalMonster.h"
#include "HorizontalMonster.h"
#include "AltriumRune.h"
#include "TimeCounter.h"
#include "PauseManager.h"
#include "SectionChanger.h"
#include "HorizontalMovingTile.h"
#include "Artrius.h"
#include "Archfiend.h"
#include "Arzeros.h"
#include "Princess.h"
#include "FinalArzeros.h"
#include "CutsceneManager.h"
#include "AudioManager.h"
#include "TutorialUI.h"

//IMPORTANT NOTE
//#define STARTING_LEVEL_INDEX 0		//this is a starting level of the game
									//change it whatever you'd like and change it back to 0 befre merging back to development branch

class LevelManager
{
private:
	static LevelManager* s_Instance;

	bool m_IsLoadNewLevel = false;

	bool m_IsDeLevel = false;

	float m_StaringTime = 0.0f;
	float m_TimePlayed = 0.0f;
	GameMap* m_CurrentGameMap;

	//name of the all levels
	std::vector<std::string> m_AllLevels;

	//pointer to certain entity
	Hero* m_Player = nullptr;
	TimeCounter* m_TimeCounter = nullptr;
	Princess* m_Princess = nullptr;
	Entity* m_BossEntity = nullptr;
	Artrius* m_Artrius = nullptr;

	//level object entities in current section
	std::vector<Entity*> m_LevelObjects;


	//========================SERIALIZE VALUE========================
	//Hero initializing property
	std::vector<Vector2D> m_PlayerStartingPos;
	std::vector<SDL_RendererFlip> m_PlayerStartingFlip;
	std::vector<Vector2D> m_PlayerDeLevelPos;

	//Altrium RUne initializing property
	std::map<int, Vector2D> m_RunePos;
	std::map<int, bool> m_RuneActive;

	//Normal Monster initializing property
	std::map<int, std::vector<Vector2D>> m_NormalMonsterPos;
	std::map<int, std::vector<SDL_RendererFlip>> m_NormalMonsterFlip;

	//Horizontal Monster initializing property
	std::map<int, std::vector<Vector2D>> m_HorizontalMonsterPos;

	//Vertical Monster initializing property
	std::map<int, std::vector<Vector2D>> m_VerticalMonsterPos;
	std::map<int, std::vector<SDL_RendererFlip>> m_VerticalMonsterFlip;

	//Section Changer initializing property
	std::map<int, int[4]> m_SectionChangerIC; //index changing
	std::map<int, Vector2D[4]> m_SectionChangerPos;

	//Horizontal Moving Tile initializing property
	std::map<int, std::vector<Vector2D>> m_HorizontalMovingTilePos;

	//Princess initializing property
	std::map<int, Vector2D> m_PrincessPos;
	std::map<int, SDL_RendererFlip> m_PrincessFlip;

	//tutorial UI initializing property
	std::map<int, Vector2D> m_TutorialPos;

	//========================SERIALIZE VALUE========================


	//========================SPAWN FUNCTION========================
	//Spawn Hero entity
	void Spawn_Player();
	//Spawn Altrium Rune entity
	void Spawn_Rune();
	//Spawn Monster entity
	void Spawn_Monsters();
	//Spawn TimeCounter entity
	void Spawn_TimeCounter();
	//Spawn Section Changer entity
	void Spawn_SectionChanger();
	//Spawn Horizontal Moving Tile
	void Spawn_HorizontalMovingTile();
	//Spawn Artrius
	void Spawn_Artrius();
	//Spawn Archfiend or Arzeros
	void Spawn_Boss();
	//Spawn Princess
	void Spawn_Princess();
	//Spawn Tutorial UI
	void Spawn_TutorialUI();
	//========================SPAWN FUNCTION========================


	//========================SET FUNCTION========================
	//parse all GameMap
	void ParseLevel();
	//set Altrium Rune starting property in all sections
	void Set_RuneProp();
	//set Normal Monster starting property in all sections
	void Set_NormalMonsterProp();
	//set Horizontal Monster starting property in all sections
	void Set_HorizonalMonsterProp();
	//set Vertical Monster starting property in all sections
	void Set_VerticalMonsterProp();
	//set Hero starting property in all sections
	void Set_HeroProp();
	//set Section Changer starting property in all sections
	void Set_SectionChangerProp();
	//set Horizontal Moving Tile starting property in all sections
	void Set_HorizontalMovingTile();
	//set Princess starting property in all sections
	void Set_PrincessProp();
	//set Tutorial UI starting property in all sections
	void Set_TutorialUIProp();
	//========================SET FUNCTION========================

	//generate new level
	void LoadNewLevel();
	//assign player pointer to all level objects
	void AssignPlayerToLevelObjects(Hero* player);

	//end game level warping
	void EndGameWarp(bool isEndGame);

public:

	//current level index
	int levelIndex;
	//current section changing
	int sectionChange;

	LevelManager() {}
	~LevelManager() {}

	//Getter
	static LevelManager* GetInstance();
	GameMap* GetMap();
	int GetLevelAmount();

	void Initialize();
	void Update(float dt);
	void Render();
	void StartGame();

	void SetLoadNewLevel();
	void SetIsDeLevel();

	void Clean();

	//========================TIME COUNTER FUNCTION========================
	//function controlling TimeCounter
	//decrease time counter
	void DecreaseTime(float amount);
	//pause time counting down
	void PauseTime();
	//resume time counting down
	void ResumeTime();
	//get current time counting down
	float GetTime();
	//set starting time counter
	void SetStartingTime(float time);
	//set blinking time counter
	void BlinkTime();
	//set stop blinking time counter
	void StopBlinkTime();
	//========================TIME COUNTER FUNCTION========================

	//Get Certain Entity
	Hero* GetHero();
	Princess* GetPrincess();
	Entity* GetBossEntity();
	Artrius* GetArtrius();

	//Get Level name
	std::string GetLevelName();

	void EasterEgg();

};
#endif