#pragma once
#ifndef SPRITEANIMATIONCOMPONENT_H
#define SPRITEANIMATIONCOMPONENT_H

#include "Component.h"
#include "SDL.h"
#include "SpriteComponent.h"

class SpriteAnimation : public Component
{
	public:
		SpriteAnimation() {}
		~SpriteAnimation() {}

		void Initialize();
		void Update(float dt) override;

		bool GetIsEnded();
		int GetAnimationSpeed();
		
		void SetProps(std::string assetTextureId, int frameCount, int animSpeed, bool repeat = true);
		
		
	private:
		float m_TimeElapsed = 0.0f;
		bool m_Repeat;
		bool m_IsEnded;
		int m_CurrentFrame;
		int m_AnimSpeed;
		int m_FrameCount;
		std::string m_TextureID, m_PrevTextureID = "";
		int m_ChangingFrame = 0, m_PrevFrame = -1;
		SDL_Texture* texture;
		Sprite* m_Sprite;
};

#endif // !SPRITEANIMATIONCOMPONENT_H
