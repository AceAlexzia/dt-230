#pragma once
#ifndef SAVESLOTBUTTON_H
#define SAVESLOTBUTTON_H

#include "TransformComponent.h"
#include "SpriteComponent.h"
#include "TextComponent.h"
#include "Vector2D.h"
#include "SDL.h"
#include "UIComponent.h"
class SaveSlotButton :public UIComponent
{
	public:
		SaveSlotButton(int x, int y, bool canClick, std::string text, std::string startingTexture, std::string selectingTexture, int textSize, int pos, bool selecting = false);
		void Initialize() override;
		void Update(float deltaTime) override;
		void SetFunction(void (*fn)());
		void SetEmptySaveSlot(void (*fn)(int i));
		void OnClick();
		void OnClick(int i);

		Entity* SelectUI(int direction);
		Entity* textButton;
		bool available = false;
		bool hasSave = false;
		bool isSet;
		int ID;
	private:
		void (*m_FnPtr)() = nullptr;
		void (*m_FnPtr2)(int) = nullptr;
		Vector2D buttonPos;
		SDL_Color textColor = { 255,255,255,255 };
		std::string buttonText;
		std::string selectingTexture;
		std::string startingTexture;
		int sizeText = 0;
};

#endif