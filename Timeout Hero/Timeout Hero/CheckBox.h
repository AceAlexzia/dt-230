#pragma once
#ifndef CHECKBOX_H
#define CHECKBOX_H

#include "UIComponent.h"
#include "Vector2D.h"
#include "TransformComponent.h"
#include "SpriteComponent.h"
#include "SaveManager.h"
class CheckBox : public UIComponent
{
	public:
		CheckBox(int x, int y, bool check ,bool selecting = false);
		void Initialize() override;
		void Update(float deltaTime)override;
		void OnClick();
		void SetFunction(void (*fn)());
		void ChangeValue();
		Entity* SelectUI(int direction);

		bool isSet;
		bool isCheck;
	private:
		Vector2D checkBoxPos;
		void (*m_FnPtr)() = nullptr;
};

#endif // !CHECKBOX_H