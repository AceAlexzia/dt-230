#ifndef SPRITECOMPONENT_H
#define SPRITECOMPONENT_H

#include "TextureManager.h"
#include "AssetManager.h"
#include "Camera.h"
#include "TransformComponent.h"

class Sprite : public Component
{
    private:
        Transform* m_Transform;

        SDL_Texture* m_Texture;

        SDL_Rect m_SourceRectangle, m_DestinationRectangle;

        int m_Width, m_Height;

    public:

        SDL_RendererFlip m_Flip; //flip horizontal or none
        bool m_IsEnable = true; //enable or diable rendering
        LayerType m_Layer; //rendering layer order
        float m_Angle = 0.0f; //rendering rotation

        Sprite(std::string assetTextureId, int width, int height, LayerType layer, SDL_RendererFlip flip = SDL_FLIP_NONE);

        void Initialize() override;
        void Update(float deltaTime) override {}
        void Render();

        int GetWidth();
        int GetHeight();

        void SetTexture(std::string assetTextureId);
        void SetSpriteFrame(int sx);
};
#endif
