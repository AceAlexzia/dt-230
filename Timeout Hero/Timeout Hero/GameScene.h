#pragma once
#ifndef GAMESCENE_H
#define GAMESCENE_H
#include "Scene.h"
class GameScene: public Scene
{
	public:
		GameScene();
		~GameScene(){}
		void Start();
};

#endif // !GAMESCENE_H