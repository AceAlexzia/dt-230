#include "AudioManager.h"
#include "DialogueManager.h"
#include "TransitionManager.h"

AudioManager* AudioManager::s_Instance = nullptr;

AudioManager* AudioManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new AudioManager();
}


void AudioManager::Initialize() 
{
	musicPlayer.SetAudioControllerPath("TimeoutHeroMusic");
	musicPlayer.Play();

	clockSfxPlayer.SetAudioControllerPath("TimeoutHeroClockTick");
	clockSfxPlayer.SetInteger("time", 60);

	dashSfxPlayer.SetAudioControllerPath("TimeoutHeroDashSfx");
	dashSfxPlayer.SetBool("play", true);

	deadSfxPlayer.SetAudioControllerPath("TimeoutHeroDeadSfx");
	deadSfxPlayer.SetBool("play", true);

	hitSfxPlayer.SetAudioControllerPath("TimeoutHeroHitSfx");
	hitSfxPlayer.SetBool("play", true);
}

void AudioManager::Update() 
{
	//check whether the cutscene event is going on -> lower down the volume
	//if not -> make it back to normal
	bool isLowerDown = DialogueManager::GetInstance()->IsSpeaking() || TransitionManager::GetInstance()->IsTransition();
	if (m_IsLowerDown != isLowerDown) 
	{
		m_IsLowerDown = isLowerDown;
		if (m_IsLowerDown) 
		{
			LowerDown();
		}
		else 
		{
			StopLowerDown();
		}
	}

	//playing audio
	musicPlayer.Update();
	clockSfxPlayer.Update();
	dashSfxPlayer.Update();
	deadSfxPlayer.Update();
	hitSfxPlayer.Update();
}

void AudioManager::SetVol(int masterVol, int musicVol, int sfxVol) 
{
	m_MasterVol = masterVol;
	m_MusicVol = musicVol;
	m_SfxVol = sfxVol;

	musicPlayer.SetVolume(masterVol * musicVol);

	clockSfxPlayer.SetVolume(masterVol * sfxVol);

	dashSfxPlayer.SetVolume(masterVol * sfxVol);
	deadSfxPlayer.SetVolume(masterVol * sfxVol);
	hitSfxPlayer.SetVolume(masterVol * sfxVol);
}

void AudioManager::LowerDown() 
{
	musicPlayer.SetVolume(m_MasterVol * m_MusicVol * 0.7f);

	clockSfxPlayer.SetVolume(m_MasterVol * m_SfxVol * 0.7f);

	dashSfxPlayer.SetVolume(m_MasterVol * m_SfxVol * 0.75f);
	deadSfxPlayer.SetVolume(m_MasterVol * m_SfxVol * 0.7f);
	hitSfxPlayer.SetVolume(m_MasterVol * m_SfxVol * 0.7f);
}

void AudioManager::StopLowerDown() 
{
	musicPlayer.SetVolume(m_MasterVol * m_MusicVol);

	clockSfxPlayer.SetVolume(m_MasterVol * m_SfxVol);

	dashSfxPlayer.SetVolume(m_MasterVol * m_SfxVol);
	deadSfxPlayer.SetVolume(m_MasterVol * m_SfxVol);
	hitSfxPlayer.SetVolume(m_MasterVol * m_SfxVol);
}
