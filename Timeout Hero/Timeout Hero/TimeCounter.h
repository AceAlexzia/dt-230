#pragma once
#ifndef TIMER_COUNTER_H
#define TIMER_COUNTER_H

#define TIME_COUNTER_FONT_NAME "ArcadeClassic"
#define TIME_COUNTER_MAX_COUNT 60.5f
#define TIME_COUNTER_BLINK_LENGTH 0.15f
#define TIME_COUNTER_BLINK_MAX_COUNT 8

#include "SDL.h"
#include "TextComponent.h"

class TimeCounter: public Component
{
	private:
		float m_TimeCount;

		Transform* m_Transform;
		Text* m_Text;

		bool m_IsPause = false;
		bool m_IsEnable = true;

		//blinking variable
		bool m_IsBlinking = false;
		int m_BlinkCount = 0;
		float m_BlinkTime = 0.0f;

		void Update_Count(float deltatime);
		void Update_Text();
		void Update_Position();
		void Update_Blinking(float deltaTime);

	public:
		TimeCounter(float time = TIME_COUNTER_MAX_COUNT);
		~TimeCounter();

		void Initialize() override;
		void Update(float dt) override;

		void SetTimeCount(float time);
		float GetTimeCount();

		void DecreaseTimeCounter(float amount);

		void Pause();
		void Resume();

		void Disable();
		void Enable();

		void Blink();
		void StopBlink();
};

#endif // !TIMER

