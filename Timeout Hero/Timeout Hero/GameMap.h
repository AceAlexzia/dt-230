#pragma once
#ifndef GAMEMAP_H
#define GAMEMAP_H

#include "TileLayer.h"

class GameMap
{
	public:
		~GameMap();

		void Render();

		std::vector<TileLayer*> GetMapLayer();
		
		//GameMap getter
		int GetRowCount();
		int GetColCount();
		int GetTileSize();

	private:
		friend class MapParser;
		std::vector<TileLayer*> m_MapLayer;

		int m_RowCount = 0;
		int m_ColCount = 0;
		int m_TileSize = 0;
};

#endif