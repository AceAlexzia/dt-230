#pragma once
#ifndef SPELL_MONSTER_H
#define SPELL_MONSTER_H

#define SPELL_MONSTER_SPEED 300

#include "Monster.h"
#include "RigidbodyComponent.h"

class SpellMonster : public Monster
{
public:
	SpellMonster(float destroyTime = 3.0f, Vector2D movingDirection = Vector2D(-1.0f, 0.0f));
	~SpellMonster() {}
	void Initialize() override;
	void Update(float deltaTime) override;

private:
	Rigidbody* m_Rb;
	Vector2D m_MovingDirection = Vector2D(-1, 0);
	float m_DestroyTime = 0.0f;
	float m_Lifespan = 0.0f;
};

#endif