#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H

#include "Entity.h"
#include "Component.h"
#include <vector>
#include <iostream>

class EntityManager {
    private:
		static EntityManager* s_Instance;
		EntityManager();
        std::vector<Entity*> entities;
    public:
		static EntityManager* GetInstance();
        void ClearData();
        ~EntityManager();
        void Update(float deltaTime);
        void Render();
        bool HasNoEntities() const;
        unsigned int GetEntityCount() const;
        void ListAllEntities() const;
        std::vector<Entity*> GetEntities() const;
        Entity& AddEntity(std::string entityName);
        void DestroyInActiveEntities();
};

#endif
