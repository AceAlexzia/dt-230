#pragma once
#ifndef TESTING
#define TESTING

#include"SDL.h"

namespace Debug 
{
	template<typename T>
	void Print(T val)
	{
		std::cout << (int)(SDL_GetTicks() / 1000.0f) << " :\t" << val << std::endl;
	}
}


#endif // !TESTING
