#include "LevelObject.h"

void LevelObject::SetPlayer(Hero* player)
{
	m_Player = player;
}
bool LevelObject::CollisionTriggerred() 
{
	PlayerCollider* playerCollider = m_Player->owner->GetComponent<PlayerCollider>();
	return CollisionHandler::GetInstance()->CheckCollision(m_Collider->Get(), playerCollider->Get(), m_Collider->m_IsEnable, playerCollider->m_IsEnable);
}