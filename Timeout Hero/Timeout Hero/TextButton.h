#pragma once
#ifndef TEXTBUTTON_H
#define TEXTBUTTON_H
#include "UIComponent.h"
#include "SDL.h"
#include "Vector2D.h"
#include "TransformComponent.h"
#include "TextComponent.h"

class TextButton : public UIComponent
{
	public:
		TextButton(int x, int y, bool canClick, int size, std::string text, std::string font, SDL_Color color , bool selecting = false);
		~TextButton();
		void Initialize() override;
		void Update(float deltaTime) override;
		void SetFunction(void (*fn)());
		void OnClick();

		Entity* SelectUI(int direction);
		Entity* textButton;
		bool available;
		bool isSet;
	private:
		void (*m_FnPtr)() = nullptr;
		Vector2D buttonPos;
		SDL_Color textColor = { 0,0,0,0 };
		int textSize = 0;
		std::string buttonText;
		std::string fontName;
};

#endif // !