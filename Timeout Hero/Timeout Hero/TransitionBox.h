#pragma once

#ifndef TRANSITION_BOX
#define TRANSITION_BOX

#include "Component.h"
#include "TransformComponent.h"
#include "SpriteComponent.h"
#include "Camera.h"
#include "PauseManager.h"

#define BOX_TRANSITION_SPEED 5000.0f;

class TransitionBox: public Component
{

private:
	float m_TransitionSpeed = BOX_TRANSITION_SPEED;
	bool m_IsTransition = false;

	void (*m_FnPtr)() = nullptr;

	Transform* m_Transform = nullptr;
	Sprite* m_Sprite = nullptr;

	Vector2D m_StartCamPos = Vector2D(0.0f, 0.0f);

public:
	TransitionBox() {}
	~TransitionBox() {}

	void Initialize() override;
	void Update(float deltatime) override;

	void SetFunction(void (*fn)());
	void Transition();

	bool IsTransition();

};

#endif // !TRANSITION_BOX
