#pragma once
#ifndef VECTOR_2D
#define VECTOR_2D

#include <iostream>

class Vector2D
{
public:
	float x, y;

	Vector2D(float x = 0, float y = 0);

	Vector2D operator+(const Vector2D& v2) const;
	Vector2D operator-(const Vector2D& v2) const;
	Vector2D operator*(const float scalar) const;

	//Vector2D GetNormalize();

	void Log(std::string msg = "");

	float Magnitude();
	Vector2D Normalize();

};

#endif // !VECTOR_2D
