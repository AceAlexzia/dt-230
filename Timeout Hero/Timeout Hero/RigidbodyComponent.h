#pragma once
#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#define UNI_MASS 1.0f
#define GRAVITY 9.8f

#include "Vector2D.h"
#include "Component.h"

class Rigidbody: public Component
{
	public:
		Vector2D m_velocity;

		Rigidbody();

		void Initialize() override {}
		void Update(float deltaTime);

		//Set Mass and Gravity
		void setMass(float mass);
		void setGravity(float gravity);

		//Force
		void ApplyForce(Vector2D f);
		void ApplyForceX(float fx);
		void ApplyForceY(float fy);
		void UnSetForce();

		//Getter
		float GetMass();
		Vector2D Position();


	private:
		float m_mass;
		float m_gravity;

		Vector2D m_force;
		Vector2D m_position;
		Vector2D m_acceleration;

};

#endif