#ifndef FONTMANAGER_H
#define FONTMANAGER_H

#include <map>
#include <string>
#include "SDL_ttf.h"
#include "TextureManager.h"
#include "Game.h"

class FontManager
{
private:
	static FontManager* s_Instance;
	std::map<std::string, TTF_Font*> m_FontMap;
	FontManager();

public:
	static FontManager* GetInstance();
	~FontManager() {}
	bool LoadFont(std::string fontID, std::string fontName, int fontSize, int outline = -1);
	void CreateFontTexture(std::string fontID, SDL_Color color, std::string display);
	void Clean();
	void DrawFontTexture(std::string id, int x, int y, int w, int h);
};

#endif
