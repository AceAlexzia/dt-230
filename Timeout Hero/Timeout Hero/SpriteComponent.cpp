#include "SpriteComponent.h"

Sprite::Sprite(std::string assetTextureId, int width, int height, LayerType layer, SDL_RendererFlip flip)
{
    SetTexture(assetTextureId);
    m_Width = width;
    m_Height = height;
    m_Layer = layer;
    m_Flip = flip;
}


void Sprite::Initialize() 
{
    m_Transform = owner->GetComponent<Transform>();
    m_SourceRectangle.x = 0;
    m_SourceRectangle.y = 0;
    m_SourceRectangle.w = m_Width;
    m_SourceRectangle.h = m_Height;
}

void Sprite::Render()
{
    if (m_IsEnable)
    {
        m_DestinationRectangle.x = (int)(m_Transform->position.x) - Camera::GetInstance()->GetPosition().x;
        m_DestinationRectangle.y = (int)(m_Transform->position.y) - Camera::GetInstance()->GetPosition().y;
        m_DestinationRectangle.w = m_Width;
        m_DestinationRectangle.h = m_Height;
        TextureManager::GetInstance()->Draw(m_Texture, m_SourceRectangle, m_DestinationRectangle, m_Flip, m_Angle);
    }
}


int Sprite::GetWidth() 
{
    return m_Width;
}

int Sprite::GetHeight() 
{
    return m_Height;
}


void Sprite::SetTexture(std::string assetTextureId)
{
    m_Texture = AssetManager::GetInstance()->GetTexture(assetTextureId);
}

void Sprite::SetSpriteFrame(int sx)
{
    m_SourceRectangle.x = sx * m_Width;
}