#pragma once
#ifndef TEXTBUTTONSAVESLOT_H
#define TEXTBUTTONSAVESLOT_H
#include "SDL.h"
#include "Vector2D.h"
#include "TransformComponent.h"
#include "SpriteComponent.h"
#include "TextComponent.h"
#include "UIComponent.h"
class TextButtonSaveSlot :public UIComponent
{
	public:
		TextButtonSaveSlot(int x, int y, bool canClick, int size, std::string text, std::string font, SDL_Color color, bool selecting = false);
		~TextButtonSaveSlot();
		void Initialize() override;
		void Update(float deltaTime) override;
		void SetFunction(void (*fn)(int i));

		void OnClick(int i);
		Entity* SelectUI(int direction);
		bool available;
		bool isSet;
		Entity* textButton;
	private:
		void (*m_FnPtr)(int i) = nullptr;
		Vector2D buttonPos;
		SDL_Color textColor = { 0,0,0,0 };
		int textSize = 0;
		std::string buttonText;
		std::string fontName;

};

#endif // !TEXTBUTTONSAVESLOT_H