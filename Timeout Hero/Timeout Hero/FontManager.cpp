#include "FontManager.h"

FontManager* FontManager::s_Instance = nullptr;

FontManager::FontManager() {}

FontManager* FontManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new FontManager();
}

bool FontManager::LoadFont(std::string fontID, std::string fontName, int fontSize, int outline)
{
	std::string filePath = "../assets/fonts/" + fontName + ".ttf";

	TTF_Font* font = TTF_OpenFont(filePath.c_str(), fontSize);

	//if the font does not exist
	if (font == nullptr)
	{
		SDL_Log("Failed to load font: %s %s", filePath.c_str(), SDL_GetError());
		return false;
	}

	//if the font exists -> destroy the font
	std::map<std::string, TTF_Font*>::iterator it = m_FontMap.find(fontID);
	if (it != m_FontMap.end())
	{
		TTF_CloseFont(it->second);
	}

	m_FontMap[fontID] = font;

	if (outline > -1) 
	{
		TTF_SetFontOutline(m_FontMap[fontID], outline);
	}

	return true;
}

void FontManager::CreateFontTexture(std::string fontID, SDL_Color color, std::string display)
{
	SDL_Surface* surface = TTF_RenderText_Solid(m_FontMap[fontID], display.c_str(), color);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(Game::GetInstance()->m_Renderer, surface);

	//if the font texture exists -> destroy the texture
	std::map<std::string, SDL_Texture*>::iterator it = TextureManager::GetInstance()->m_textureMap.find(fontID);
	if (it != TextureManager::GetInstance()->m_textureMap.end())
	{
		SDL_DestroyTexture(TextureManager::GetInstance()->m_textureMap[fontID]);
	}

	TextureManager::GetInstance()->m_textureMap[fontID] = texture;
	SDL_FreeSurface(surface);
}

void FontManager::Clean()
{
	std::map<std::string, TTF_Font*>::iterator it;
	for (it = m_FontMap.begin(); it != m_FontMap.end(); it++)
	{
		TTF_CloseFont(it->second);
	}

	m_FontMap.clear();
}

void FontManager::DrawFontTexture(std::string id, int x, int y, int w, int h)
{
	SDL_Rect srcRect = { 0, 0, w, h };
	SDL_Rect dstRect = { x, y, w, h };
	TextureManager::GetInstance()->Draw(TextureManager::GetInstance()->m_textureMap[id], srcRect, dstRect, SDL_FLIP_NONE);
}

