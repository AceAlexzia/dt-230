#pragma once

#ifndef ARCHFIEND
#define ARCHFIEND

#define ARCHFIEND_ACCELERATION 200.0f
#define ARCHFIEND_MAX_VELOCITY 500.0f
#define ARCHFIEND_FLIP_VELOCITY 150.0f
#define ARCHFIEND_HURT_TIME 0.15f
#define ARCHFIEND_ATTACK_COOLDOWN_STATE_3 1.5f
#define ARCHFIEND_ATTACK_COOLDOWN_STATE_4 2.5f
#define ARCHFIEND_ATTACK_COOLDOWN_STATE_5 2.0f
#define ARCHFIEND_ATTACK_DELAY 0.4f
#define ARCHFIEND_BLINK_LENGTH 0.20f
#define ARCHFIEND_SHAKE_AMOUNT 500.0f

#include "ArchMonster.h"

class Archfiend: public LevelObject
{

private :
	int m_State = -1;

	bool m_FinishState = false;

	bool m_IsHurt = false;
	float m_HurtTime = 0.0f;

	bool m_IsAttack = false;
	float m_AttackCooldown = 0.0f;
	float m_AttackDelay = 0.0f;
	int m_AttackCount = 0;

	float m_BlinkTime = 0.0f;

	bool m_IsDead = false;
	float m_DeadTimeCounter = 0.0f;

	Vector2D m_ShakingPos = Vector2D(0.0f, 0.0f);

	std::map<int, std::vector<Vector2D>> m_ArchmonsterDirection;
	std::map<int, std::vector<float>> m_ArchmonsterDestroyTime;

	Rigidbody* m_Rigidbody = nullptr;

	//Gamemap variable
	int m_RowCount = 0;
	int m_ColCount = 0;
	int m_TileSize = 0;

	//private function
	//sub initialize function
	//initialize all components
	void Initialize_Component();
	//initialize according to its state
	void Initialize_State();
	//initialize archmonster property
	void Initialize_AttackProperty();
	//sub update function
	//update according to its state
	void Update_FinishState(float deltatime);
	//Update Hurt
	void Update_Hurt(float deltatime);
	//Update_Attack
	void Update_Attack(float deltatime);
	//Update Moving
	void Update_Moving();
	//Update Collision
	void Update_Collision();
	//Update Animation
	void Update_Animation();
	//setter
	void SetCollider();

	//archmonster spawner
	void SpawnArchMonster();

	//blinking
	void Blink(float deltatime);

	void Dead();
	void FinishState();

public :

	Archfiend(int state);
	virtual ~Archfiend() {}
	
	void Initialize() override;
	void Update(float deltaTime) override;

	//setter
	void Attack();

	bool IsDead();
};

#endif // !ARCHFIEND

