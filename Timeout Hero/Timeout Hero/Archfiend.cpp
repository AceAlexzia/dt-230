﻿#include "Archfiend.h"
#include "LevelManager.h"
#include "TransitionManager.h"

Archfiend::Archfiend(int state)
{
	//set initialized state
	m_State = state;

	//check whether the state is valid
	if (m_State < 0 || m_State > 5)
	{
		std::cout << "Archfiend invalid state :  " << m_State << std::endl;
		//if not -> destory this object
		owner->Destroy();
	}
}

void Archfiend::Initialize()
{
	//add all neccessary components
	Initialize_Component();

	//initialize according to its state
	Initialize_State();

	//initialize archmonster property
	Initialize_AttackProperty();

	//set collider box position
	SetCollider();

	GameMap* gameMap = LevelManager::GetInstance()->GetMap();
	m_RowCount = gameMap->GetRowCount();
	m_ColCount = gameMap->GetColCount();
	m_TileSize = gameMap->GetTileSize();
}

void Archfiend::Update(float deltaTime)
{
	//finish state updating
	Update_FinishState(deltaTime);
	//update hurt state
	Update_Hurt(deltaTime);
	//update attack state
	Update_Attack(deltaTime);
	//update moving direction
	Update_Moving();
	//updating collision
	Update_Collision();
	//updating animation state
	Update_Animation();
}


void Archfiend::Initialize_Component()
{
	//initialize component
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(0.0f, 0.0f);
		m_Transform = owner->GetComponent<Transform>();
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("archfiend_fly", 225, 225, RUNE_LAYER);
		m_Sprite = owner->GetComponent<Sprite>();
	}
	if (!owner->HasComponent<SpriteAnimation>())
	{
		owner->AddComponent<SpriteAnimation>();
		m_Anim = owner->GetComponent<SpriteAnimation>();
	}
	if (!owner->HasComponent<Collider>())
	{
		owner->AddComponent<Collider>();
		m_Collider = owner->GetComponent<Collider>();
	}
	if (!owner->HasComponent<Rigidbody>())
	{
		owner->AddComponent<Rigidbody>();
		m_Rigidbody = owner->GetComponent<Rigidbody>();
	}

	m_Rigidbody->setGravity(0.0f);

	m_Anim->SetProps("archfiend_fly", 8, 100);
}

void Archfiend:: Initialize_State()
{
	if (m_State == 0)
	{

		//Level-T-8
		if (LevelManager::GetInstance()->levelIndex == 7) 
		{
			m_Transform->position = Vector2D(240.0f, 288.0f);

			m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;

			//TransitionManager::GetInstance()->FadeTransition(0, 0, 0);
		}
		//Level-B-1
		else 
		{
			m_Transform->position = Vector2D(768.0f, 336.0f);

			m_Sprite->m_Flip = SDL_FLIP_NONE;

			//TransitionManager::GetInstance()->FadeTransition(255, 255, 255);
			//Camera::GetInstance()->CameraShake(1000.0f, 3.5f);
		}

		m_Collider->m_IsEnable = false;

	}
	else if (m_State == 1)
	{
		m_Transform->position = Vector2D(240.0f, 288.0f);

		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;

		m_Collider->m_IsEnable = false;

		m_FinishState = true;
	}
	else if (m_State == 2)
	{
		m_Transform->position = Vector2D(720.0f, 144.0f);

		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;

		m_Collider->m_IsEnable = false;

		m_FinishState = true;

		m_Rigidbody->m_velocity.y = -ARCHFIEND_MAX_VELOCITY / 2.0f;
	}
	else if (m_State == 3)
	{
		m_Transform->position = Vector2D(1392.0f, 48.0f);

		m_Sprite->m_Flip = SDL_FLIP_NONE;

		m_Collider->m_IsEnable = true;
	}
	else if (m_State == 4)
	{
		m_Transform->position = Vector2D(1872.0f, 480.0f);

		m_Sprite->m_Flip = SDL_FLIP_NONE;

		m_Collider->m_IsEnable = true;
	}
	else if (m_State == 5)
	{
		m_Transform->position = Vector2D(1824.0f, 576.0f);

		m_Sprite->m_Flip = SDL_FLIP_NONE;

		m_Collider->m_IsEnable = true;
	}
}

void Archfiend::Initialize_AttackProperty()
{
	if (m_State == 0) 
	{
		//temp variable
		int index = 0;

		//first attack
		float degree = 28.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[0].push_back(5.0f);
	}
	else if (m_State == 3) 
	{

		//temp variable
		int index = 0;

		//first attack
		m_ArchmonsterDirection[0].push_back(Vector2D(-1.0f, 0.0f));
		m_ArchmonsterDestroyTime[0].push_back(2.4f);

	}
	else if (m_State == 4) 
	{

		//temp variable
		int index = 0;
		float degree = 0.0f;


		//first attack
		index = 0;

		degree = 10.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(4.4f);


		//second attack
		index = 1;

		degree = 0.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(5.9f);


		//third attack
		index = 2;

		degree = 15.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(4.3f);

		degree = 5.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(5.9f);

	}
	else if (m_State == 5) 
	{
		//temp variable
		int index = 0;
		float degree = 0.0f;


		//first attack
		index = 0;

		degree = 5.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(3.6f);

		degree = 7.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(4.3f);


		//second attack
		index = 1;

		degree = 0.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(4.8f);


		//third attack
		index = 2;

		degree = 20.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(1.6f);


		//fourth attack
		index = 3;

		degree = 2.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(5.9f);

		degree = 10.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(-cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(3.0f);
	}

	if (m_State >= 3) 
	{
		m_IsAttack = true;
	}
	

}

void Archfiend::Update_FinishState(float deltatime)
{
	//if the state is finished
	if (m_FinishState) 
	{
		if (m_State == 1)
		{
			m_Rigidbody->ApplyForceX(-ARCHFIEND_ACCELERATION);
		}
		else if (m_State == 2) 
		{
			m_Rigidbody->ApplyForceY(-ARCHFIEND_ACCELERATION);
		}
		else if (m_State == 3) 
		{
			m_Rigidbody->ApplyForceX(ARCHFIEND_ACCELERATION);
			Blink(deltatime);
		}
		else if (m_State == 4) 
		{
			m_Rigidbody->ApplyForceX(ARCHFIEND_ACCELERATION);
			Blink(deltatime);
		}
		else if (m_State == 5) 
		{
			if (m_IsDead) 
			{

				//set offset of x and y
				float offsetX = GetRandom(-1.0f, 1.0f) * ARCHFIEND_SHAKE_AMOUNT * deltatime;
				float offsetY = GetRandom(-1.0f, 1.0f) * ARCHFIEND_SHAKE_AMOUNT * deltatime;

				//set position = currentPosition + offset
				m_Transform->position = Vector2D(m_ShakingPos.x + offsetX, m_ShakingPos.y + offsetY);

				CutsceneManager::GetInstance()->m_IsBossEvent = false;
			}
			else 
			{
				Blink(deltatime);

				//first frame
				if (m_DeadTimeCounter == 0.0f) 
				{

					LevelManager::GetInstance()->PauseTime();
				}

				//time counting before entering dead animation state
				m_DeadTimeCounter += deltatime;
				if (m_DeadTimeCounter > 2.0f)
				{
					Dead();
					m_DeadTimeCounter = 0.0f;
				}
			}
		}
	}
	
}


void Archfiend::Update_Hurt(float deltatime) 
{
	if (m_IsHurt) 
	{
		m_HurtTime += deltatime;

		if (m_HurtTime >= ARCHFIEND_HURT_TIME) 
		{
			m_HurtTime = 0.0f;
			m_IsHurt = false;

			//set finish state
			m_FinishState = true;

		}
	}
}


void Archfiend::Update_Attack(float deltatime)
{
	if ((m_State != 0 && m_State < 3) || m_IsHurt || m_FinishState) 
	{
		m_IsAttack = false;
		return;
	}

	if (!m_IsAttack) 
	{
		m_AttackCooldown += deltatime;

		if ((m_State == 3 && m_AttackCooldown >= ARCHFIEND_ATTACK_COOLDOWN_STATE_3) || (m_State == 4 && m_AttackCooldown >= ARCHFIEND_ATTACK_COOLDOWN_STATE_4) || (m_State == 5 && m_AttackCooldown >= ARCHFIEND_ATTACK_COOLDOWN_STATE_5))
		{
			m_IsAttack = true;
		}
	}
	else
	{
		if (m_AttackCount < m_ArchmonsterDestroyTime.size())
		{
			m_AttackDelay += deltatime;

			if (m_AttackDelay >= ARCHFIEND_ATTACK_DELAY) 
			{
				m_AttackDelay = 0.0f;
				m_AttackCount++;

				SpawnArchMonster();
			}
		}

		//end of attacking animation
		if (m_Anim->GetIsEnded()) 
		{
			if (m_AttackCount < m_ArchmonsterDestroyTime.size())
			{
				m_Anim->SetProps("archfiend_fly", 8, 100);
				m_IsAttack = true;
			}
			else 
			{
				m_IsAttack = false;
				m_AttackCount = 0;
			}

			m_AttackDelay = 0.0f;
			m_AttackCooldown = 0.0f;
		}
		
	}
}


void Archfiend::Update_Moving()
{

	//limits the velocity
	//velocity X
	if (m_Rigidbody->m_velocity.x < -ARCHFIEND_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.x = -ARCHFIEND_MAX_VELOCITY;
	}
	else if (m_Rigidbody->m_velocity.x > ARCHFIEND_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.x = ARCHFIEND_MAX_VELOCITY;
	}
	//velocity Y
	if (m_Rigidbody->m_velocity.y < -ARCHFIEND_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.y = -ARCHFIEND_MAX_VELOCITY;
	}
	else if (m_Rigidbody->m_velocity.y > ARCHFIEND_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.y = ARCHFIEND_MAX_VELOCITY;
	}

	//change sprite flipping direction
	if (m_Rigidbody->m_velocity.x < -ARCHFIEND_FLIP_VELOCITY && m_Sprite->m_Flip == SDL_FLIP_HORIZONTAL)
	{
		m_Sprite->m_Flip = SDL_FLIP_NONE;
	}
	else if (m_Rigidbody->m_velocity.x > ARCHFIEND_FLIP_VELOCITY && m_Sprite->m_Flip == SDL_FLIP_NONE)
	{
		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
	}

	//set new position
	m_Transform->Translate(m_Rigidbody->Position());
	SetCollider();

	//check whether the object's position is off the map
	if (m_Collider->Get().x + m_Collider->Get().w < 0 || m_Collider->Get().x > m_ColCount * m_TileSize || m_Collider->Get().y + m_Collider->Get().h < 0 || m_Collider->Get().y > m_RowCount * m_TileSize)
	{
		CutsceneManager::GetInstance()->m_IsBossEvent = false;

		LevelManager::GetInstance()->ResumeTime();

		//destroy the object
		owner->Destroy();
	}

}


void Archfiend::Update_Collision()
{

	//if player collides with this
	if (CollisionTriggerred()) 
	{
		//if the player is dashing
		if (m_Player->Get_IsDashing())
		{
			//set IsDashThrough of Hero
			m_Player->m_IsDashThrough = true;
			m_IsHurt = true;

			//collision is disabled
			m_Collider->m_IsEnable = false;

			CutsceneManager::GetInstance()->m_IsBossEvent = true;
		}
		//if the player is not blinking
		else if (!m_Player->Get_IsBlinking())
		{
			//the player hurts
			m_Player->Dead();
		}
	}
}


void Archfiend::Update_Animation() 
{
	
	if (m_State == 5 && m_FinishState) 
	{
		if (m_IsDead) 
		{
			m_Anim->SetProps("archfiend_dead", 4, 250);
		}
		else 
		{
			m_Anim->SetProps("archfiend_hurt", 2, 120);
		}
	}
	else if (m_IsHurt)
	{
		m_Anim->SetProps("archfiend_hurt", 2, 0);
	}
	else if (m_IsAttack) 
	{
		m_Anim->SetProps("archfiend_attack", 9, 80, false);
	}
	else 
	{
		m_Anim->SetProps("archfiend_fly", 8, 100);
	}
}


void Archfiend::SetCollider() 
{
	if (m_Sprite->m_Flip == SDL_FLIP_HORIZONTAL) 
	{
		m_Collider->SetBuffer(-80, -40, 135, 55);
	}
	else 
	{
		m_Collider->SetBuffer(-55, -40, 135, 55);
	}
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}


void Archfiend::SpawnArchMonster()
{

	float spawnOffsetX;
	if (m_State >= 3) 
	{
		spawnOffsetX = 10.0f;
	}
	else 
	{
		spawnOffsetX = m_Sprite->GetWidth() - 100.0f;
	}

	for (int i = 0; i < m_ArchmonsterDestroyTime[m_AttackCount - 1].size(); i++)
	{
		//initialize Normal Monster Entity
		Entity& archMonster(EntityManager::GetInstance()->AddEntity("ArchMonster"));

		//initialize components for Normal Monster entity
		//Transform Component
		archMonster.AddComponent<Transform>(m_Transform->position.x + spawnOffsetX, m_Transform->position.y + 30);
		//Sprite Component
		archMonster.AddComponent<Sprite>("arch-monster-idle", 96, 96, MONSTER_LAYER);
		//Collider Component
		archMonster.AddComponent<Collider>();
		//SpriteAnimation Component
		archMonster.AddComponent<SpriteAnimation>();
		//Rigidbody Component
		archMonster.AddComponent<Rigidbody>();
		//Monster Component
		archMonster.AddComponent<ArchMonster>(m_ArchmonsterDestroyTime[m_AttackCount - 1][i], m_ArchmonsterDirection[m_AttackCount - 1][i]);

		archMonster.GetComponent<ArchMonster>()->SetPlayer(m_Player);
	}
	
}


void Archfiend::Blink(float deltatime)
{
	m_BlinkTime += deltatime;

	if (m_BlinkTime >= ARCHFIEND_BLINK_LENGTH)
	{
		m_BlinkTime = 0.0f;

		m_Sprite->m_IsEnable = !m_Sprite->m_IsEnable;
	}
}


void Archfiend::FinishState() 
{
	m_FinishState = true;
}


void Archfiend::Attack()
{
	m_IsAttack = true;
}


void Archfiend::Dead() 
{
	m_Sprite->m_IsEnable = true;
	m_IsDead = true;
	m_ShakingPos = m_Transform->position;
}


bool Archfiend::IsDead() 
{
	return m_IsDead;
}