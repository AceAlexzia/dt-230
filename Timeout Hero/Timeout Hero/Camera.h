#pragma once
#ifndef CAMERA
#define CAMERA

#define CAMERA_OFFSET_Y 60.8f
#define CAMERA_LERPING_SPEED 16.5f

#include "SDL.h"
#include "Vector2D.h"
#include "Entity.h"
#include "Constants.h"
#include "SaveManager.h"

class Camera
{
public:
	~Camera() {}

	//get instance of the camera
	static Camera* GetInstance();

	//get camera's view box
	SDL_Rect GetViewBox();
	//get camera positon
	Vector2D GetPosition();

	//set hero position
	void SetHeroOrigin(Vector2D pos);
	//set boundary's position
	void SetBoundary(Vector2D pos);

	//set camera shaking value
	void CameraShake(float amount, float time);

	void Initialize();
	void Update(float deltatime);
	void Reset();

private:

	static Camera* s_Instance;

	//hero's position
	Vector2D m_HeroOrigin;

	//camera's view box
	SDL_Rect m_ViewBox;

	//camera's boundary position
	Vector2D m_Boundary;

	//camera shaking amount
	float m_ShakeAmount;
	//camera shaking time period
	float m_ShakeTime;


	Camera();

	//private funtion
	//set camera postion
	void SetPosition(Vector2D pos);

	//update camera's position
	void Update_Positon(float deltatime);
	//checking whether the camera exits its boundary postion
	void Update_Boundary();
	//update camera shaking
	void Update_CameraShake(float deltatime);

};

#endif // !CAMERA

