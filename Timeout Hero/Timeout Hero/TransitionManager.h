#pragma once
#ifndef TRANSITION_MANAGER
#define TRANSITION_MANAGER

#define TRANSITION_FUNCTION_LEVEL_MANAGER "LevelManager"
#define TRANSITION_FUNCTION_START_GAME "StartGame"

#include <map>
#include "TransitionBox.h"
#include "TransitionFunction.h"

class TransitionManager
{
private :

	static TransitionManager* s_Instance;

	TransitionBox* m_TransBox;

	SDL_Color m_RectColor = { 0, 0, 0, 0 };
	bool m_IsFade = false;
	float m_FadeTimeCounter = 0.0f;

	TransitionManager() {}

	std::map<std::string, void (*)()> m_FunctionDict;

	void SetFunction(std::string fn);

public:

	~TransitionManager() {}

	static TransitionManager* GetInstance();
	void Initialize();

	void BoxTransition(std::string);
	void FadeTransition(Uint8 r = 255, Uint8 g = 255, Uint8 b = 255);

	void DrawRect(Uint8 r, Uint8 g, Uint8 b, Uint8 a);

	void Update(float deltatime);
	void Render();

	bool IsFadeTransition();
	bool IsBoxTransition();
	bool IsTransition();

};

#endif // !TRANSITION_MANAGER
