#pragma once
#ifndef CONTROLLER_MANAGER
#define CONTROLLER_MANAGER

//====SERIALIZE VALUE====
#define DEAD_ZONE 10000.0f
#define MAX_AXIS 32768.0f;


#include "SDL.h"
#include <iostream>

//struct for a gamepad
struct GamePad
{
	bool buttons[SDL_CONTROLLER_BUTTON_MAX];
	int axis[SDL_CONTROLLER_AXIS_MAX];
};

class ControllerManager 
{
private :
	SDL_GameController* m_Controller = nullptr;
	GamePad m_CurrentInput;
	GamePad m_LastInput;

	//private function
	void Close();
	void Reset();
	void Connect();

public :
	ControllerManager();
	virtual ~ControllerManager();
	void BeforeListen();
	void Listen(SDL_Event event);

	//Controller Getter
	bool GetKey(SDL_GameControllerButton button);
	bool GetKeyUp(SDL_GameControllerButton button);
	bool GetKeyDown(SDL_GameControllerButton button);
	float GetAxisController(SDL_GameControllerAxis axis);

	bool HasController();
	
};

#endif // !CONTROLLER_MANAGER
