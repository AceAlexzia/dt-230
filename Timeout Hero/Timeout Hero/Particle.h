#pragma once
#ifndef PARTICLE_H
#define PARTICLE_H

#include "TransformComponent.h"
#include "SpriteAnimationComponent.h"
#include <iostream>

//using ParticleList = std::vector<ParticleProperty>;


class Particle : public Component
{
public:
    Particle() {}
    ~Particle() {}

    void Initialize() override;
    void Update(float deltaTime) override;

    float despawn_time = 0.0f;
    bool finish_loop = false;


private:
    SpriteAnimation* m_Anim;
    Transform* m_Transform;
    Sprite* m_Sprite;

};

#endif