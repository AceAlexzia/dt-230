#include "SpellMonster.h"

SpellMonster::SpellMonster(float destroyTime, Vector2D movingDirection)
{
	m_DestroyTime = destroyTime;
	m_MovingDirection = movingDirection;
}


void SpellMonster::Initialize()
{
	m_Collider = owner->GetComponent<Collider>();
	m_Transform = owner->GetComponent<Transform>();
	m_Sprite = owner->GetComponent<Sprite>();
	m_Anim = owner->GetComponent<SpriteAnimation>();
	m_Rb = owner->GetComponent<Rigidbody>();

	m_Rb->setGravity(0.0f);
	m_Anim->SetProps("spell-monster-idle", 4, 100, true);
	m_Collider->SetBuffer(-13, -34, 52, 52);
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}

void SpellMonster::Update(float deltaTime)
{
	//increase arch monster lifespawn in every frame
	m_Lifespan += deltaTime;

	//when the life span is end
	if (m_Lifespan >= m_DestroyTime)
	{
		//monster is not alive
		m_IsAlive = false;
		m_Sprite->m_IsEnable = false;

		//spawn a particle
		ParticleManager::GetInstance()->Spawn("spellmonster_explosion", Vector2D(m_Transform->position.x - 48, m_Transform->position.y - 48));

		Camera::GetInstance()->CameraShake(900.0f, 0.3f);

		//destroy the entity
		owner->Destroy();

		return;
	}

	//updating its velocity
	m_Rb->m_velocity = m_MovingDirection * SPELL_MONSTER_SPEED;

	//move entity and its collider
	m_Transform->Translate(m_Rb->Position());
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());

	if (CollisionTriggerred() && !m_Player->Get_IsBlinking())
	{
		m_Lifespan = m_DestroyTime;

		m_Player->m_IsHurt = true;
		m_Player->m_Damagex2 = true;
	}

}
