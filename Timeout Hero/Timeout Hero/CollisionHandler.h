#pragma once
#ifndef COLLISION_HANDLER
#define COLLISION_HANDLER

#define MAX_LOOP_COUNT 5

#include "SDL.h"
#include <vector>
#include "TileLayer.h"
#include "GameMap.h"
#include "Game.h"
#include "Vector2D.h"
class LevelManager;
class CollisionHandler
{
public:
	bool CheckCollision(SDL_Rect a, SDL_Rect b, bool a_enable = true, bool b_enable = true);
	//directional collision checking can be done only when CheckCollision returns true
	bool CheckCollision_Left(SDL_Rect a, SDL_Rect b);
	bool CheckCollision_Right(SDL_Rect a, SDL_Rect b);
	bool CheckCollision_Up(SDL_Rect a, SDL_Rect b);
	bool CheckCollision_Down(SDL_Rect a, SDL_Rect b);

	bool MapCollision_Inside(SDL_Rect a);
	bool MapCollision_Up(SDL_Rect a, float& tilePos);
	bool MapCollision_Down(SDL_Rect a, float& tilePos);
	bool MapCollision_Left(SDL_Rect a, float& tilePos);
	bool MapCollision_Right(SDL_Rect a, float& tilePos);

	bool SpikeCollision_Inside(SDL_Rect a);
	bool SpikeCollision_Up(SDL_Rect a);
	bool SpikeCollision_Down(SDL_Rect a);
	bool SpikeCollision_Left(SDL_Rect a);
	bool SpikeCollision_Right(SDL_Rect a);
	static CollisionHandler* GetInstance();
	~CollisionHandler() {}

	void InitMapCollision();
	bool MapCollision_Spike(SDL_Rect a);
	int GetTileSize();
private:
	CollisionHandler();
	TileMap m_CollisionTileMap;
	TileLayer* m_CollisionLayer;
	static CollisionHandler* s_Instance;

	TileMap m_SpikeTileMap;
	TileLayer* m_SpikeLayer;
};

#endif // !COLLISION_HANDLER
