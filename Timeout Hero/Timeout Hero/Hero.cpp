﻿#include "Hero.h"
#include "LevelManager.h"
#include "TransitionManager.h"


void Hero::Initialize() 
{
	//Initialize Component
	m_Rigidbody = owner->GetComponent<Rigidbody>();
	m_Transform = owner->GetComponent<Transform>();
	m_Animation = owner->GetComponent<SpriteAnimation>();
	m_Sprite = owner->GetComponent<Sprite>();
	m_Collider = owner->GetComponent<PlayerCollider>();

	//initialize all variables
	Reset();
	//assign input scheme
	AssignInput();

	//Set starting component values
	//collider component
	m_Collider->SetBuffer(-17, -28, 34, 28);
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
	//sprite animation component
	m_Animation->SetProps("Hero-idle", 4, 200, true);
	//rigidbody component
	m_Rigidbody->setGravity(HERO_GRAVITY);
}

void Hero::Update(float deltaTime)
{
	if (m_IsStop) 
	{
		return;
	}
	Update_Input();

	//one of these update function passes -> no need to update other states
	if (Update_Dead(deltaTime))
	{
		return;
	}
	else if (Update_GettingRune(deltaTime))
	{
		return;
	}
	else if (Update_DeadCondition()) 
	{
		return;
	}
	else if (Update_IsWarp(deltaTime))
	{
		return;
	}

	
	Update_IsDashThrough();
	Update_Running(deltaTime);
	Update_Dashing(deltaTime);
	Update_Jumping(deltaTime);
	Update_Falling();
	Update_Gliding(deltaTime);
	Update_KnockBack(deltaTime);
	Update_Blinking(deltaTime);
	Update_MapCollision(deltaTime);
	Update_RelativePosition(deltaTime);
	Update_Climbing(deltaTime);
	Update_AnimationState();
	Update_Boundary();
}


bool Hero::Get_IsDashing() 
{ 
	return m_IsDashing; 
}

bool Hero::Get_IsBlinking() 
{ 
	return m_IsBlinking; 
}

bool Hero::Get_IsKnockBack() 
{
	return m_IsKnockBack;
}


void Hero::Dead()
{
	if (m_IsEvent) 
	{
		return;
	}

	m_IsDead = true;
	//set screen shake
	Camera::GetInstance()->CameraShake(1200.0f, 0.4f);
	SaveManager::GetInstance()->SetDeath();

	LevelManager* levelManager = LevelManager::GetInstance();

	//pause and stop blinking TimeCounter
	levelManager->PauseTime();
	levelManager->StopBlinkTime();
	//level index is chabged according to the secition increased
	levelManager->levelIndex -= LevelManager::GetInstance()->sectionChange;
	//section changing is reset
	levelManager->sectionChange = 0;

	m_Collider->m_IsEnable = false;
	
}

void Hero::ReachRune() 
{
	m_IsGetRune = true;
	LevelManager::GetInstance()->StopBlinkTime();
}

void Hero::Warp(Direction dir)
{
	m_WarpDirection = dir;
	m_IsWarp = true;
	LevelManager::GetInstance()->StopBlinkTime();
}


void Hero::MoveX(float x) 
{
	m_Transform->TranslateX(x);
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}

void Hero::MoveY(float y)
{
	m_Transform->TranslateY(y);
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}


void Hero::CollideX(Direction dir)
{
	m_CollisionDirections[dir] = true;

	m_Transform->position.x = m_Collider->lastSafePos.x;
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());

	m_Collider->objectCollision[0] = false;

}

void Hero::CollideY(Direction dir)
{

	//object collision is triggered
	if (m_Collider->objectCollision[1])
	{
		//reset the checkign boolean
		m_Collider->objectCollision[1] = false;

		//if the object is below and the charcter is moving up
		if (dir == DOWN && (m_IsJumping || m_IsDashing && m_DashDirection.y < 0.0f)) 
		{
			return;
		}
		//the object is above and the character is in falling state
		else if (dir == UP && m_IsFalling) 
		{
			return;
		}
	}


	m_CollisionDirections[dir] = true;

	m_Transform->position.y = m_Collider->lastSafePos.y;
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());

	m_Collider->objectCollision[1] = false;

}


void Hero::UnlockAbility(bool jump, bool dash, bool wallJump, bool forceDead, bool glide)
{
	m_UnlockJump = jump;
	m_UnlockDash = dash;
	m_UnlockWallJump = wallJump;
	m_UnlockForceDead = forceDead;
	m_UnlockGlide = glide;
}


void Hero::AssignInput() 
{
	//assign KEYBOARD input
	m_KeyboardInputs[UP_KEY] = SDL_SCANCODE_W;
	m_KeyboardInputs[LEFT_KEY] = SDL_SCANCODE_A;
	m_KeyboardInputs[DOWN_KEY] = SDL_SCANCODE_S;
	m_KeyboardInputs[RIGHT_KEY] = SDL_SCANCODE_D;
	m_KeyboardInputs[FORCE_DEAD_KEY] = SDL_SCANCODE_B;
	m_KeyboardInputs[JUMP_KEY] = SDL_SCANCODE_SPACE;
	m_KeyboardInputs[CLIMB_KEY] = SDL_SCANCODE_J;
	m_KeyboardInputs[DASH_KEY] = SDL_SCANCODE_K;

	//assign CONTROLLER input
	m_ControllerInputs[UP_KEY] = SDL_CONTROLLER_BUTTON_INVALID;
	m_ControllerInputs[LEFT_KEY] = SDL_CONTROLLER_BUTTON_INVALID;
	m_ControllerInputs[DOWN_KEY] = SDL_CONTROLLER_BUTTON_INVALID;
	m_ControllerInputs[RIGHT_KEY] = SDL_CONTROLLER_BUTTON_INVALID;
	m_ControllerInputs[FORCE_DEAD_KEY] = SDL_CONTROLLER_BUTTON_RIGHTSTICK;
	m_ControllerInputs[JUMP_KEY] = SDL_CONTROLLER_BUTTON_A;
	m_ControllerInputs[CLIMB_KEY] = SDL_CONTROLLER_BUTTON_X;
	m_ControllerInputs[DASH_KEY] = SDL_CONTROLLER_BUTTON_RIGHTSHOULDER;


	//assign ALT KEYBOARD input
	/*m_AltKeyboardInputs[UP_KEY] = m_KeyboardInputs[UP_KEY];
	m_AltKeyboardInputs[LEFT_KEY] = m_KeyboardInputs[LEFT_KEY];
	m_AltKeyboardInputs[DOWN_KEY] = m_KeyboardInputs[DOWN_KEY];
	m_AltKeyboardInputs[RIGHT_KEY] = m_KeyboardInputs[RIGHT_KEY];
	m_AltKeyboardInputs[FORCE_DEAD_KEY] = m_KeyboardInputs[FORCE_DEAD_KEY];
	m_AltKeyboardInputs[JUMP_KEY] = m_KeyboardInputs[JUMP_KEY];
	m_AltKeyboardInputs[CLIMB_KEY] = m_KeyboardInputs[CLIMB_KEY];
	m_AltKeyboardInputs[DASH_KEY] = m_KeyboardInputs[DASH_KEY];*/

	//assign ALT CONTROLLER input
	m_AltControllerInputs[UP_KEY] = SDL_CONTROLLER_BUTTON_INVALID;
	m_AltControllerInputs[LEFT_KEY] = SDL_CONTROLLER_BUTTON_INVALID;
	m_AltControllerInputs[DOWN_KEY] = SDL_CONTROLLER_BUTTON_INVALID;
	m_AltControllerInputs[RIGHT_KEY] = SDL_CONTROLLER_BUTTON_INVALID;
	m_AltControllerInputs[FORCE_DEAD_KEY] = SDL_CONTROLLER_BUTTON_RIGHTSTICK;
	m_AltControllerInputs[JUMP_KEY] = SDL_CONTROLLER_BUTTON_Y;
	m_AltControllerInputs[CLIMB_KEY] = SDL_CONTROLLER_BUTTON_B;
	m_AltControllerInputs[DASH_KEY] = SDL_CONTROLLER_BUTTON_LEFTSHOULDER;
}


void Hero::CheckMovingInput(bool& left, bool& right, Direction& lastDirection) 
{
	//check last directional input when play on the keyboard
	if (left && !right)
	{
		lastDirection = LEFT;
	}
	else if (right && !left)
	{
		lastDirection = RIGHT;
	}
	else if (left && right)
	{
		if (lastDirection == LEFT)
		{
			left = false;
		}
		else if (lastDirection == RIGHT)
		{
			right = false;
		}
	}
}

void Hero::Reset()
{
	m_IsRunning = false;
	m_IsJumping = false;
	m_IsFalling = false;
	m_IsGrounded = false;
	m_IsDashing = false;
	m_IsKnockBack = false;
	m_IsBlinking = false;
	m_IsGliding = false;
	m_IsDead = false;
	m_IsGetRune = false;
	m_IsWarp = false;
	m_IsClimbing = false;

	m_IsFirstMove = false;

	m_CanJump = false;
	m_CanDash = false;
	m_CanGlide = false;

	m_StartCollideUp = false;
	m_StartCollideDown = false;

	m_JumpTime = HERO_JUMP_TIME;
	m_JumpForce = HERO_JUMP_FORCE;
	m_DashForce = HERO_DASH_FORCE;
	m_DashTime = HERO_DASH_TIME;
	m_KnockBackForce = HERO_KNOCK_BACK_FORCE;
	m_KnockBackTime = HERO_KNOCK_BACK_TIME;
	m_DeadTime = HERO_DEAD_TIME;
	m_RuneTime = HERO_RUNE_TIME;
	m_WarpTime = HERO_WARP_TIME;

	m_Sprite->m_IsEnable = true;

	SetBaseDamage();

	ParticleManager::GetInstance()->Spawn("hero_warp_x", Vector2D(m_Transform->position.x - 60, m_Transform->position.y + 30));
}

void Hero::SetBaseDamage() 
{
	int saveIndex = SaveManager::GetInstance()->currentSlotPos;
	std::string mode = SaveManager::GetInstance()->save[saveIndex].mode;
	//easy mode
	if (mode == "Easy") 
	{
		m_BaseDamage = 0;
	}
	//hard mode
	else if (mode == "Hard") 
	{
		m_BaseDamage = 10;
	}
	//hardcore mode
	else if (mode == "Timeout")
	{
		m_BaseDamage = 61;
	}
	//no hero stands mode
	else if (mode == "No-Hero-Stands")
	{
		m_BaseDamage = 61;
	}
	//normal mode
	else 
	{
		m_BaseDamage = 5;
	}
}

bool Hero::IsHurt()
{
	if (m_IsBlinking)
	{
		return false;
	}
	else
	{
		m_KnockBackDirection = Vector2D(0.0f, 0.0f);
		//Hero hurts by MONSTER
		if (m_IsHurt)
		{
			m_KnockBackDirection.y = -1.0f;
			if (m_Sprite->m_Flip == SDL_FLIP_NONE)
			{
				m_KnockBackDirection.x = -1.0f;
			}
			else
			{
				m_KnockBackDirection.x = 1.0f;
			}
			return true;
		}
		//Hero hurts by SPIKE
		else if (m_Collider->CollideWithSpike())
		{
			m_KnockBackDirection.y = -1.0f;
			if (m_Collider->CollideWithSpike_Up() && !m_IsGrounded && !m_IsFalling)
			{
				m_KnockBackDirection.y = 1.0f;
			}

			if (m_Collider->CollideWithSpike_Left() && !m_Collider->CollideWithSpike_Right())
			{
				m_KnockBackDirection.x = 1.0f;
			}
			else if (m_Collider->CollideWithSpike_Right() && !m_Collider->CollideWithSpike_Left())
			{
				m_KnockBackDirection.x = -1.0f;
			}
			else
			{
				if (m_Sprite->m_Flip == SDL_FLIP_NONE)
				{
					m_KnockBackDirection.x = -1.0f;
				}
				else
				{
					m_KnockBackDirection.x = 1.0f;
				}
			}
			return true;
		}
		//Hero does not hurt
		else
		{
			return false;
		}
	}
}

//UPDATE FUNCTION FOR CHECKING HERO STATE

void Hero::Update_Input() 
{
	//reset all input key
	for (int i = 0; i < INPUTKEYS; i++)
	{
		m_InputArray[i] = false;
	}

	if (m_IsEvent) 
	{
		LevelManager::GetInstance()->PauseTime();
		return;
	}
	else if (m_IsFirstMove && !m_IsWarp && !m_IsGetRune && !m_IsDead)
	{
		LevelManager::GetInstance()->ResumeTime();
	}

	//assign all input as an array
	m_InputArray[DASH_KEY] = InputManager::GetInstance()->GetKeyDown(m_KeyboardInputs[DASH_KEY]) || InputManager::GetInstance()->GetKeyDown(m_ControllerInputs[DASH_KEY]) || InputManager::GetInstance()->GetKeyDown(m_AltControllerInputs[DASH_KEY]);
	m_InputArray[FORCE_DEAD_KEY] = InputManager::GetInstance()->GetKeyDown(m_KeyboardInputs[FORCE_DEAD_KEY]) || InputManager::GetInstance()->GetKeyDown(m_ControllerInputs[FORCE_DEAD_KEY]);
	m_InputArray[RIGHT_KEY] = InputManager::GetInstance()->GetKey(m_KeyboardInputs[RIGHT_KEY]) || InputManager::GetInstance()->GetAxisController(SDL_CONTROLLER_AXIS_LEFTX) > 0.0f;
	m_InputArray[LEFT_KEY] = InputManager::GetInstance()->GetKey(m_KeyboardInputs[LEFT_KEY]) || InputManager::GetInstance()->GetAxisController(SDL_CONTROLLER_AXIS_LEFTX) < 0.0f;
	m_InputArray[UP_KEY] = InputManager::GetInstance()->GetKey(m_KeyboardInputs[UP_KEY]) || InputManager::GetInstance()->GetAxisController(SDL_CONTROLLER_AXIS_LEFTY) < -HERO_CONTROLLER_DEAD_ZONE_DASH;
	m_InputArray[DOWN_KEY] = InputManager::GetInstance()->GetKey(m_KeyboardInputs[DOWN_KEY]) || InputManager::GetInstance()->GetAxisController(SDL_CONTROLLER_AXIS_LEFTY) > HERO_CONTROLLER_DEAD_ZONE_DASH;
	m_InputArray[JUMP_KEY] = InputManager::GetInstance()->GetKey(m_KeyboardInputs[JUMP_KEY]) || InputManager::GetInstance()->GetKey(m_ControllerInputs[JUMP_KEY]) || InputManager::GetInstance()->GetKey(m_AltControllerInputs[JUMP_KEY]);
	m_InputArray[CLIMB_KEY] = InputManager::GetInstance()->GetKey(m_KeyboardInputs[CLIMB_KEY]) || InputManager::GetInstance()->GetKey(m_ControllerInputs[CLIMB_KEY]) || InputManager::GetInstance()->GetKey(m_AltControllerInputs[CLIMB_KEY]);

	CheckMovingInput(m_InputArray[LEFT_KEY], m_InputArray[RIGHT_KEY], m_LastMovingDirectionX);
	
}

bool Hero::Update_Dead(float deltaTime)
{
	//if the hero is dead and transitioin is not done
	if (m_IsDead && m_DeadTime > 0.0f)
	{
		//first frame in dead state
		if (m_DeadTime == HERO_DEAD_TIME) 
		{
			//spawn dead particle
			Spawn_DeadParticle();

		}

		//disabling sprite
		m_Sprite->m_IsEnable = false;

		//dead time variable decreasing
		m_DeadTime -= deltaTime;

		//if dead time is decreased < 0.0
		if (m_DeadTime <= 0.0f) 
		{

			int saveIndex = SaveManager::GetInstance()->currentSlotPos;

			if (SaveManager::GetInstance()->save[saveIndex].mode == "No-Hero-Stands")
			{
				//delete the save file and go back to menu
				UIManager::GetInstance()->NoHeroStandDeleteSave(saveIndex);
			}
			else 
			{
				//create scene transition
				TransitionManager::GetInstance()->BoxTransition(TRANSITION_FUNCTION_LEVEL_MANAGER);
			}
			
		}
	}
	
	return m_IsDead;
}

bool Hero::Update_GettingRune(float deltaTime) 
{
	//if the hero gets rune and transitioin is not done
	if (m_IsGetRune && m_RuneTime > 0.0f)
	{
		//first frame
		if (m_RuneTime == HERO_RUNE_TIME)
		{
			LevelManager::GetInstance()->PauseTime();
			//spawn particle
			Spawn_RuneParticle();
		}

		//enabling sprite if it is blinking
		m_Sprite->m_IsEnable = false;
		//rune time variable decreasing
		m_RuneTime -= deltaTime;


		//if rune time is decreased < 0.0
		if (m_RuneTime <= 0.0f)
		{
			//create scene transition
			TransitionManager::GetInstance()->BoxTransition(TRANSITION_FUNCTION_LEVEL_MANAGER);

			//go to next level
			LevelManager::GetInstance()->levelIndex++;
			//section changing is reset
			LevelManager::GetInstance()->sectionChange = 0;
		}
	}

	return m_IsGetRune;
}

bool Hero::Update_DeadCondition()
{

	if (!m_UnlockForceDead) 
	{
		return false;
	}

	//checking condition which gonna set Hero to dead state
	//Force Dead or Time's up
	if (m_InputArray[FORCE_DEAD_KEY] || LevelManager::GetInstance()->GetTime() <= 0.0f)
	{
		//set Hero to dead
		Dead();

		return true;
	}
	else
	{
		return false;
	}
}

bool Hero::Update_IsWarp(float deltaTime) 
{
	if (m_IsWarp) 
	{
		//first frame
		if (m_WarpTime == HERO_WARP_TIME) 
		{
			Spawn_WarpParticle();
		}

		//disabling sprite
		m_Sprite->m_IsEnable = false;

		//warp time variable decreasing
		m_WarpTime -= deltaTime;

		//if rune time is decreased < 0.0
		if (m_WarpTime <= 0.0f)
		{
			//set load new level
			LevelManager::GetInstance()->SetLoadNewLevel();
		}
	}

	return m_IsWarp;
}

void Hero::Update_IsDashThrough()
{
	if (m_IsDashThrough)
	{
		//set to false
		m_IsDashThrough = false;

		//slow motion effect
		Timer::GetInstance()->SlowMotion();

		//reset jump and dash
		m_CanJump = m_CanDash = true;

		//set screen shake
		Camera::GetInstance()->CameraShake(2000.0f, 0.3f);

		//play dash sound effect
		AudioManager::GetInstance()->dashSfxPlayer.Play();
	}
}

void Hero::Update_Running(float deltaTime) 
{
	m_IsRunning = false;

	//not update the runnung input while climbing
	if (m_IsClimbing) 
	{
		return;
	}

	//Run Forward
	//Run in right direction
	if (m_InputArray[RIGHT_KEY] && !m_IsDashing && !m_IsKnockBack)
	{
		m_Sprite->m_Flip = SDL_FLIP_NONE;
		m_IsRunning = true;

		//move RIGHT while GLIDING
		if (m_IsGliding)
		{
			//check before applying
			if (m_Rigidbody->m_velocity.x < 0.0f)
			{
				m_Rigidbody->m_velocity.x = 0;
			}
			float speed = HERO_RUN_SPEED * HERO_RUN_ACC_MUL * deltaTime;
			//apply velocity x
			m_Rigidbody->m_velocity.x += speed;
			if (m_Rigidbody->m_velocity.x > HERO_RUN_SPEED)
			{
				m_Rigidbody->m_velocity.x = HERO_RUN_SPEED;
			}
		}
		//move RIGHT while NOT GLIDING
		else
		{
			m_Rigidbody->m_velocity.x = HERO_RUN_SPEED;
		}

	}
	//Run Backward
	//Run in left direction
	else if (m_InputArray[LEFT_KEY] && !m_IsDashing && !m_IsKnockBack)
	{
		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
		m_IsRunning = true;

		//move LEFT while GLIDING
		if (m_IsGliding)
		{
			//check before applying
			if (m_Rigidbody->m_velocity.x > 0.0f)
			{
				m_Rigidbody->m_velocity.x = 0;
			}
			float speed = HERO_RUN_SPEED * HERO_RUN_ACC_MUL * deltaTime;
			//apply velocity x
			m_Rigidbody->m_velocity.x -= speed;
			if (m_Rigidbody->m_velocity.x < -HERO_RUN_SPEED)
			{
				m_Rigidbody->m_velocity.x = -HERO_RUN_SPEED;
			}
		}
		//move LEFT while NOT GLIDING
		else
		{
			m_Rigidbody->m_velocity.x = -HERO_RUN_SPEED;
		}

	}
	else
	{
		m_Rigidbody->m_velocity.x = 0.0f;
	}
}

void Hero::Update_Dashing(float deltaTime) 
{
	//the character does not unlock dashing state
	if (!m_UnlockDash)
	{
		return;
	}


	//CanDash checking
	if (m_IsGrounded && !m_IsDashing && !m_CanDash) 
	{
		m_CanDash = true;
	}

	//Dash
	if (m_InputArray[DASH_KEY] && !m_IsDashing && m_CanDash && !m_IsKnockBack)
	{
		m_IsDashing = true;
		m_CanDash = false;
		m_CanJump = false;

		//reset dash direction
		m_DashDirection = Vector2D(0.0f, 0.0f);

		//Checking Dash Direction
		//y direction
		if (m_InputArray[DOWN_KEY])
		{
			m_DashDirection.y = 1.0f;
		}
		else if (m_InputArray[UP_KEY])
		{
			m_DashDirection.y = -1.0f;
		}
		//x direction
		if (m_InputArray[LEFT_KEY])
		{
			m_DashDirection.x = -1.0f;
		}
		else if (m_InputArray[RIGHT_KEY])
		{
			m_DashDirection.x = 1.0f;
		}

	}

	//Dashing
	if (m_IsDashing)
	{

		//checking dash state duration
		m_DashTime -= deltaTime;
		if (m_DashTime <= 0.0f)
		{
			m_IsDashing = false;
			m_DashTime = HERO_DASH_TIME;
			return;
		}

		//spawn dashing particle
		Spawn_DashParticle();

		//default dash direction
		if (m_DashDirection.x == 0 && m_DashDirection.y == 0) 
		{
			if (m_Sprite->m_Flip == SDL_FLIP_NONE) 
			{
				m_DashDirection = Vector2D(1.0f, 0.0f);
			}
			else
			{
				m_DashDirection = Vector2D(-1.0f, 0.0f);
			}
		}
		//apply force
		if (m_DashDirection.x != 0 && m_DashDirection.y != 0) 
		{
			m_Rigidbody->m_velocity = m_DashDirection * m_DashForce * (sqrt(2) / 2.0f);
		}
		else
		{
			m_Rigidbody->m_velocity = m_DashDirection * m_DashForce;
		}

		m_DashDirectionForCollisionChecking = Vector2D(m_DashDirection.x, m_DashDirection.y);

		//fliping check
		if (m_DashDirection.x > 0.0f) 
		{
			m_Sprite->m_Flip = SDL_FLIP_NONE;
		}
		else if (m_DashDirection.x < 0.0f)
		{
			m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
		}
	}
}

void Hero::Update_Jumping(float deltaTime)
{
	//the character does not unlock jumping state
	if (!m_UnlockJump) 
	{
		return;
	}

	//CanJump Checking
	if (!m_CanJump && m_IsGrounded && !m_IsDashing) 
	{
		m_CanJump = true;
	}

	//Jump
	if (m_InputArray[JUMP_KEY] && m_CanJump && !m_IsDashing && !m_IsKnockBack)
	{
		m_IsJumping = true;
		m_CanJump = false;
		m_Rigidbody->m_velocity.y = m_JumpForce * -1 * m_JumpTime;
	}
	else if (m_InputArray[JUMP_KEY] && m_IsJumping && m_JumpTime > 0.0f && !m_IsDashing && !m_IsKnockBack)
	{
		m_Rigidbody->m_velocity.y = m_JumpForce * -1 * m_JumpTime;
		m_JumpTime -= deltaTime;
	}
	//stop jumping frame
	else if (m_IsJumping)
	{
		m_IsJumping = false;
		m_JumpTime = HERO_JUMP_TIME;
	}
}

void Hero::Update_Falling()
{
	//Falling
	if (m_Rigidbody->m_velocity.y > 0 && !m_IsGrounded)
	{
		//start falling frame
		if (!m_IsFalling) 
		{
			m_CanGlide = true;
		}

		m_IsFalling = true;
	}
	else
	{
		//start falling frame
		if (m_IsFalling)
		{
			m_CanGlide = false;
		}
		m_IsFalling = false;
	}
}

void Hero::Update_Gliding(float deltaTime)
{

	//the character does not unlock gliding state
	if (!m_UnlockGlide)
	{
		return;
	}

	//Can glide checking is done in START and STOP FALLING FRAME (Update_Falling)

	//gliding
	if (m_IsFalling && m_CanGlide && m_InputArray[JUMP_KEY])
	{
		m_IsGliding = true;
		if (m_Rigidbody->m_velocity.y > HERO_GLIDE_SPEED)
		{
			m_Rigidbody->m_velocity.y -= HERO_GLIDE_SPEED * HERO_GLIDE_ACC_MUL * deltaTime;
			if (m_Rigidbody->m_velocity.y < HERO_GLIDE_SPEED)
			{
				m_Rigidbody->m_velocity.y = HERO_GLIDE_SPEED;
			}
		}
		else
		{
			m_Rigidbody->m_velocity.y = HERO_GLIDE_SPEED;
		}
	}
	else
	{
		m_IsGliding = false;
	}



	//cancel moving up
	if (m_InputArray[DOWN_KEY])
	{
		if (m_IsJumping)
		{
			m_IsJumping = false;
			m_JumpTime = HERO_JUMP_TIME;
		}

		if (m_Rigidbody->m_velocity.y < HERO_RUN_SPEED * 1.2f && !m_IsDashing && !m_IsGliding)
		{
			m_Rigidbody->m_velocity.y = HERO_RUN_SPEED * 1.2f;
		}
	}
}

void Hero::Update_KnockBack(float deltaTime) 
{
	//Knock back check and knocking back
	if (IsHurt())
	{
		m_IsBlinking = m_IsKnockBack = true;
		m_CanJump = m_CanDash = m_IsHurt = false;
		if (m_IsDashing)
		{
			m_DashTime = 0.0f;
		}
		else if (m_IsJumping) 
		{
			m_JumpTime = 0.0f;
		}
		
		float damage = m_BaseDamage;
		if (m_Damagex2) 
		{
			damage *= 2.0f;
			m_Damagex2 = false;
		}

		//damage the timeCount
		LevelManager::GetInstance()->DecreaseTime(damage);
		//set screen shake
		Camera::GetInstance()->CameraShake(1200.0f, 0.4f);
		//blinking time counter
		LevelManager::GetInstance()->BlinkTime();

		//play hurt audio
		AudioManager::GetInstance()->hitSfxPlayer.Play();
	}
	else if (m_IsKnockBack)
	{

		//apply knock back force
		m_Rigidbody->m_velocity = Vector2D(m_KnockBackForce * m_KnockBackDirection.x, m_JumpForce / 2.0f * m_KnockBackTime * m_KnockBackDirection.y);
		if (m_KnockBackDirection.x > 0.0f)
		{
			m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
		}
		else {
			m_Sprite->m_Flip = SDL_FLIP_NONE;
		}

		m_KnockBackTime -= deltaTime;
		if (m_KnockBackTime <= 0.0f) {
			m_KnockBackTime = HERO_KNOCK_BACK_TIME;
			m_IsKnockBack = false;
		}
	}
}

void Hero::Update_Blinking(float deltaTime) 
{
	//Blinking
	if (m_IsBlinking) 
	{
		m_Sprite->m_IsEnable = m_BlinkCount % 2 != 0;
		m_BlinkTime += deltaTime;

		if (m_BlinkTime >= HERO_BLINK_LENGTH) 
		{
			m_BlinkTime = 0.0f;
			m_BlinkCount++;
			if (m_BlinkCount == HERO_BLINK_MAX_COUNT) 
			{
				m_BlinkCount = 0;
				m_IsBlinking = false;
			}
		}
	}
}

void Hero::Update_MapCollision(float deltaTime) 
{
	
	//if the character move along the x-axis for the first time
	if (!m_IsFirstMove && m_Rigidbody->Position().x != 0)
	{
		//the character is moved for the first time
		m_IsFirstMove = true;
		//resume time counter
		LevelManager::GetInstance()->ResumeTime();
	}

	//reset collision direction
	m_CollisionDirections[0] = { false }; m_CollisionDirections[1] = { false }; m_CollisionDirections[2] = { false }; m_CollisionDirections[3] = { false };

	//move and collision checking on X-axis
	MoveX(m_Rigidbody->Position().x);

	if (m_DashDirectionForCollisionChecking.x != 0)
	{
		if (m_DashDirectionForCollisionChecking.x > 0)
		{
			if (m_Collider->CollideWithMap_Right() || m_Collider->CollideWithObject_Right())
			{
				CollideX(RIGHT);
			}
		}
		else
		{
			if (m_Collider->CollideWithMap_Left() || m_Collider->CollideWithObject_Left())
			{
				CollideX(LEFT);
			}
		}
		m_DashDirectionForCollisionChecking.x = 0;
	}
	else if (m_Collider->CollideWithMap_Right() || m_Collider->CollideWithObject_Right())
	{
		CollideX(RIGHT);
	}
	else if (m_Collider->CollideWithMap_Left() || m_Collider->CollideWithObject_Left())
	{
		CollideX(LEFT);
	}


	//move and collision checking on Y-axis
	MoveY(m_Rigidbody->Position().y);

	//if collide with map in BELOW direction
	if (m_Collider->CollideWithMap_Down() || m_Collider->CollideWithObject_Down())
	{
		CollideY(DOWN);

		if (m_Rigidbody->m_velocity.y > 0.0f)
		{
			m_Rigidbody->m_velocity.y = 0.0f;
		}

		//just touching ground
		if (!m_StartCollideDown) 
		{
			m_StartCollideDown = true;

			m_IsGrounded = true;
			m_CanJump = false;

		}

	}
	//if collide with map in UPPER direction
	else if (m_Collider->CollideWithMap_Up() || m_Collider->CollideWithObject_Up())
	{

		CollideY(UP);

		if (m_Rigidbody->m_velocity.y < 0.0f)
		{
			m_Rigidbody->m_velocity.y = 0.0f;
		}

		//just touching upper tile
		if (!m_StartCollideUp) 
		{
			m_StartCollideUp = true;

			//stop jumping frame
			if (m_IsJumping)
			{
				m_IsJumping = false;
				m_JumpTime = HERO_JUMP_TIME;

			}

		}

	}
	else
	{
		m_IsGrounded = false;

		m_StartCollideDown = false;
		m_StartCollideUp = false;
	}

	m_Collider->ResetObjectCollision();

}

void Hero::Update_RelativePosition(float deltatime)
{

	//if there is a colliding level object
	if (m_CollidingEntity != nullptr)
	{
		//get object colldier and hero collider
		SDL_Rect objCollider = m_CollidingEntity->GetComponent<Collider>()->Get();
		SDL_Rect heroCollider = m_Collider->Get();

		//checking condition to set the relative position
		//if hero is climbing
		if (m_IsClimbing)
		{

			//check whether the player is not going out of climbable area
			if (!(heroCollider.y >= objCollider.y + objCollider.h || heroCollider.y + heroCollider.h <= objCollider.y))
			{
				//if the input left is done and there is no collision on the left
				if (m_InputArray[LEFT_KEY] && !m_CollisionDirections[LEFT])
				{
						//the collision on the left is set to true
						m_CollisionDirections[LEFT] = true;
				}
				//if the input right is done and there is no collision on the right
				else if (m_InputArray[RIGHT_KEY] && !m_CollisionDirections[RIGHT])
				{
						//the collision on the right is set to true
						m_CollisionDirections[RIGHT] = true;
				}
			}

		}
		//if the collding entity is moving along x-axis and hero is standing on the platform
		else if (m_CollidingEntity->GetComponent<Rigidbody>()->Position().x != 0 && objCollider.y == heroCollider.y + heroCollider.h && objCollider.x < heroCollider.x + heroCollider.w && objCollider.x + objCollider.w > heroCollider.x)
		{
			//move hero accordingly to the collding entity velocity along x-axis
			MoveX(m_CollidingEntity->GetComponent<Rigidbody>()->Position().x);
		}
		//none of relative condition is done
		else
		{
			//prevent falling down and stick to the platform
			if (!m_IsClimbing && m_IsFalling)
			{
				//sticking when the platform is staying still
				if (m_CollisionDirections[LEFT])
				{
					MoveX((objCollider.x + objCollider.w) - heroCollider.x + 1);
				}
				else if (m_CollisionDirections[RIGHT])
				{
					MoveX(objCollider.x - (heroCollider.x + heroCollider.w) - 1);
				}
				//sticking when the platform is moving
				else if (heroCollider.x - (objCollider.x + objCollider.w) > - 1 && heroCollider.x - (objCollider.x + objCollider.w) < 3)
				{
					MoveX(HERO_RUN_SPEED * deltatime);
				}
				else if (objCollider.x - (heroCollider.x + heroCollider.w) > -1 && objCollider.x - (heroCollider.x + heroCollider.w) < 3)
				{
					MoveX(-HERO_RUN_SPEED * deltatime);
				}

			}


			//colliding frame is increased
			m_CollidingFrame++;

			//if the collding frame is out of range
			//set the colllding entity to nullptr
			if (m_CollidingFrame > HERO_MAX_COLLDING_FRAME)
			{
				m_CollidingFrame = 0;
				m_CollidingEntity = nullptr;
			}
		}
	}
}

void Hero::Update_Climbing(float deltaTime)
{
	if (!m_UnlockWallJump)
	{
		return;
	}

	//climbing
	//climbing input checking
	//direction input and collisision direction checking
	bool getDirectionInput = (m_CollisionDirections[LEFT]) || (m_CollisionDirections[RIGHT]);

	if (m_InputArray[CLIMB_KEY] && getDirectionInput && !m_IsKnockBack && !m_IsDashing && m_Rigidbody->m_velocity.y >= 0)
	{

		//set velocity Y
		float slippingSpeed = HERO_GLIDE_SPEED / 3.0f;
		//float slippingSpeed = 0.0f;
		m_Rigidbody->m_velocity.y = slippingSpeed;


		//enable climbing state
		m_IsClimbing = true;

	}
	else
	{
		//first frame that Hero Jump off the wall
		if (m_IsClimbing && !m_IsDashing)
		{
			//hero can jump and dash again
			m_CanJump = true;
			if (m_UnlockDash) 
			{
				m_CanDash = true;
			}
		}

		m_IsClimbing = false;
	}
}

void Hero::Update_AnimationState()
{
	
	//Knocking back
	if (m_IsKnockBack) 
	{
		m_Animation->SetProps("Hero-hurt", 2, 150);
	}
	//Dashing
	else if (m_IsDashing) 
	{
		m_Animation->SetProps("Hero-dash", 1, 100);
	}
	//climbing
	else if (m_IsClimbing)
	{
		m_Animation->SetProps("Hero-wall-jump", 1, 0);
	}
	//Falling
	else if (m_IsFalling) 
	{
		if (!m_CanDash && m_UnlockDash) 
		{
			m_Animation->SetProps("Hero-fall-grey", 4, 80);
		}
		else 
		{
			m_Animation->SetProps("Hero-fall-yellow", 4, 80);
		}
		
	}
	//Jumping
	else if (!m_IsGrounded) 
	{
		if (!m_CanDash && !m_IsJumping && m_UnlockDash) 
		{
			m_Animation->SetProps("Hero-jump-grey", 4, 80);
		}
		else 
		{
			m_Animation->SetProps("Hero-jump-idle", 4, 80);
		}
	}
	//Running
	else if (m_IsRunning) 
	{
		m_Animation->SetProps("Hero-walk", 4, 100);
	}
	//Idling
	else 
	{
		m_Animation->SetProps("Hero-idle", 4, 200);
	}

}

void Hero::Update_Boundary() 
{

	//checking boundary on X_AXIS
	if (m_Transform->position.x < -20.0f) 
	{
		m_Transform->position.x = -20.0f;
		m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
	}
	else if (m_Transform->position.x > m_Boundary.x - m_Collider->Get().w - 20.0f) 
	{
		m_Transform->position.x = m_Boundary.x - m_Collider->Get().w - 20.0f;
		m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
	}

}



void Hero::Spawn_DashParticle() 
{

	float destroyTime = 0.06f;

	//SPAWN DASH PARTICLE!!!!!!
	if (m_Sprite->m_Flip == SDL_FLIP_NONE) {
		ParticleManager::GetInstance()->Spawn("hero_dash", m_Transform->position, destroyTime);
	}
	else if (m_Sprite->m_Flip == SDL_FLIP_HORIZONTAL) {
		ParticleManager::GetInstance()->Spawn("hero_dash_left", m_Transform->position, destroyTime);
	}
}


void Hero::Spawn_DeadParticle() 
{
	//SPAWN DEAD PARTICLE!!!!!!
	ParticleManager::GetInstance()->Spawn("hero_explosion", m_Transform->position);

	//play dead audio
	AudioManager::GetInstance()->deadSfxPlayer.Play();
}


void Hero::Spawn_WarpParticle()
{
	//vertically
	if (m_WarpDirection == DOWN || m_WarpDirection == UP) 
	{
		ParticleManager::GetInstance()->Spawn("hero_warp", Vector2D(m_Transform->position.x - 30, m_Transform->position.y - 60));
	}
	//horizontally
	else 
	{
		ParticleManager::GetInstance()->Spawn("hero_warp_x", Vector2D(m_Transform->position.x - 60, m_Transform->position.y));
	}
}


void Hero::Spawn_RuneParticle()
{
	//SPAWN RUNE PARTICLE!!!!!!
	ParticleManager::GetInstance()->Spawn("hero_getting_rune", Vector2D(m_Transform->position.x - 35, m_Transform->position.y - 200));
}