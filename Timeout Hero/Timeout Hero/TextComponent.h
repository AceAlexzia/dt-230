#pragma once
#ifndef TEXT
#define TEXT

#include "Component.h"
#include "SDL.h"
#include "FontManager.h"
#include "TransformComponent.h"
#include "Constants.h"

class Text: public Component
{
private :
	std::string m_TextID = "";
	Transform* m_Transform = nullptr;
	int m_Width = 0;
	int m_Height = 0;

public :

	std::string font = "";
	int size = 0;
	SDL_Color color = { 0, 0, 0 };
	std::string text = "";
	int outlineSize = 0;;
	SDL_Color outlineColor = { 0, 0, 0 };

	bool m_IsEnable = true;
	LayerType m_Layer;

	Text(LayerType layer, std::string textID = "", std::string fontName = "", int size = 0, SDL_Color color = { 0, 0, 0 }, std::string text = "");

	void Initialize() override;
	void Update(float deltatime) override;
	void Render();

	int GetWidth();
	int GetHeight();

};

#endif // !TEXT
