#ifndef COLLIDERCOMPONENT_H
#define COLLIDERCOMPONENT_H

#include <SDL.h>
#include "Game.h"
#include "Vector2D.h"
#include "Camera.h"
class Collider : public Component
{
protected :

    SDL_Rect m_Buffer;
    SDL_Rect m_Collider;

public:

    bool m_IsEnable = true;

    Collider();
    Collider(int x, int y, int w, int h);

    void Initialize() override {}
    void Update(float deltaTime) {}
    void Render();

    SDL_Rect Get();
    void SetBuffer(int x, int y, int w, int h);
    void Set(int x, int y, int w, int h);
};
#endif