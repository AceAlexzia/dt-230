#include "EntityManager.h"
#include "PlayerColliderComponent.h"
#include "SpriteComponent.h"
#include "TextComponent.h"
#include "PauseManager.h"
#include "TransitionManager.h"
#include "UIManager.h"

EntityManager* EntityManager::s_Instance = nullptr;

EntityManager::EntityManager() {}

EntityManager* EntityManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new EntityManager();
}

void EntityManager::ClearData()
{
    for (auto& entity: entities) 
    {
        entity->Destroy();
    }
}

EntityManager::~EntityManager()
{
    for (int i = 0; i < entities.size(); i++)
    {
        delete entities[i];
    }

    entities.clear();
}
void EntityManager::DestroyInActiveEntities()
{
    for (int i = 0; i < entities.size(); i++)
    {
        if (!entities[i]->IsActive())
        {
            //delete entity ans sets the memory allocation to nullptr
            Entity* deletedEntity = entities[i];;
            delete deletedEntity;
            deletedEntity = nullptr;
            //erase from the entity vector
            entities.erase(entities.begin() + i);
            i--;
        }
    }
}

void EntityManager::Update(float deltaTime) 
{
    //is the game pausing
    bool isPause = PauseManager::GetInstance()->GetIsPause();

    //if it's NOT pausing
    if (!isPause) 
    {
        //update every entity properly
        for (int i = 0; i < entities.size(); i++)
        {
            if (entities[i] != nullptr)
            {
                entities[i]->Update(deltaTime);
            }
        }
        TransitionManager::GetInstance()->Update(deltaTime);
    }
    //if it's pausing
    else
    {
        //update only certain entity when pause
        //loop through all entities
        for (auto& entity : entities)
        {
            //if the entity has a certain component -> update it properly
            if (entity->HasComponent<TransitionBox>())
            {
                entity->Update(deltaTime);
            }
            //UI Part Entities Update
            else if (entity->HasComponent<Button>() || entity->HasComponent<CheckBox>() || entity->HasComponent<TextButton>() || entity->HasComponent<Adjuster>() || entity->HasComponent<AdjustArrow>() || (entity->HasComponent<Text>() && !entity->HasComponent<TimeCounter>()))
            {
                entity->Update(deltaTime);
            }
        }
    }
    
}

void EntityManager::Render() 
{
    
    std::vector<Entity*> vect = entities;
    for (int i = 0; i < vect.size(); i++)
    {
        //if it has no sprite component
        if (!vect[i]->HasComponent<Sprite>() && !vect[i]->HasComponent<Text>())
        {
            //erase it from vector
            vect.erase(vect.begin() + i);
            i--;
        }
    }

    //rendering sprite
    //loop through all existing layers
    for (int layernumber = 0; layernumber < LAYER_COUNT; layernumber++) 
    {
        //loop through all entities temporary vector
         for (int i = 0; i < vect.size(); i++)
         {
            //if it has Sprite component
            //and layer checking
            if (vect[i]->HasComponent<Sprite>() && vect[i]->GetComponent<Sprite>()->m_Layer == layernumber)
            {
                //render the sprite
                vect[i]->GetComponent<Sprite>()->Render();
                //remove the entity from the temporary vector
                vect.erase(vect.begin() + i);
                i--;
            }
            //if it has Text component
            //and layer checking
            else if (vect[i]->HasComponent<Text>() && vect[i]->GetComponent<Text>()->m_Layer == layernumber)
            {
                //render the sprite
                vect[i]->GetComponent<Text>()->Render();
                //remove the entity from the temporary vector
                vect.erase(vect.begin() + i);
                i--;
            }

         }

         if (layernumber == TRANSITION_LAYER) 
         {
             TransitionManager::GetInstance()->Render();
         }

    }

    //ONLY FOR COLLISION TESTING
    //rendering collider
    /*for (auto& entity : entities)
    {
        if (entity->HasComponent<Collider>())
        {
            entity->GetComponent<Collider>()->Render();
        }
        else if (entity->HasComponent<PlayerCollider>()) 
        {
            entity->GetComponent<PlayerCollider>()->Render();
        }
    }*/
}

bool EntityManager::HasNoEntities() const 
{
    return entities.size() == 0;
}

unsigned int EntityManager::GetEntityCount() const 
{
    return entities.size();
}

std::vector<Entity*> EntityManager::GetEntities() const 
{
    return entities;
}

void EntityManager::ListAllEntities() const 
{
    unsigned int i = 0;
    for (auto& entity: entities) 
    {
        std::cout << "Entity[" << i << "]: " << entity->name << std::endl;
        entity->ListAllComponents();
        i++;
    }
}


Entity& EntityManager::AddEntity(std::string entityName) 
{
    Entity *entity = new Entity(*this, entityName);
    entities.emplace_back(entity);
    return *entity;
}
