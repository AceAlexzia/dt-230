#pragma once
#ifndef HERO
#define HERO

//====SERIALIZE VALUE====
#define HERO_JUMP_TIME 0.5f
#define HERO_JUMP_FORCE 1800.0f

#define HERO_RUN_ACC_MUL 3.0f 
#define HERO_RUN_SPEED 320.0f

#define HERO_DASH_FORCE 1000.0f
#define HERO_DASH_TIME 0.2f

#define HERO_KNOCK_BACK_FORCE 300.0f
#define HERO_KNOCK_BACK_TIME 0.5f

#define HERO_BLINK_LENGTH 0.15f
#define HERO_BLINK_MAX_COUNT 8

#define HERO_GLIDE_SPEED 100.0f
#define HERO_GRAVITY 4000.0f //used to be 4500
#define HERO_GLIDE_ACC_MUL 160.0f

#define HERO_DEAD_TIME 0.8f
#define HERO_RUNE_TIME 1.0f
#define HERO_WARP_TIME 0.7f

#define HERO_CONTROLLER_DEAD_ZONE_DASH 0.32f

#define HERO_MAX_COLLDING_FRAME 1

//=======================
#include "RigidbodyComponent.h"
#include "TransformComponent.h"
#include "Component.h"
#include "PlayerColliderComponent.h"
#include "SpriteAnimationComponent.h"
#include "InputManager.h"
#include "Timer.h"
#include "SaveManager.h"

enum InputKey
{
	UP_KEY,
	LEFT_KEY,
	DOWN_KEY,
	RIGHT_KEY,
	FORCE_DEAD_KEY,
	JUMP_KEY,
	CLIMB_KEY,
	DASH_KEY,
	INPUTKEYS
};


class Hero : public Component
{
public:
	bool m_IsHurt = false;
	bool m_Damagex2 = false;
	bool m_IsDashThrough = false;
	bool m_IsEvent = false;
	bool m_IsStop = false;

	Entity* m_CollidingEntity = nullptr;

	Hero(int boundX, int boundY) { m_Boundary = Vector2D((float)boundX, (float)boundY); }
	~Hero() {}

	void Initialize() override;
	void Update(float deltaTime) override;

	//getter
	bool Get_IsDashing();
	bool Get_IsBlinking();
	bool Get_IsKnockBack();

	//setter
	void Dead();
	void ReachRune();
	void Warp(Direction dir);
	void UnlockAbility(bool jump, bool dash, bool wallJump, bool forceDead, bool glide);

	//spawn a particle when hero is dead
	void Spawn_DeadParticle();


private:
	//hero state
	bool m_IsRunning;
	bool m_IsJumping;
	bool m_IsFalling;
	bool m_IsGrounded;
	bool m_IsDashing;
	bool m_IsKnockBack;
	bool m_IsBlinking;
	bool m_IsGliding;
	bool m_IsDead;
	bool m_IsGetRune;
	bool m_IsWarp;
	bool m_IsClimbing;
	//controlling time counter
	bool m_IsFirstMove;

	//enable/disable entering state
	bool m_CanJump;
	bool m_CanDash;
	bool m_CanGlide;

	float m_JumpTime;
	float m_JumpForce;
	float m_DashForce;
	float m_DashTime;
	float m_KnockBackForce;
	float m_KnockBackTime;

	float m_BlinkTime;
	int m_BlinkCount;

	float m_DeadTime;
	float m_RuneTime;
	float m_WarpTime;

	bool m_StartCollideUp;
	bool m_StartCollideDown;

	Vector2D m_DashDirection;
	Vector2D m_DashDirectionForCollisionChecking;
	Vector2D m_KnockBackDirection;
	Vector2D m_Boundary;

	bool m_CollisionDirections[4] = { false, false, false, false };

	int m_BaseDamage = 5;

	//main input
	SDL_Scancode m_KeyboardInputs[INPUTKEYS];
	SDL_GameControllerButton m_ControllerInputs[INPUTKEYS];
	//alt input
	//SDL_Scancode m_AltKeyboardInputs[INPUTKEYS];
	SDL_GameControllerButton m_AltControllerInputs[INPUTKEYS];
	//last moving directional input
	Direction m_LastMovingDirectionX = RIGHT;
	Direction m_LastMovingDirectionY = DOWN;

	//warping direction
	Direction m_WarpDirection = RIGHT;

	//is unlock ability
	bool m_UnlockJump = false;
	bool m_UnlockDash = false;
	bool m_UnlockWallJump = false;
	bool m_UnlockGlide = false;
	bool m_UnlockForceDead = false;

	//relative position checking for moving tile collision
	int m_CollidingFrame = 0;

	//get input boolean
	bool m_InputArray[INPUTKEYS];

	//private function
	void Reset();
	bool IsHurt();
	void SetBaseDamage();
	//use when hero collides X
	void CollideX(Direction dir);
	//use when hero collides Y
	void CollideY(Direction dir);
	//Move Hero along x-axis
	void MoveX(float x);
	//Move Hero along y-axis
	void MoveY(float y);
	//control scheme assigning
	void AssignInput();
	//checking the moving direction according to the last hero's input
	void CheckMovingInput(bool& left, bool& right, Direction& lastDirection);

	//private function (update time)
	//update input boolean
	void Update_Input();
	//update function that can immediately skip all states
	bool Update_Dead(float deltaTime);
	bool Update_GettingRune(float deltaTime);
	bool Update_DeadCondition();
	bool Update_IsWarp(float deltatime);
	//Update Hero state while not entering special condition
	void Update_IsDashThrough();
	void Update_Running(float deltaTime);
	void Update_Dashing(float deltaTime);
	void Update_Jumping(float deltaTime);
	void Update_Falling();
	void Update_Gliding(float deltaTime);
	void Update_KnockBack(float deltaTime);
	void Update_Blinking(float deltaTime);
	void Update_MapCollision(float deltaTime);
	void Update_RelativePosition(float deltatime);
	void Update_Climbing(float deltaTime);
	void Update_AnimationState();
	void Update_Boundary();

	//particle spawning
	//spawn particles while dashing
	void Spawn_DashParticle();
	//spawn particles when hero warps to the next section
	void Spawn_WarpParticle();
	//spawn particles when hero gets the rune
	void Spawn_RuneParticle();


	//Hero's Component
	Rigidbody* m_Rigidbody;
	Transform* m_Transform;
	Sprite* m_Sprite;
	SpriteAnimation* m_Animation;
	PlayerCollider* m_Collider;
};

#endif //HERO