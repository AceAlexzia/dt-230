#include "SaveManager.h"


SaveManager* SaveManager::s_Instance = nullptr;


SaveManager* SaveManager::GetInstance()
{
    return s_Instance = (s_Instance != nullptr) ? s_Instance : new SaveManager();
}

void SaveManager::Initialize()
{
    for (int i = 0; i < 3; i++)
    {
        std::ifstream readfile(savefile[i]);
        if (!readfile)
        {
            hasSave[i] = false;
        }
        else
        {
            hasSave[i] = true;
        }
        LoadSave(i);
    }
    //SetSave(0, 1, 1, "Hard", 50);
}

void SaveManager::LoadSave(int pos)
{
    Readfile(pos,savefile[pos]);
}

void SaveManager::LoadLevel()
{
    std::ifstream readfile(savefile[currentSlotPos]);
    std::string line;
    std::string play, dead, m, tTime;
    if (!readfile)
    {
        //std::cout << "Failed to read: " << filename << std::endl;
    }
    else
    {
        while (!readfile.eof())
        {
            std::getline(readfile, line);
            std::istringstream iss(line);
            std::getline(iss, play, ',');
            std::getline(iss, dead, ',');
            std::getline(iss, m, ',');
            std::getline(iss, tTime, ',');
            save[currentSlotPos].death = std::stod(play);
        }
        readfile.close();
    }
}

void SaveManager::LoadTime()
{
    std::ifstream readfile(savefile[currentSlotPos]);
    std::string line;
    std::string play, dead, m, tTime;
    if (!readfile)
    {
        //std::cout << "Failed to read: " << filename << std::endl;
    }
    else
    {
        while (!readfile.eof())
        {
            std::getline(readfile, line);
            std::istringstream iss(line);
            std::getline(iss, play, ',');
            std::getline(iss, dead, ',');
            std::getline(iss, m, ',');
            std::getline(iss, tTime, ',');
            save[currentSlotPos].totalTime = std::stod(tTime);
        }
        readfile.close();
    }
}

void SaveManager::LoadDead()
{
    std::ifstream readfile(savefile[currentSlotPos]);
    std::string line;
    std::string play, dead, m, tTime;
    if (!readfile)
    {
        //std::cout << "Failed to read: " << filename << std::endl;
    }
    else
    {
        while (!readfile.eof())
        {
            std::getline(readfile, line);
            std::istringstream iss(line);
            std::getline(iss, play, ',');
            std::getline(iss, dead, ',');
            std::getline(iss, m, ',');
            std::getline(iss, tTime, ',');
            save[currentSlotPos].death = std::stod(dead);
        }
        readfile.close();
    }
}

void SaveManager::Readfile(int pos, std::string filename)
{
    std::ifstream readfile(filename);
    std::string line;
    std::string play, dead, m, tTime;
    if (!readfile)
    {
        //std::cout << "Failed to read: " << filename << std::endl;
    }
    else
    {
        while (!readfile.eof())
        {
            std::getline(readfile, line);
            std::istringstream iss(line);
            std::getline(iss, play, ',');
            std::getline(iss, dead, ',');
            std::getline(iss, m, ',');
            std::getline(iss, tTime, ',');
            save[pos].playStage = std::stod(play);
            save[pos].death = std::stod(dead);
            save[pos].mode = m;
            save[pos].totalTime = std::stod(tTime);
        }
        readfile.close();
        /*std::cout << "Save[" << pos << "]" << std::endl;
        std::cout << "\t" << "Total Stage:" << save[pos].playStage << std::endl;
        std::cout << "\t" << "Total Death:" << save[pos].death << std::endl;
        std::cout << "\t" << "Mode:" << save[pos].mode << std::endl;
        std::cout << "\t" << "Total Time:" << save[pos].totalTime << std::endl;*/
    }
}

void SaveManager::Writefile(int pos, std::string filename, int play, int dead, std::string m, float tTotal)
{
    std::ofstream writefile(filename);
    
    writefile << play << "," << dead << "," << m << "," << tTotal;
    writefile.close();
}

void SaveManager::SetSave(int pos, int play, int dead, std::string m, float tTotal)
{
    Writefile(pos, savefile[pos], play, dead, m, tTotal);
    //Readfile(pos, savefile[pos]);
}

void SaveManager::SetLevel(int index)
{
    LoadLevel();
    std::ofstream writefile(savefile[currentSlotPos]);
    writefile << index << "," << save[currentSlotPos].death << "," << save[currentSlotPos].mode << "," << save[currentSlotPos].totalTime;
    writefile.close();
    LoadSave(currentSlotPos);
}

void SaveManager::SetDeath()
{
    LoadDead();
    std::ofstream writefile(savefile[currentSlotPos]);
    writefile << save[currentSlotPos].playStage << "," << save[currentSlotPos].death + 1 << "," << save[currentSlotPos].mode << "," << save[currentSlotPos].totalTime;
    writefile.close();
    LoadSave(currentSlotPos);
}

void SaveManager::SetTime(float t)
{
    std::ofstream writefile(savefile[currentSlotPos]);
    writefile << save[currentSlotPos].playStage << "," << save[currentSlotPos].death << "," << save[currentSlotPos].mode << "," << t;
    writefile.close();
    LoadTime();
}

void SaveManager::SetOption(int width, int height, bool full, int vol1, int vol2, int vol3, int shake, bool skip)
{
    std::ofstream writefile(optionFile);
    writefile << width << "," << height << ",";
    if (full)
    {
        writefile << 1 << ",";
    }
    else
    {
        writefile << 0 << ",";
    }
    writefile << vol1 << "," << vol2 << "," << vol3 << "," << shake << ",";
    if (skip)
    {
        writefile << 1;
    }
    else
    {
        writefile << 0;
    }
    writefile.close();
}

void SaveManager::LoadOption()
{
    std::ifstream readfile(optionFile);
    std::string line;
    std::string width, height, full, vol1, vol2, vol3, shake, run;
    if (!readfile)
    {
        //std::cout << "Failed to read: " << optionFile << std::endl;
        SetOption(1920,1080,false,10,10,10,10,false);
    }
    else
    {
        while (!readfile.eof())
        {
            std::getline(readfile, line);
            std::istringstream iss(line);
            std::getline(iss, width, ',');
            std::getline(iss, height, ',');
            std::getline(iss, full, ',');
            std::getline(iss, vol1, ',');
            std::getline(iss, vol2, ',');
            std::getline(iss, vol3, ',');
            std::getline(iss, shake, ',');
            std::getline(iss, run, ',');
            opt.resolution_width = std::stod(width);
            opt.resolution_height = std::stod(height);
            if (std::stod(full) == 0)
            {
                opt.full = false;
            }
            else
            {
                opt.full = true;
            }
            opt.master = std::stod(vol1);
            opt.sfx = std::stod(vol2);
            opt.music = std::stod(vol3);
            opt.screenshake = std::stod(shake);
            if (std::stod(run) == 0)
            {
                opt.skipDialogue = false;
            }
            else
            {
                opt.skipDialogue = true;
            }
        }
        readfile.close();
    }

    // Apply Setting
    SDL_SetWindowSize(Game::GetInstance()->GetWindow(), opt.resolution_width, opt.resolution_height);

    if (opt.full )
    {
        SDL_SetWindowFullscreen(Game::GetInstance()->GetWindow(), SDL_WINDOW_FULLSCREEN);
    }
    else if(!opt.full )
    {
        SDL_SetWindowFullscreen(Game::GetInstance()->GetWindow(), SDL_WINDOW_SHOWN);
    }
    SDL_SetWindowPosition(Game::GetInstance()->GetWindow(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);


    //--------- set audio property -------
    AudioManager::GetInstance()->SetVol(opt.master, opt.music, opt.sfx);
}