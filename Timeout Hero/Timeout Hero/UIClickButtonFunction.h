#pragma once
#ifndef UICLICKBUTTONFUNCTION_H
#define UICLICKBUTTONFUNCTION_H
#include "SceneManager.h"
#include "UIManager.h"
#include "Game.h"
#include "PauseManager.h"
#include "TransitionManager.h"

namespace ClickButton
{
	static void ClickApplyButton()
	{
		UIManager::GetInstance()->PressApplyButton();
	}
	static void ClickPlayButton()
	{
		TransitionManager* trans = TransitionManager::GetInstance();
		if (!trans->IsBoxTransition())
		{
			trans->BoxTransition(TRANSITION_FUNCTION_START_GAME);
		}
	}
	static void ClickSplash()
	{
		SceneManager::GetInstance()->LoadScene(SPLASH_SCENE);
	}
	static void ClickEmpty(int i)
	{
		UIManager::GetInstance()->PressEmpty(i);
	}
	static void CreateSave(int i)
	{
		UIManager::GetInstance()->CreatSlot(i);
	}
	static void ClickMenuButton()
	{
		PauseManager::GetInstance()->Resume();
		UIManager::GetInstance()->GoToMainScene();
	}
	static void PressMainMenu()
	{
		UIManager::GetInstance()->PressMainMenu();
	}
	static void PressOptionManMenu()
	{
		UIManager::GetInstance()->PressOption();
	}
	static void ExitGame()
	{
		Game::GetInstance()->Quit();
	}
	static void PressResume()
	{
		PauseManager::GetInstance()->Resume();
		UIManager::GetInstance()->checkPause = false;
	}
	static void CancelSaveSlot(int i)
	{
		UIManager::GetInstance()->CancelSaveSlot(i);
	}
	static void CreateAllSaveSlot()
	{
		UIManager::GetInstance()->SpawnSaveSlotUI();
	}
	static void DeleteEachSlot(int i)
	{
		UIManager::GetInstance()->DeleteSlot(i);
	}
	static void PressUndoPauseOption()
	{
		UIManager::GetInstance()->PressUndoPauseOption();
	}
	static void PressOptionPause()
	{
		UIManager::GetInstance()->PressOptionPause();
	}
}


#endif // !UICLICKBUTTON_H
