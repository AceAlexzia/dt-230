#pragma once
#ifndef SECTION_CHANGER
#define SECTION_CHANGER

#include "LevelObject.h"

#define SC_LENGTH 4
#define SC_IMAGE_OFFSET 24

class SectionChanger : public LevelObject
{

private:

	int m_IndexChanging = 0;

	Direction m_Direction = RIGHT;

	Vector2D m_ImagePos;

	void Initialize_SC(int tileSize, int colCount, int rowCount);
	void Initialize_Image(int tileSize, int colCount, int rowCount);

public:

	SectionChanger(int indexChanging, Direction dir, Vector2D imagePos);
	~SectionChanger() {}

	void Initialize() override;
	void Update(float deltaTime) override;

};

#endif // !SECTION_CHANGER

