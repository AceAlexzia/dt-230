#include "CheckBox.h"

CheckBox::CheckBox(int x, int y, bool check, bool selecting)
{
	checkBoxPos.x = x;
	checkBoxPos.y = y;
	isCheck = check;
	if (selecting)
	{
		isSelecting = selecting;
	}
}

void CheckBox::Initialize()
{
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(checkBoxPos.x, checkBoxPos.y);
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("CheckBox", 40, 40, TIME_COUNT_LAYER);
	}
	isSet = true;
}

void CheckBox::Update(float deltaTime)
{
	if (isSelecting && isCheck)
	{
		owner->GetComponent<Sprite>()->SetTexture("CheckBox4");
	}
	else if (!isSelecting && isCheck)
	{
		owner->GetComponent<Sprite>()->SetTexture("CheckBox2");
	}
	else if (!isSelecting && !isCheck)
	{
		owner->GetComponent<Sprite>()->SetTexture("CheckBox");
	}
	else if (isSelecting && !isCheck)
	{
		owner->GetComponent<Sprite>()->SetTexture("CheckBox3");
	}
	/*if (isSet)
	{
		
		isSet = false;
	}*/
}

void CheckBox::OnClick()
{
	if (!isCheck)
	{
		isCheck = true;
	}
	else
	{
		isCheck = false;
	}
	ChangeValue();

}

void CheckBox::SetFunction(void(*fn)())
{
	m_FnPtr = fn;
}

void CheckBox::ChangeValue()
{
	if (owner->name == "FullscreenCheckBox")
	{
		SaveManager::GetInstance()->opt.full = isCheck;
	}
	else if (owner->name == "speedRunClockCheckBox")
	{
		SaveManager::GetInstance()->opt.skipDialogue = isCheck;
	}
}

Entity* CheckBox::SelectUI(int direction)
{
	return nextUI[direction];;
}
