#pragma once
#ifndef BUTTON_H
#define BUTTON_H
#include <string>
#include "UIComponent.h"
#include "TransformComponent.h"
#include "SpriteComponent.h"
#include "Vector2D.h"
#include "Entity.h"
#include "TextComponent.h"

class Button: public UIComponent
{
	public:
		Button(int x, int y, bool canClick, std::string text, std::string startingTexture, std::string selectingTexture, int textSize ,bool selecting = false);
		void Initialize() override;
		void Update(float deltaTime) override;
		void SetFunction(void (*fn)());
		void OnClick();

		Entity* SelectUI(int direction);
		Entity* textButton;
		bool available;
		bool isSet = false;
	private:
		void (*m_FnPtr)() = nullptr;
		Vector2D buttonPos;
		SDL_Color textColor = { 255,255,255,255 };
		std::string buttonText;
		std::string selectingTexture;
		std::string startingTexture;
		int sizeText = 0;
		bool firstSet = true;
		Vector2D getPos;
};

#endif // !BUTTONCOMPONENT_H