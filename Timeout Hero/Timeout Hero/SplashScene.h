#pragma once
#ifndef SPLASHSCENE_H
#define SPLASHSCENE_H
#include "Scene.h"
#include "UIManager.h"
#include "Credit.h"
class SplashScene: public Scene
{
	public:
		SplashScene();
		~SplashScene();
		void Start();
	private:
		Entity* cd;
};

#endif // !SPLASHSCENE_H