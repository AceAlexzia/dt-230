#include "Adjuster.h"

Adjuster::Adjuster(int x, int y, std::vector<std::string> a,int now, bool selecting)
{
	canvasPos.x = x;
	canvasPos.y = y;
	if (selecting)
	{
		isSelecting = true;
	}
	for (int i = 0; i < a.size(); i++/*std::vector<std::string>::iterator it = a.begin(); it != a.end(); it++*/)
	{
		setIndex.push_back(a[i]);
	}
	currentIndex = now;
}

void Adjuster::Initialize()
{
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(canvasPos.x, canvasPos.y);
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("AdjusterBox", 172, 40, TIME_COUNT_LAYER);
	}

	Entity& text(EntityManager::GetInstance()->AddEntity("AdjustArrowText" + owner->name));
	text.AddComponent<Transform>(owner->GetComponent<Transform>()->position.x, owner->GetComponent<Transform>()->position.y);
	text.AddComponent<Text>(TIME_COUNT_LAYER, "AdjusterText" + owner->name, "DisposableDroidBB", 30, color, setIndex[0]);

	adjusterText = &text;

	Entity& left(EntityManager::GetInstance()->AddEntity("LeftArrow" + owner->name));
	left.AddComponent<Transform>(100, 100);
	left.AddComponent<Sprite>("AdjusterArrow", 32, 32, TIME_COUNT_LAYER);
	left.AddComponent<AdjustArrow>(canvasPos.x - left.GetComponent<Sprite>()->GetWidth() -10 - getPos.x, canvasPos.y + 3 - getPos.y, owner);



	Entity& right(EntityManager::GetInstance()->AddEntity("RightArrow" + owner->name));
	right.AddComponent<Transform>(100, 100);
	right.AddComponent<Sprite>("AdjusterArrow", 32, 32, TIME_COUNT_LAYER, SDL_FLIP_HORIZONTAL);
	right.AddComponent<AdjustArrow>(canvasPos.x + owner->GetComponent<Sprite>()->GetWidth() + 10 - getPos.x, canvasPos.y + 3 - getPos.y, owner);

	leftButton = &left;
	rightButton = &right;

	nextUI[LEFT] = leftButton;
	nextUI[RIGHT] = rightButton;
	isSet = true;
	getPos = Camera::GetInstance()->GetPosition();
}

void Adjuster::Update(float deltaTime)
{
	//if (isSet)
	//{
		adjusterText->GetComponent<Transform>()->position.x = owner->GetComponent<Sprite>()->GetWidth() / 2 - (adjusterText->GetComponent<Text>()->GetWidth() / 2) + owner->GetComponent<Transform>()->position.x - getPos.x;
		adjusterText->GetComponent<Transform>()->position.y = owner->GetComponent<Sprite>()->GetHeight() / 2 - (adjusterText->GetComponent<Text>()->GetHeight() / 2) + owner->GetComponent<Transform>()->position.y - getPos.y;
		adjusterText->GetComponent<Text>()->text = setIndex[currentIndex];
		//isSet = false;
	//}
}
void Adjuster::AdjustIndex()
{
	if (leftButton->GetComponent<AdjustArrow>()->isPressed)
	{
		currentIndex--;
		if (currentIndex < 0)
		{
			currentIndex = setIndex.size() - 1;
		}
		leftButton->GetComponent<AdjustArrow>()->isPressed = false;
	}
	else if (rightButton->GetComponent<AdjustArrow>()->isPressed)
	{
		currentIndex++;
		
		if (currentIndex > setIndex.size() - 1)
		{
			currentIndex = 0;
		}
		rightButton->GetComponent<AdjustArrow>()->isPressed = false;
	}
	SaveValue();
}
void Adjuster::SaveValue()
{
	if (owner->name == "ResolutionAdjuster")
	{
		std::string wid, hei;
		std::stringstream word(setIndex[currentIndex]);
		std::getline(word, wid, 'x');
		std::getline(word, hei);
		SaveManager::GetInstance()->opt.resolution_width = std::stod(wid);
		SaveManager::GetInstance()->opt.resolution_height = std::stod(hei);
	}
	else if (owner->name == "MasterAdjuster")
	{
		SaveManager::GetInstance()->opt.master = currentIndex;
	}
	else if (owner->name == "SFXAdjuster")
	{
		SaveManager::GetInstance()->opt.sfx = currentIndex;
	}
	else if (owner->name == "MusicAdjuster")
	{
		SaveManager::GetInstance()->opt.music = currentIndex;
	}
	else if (owner->name == "screenShakeAdjuster")
	{
		SaveManager::GetInstance()->opt.screenshake = currentIndex;
	}
}
Entity* Adjuster::SelectUI(int direction)
{
	return nextUI[direction];
}
