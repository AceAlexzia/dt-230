#include "ArchMonster.h"

ArchMonster::ArchMonster(float destroyTime, Vector2D movingDirection)
{
	m_DestroyTime = destroyTime;
	m_MovingDirection = movingDirection;
}


void ArchMonster::Initialize()
{
	m_Collider = owner->GetComponent<Collider>();
	m_Transform = owner->GetComponent<Transform>();
	m_Sprite = owner->GetComponent<Sprite>();
	m_Anim = owner->GetComponent<SpriteAnimation>();
	m_Rb = owner->GetComponent<Rigidbody>();

	m_Rb->setGravity(0.0f);
	m_Anim->SetProps("arch-monster-idle", 4, 100, true);
	m_Collider->SetBuffer(-26, -34, 52, 52);
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());

	if (m_MovingDirection.x > 0.0f) 
	{
		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
	}
}

void ArchMonster::Update(float deltaTime)
{
	//if the princess collider is not assigned
	if (m_PrincessCollider.w == 0 && m_PrincessCollider.h == 0) 
	{
		//increase arch monster lifespawn in every frame 
		m_Lifespan += deltaTime;
	}
	

	//when the life span is end
	if (m_Lifespan >= m_DestroyTime) 
	{
		//monster is not alive
		m_IsAlive = false;
		m_Sprite->m_IsEnable = false;

		//spawn a particle
		ParticleManager::GetInstance()->Spawn("archmonster_disppear", Vector2D(m_Transform->position.x - 15, m_Transform->position.y - 18));

		Camera::GetInstance()->CameraShake(900.0f, 0.3f);

		//destroy the entity
		owner->Destroy();

		return;
	}

	//updating its velocity
	m_Rb->m_velocity = m_MovingDirection * ARCH_MONSTER_SPEED;

	//move entity and its collider
	m_Transform->Translate(m_Rb->Position());
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());

	if (CollisionTriggerred() && !m_Player->Get_IsBlinking())
	{
		m_Lifespan = m_DestroyTime;
	}

	Update_Collision(true);
	Update_PrincessCollision();

}


void ArchMonster::SetPrincessCollider(SDL_Rect princessCollider) 
{
	m_PrincessCollider = princessCollider;
}


void ArchMonster::Update_PrincessCollision() 
{
	if (CollisionHandler::GetInstance()->CheckCollision(m_PrincessCollider, m_Collider->Get(), m_Collider->m_IsEnable) && !m_Player->Get_IsBlinking())
	{

		//spawn a particle
		ParticleManager::GetInstance()->Spawn("archmonster_disppear", Vector2D(m_Transform->position.x - 20, m_Transform->position.y - 10));

		m_Player->Dead();

		m_MovingDirection = Vector2D(0.0f, 0.0f);

		m_Collider->m_IsEnable = false;
	}
}
