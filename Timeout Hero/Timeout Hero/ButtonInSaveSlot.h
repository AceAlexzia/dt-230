#pragma once
#ifndef BUTTONINSAVESLOT_H
#define BUTTONINSAVESLOT_H
#include "Vector2D.h"
#include "TransformComponent.h"
#include "SpriteComponent.h"
#include "UIComponent.h"
class ButtonInSaveSlot : public UIComponent
{
	public:
		ButtonInSaveSlot(int x, int y, bool canClick, std::string startingTexture, std::string selectingTexture, int pos, bool selecting = false);
		void Initialize() override;
		void Update(float deltaTime) override;
		void SetFunction(void (*fn)(int i));
		void OnClick(int i);

		Entity* SelectUI(int direction);
		Entity* textButton;
		bool available;
		bool isSet;
		int ID;
	private:
		void (*m_FnPtr)(int) = nullptr;
		Vector2D buttonPos;
		std::string buttonText;
		std::string selectingTexture;
		std::string startingTexture;
};

#endif // !BUTTONINSAVESLOT_H