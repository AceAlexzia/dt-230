#include "Particle.h"

void Particle::Initialize()
{
    m_Transform = owner->GetComponent<Transform>();
    m_Sprite = owner->GetComponent<Sprite>();
    m_Anim = owner->GetComponent<SpriteAnimation>();
}

void Particle::Update(float deltaTime)
{
    //m_Anim->Update(deltaTime);
    despawn_time -= deltaTime;

    if (despawn_time <= 0.0f && finish_loop == false) {
        owner->Destroy();
    }
    else if (m_Anim->GetIsEnded() && finish_loop == true) {
        owner->Destroy();
    }
}