#pragma once
#ifndef TRANSITION_FUNCTION
#define TRANSITION_FUNCTION

#include <map>
#include "LevelManager.h"

namespace Transition 
{

	static void SetLoadNewLevel()
	{
		LevelManager::GetInstance()->SetLoadNewLevel();
	}

	static void StartGame() 
	{
		SceneManager::GetInstance()->LoadScene(GAME_SCENE);
		UIManager::GetInstance()->RemoveMainUI();
	}

}

#endif // !TRANSITION_FUNCTION
