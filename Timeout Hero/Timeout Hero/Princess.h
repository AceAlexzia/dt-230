#pragma once

#ifndef PRINCESS
#define PRINCESS

#include "RigidbodyComponent.h"
#include "TransformComponent.h"
#include "PlayerColliderComponent.h"
#include "SpriteAnimationComponent.h"

class Princess: public Component
{

private:

	//Princess property
	bool m_IsCaged = true;
	bool m_IsUnCaged = false;

	bool m_IsMoveUp = false;

	//Princess' Component
	Transform* m_Transform = nullptr;
	Sprite* m_Sprite = nullptr;
	SpriteAnimation* m_Animation = nullptr;
	Collider* m_Collider = nullptr;
	Rigidbody* m_Rigidbody = nullptr;

	//private function
	//update animation state
	void Update_AnimationState();
	//update moving up
	void Update_MovingUp();

public:

	void Initialize() override;
	void Update(float deltatime) override;

	void MoveUp();
	void UnCage();

};

#endif // !PRINCESS
