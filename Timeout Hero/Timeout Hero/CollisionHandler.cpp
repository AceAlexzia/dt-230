#include "CollisionHandler.h"
#include "LevelManager.h"

CollisionHandler* CollisionHandler::s_Instance = nullptr;

CollisionHandler::CollisionHandler() 
{
	InitMapCollision();
}


CollisionHandler* CollisionHandler::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new CollisionHandler();
}

int CollisionHandler::GetTileSize()
{
	return m_CollisionLayer->m_TileSize;
}


void CollisionHandler::InitMapCollision()
{
	GameMap* gamemap = LevelManager::GetInstance()->GetMap();

	if (gamemap != nullptr) 
	{
		m_CollisionLayer = gamemap->GetMapLayer()[1];
		m_CollisionTileMap = m_CollisionLayer->GetTileMap();
		m_SpikeLayer = gamemap->GetMapLayer()[2];
		m_SpikeTileMap = m_SpikeLayer->GetTileMap();
	}

}


bool CollisionHandler::CheckCollision(SDL_Rect a, SDL_Rect b, bool a_enable, bool b_enable)
{
	//check whether the collider is disable -> return immediately
	if (!a_enable || !b_enable)
	{
		return false;
	}

	bool x_overlaps = (a.x <= b.x + b.w) && (a.x + a.w >= b.x);
	bool y_overlaps = (a.y <= b.y + b.h) && (a.y + a.h >= b.y);
	
	return x_overlaps && y_overlaps;
}

bool CollisionHandler::CheckCollision_Left(SDL_Rect a, SDL_Rect b) 
{
	float bottomCol = (a.y + a.h) - b.y;
	float topCol = (b.y + b.h) - a.y;
	float leftCol = (b.x + b.w) - a.x;
	float rightCol = (a.x + a.w) - b.x;
	
	return leftCol <= rightCol && leftCol <= topCol && leftCol <= bottomCol;

}

bool CollisionHandler::CheckCollision_Right(SDL_Rect a, SDL_Rect b)
{
	float bottomCol = (a.y + a.h) - b.y;
	float topCol = (b.y + b.h) - a.y;
	float leftCol = (b.x + b.w) - a.x;
	float rightCol = (a.x + a.w) - b.x;

	return rightCol <= leftCol && rightCol <= topCol && rightCol <= bottomCol;

}

bool CollisionHandler::CheckCollision_Up(SDL_Rect a, SDL_Rect b)
{
	float bottomCol = (a.y + a.h) - b.y;
	float topCol = (b.y + b.h) - a.y;
	float leftCol = (b.x + b.w) - a.x;
	float rightCol = (a.x + a.w) - b.x;

	return topCol <= rightCol && topCol <= leftCol && topCol <= bottomCol;

}

bool CollisionHandler::CheckCollision_Down(SDL_Rect a, SDL_Rect b)
{
	float bottomCol = (a.y + a.h) - b.y;
	float topCol = (b.y + b.h) - a.y;
	float leftCol = (b.x + b.w) - a.x;
	float rightCol = (a.x + a.w) - b.x;

	return bottomCol <= rightCol && bottomCol <= topCol && bottomCol <= leftCol;

}


bool CollisionHandler::MapCollision_Inside(SDL_Rect a)
{
	int tileSize = m_CollisionLayer->m_TileSize;
	int rowCount = m_CollisionLayer->m_RowCount;
	int colCount = m_CollisionLayer->m_ColCount;

	int centerX = valueCap((a.x + (a.w / 2)) / tileSize, colCount);
	int centerY = valueCap((a.y + (a.h / 2)) / tileSize, rowCount);

	return m_CollisionTileMap[centerY][centerX] > 0;
}

//checking MAP collision from UP direction
bool CollisionHandler::MapCollision_Up(SDL_Rect a, float& tilePos) 
{
	int tileSize = m_CollisionLayer->m_TileSize;
	int rowCount = m_CollisionLayer->m_RowCount;
	int colCount = m_CollisionLayer->m_ColCount;
	bool isLoop = MapCollision_Inside(a);
	int loopCount = 0;

	do
	{
		loopCount++;

		int left_tile = valueCap(a.x / tileSize, colCount);
		int right_tile = valueCap((a.x + a.w) / tileSize, colCount);
		int top_tile = valueCap(a.y / tileSize, rowCount);

		int emptyTileCount = 0;


		for (int j = left_tile; j <= right_tile; j++)
		{
			if (m_CollisionTileMap[top_tile][j] > 0)
			{
				if (!MapCollision_Inside(a))
				{
					tilePos = top_tile * tileSize;
					return true;
				}
				else {
					a = { a.x, a.y + (tileSize / 2), a.w, a.h };
					isLoop = true;
					break;
				}
			}
			else 
			{
				emptyTileCount++;
				if (emptyTileCount == right_tile - left_tile + 1)
				{
					if (loopCount == 1)
					{
						return false;
					}
					else
					{
						tilePos = (top_tile * tileSize) - tileSize;
						return true;
					}

				}
			}
		}

	} while (isLoop);

	return false;
}

//checking MAP collision from DOWN direction
bool CollisionHandler::MapCollision_Down(SDL_Rect a, float& tilePos) 
{
	int tileSize = m_CollisionLayer->m_TileSize;
	int rowCount = m_CollisionLayer->m_RowCount;
	int colCount = m_CollisionLayer->m_ColCount;
	bool isLoop = MapCollision_Inside(a);
	int loopCount = 0;

	do 
	{
		loopCount++;

		int left_tile = valueCap(a.x / tileSize, colCount);
		int right_tile = valueCap((a.x + a.w) / tileSize, colCount);
		int bottom_tile = valueCap((a.y + a.h) / tileSize, rowCount);

		int emptyTileCount = 0;

		for (int j = left_tile; j <= right_tile; j++)
		{
			if (m_CollisionTileMap[bottom_tile][j] > 0)
			{
				if (!MapCollision_Inside(a)) 
				{
					tilePos = bottom_tile * tileSize;
					return true;
				}
				else {
					a = { a.x, a.y - (tileSize / 2), a.w, a.h };
					isLoop = true;
					break;
				}
			}
			else 
			{
				emptyTileCount++;
				if (emptyTileCount == right_tile - left_tile + 1)
				{
					if (loopCount == 1)
					{
						return false;
					}
					else
					{
						tilePos = (bottom_tile * tileSize) + tileSize;
						return true;
					}

				}
			}
		}

	} while (isLoop);
	
	return false;
}

//checking MAP collision from LEFT direction
bool CollisionHandler::MapCollision_Left(SDL_Rect a, float& tilePos) 
{
	int tileSize = m_CollisionLayer->m_TileSize;
	int rowCount = m_CollisionLayer->m_RowCount;
	int colCount = m_CollisionLayer->m_ColCount;
	bool isLoop = MapCollision_Inside(a);
	int loopCount = 0;
	do
	{
		loopCount++;

		int left_tile = valueCap(a.x / tileSize, colCount);
		int top_tile = valueCap(a.y / tileSize, rowCount);
		int bottom_tile = valueCap((a.y + a.h) / tileSize, rowCount);

		int emptyTileCount = 0;

		for (int i = top_tile; i <= bottom_tile; i++)
		{
			if (m_CollisionTileMap[i][left_tile] > 0)
			{
				if (!MapCollision_Inside(a))
				{
					tilePos = left_tile * tileSize;
					return true;
				}
				else 
				{
					a = { a.x + (tileSize / 2), a.y , a.w, a.h };
					isLoop = true;
					break;
				}
			}
			else {
				emptyTileCount++;
				if (emptyTileCount == bottom_tile - top_tile + 1) 
				{
					if (loopCount == 1) 
					{
						return false;
					}
					else 
					{
						tilePos = (left_tile * tileSize) - tileSize;
						return true;
					}
					
				}
			}
		}

	} while (isLoop);

}

//checking MAP collision from RIGHT direction
bool CollisionHandler::MapCollision_Right(SDL_Rect a, float& tilePos) 
{
	int tileSize = m_CollisionLayer->m_TileSize;
	int rowCount = m_CollisionLayer->m_RowCount;
	int colCount = m_CollisionLayer->m_ColCount;
	bool isLoop = MapCollision_Inside(a);
	int loopCount = 0;

	do
	{
		loopCount++;

		int right_tile = valueCap((a.x + a.w) / tileSize, colCount);
		int top_tile = valueCap(a.y / tileSize, rowCount);
		int bottom_tile = valueCap((a.y + a.h) / tileSize, rowCount);

		int emptyTileCount = 0;

		for (int i = top_tile; i <= bottom_tile; i++)
		{
			if (m_CollisionTileMap[i][right_tile] > 0)
			{
				if (!MapCollision_Inside(a))
				{
					////std::cout << "COLLIDE RIGHT" << std::endl;
					tilePos = right_tile * tileSize;
					return true;
				}
				else 
				{
					a = { a.x - (tileSize / 2), a.y , a.w, a.h };
					isLoop = true;
					break;
				}
			}
			else {
				emptyTileCount++;
				if (emptyTileCount == bottom_tile - top_tile + 1)
				{
					if (loopCount == 1)
					{
						return false;
					}
					else
					{
						tilePos = (right_tile * tileSize) + tileSize;
						return true;
					}

				}
			}
		}

	} while (isLoop);

	return false;
}


//checking SPIKE collision from UP direction
bool CollisionHandler::SpikeCollision_Up(SDL_Rect a) 
{
	int tileSize = m_SpikeLayer->m_TileSize;
	int rowCount = m_SpikeLayer->m_RowCount;
	int colCount = m_SpikeLayer->m_ColCount;

	int left_tile = valueCap(a.x / tileSize, colCount);
	int right_tile = valueCap((a.x + a.w) / tileSize, colCount);
	int top_tile = valueCap(a.y / tileSize, rowCount);

	for (int j = left_tile; j <= right_tile; j++)
	{
		if (m_SpikeTileMap[top_tile][j] > 0)
		{
			return true;
		}
	}
	return false;
}

//checking SPIKE collision from DOWN direction
bool CollisionHandler::SpikeCollision_Down(SDL_Rect a)
{
	int tileSize = m_SpikeLayer->m_TileSize;
	int rowCount = m_SpikeLayer->m_RowCount;
	int colCount = m_SpikeLayer->m_ColCount;

	int left_tile = valueCap(a.x / tileSize, colCount);
	int right_tile = valueCap((a.x + a.w) / tileSize, colCount);
	int bottom_tile = valueCap((a.y + a.h) / tileSize, rowCount);

	for (int j = left_tile; j <= right_tile; j++)
	{
		if (m_SpikeTileMap[bottom_tile][j] > 0)
		{
			return true;
		}
	}
	return false;
}

//checking SPIKE collision from LEFT direction
bool CollisionHandler::SpikeCollision_Left(SDL_Rect a) 
{
	int tileSize = m_SpikeLayer->m_TileSize;
	int rowCount = m_SpikeLayer->m_RowCount;
	int colCount = m_SpikeLayer->m_ColCount;

	int left_tile = valueCap(a.x / tileSize, colCount);
	int top_tile = valueCap(a.y / tileSize, rowCount);
	int bottom_tile = valueCap((a.y + a.h) / tileSize, rowCount);

	for (int i = top_tile; i <= bottom_tile; i++)
	{
		if (m_SpikeTileMap[i][left_tile] > 0)
		{
			return true;
		}
	}
	return false;
}

//checking SPIKE collision from RIGHT direction
bool CollisionHandler::SpikeCollision_Right(SDL_Rect a) 
{
	int tileSize = m_SpikeLayer->m_TileSize;
	int rowCount = m_SpikeLayer->m_RowCount;
	int colCount = m_SpikeLayer->m_ColCount;

	int right_tile = valueCap((a.x + a.w) / tileSize, colCount);
	int top_tile = valueCap(a.y / tileSize, rowCount);
	int bottom_tile = valueCap((a.y + a.h) / tileSize, rowCount);

	for (int i = top_tile; i <= bottom_tile; i++)
	{
		if (m_SpikeTileMap[i][right_tile] > 0)
		{
			return true;
		}
	}
	return false;
}

bool CollisionHandler::SpikeCollision_Inside(SDL_Rect a)
{
	int tileSize = m_SpikeLayer->m_TileSize;
	int rowCount = m_SpikeLayer->m_RowCount;
	int colCount = m_SpikeLayer->m_ColCount;

	int centerX = valueCap((a.x + (a.w / 2)) / tileSize, colCount);
	int centerY = valueCap((a.y + (a.h / 2)) / tileSize, rowCount);

	return m_SpikeTileMap[centerY][centerX] > 0;
}