#include "TransitionBox.h"

void TransitionBox::Initialize() 
{
	//get camera postion
	Vector2D camPos = Camera::GetInstance()->GetPosition();

	//add Transform if does not have
	if (!owner->HasComponent<Transform>()) 
	{
		owner->AddComponent<Transform>(camPos.x, camPos.y);
	}
	//add Sprite if does not have
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("blackscreen", RENDER_LOGICAL_WIDTH, RENDER_LOGICAL_HEIGHT, TRANSITION_LAYER);
	}

	//assign component as a pointer
	m_Transform = owner->GetComponent<Transform>();
	m_Sprite = owner->GetComponent<Sprite>();
	//disable sprite
	m_Sprite->m_IsEnable = false;
}

void TransitionBox::Update(float deltatime)
{
	//if the transition is done
	if (m_IsTransition) 
	{
		//always resume the game
		PauseManager::GetInstance()->Resume();

		//get current camera postion
		Vector2D camPos = Camera::GetInstance()->GetPosition();

		//if the camera positon is not the same
		//translate to new camera postion without bothering the transition
		if (m_StartCamPos.x != camPos.x) 
		{
			m_Transform->TranslateX(camPos.x - m_StartCamPos.x);
			m_StartCamPos.x = camPos.x;
		}
		if (m_StartCamPos.y != camPos.y) 
		{
			m_Transform->TranslateY(camPos.y - m_StartCamPos.y);
			m_StartCamPos.y = camPos.y;
		}

		//translate the box from right to left
		m_Transform->TranslateX(m_TransitionSpeed * -1.0f * deltatime);

		//if the box is in the middle of the screen
		if (m_Transform->position.x <= m_StartCamPos.x && m_FnPtr != nullptr)
		{
			//set the box at the middle of the screen
			m_Transform->position = m_StartCamPos;
			//use the transition function
			m_FnPtr();
			m_FnPtr = nullptr;

		}

		//if the box is off the screen
		if (m_Transform->position.x <= m_StartCamPos.x - RENDER_LOGICAL_WIDTH)
		{
			//stop transitioning
			m_IsTransition = false;
			//disable the sprite
			m_Sprite->m_IsEnable = false;

		}
	}

}


void TransitionBox::SetFunction(void (*fn)())
{
	m_FnPtr = fn;
}


void TransitionBox::Transition()
{
	//set start transition
	m_IsTransition = true;

	//assign camera starting position
	m_StartCamPos = Camera::GetInstance()->GetPosition();

	//get logical render width
	int renderWidth = RENDER_LOGICAL_WIDTH;

	//set starting Transition Box position
	m_Transform->position.x = m_StartCamPos.x + renderWidth;
	m_Transform->position.y = m_StartCamPos.y;

	//enable sprite
	m_Sprite->m_IsEnable = true;

}

bool TransitionBox::IsTransition() 
{
	return m_IsTransition;
}
