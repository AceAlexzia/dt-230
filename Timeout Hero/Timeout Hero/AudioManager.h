#pragma once

#ifndef AUDIO_MANAGER
#define AUDIO_MANAGER

#include "Core/AudioPlayer.h"


class AudioManager
{
private:
	static AudioManager* s_Instance;

	int m_MasterVol = 10;
	int m_MusicVol = 10;
	int m_SfxVol = 10;

	bool m_IsLowerDown = false;

	AudioManager() {}

	void LowerDown();
	void StopLowerDown();

public:
	~AudioManager() {}

	static AudioManager* GetInstance();

	void Initialize();
	void Update();
	
	//setter
	void SetVol(int masterVol, int musicVol, int sfxVol);

	//music player
	KHORA::AudioPlayer musicPlayer;
	//sound effect player
	//clock tick sound effect
	KHORA::AudioPlayer clockSfxPlayer;
	//hero sound effect
	KHORA::AudioPlayer dashSfxPlayer;
	KHORA::AudioPlayer deadSfxPlayer;
	KHORA::AudioPlayer hitSfxPlayer;

};

#endif // !AUDIO_MANAGER
