#pragma once

#ifndef TUTORIAL_UI
#define TUTORIAL_UI

#include "SpriteComponent.h"
#include "InputManager.h"
#include "DialogueManager.h"

class TutorialUI: public Component
{
private:
	bool m_IsController = false;
	int m_LevelIndex = 0;

	float m_BlinkCounter = 0.0f;

	std::string GetSpriteID();

	Sprite* m_Sprite = nullptr;

public:
	TutorialUI(int levelindex);
	virtual ~TutorialUI() {}

	void Initialize() override;
	void Update(float deltatime) override;
};


#endif // !TUTORIAL_UI
