#include "AdjustArrow.h"

AdjustArrow::AdjustArrow(int x, int y, Entity* ad, bool selecting)
{
	arrowPos.x = x;
	arrowPos.y = y;
	ownAdjuster = ad;
}

void AdjustArrow::Initialize()
{
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(arrowPos.x, arrowPos.y);
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("AdjusterButton", 32, 32, TIME_COUNT_LAYER);
	}
	getPos = Camera::GetInstance()->GetPosition();
	owner->GetComponent<Transform>()->position.x = arrowPos.x;//+ getPos.x;
	owner->GetComponent<Transform>()->position.y = arrowPos.y;// +getPos.y;
	isSet = true;
}

void AdjustArrow::Update(float deltaTime)
{
	if (isSet)
	{
		if (!isSelecting)
		{
			owner->GetComponent<Sprite>()->SetTexture("AdjusterButton");
		}
		else
		{
			owner->GetComponent<Sprite>()->SetTexture("AdjusterButton2");
		}
		isSet = false;
	}
}


Entity* AdjustArrow::SelectUI(int direction)
{
	return nextUI[direction];
}
