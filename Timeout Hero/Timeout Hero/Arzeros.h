#pragma once

#ifndef ARZEROS
#define ARZEROS

#define ARZEROS_ACCELERATION 300.0f
#define ARZEROS_MAX_VELOCITY 500.0f
#define ARZEROS_FLIP_VELOCITY 200.0f
#define ARZEROS_HURT_TIME 0.15f
#define ARZEROS_ATTACK_COOLDOWN_STATE_1 1.0f
#define ARZEROS_ATTACK_COOLDOWN_STATE_2 1.0f
#define ARZEROS_ATTACK_COOLDOWN_STATE_4 1.9f
#define ARZEROS_ATTACK_COOLDOWN_STATE_5 1.5f
#define ARZEROS_ATTACK_DELAY 0.5f
#define ARZEROS_BLINK_LENGTH 0.20f
#define ARZEROS_SHAKE_AMOUNT 500.0f
#define ARZEROS_SPELL_PERCENTAGE 30.0f

#include "ArchMonster.h"
#include "SpellMonster.h"

class Arzeros : public LevelObject
{

private:
	int m_State = -1;

	bool m_FinishState = false;

	bool m_IsHurt = false;
	float m_HurtTime = 0.0f;

	bool m_IsAttack = false;
	float m_AttackCooldown = 0.0f;
	float m_AttackDelay = 0.0f;
	int m_AttackCount = 0;

	float m_BlinkTime = 0.0f;

	bool m_CanMove = true;

	std::map<int, std::vector<Vector2D>> m_ArchmonsterDirection;
	std::map<int, std::vector<float>> m_ArchmonsterDestroyTime;

	Rigidbody* m_Rigidbody = nullptr;

	//Gamemap variable
	int m_RowCount = 0;
	int m_ColCount = 0;
	int m_TileSize = 0;

	//private function
	//sub initialize function
	//initialize all components
	void Initialize_Component();
	//initialize according to its state
	void Initialize_State();
	//initialize archmonster property
	void Initialize_AttackProperty();
	//sub update function
	//update according to its state
	void Update_FinishState(float deltatime);
	//Update Hurt
	void Update_Hurt(float deltatime);
	//Update_Attack
	void Update_Attack(float deltatime);
	//Update Moving
	void Update_Moving();
	//Update Collision
	void Update_Collision();
	//Update Animation
	void Update_Animation();
	//setter
	void SetCollider();

	//archmonster spawner
	void SpawnArchMonster();

	//blinking
	void Blink(float deltatime);

public:

	Arzeros(int state);
	virtual ~Arzeros() {}

	void Initialize() override;
	void Update(float deltaTime) override;

	//setter
	void FinishState();
	void Attack();
	void SetMoving(bool set);

	//Getter
	bool IsFinish();
};

#endif // !ARZEROS

