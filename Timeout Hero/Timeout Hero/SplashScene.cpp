#include "SplashScene.h"
SplashScene::SplashScene()
{
	Start();
}
SplashScene::~SplashScene()
{
	cd->Destroy();
}
void SplashScene::Start()
{
	name = "SplashScene";
	UIManager::GetInstance()->InitializeSplash();
	Entity& credit(EntityManager::GetInstance()->AddEntity("Credit"));
	credit.AddComponent<Credit>();
	cd = &credit;
}