#include "MovingTile.h"

void MovingTile::Initialize()
{
    m_Collider = owner->GetComponent<Collider>();
    m_Transform = owner->GetComponent<Transform>();
    m_Sprite = owner->GetComponent<Sprite>();
    m_Anim = owner->GetComponent<SpriteAnimation>();
    m_Anim->SetProps("Horizontal_Moving_Tile", 1, 0);
    m_Collider->SetBuffer(0, 0, 0, 0);
    m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}

void MovingTile::Update(float deltaTime)
{
    Update_Collision();
}



void MovingTile::Update_Collision()
{
    SDL_Rect thisCollider = m_Collider->Get();

    PlayerCollider* playerCollider = m_Player->owner->GetComponent<PlayerCollider>();
    SDL_Rect playerRect = playerCollider->Get();


    if (CollisionTriggerred())
    {
        if (CollisionHandler::GetInstance()->CheckCollision_Left(playerRect, thisCollider))
        {
            playerCollider->SetObjectCollision(thisCollider, LEFT);
        }
        else if (CollisionHandler::GetInstance()->CheckCollision_Right(playerRect, thisCollider))
        {
            playerCollider->SetObjectCollision(thisCollider, RIGHT);
        }
        else if (CollisionHandler::GetInstance()->CheckCollision_Up(playerRect, thisCollider))
        {
            playerCollider->SetObjectCollision(thisCollider, UP);
        }
        else if (CollisionHandler::GetInstance()->CheckCollision_Down(playerRect, thisCollider))
        {
            playerCollider->SetObjectCollision(thisCollider, DOWN);
        }

        //assign this entity to Hero
        m_Player->m_CollidingEntity = owner;

    }

}