#pragma once
#ifndef MENUSCENE_H
#define MENUSCENE_H
#include "Scene.h"
#include "UIManager.h"
#include <iostream>
class MenuScene: public Scene
{
	public:
		MenuScene();
		~MenuScene(){}
		void Start();
};

#endif // !MENUSCENE_H