#include "TutorialUI.h"
#include "TransitionManager.h"

TutorialUI::TutorialUI(int levelIndex) 
{
	m_LevelIndex = levelIndex;
	m_IsController = InputManager::GetInstance()->HasController();
}

void TutorialUI::Initialize()
{
	owner->AddComponent<Sprite>(GetSpriteID(), 380, 192, MONSTER_LAYER);
	m_Sprite = owner->GetComponent<Sprite>();
}

void TutorialUI::Update(float deltatime) 
{
	bool hasController = InputManager::GetInstance()->HasController();

	if (m_IsController != hasController)
	{
		m_IsController = hasController;

		m_Sprite->SetTexture(GetSpriteID());
	}
	m_Sprite->m_IsEnable = !(TransitionManager::GetInstance()->IsFadeTransition() || DialogueManager::GetInstance()->IsSpeaking());
}

std::string TutorialUI::GetSpriteID()
{
	std::string id = "";

	//no controller connected
	if (!m_IsController) 
	{
		id = "kb_";
	}
	//controller connected
	else 
	{
		id = "ctrl_";
	}

	//checking for the level index
	if (m_LevelIndex == 0) 
	{
		id += "move";
	}
	else if (m_LevelIndex == 1)
	{
		id += "jump";
	}
	else if (m_LevelIndex == 2)
	{
		id = "jumphere";
	}
	else if (m_LevelIndex == 3)
	{
		id += "climb";
	}
	else if (m_LevelIndex == 4)
	{
		id += "dash";
	}
	else if (m_LevelIndex == 5)
	{
		id += "multidash";
	}
	else if (m_LevelIndex == 9)
	{
		id += "die";
	}
	else if (m_LevelIndex == 37)
	{
		id += "glide";
	}
	else if (m_LevelIndex == 38)
	{
		id += "canceljump";
	}

	return id;
}
