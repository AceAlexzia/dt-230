#include "RigidbodyComponent.h"

Rigidbody::Rigidbody()
{
	m_mass = UNI_MASS;
	m_gravity = GRAVITY;
}


//Update Rigidbody value
void Rigidbody::Update(float deltaTime)
{
	m_acceleration.x = (m_force.x) / m_mass;
	m_acceleration.y = (m_gravity + m_force.y) / m_mass;
	m_velocity = m_velocity + (m_acceleration * deltaTime);
	m_position = m_velocity * deltaTime;
}


//Set Mass and Gravity
void Rigidbody::setMass(float mass)
{
	m_mass = mass;
}

void Rigidbody::setGravity(float gravity)
{
	m_gravity = gravity;
}


//Force
void Rigidbody::ApplyForce(Vector2D f)
{
	m_force = f;
}

void Rigidbody::ApplyForceX(float fx)
{
	m_force.x = fx;
}

void Rigidbody::ApplyForceY(float fy)
{
	m_force.y = fy;
}

void Rigidbody::UnSetForce() {
	m_force = Vector2D(0, 0);
}


//Getter
float Rigidbody::GetMass()
{
	return m_mass;
}

Vector2D Rigidbody::Position()
{
	return m_position;
}