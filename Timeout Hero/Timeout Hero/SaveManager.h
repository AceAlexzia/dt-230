#pragma once
#ifndef SAVEMANAGER_H
#define SAVEMANAGER_H
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <SDL.h>
#include "Game.h"
struct Saveslot
{
	int playStage;
	int death;
	std::string mode;
	float totalTime;
};
struct Option
{
	int resolution_width, resolution_height;
	bool full;
	int master;
	int sfx;
	int music;
	int screenshake;
	bool skipDialogue;
};

class SaveManager
{
	public:
		static SaveManager* GetInstance();
		void Initialize();
		void LoadSave(int pos);
		void LoadLevel();
		void LoadTime();
		void LoadDead();
		void Readfile(int pos, std::string filename);
		void Writefile(int pos, std::string filename, int play, int dead, std::string m, float tTotal);
		void SetSave(int pos, int play, int dead, std::string m, float tTotal);
		void SetLevel(int index);
		void SetDeath();
		void SetTime(float t);
		void SetOption(int width, int height, bool full, int vol1, int vol2, int vol3, int shake, bool skip);
		void LoadOption();
		std::string savefile[3] = { "save1.txt", "save2.txt", "save3.txt" };

		Saveslot save[3];
		Option opt;
		bool hasSave[3];
		int currentSlotPos;
	private:
		static SaveManager* s_Instance;
		std::string  optionFile = "Option.txt";
};

#endif