#include "TextComponent.h"

Text::Text(LayerType layer, std::string textID, std::string fontName, int size, SDL_Color color, std::string text) : m_Layer(layer), m_TextID(textID), font(fontName), size(size), color(color), text(text) {}

void Text::Initialize() 
{
	m_Transform = owner->GetComponent<Transform>();
}

void Text::Update(float deltatime) 
{
	if (m_TextID != "" && font != "" && size != 0) 
	{
		FontManager::GetInstance()->LoadFont(m_TextID, font, size);
		FontManager::GetInstance()->LoadFont(m_TextID + "_outline", font, size, outlineSize);

		FontManager::GetInstance()->CreateFontTexture(m_TextID, color, text);
		FontManager::GetInstance()->CreateFontTexture(m_TextID + "_outline", outlineColor, text);

		SDL_QueryTexture(TextureManager::GetInstance()->GetTexture(m_TextID), NULL, NULL, &m_Width, &m_Height);
	}
}

void Text::Render() 
{
	if (m_IsEnable)
	{
		FontManager::GetInstance()->DrawFontTexture(m_TextID + "_outline", m_Transform->position.x, m_Transform->position.y, m_Width, m_Height);
		FontManager::GetInstance()->DrawFontTexture(m_TextID, m_Transform->position.x, m_Transform->position.y, m_Width, m_Height);
	}
}


int Text::GetWidth() 
{
	return m_Width;
}

int Text::GetHeight() 
{
	return m_Height;
}
