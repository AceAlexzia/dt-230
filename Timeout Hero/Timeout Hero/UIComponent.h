#pragma once
#ifndef UICOMPONENT_H
#define UICOMPONENT_H

#include "Component.h"
#include "Entity.h"

#include <string>
class UIComponent : public Component
{
	public:
		UIComponent() {};
		void Initialize() override {};
		void Update(float deltaTime)override {};
		Entity* nextUI[4];
		std::string textureID, name;
		bool isSelecting;
	private:
};

#endif // !UICOMPONENT_H