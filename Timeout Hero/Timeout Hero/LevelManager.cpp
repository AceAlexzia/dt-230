#include "LevelManager.h"

LevelManager* LevelManager::s_Instance = nullptr;

LevelManager* LevelManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new LevelManager();
}

GameMap* LevelManager::GetMap()
{
	return m_CurrentGameMap;
}

int LevelManager::GetLevelAmount()
{
	return m_AllLevels.size();
}

void LevelManager::ParseLevel()
{
	//set all level and section name
	m_AllLevels.push_back("Level-T-1"); //0
	m_AllLevels.push_back("Level-T-2"); //1
	m_AllLevels.push_back("Level-T-3"); //2
	m_AllLevels.push_back("Level-T-4"); //3
	m_AllLevels.push_back("Level-T-5"); //4
	m_AllLevels.push_back("Level-T-6"); //5
	m_AllLevels.push_back("Level-T-7"); //6
	m_AllLevels.push_back("Level-T-8"); //7
	m_AllLevels.push_back("Level-0-1"); //8
	m_AllLevels.push_back("Level-1-1"); //9
	m_AllLevels.push_back("Level-1-2"); //10
	m_AllLevels.push_back("Level-1-3"); //11
	m_AllLevels.push_back("Level-2-1"); //12
	m_AllLevels.push_back("Level-2-2"); //13
	m_AllLevels.push_back("Level-2-3"); //14
	m_AllLevels.push_back("Level-3-1"); //15
	m_AllLevels.push_back("Level-3-2"); //16
	m_AllLevels.push_back("Level-4-1"); //17
	m_AllLevels.push_back("Level-4-2"); //18
	m_AllLevels.push_back("Level-4-3"); //19
	m_AllLevels.push_back("Level-U-1"); //20
	m_AllLevels.push_back("Level-U-2"); //21
	m_AllLevels.push_back("Level-U-3"); //22
	m_AllLevels.push_back("Level-U-4"); //23
	m_AllLevels.push_back("Level-U-5"); //24
	m_AllLevels.push_back("Level-U-6"); //25
	m_AllLevels.push_back("Level-U-7"); //26
	m_AllLevels.push_back("Level-U-8"); //27
	m_AllLevels.push_back("Level-A-0"); //28
	m_AllLevels.push_back("Level-A-1"); //29
	m_AllLevels.push_back("Level-A-2"); //30
	m_AllLevels.push_back("Level-A-3"); //31
	m_AllLevels.push_back("Level-A-4"); //32
	m_AllLevels.push_back("Level-C-1"); //33
	m_AllLevels.push_back("Level-C-2"); //34
	m_AllLevels.push_back("Level-C-3"); //35
	m_AllLevels.push_back("Level-D-1"); //36
	m_AllLevels.push_back("Level-5-1"); //37
	m_AllLevels.push_back("Level-5-2"); //38
	m_AllLevels.push_back("Level-5-3"); //39
	m_AllLevels.push_back("Level-6-1"); //40
	m_AllLevels.push_back("Level-6-2"); //41
	m_AllLevels.push_back("Level-6-3"); //42
	m_AllLevels.push_back("Level-E-1"); //43
	m_AllLevels.push_back("Level-E-2"); //44
	m_AllLevels.push_back("Level-E-3"); //45
	m_AllLevels.push_back("Level-F-1"); //46
	m_AllLevels.push_back("Level-F-2"); //47
	m_AllLevels.push_back("Level-F-3"); //48
	m_AllLevels.push_back("Level-G-1"); //49
	m_AllLevels.push_back("Level-H-1"); //50

	//GameMap file path
	std::string levelPath = LEVEL_PATH;

	//loop through level name vector
	for (int i = 0; i < m_AllLevels.size(); i++) 
	{
		//load GameMap
		//if it's failed -> print out
		if (!MapParser::GetInstance()->Load(m_AllLevels[i], levelPath + m_AllLevels[i] + ".tmx")) 
		{
			std::cout << "Failed to load tile map :  " << m_AllLevels[i] << std::endl;
		}
		
	}

}


void LevelManager::Render()
{
	//Render Tilemap
	if (m_CurrentGameMap != nullptr)
	{
		m_CurrentGameMap->Render();

	}

}

void LevelManager::Update(float dt)
{
	m_TimePlayed += dt;
	
	//if the new level should be loaded -> load new level
	if (m_IsLoadNewLevel) 
	{
		m_IsLoadNewLevel = false;
		LoadNewLevel();
	}


	//update others
	//update PauseManager
	PauseManager::GetInstance()->Update();

	//get player's hit box 
	SDL_Rect playerHitBox = m_Player->owner->GetComponent<PlayerCollider>()->Get();
	//set hero origin in Camera class
	Camera::GetInstance()->SetHeroOrigin(GetOrigin(playerHitBox));
	//update Camera
	Camera::GetInstance()->Update(dt);

	//update cutscene manager
	CutsceneManager::GetInstance()->Update(dt);


	//============================ONLY FOR TESTING============================
	if (CutsceneManager::GetInstance()->IsEndGamex2() || m_Player->m_IsEvent)
	{
		return;
	}
	if (InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_LEFT)) 
	{
		levelIndex--;
		sectionChange--;
		if (levelIndex < 0) 
		{
			levelIndex = 0;
		}

		SetLoadNewLevel();
		m_TimeCounter->SetTimeCount(TIME_COUNTER_MAX_COUNT);

		if (AudioManager::GetInstance()->clockSfxPlayer.isPlaying())
		{
			AudioManager::GetInstance()->clockSfxPlayer.Stop();
			AudioManager::GetInstance()->clockSfxPlayer.SetInteger("time", 60);
		}
	}
	else if (InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_RIGHT))
	{
		levelIndex++;
		sectionChange++;
		if (levelIndex == m_AllLevels.size())
		{
			levelIndex = m_AllLevels.size() - 1;
		}
		SetLoadNewLevel();
		m_TimeCounter->SetTimeCount(TIME_COUNTER_MAX_COUNT);

		if (AudioManager::GetInstance()->clockSfxPlayer.isPlaying())
		{
			AudioManager::GetInstance()->clockSfxPlayer.Stop();
			AudioManager::GetInstance()->clockSfxPlayer.SetInteger("time", 60);
		}
	}

	//REGAIN TIME CHEAT
	if (InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_T)) 
	{
		m_TimeCounter->SetTimeCount(TIME_COUNTER_MAX_COUNT);

		if (AudioManager::GetInstance()->clockSfxPlayer.isPlaying())
		{
			AudioManager::GetInstance()->clockSfxPlayer.Stop();
			AudioManager::GetInstance()->clockSfxPlayer.SetInteger("time", 60);
		}
	}
	
	//============================ONLY FOR TESTING============================

}

void LevelManager::EndGameWarp(bool isEndGame) 
{
	if (isEndGame)
	{
		sectionChange = 0;
		if (levelIndex == 3)
		{
			levelIndex = 6;
		}
		else if (levelIndex == 8)
		{
			levelIndex = 10;
			m_StaringTime = TIME_COUNTER_MAX_COUNT;
		}
		else if (levelIndex == 11)
		{
			levelIndex = 12;
		}
		else if (levelIndex == 13)
		{
			levelIndex = 18;
			m_StaringTime = TIME_COUNTER_MAX_COUNT;
		}
		else if (levelIndex == 19)
		{
			levelIndex = 23;
			m_StaringTime = TIME_COUNTER_MAX_COUNT;
		}
		else if (levelIndex == 25)
		{
			levelIndex = 27;
		}
		else if (levelIndex == 29)
		{
			levelIndex = 37;
		}
		else if (levelIndex == 38)
		{
			levelIndex = 43;
		}
		else if (levelIndex == 44)
		{
			levelIndex = 46;
		}
		else if (levelIndex == 47)
		{
			levelIndex = 50;
		}
	}
}


void LevelManager::LoadNewLevel()
{
	//clean dynamic allocated object
	Clean();

	//FOR TESTING 
	//set level cap
	if (levelIndex == m_AllLevels.size())
	{
		levelIndex = 0;
		CutsceneManager::GetInstance()->EndGame();

		//end the game for the first time -> reset music playing
		if (!CutsceneManager::GetInstance()->IsEndGamex2()) 
		{
			AudioManager::GetInstance()->musicPlayer.SetInteger("LevelIndex", 0);
			AudioManager::GetInstance()->musicPlayer.Stop();
			AudioManager::GetInstance()->musicPlayer.Play();
		}
		
	}

	bool isEndGame = CutsceneManager::GetInstance()->IsEndGame();

	//if entering new level and section counter is not reset
	if (m_AllLevels[levelIndex].back() == '1')
	{
		if (sectionChange != 0) 
		{
			//reset section counter
			sectionChange = 0;
		}

		//if tje game is not end
		if (!isEndGame)
		{
			//SAVE GAME
			//Save level
			SaveManager::GetInstance()->save[SaveManager::GetInstance()->currentSlotPos].playStage = levelIndex;
			//Save time played
			m_TimePlayed += SaveManager::GetInstance()->save[SaveManager::GetInstance()->currentSlotPos].totalTime;
			SaveManager::GetInstance()->SetTime(m_TimePlayed);
			m_TimePlayed = 0.0f;
		}
	}

	EndGameWarp(isEndGame);

	//get map
	m_CurrentGameMap = MapParser::GetInstance()->GetMap(m_AllLevels[levelIndex]);

	//get map boundary on X-axis
	int boundaryX = m_CurrentGameMap->GetColCount() * m_CurrentGameMap->GetTileSize();
	//get map boundary on Y-axis
	int boundaryY = m_CurrentGameMap->GetRowCount() * m_CurrentGameMap->GetTileSize();
	//set camera boundary
	Camera::GetInstance()->SetBoundary(Vector2D(boundaryX, boundaryY));

	//spawn player
	Spawn_Player();

	//spawn princess
	Spawn_Princess();

	//=============init level object=============
	//spawn rune
	Spawn_Rune();
	//spawn monster
	Spawn_Monsters();
	//spawn section changer
	Spawn_SectionChanger();


	//spawn time
	Spawn_TimeCounter();
	
	//spawn Horizontal Moving Tile
	Spawn_HorizontalMovingTile();

	//Spawn Artrius
	Spawn_Artrius();

	//Spawn Archfiend
	Spawn_Boss();

	//spawn tutorial UI
	Spawn_TutorialUI();

	//set player to level object vector
	AssignPlayerToLevelObjects(m_Player);

	//initialize map collision 
	CollisionHandler::GetInstance()->InitMapCollision();

	//reset is delevel
	m_IsDeLevel = false;

	//set new cutscene name
	CutsceneManager::GetInstance()->SetCutsceneName(m_AllLevels[levelIndex]);

	//set music level
	AudioManager::GetInstance()->musicPlayer.SetBool("isGameplay", true);
	AudioManager::GetInstance()->musicPlayer.SetInteger("LevelIndex", levelIndex);

	
}

void LevelManager::SetLoadNewLevel() 
{
	m_IsLoadNewLevel = true;
}

void LevelManager::SetIsDeLevel() 
{
	m_IsDeLevel = true;
}

//====================INITIALIZE FUNCTION====================

void LevelManager::Initialize()
{
	//initialize all required properties when the game is first launched
	//GameMap parsing
	ParseLevel();
	//set Altrium Rune starting property
	Set_RuneProp();
	//set Normal Monster starting property
	Set_NormalMonsterProp();
	//set Horizontal Monster starting property
	Set_HorizonalMonsterProp();
	//set Vertical Monster starting property
	Set_VerticalMonsterProp();
	//set Hero starting starting property
	Set_HeroProp();
	//set Section Changer starting property
	Set_SectionChangerProp();
	//set Horizontal Moving Tile starting property
	Set_HorizontalMovingTile();
	//set Princess starting property
	Set_PrincessProp();
	//set Tutorial UI property
	Set_TutorialUIProp();
}

void LevelManager::StartGame()
{

	//set starting level index
	levelIndex = SaveManager::GetInstance()->save[SaveManager::GetInstance()->currentSlotPos].playStage;
	//set starting section changing
	sectionChange = 0;

	//initialize cutscene property
	CutsceneManager::GetInstance()->Initialize();

	SetLoadNewLevel();//laod new level
}

void LevelManager::Spawn_Player()
{

	int tileSize = m_CurrentGameMap->GetTileSize();
	int rowCount = m_CurrentGameMap->GetRowCount();
	int colCount = m_CurrentGameMap->GetColCount();

	//initialize Hero Entity
	Entity& hero(EntityManager::GetInstance()->AddEntity("Hero"));

	//initialize components for Hero entity
	//Transform Component
	if (!m_IsDeLevel) 
	{
		hero.AddComponent<Transform>(m_PlayerStartingPos[levelIndex].x, m_PlayerStartingPos[levelIndex].y);
	}
	else 
	{
		hero.AddComponent<Transform>(m_PlayerDeLevelPos[levelIndex].x, m_PlayerDeLevelPos[levelIndex].y);
	}
	
	//Sprite Component
	hero.AddComponent<Sprite>("Hero-idle", 66, 66, HERO_LAYER, m_PlayerStartingFlip[levelIndex]);
	//SpriteAnimation Component
	hero.AddComponent<SpriteAnimation>();
	//Rigidbody Component
	hero.AddComponent<Rigidbody>();
	//PlayerCollider Component
	hero.AddComponent<PlayerCollider>();
	//Hero Component
	hero.AddComponent<Hero>(colCount * tileSize, rowCount * tileSize);

	//assign a pointer to Hero Component
	m_Player = hero.GetComponent<Hero>();

	//when the player is going to unlock abilities
	//level with starting cutscene event
	if (levelIndex == 7) 
	{
		m_Player->UnlockAbility(false, false, false, false, false);
	}
	//normal level
	else 
	{
		bool isEndGame = CutsceneManager::GetInstance()->IsEndGame() || CutsceneManager::GetInstance()->IsEndGamex2();
		m_Player->UnlockAbility(levelIndex > 0, levelIndex > 3, levelIndex > 2, levelIndex > 7 && !isEndGame, levelIndex > 35);
	}
	

	//get player's hit box 
	SDL_Rect playerHitBox = m_Player->owner->GetComponent<PlayerCollider>()->Get();
	//set hero origin in Camera class
	Camera::GetInstance()->SetHeroOrigin(GetOrigin(playerHitBox));
	//initialize camera
	Camera::GetInstance()->Initialize();

}

void LevelManager::Spawn_Rune()
{

	std::map<int, Vector2D>::iterator it_vec;
	std::map<int, bool>::iterator it_bool;

	it_vec = m_RunePos.find(levelIndex);
	it_bool = m_RuneActive.find(levelIndex);

	if (it_vec != m_RunePos.end() && it_bool != m_RuneActive.end())
	{
		//initialize Altrium Entity
		Entity& Rune(EntityManager::GetInstance()->AddEntity("Rune"));

		//initialize components for Altrium Rune entity
		//Transform Component
		Rune.AddComponent<Transform>((m_RunePos[levelIndex]).x, (m_RunePos[levelIndex]).y);
		//Sprite Component
		Rune.AddComponent<Sprite>("altrium_rune_off", 192, 192, RUNE_LAYER);
		//Collider Component
		Rune.AddComponent<Collider>();
		//SpriteAnimation Component
		Rune.AddComponent<SpriteAnimation>();
		//AltriumRune Component
		Rune.AddComponent<AltriumRune>(m_RuneActive[levelIndex]);

		//add the initialized entity to the vector
		m_LevelObjects.push_back(&Rune);
	}
	
}

void LevelManager::Spawn_Monsters()
{
	//loop through Normal Monster position vector
	for (int i = 0; i < m_NormalMonsterPos[levelIndex].size(); i++)
	{
		//initialize Normal Monster Entity
		Entity& monster(EntityManager::GetInstance()->AddEntity("Monster" + std::to_string(i)));

		//initialize components for Normal Monster entity
		//Transform Component
		monster.AddComponent<Transform>((m_NormalMonsterPos[levelIndex])[i].x, (m_NormalMonsterPos[levelIndex])[i].y);
		//Sprite Component
		monster.AddComponent<Sprite>("normal_monster_idle", 96, 96, MONSTER_LAYER, m_NormalMonsterFlip[levelIndex][i]);
		//Collider Component
		monster.AddComponent<Collider>();
		//SpriteAnimation Component
		monster.AddComponent<SpriteAnimation>();
		//Rigidbody Component
		monster.AddComponent<Rigidbody>();
		//Monster Component
		monster.AddComponent<Monster>();

		//add the initialized entity to the vector
		m_LevelObjects.push_back(&monster);

	}


	//loop through Horizontal Monster position vector
	for (int i = 0; i < m_HorizontalMonsterPos[levelIndex].size(); i++)
	{
		//initialize Horizontal Monster Entity
		Entity& monster(EntityManager::GetInstance()->AddEntity("HorizontalMonster" + std::to_string(i)));

		//initialize components for Horizontal Monster entity
		//Transform Component
		monster.AddComponent<Transform>((m_HorizontalMonsterPos[levelIndex])[i].x, (m_HorizontalMonsterPos[levelIndex])[i].y);
		//Sprite Component
		monster.AddComponent<Sprite>("horizontal_monster_idle", 96, 96, MONSTER_LAYER);
		//Collider Component
		monster.AddComponent<Collider>();
		//SpriteAnimation Component
		monster.AddComponent<SpriteAnimation>();
		//Rigidbody Component
		monster.AddComponent<Rigidbody>();
		//HorizontalMonster Component
		monster.AddComponent<HorizontalMonster>();

		//add the initialized entity to the vector
		m_LevelObjects.push_back(&monster);

	}


	//loop through Verical Monster position vector
	for (int i = 0; i < m_VerticalMonsterPos[levelIndex].size(); i++)
	{
		//initialize Vertical Monster Entity
		Entity& monster(EntityManager::GetInstance()->AddEntity("VerticalMonster" + std::to_string(i)));

		//initialize components for Verical Monster entity
		//Transform Component
		monster.AddComponent<Transform>((m_VerticalMonsterPos[levelIndex])[i].x, (m_VerticalMonsterPos[levelIndex])[i].y);
		//Sprite Component
		monster.AddComponent<Sprite>("vertical_monster_idle", 96, 96, MONSTER_LAYER, m_VerticalMonsterFlip[levelIndex][i]);
		//Collider Component
		monster.AddComponent<Collider>();
		//SpriteAnimation Component
		monster.AddComponent<SpriteAnimation>();
		//Rigidbody Component
		monster.AddComponent<Rigidbody>();
		//VericalMonster Component
		monster.AddComponent<VerticalMonster>();

		//add the initialized entity to the vector
		m_LevelObjects.push_back(&monster);

	}

}

void LevelManager::Spawn_TimeCounter()
{
	//initialize Time Counter Entity
	Entity& timeCounter(EntityManager::GetInstance()->AddEntity("TimeCounter"));

	//initialize components for Time Counter entity
	//Transform Component
	timeCounter.AddComponent<Transform>(0, 0);
	//Text Component
	timeCounter.AddComponent<Text>(TIME_COUNT_LAYER, "TimeCounter");

	//TimeCounter Component
	if (m_StaringTime > 0.0f)
	{
		timeCounter.AddComponent<TimeCounter>(m_StaringTime);
		m_StaringTime = 0.0f;
	}
	else 
	{
		timeCounter.AddComponent<TimeCounter>();
	}

	//assign a pointer to TimeCounter Component
	m_TimeCounter = timeCounter.GetComponent<TimeCounter>();

	if (levelIndex < 8) 
	{
		m_TimeCounter->Disable();
		return;
	}

	//if it is not delevel -> pause the time counter
	if (!m_IsDeLevel) 
	{
		m_TimeCounter->Pause();
	}

	
}

void LevelManager::Spawn_SectionChanger() 
{	

	for (int i = 0; i < 4; i++) 
	{

		if (m_SectionChangerIC[levelIndex][i] == 0) 
		{
			continue;
		}
		else 
		{
			//initialize Section Changer Entity
			Entity& sectionChanger(EntityManager::GetInstance()->AddEntity("SectionChanger" + std::to_string(i)));

			//initialize components for Section Changer entity
			//Transform Component
			sectionChanger.AddComponent<Transform>(0.0f, 0.0f);
			//Collider Component
			sectionChanger.AddComponent<Collider>();
			//get direction
			Direction dir = static_cast<Direction>(i);
			//SectionChanger Component
			sectionChanger.AddComponent<SectionChanger>(m_SectionChangerIC[levelIndex][i], dir, m_SectionChangerPos[levelIndex][i]);

			//add the initialized entity to the vector
			m_LevelObjects.push_back(&sectionChanger);

		}		
		
	}
}

void LevelManager::Spawn_HorizontalMovingTile() 
{
	for (int i = 0; i < m_HorizontalMovingTilePos[levelIndex].size(); i++)
	{
		Entity& movingTile(EntityManager::GetInstance()->AddEntity("MovingTile" + std::to_string(i)));
		movingTile.AddComponent<Transform>((m_HorizontalMovingTilePos[levelIndex])[i].x, (m_HorizontalMovingTilePos[levelIndex])[i].y);
		movingTile.AddComponent<Sprite>("Horizontal_Moving_Tile", 267, 140, MONSTER_LAYER);
		movingTile.AddComponent<Collider>();
		movingTile.AddComponent<Rigidbody>();
		movingTile.AddComponent<HorizontalMovingTile>();

		m_LevelObjects.push_back(&movingTile);
	}
}

void LevelManager::Spawn_Artrius()
{
	if (levelIndex < 8 || levelIndex > 36)
	{
		return;
	}

	Entity& artrius(EntityManager::GetInstance()->AddEntity("Artrius"));

	Vector2D playerPos = m_Player->owner->GetComponent<Transform>()->position;

	artrius.AddComponent<Transform>(playerPos.x, playerPos.y);
	artrius.AddComponent<Sprite>("Artrius_idle", 32, 32, ARTRIUS_LAYER);
	artrius.AddComponent<Collider>();
	artrius.AddComponent<SpriteAnimation>();
	artrius.AddComponent<Rigidbody>();
	artrius.AddComponent<Artrius>();

	m_LevelObjects.push_back(&artrius);

	m_Artrius = artrius.GetComponent<Artrius>();
}

void LevelManager::Spawn_Boss() 
{

	int state = -1;

	//==========================ARCHFIEND==========================

	//set the archfiend state
	//Level-T-8 //Level-A-4
	if (levelIndex == 7 || levelIndex == 32)  
	{
		state = 0;
	}
	//Level-U-8 //Level-A-0 
	else if (levelIndex >= 27 && levelIndex <= 28) 
	{
		state = levelIndex - 26;
	}
	//Level-C-1 //Level-C-2 //Level-C-3
	else if (levelIndex >= 33 && levelIndex <= 35)
	{
		state = levelIndex - 30;
	}

	//if state is valid -> spawn archfiend
	if (state > -1) 
	{
		Entity& archfiend(EntityManager::GetInstance()->AddEntity("Archfiend"));

		archfiend.AddComponent<Archfiend>(state);

		m_LevelObjects.push_back(&archfiend);

		m_BossEntity = &archfiend;

		return;
	}

	//==========================ARCHFIEND==========================


	//===========================ARZEROS===========================

	//set the arzeros state
	//Level-5-1
	if (levelIndex == 37)
	{
		state = 0;
	}
	//Level-E-1 //Level-E-2 //Level-E-3 //Level-F-1 //Level-F-2 //Level-F-3
	else if (levelIndex >= 43 && levelIndex <= 48)
	{
		state = levelIndex - 43;
	}

	//if state is valid -> spawn arzeros
	if (state > -1)
	{
		Entity& arzeros(EntityManager::GetInstance()->AddEntity("Arzeros"));

		arzeros.AddComponent<Arzeros>(state);

		m_LevelObjects.push_back(&arzeros);

		m_BossEntity = &arzeros;

		return;
	}

	//===========================ARZEROS===========================


	//===========================FINAL_ARZEROS===========================

	//set the arzeros state
	//Level-G-1
	if (levelIndex == 49)
	{
		state = 0;
	}

	//if state is valid -> spawn arzeros
	if (state > -1)
	{
		Entity& finalArzeros(EntityManager::GetInstance()->AddEntity("FinalArzeros"));

		finalArzeros.AddComponent<FinalArzeros>();

		m_LevelObjects.push_back(&finalArzeros);

		//get princess collider
		SDL_Rect princessCollider = m_Princess->owner->GetComponent<Collider>()->Get();
		//set the princess collider
		finalArzeros.GetComponent<FinalArzeros>()->SetPrincessCollider(princessCollider);

		m_BossEntity = &finalArzeros;
	}

	//===========================FINAL_ARZEROS===========================
}

void LevelManager::Spawn_Princess() 
{
	//if there is no princess is this level -> return the function instantly
	if (m_PrincessPos.find(levelIndex) == m_PrincessPos.end())
	{
		return;
	}

	//initialize Princess Entity
	Entity& princess(EntityManager::GetInstance()->AddEntity("Princess"));

	//initialize components for Princess entity
	//Transform Component
	princess.AddComponent<Transform>(m_PrincessPos[levelIndex].x, m_PrincessPos[levelIndex].y);
	//Sprite Component
	princess.AddComponent<Sprite>("princess_idle_cage", 128, 104, HERO_LAYER, m_PrincessFlip[levelIndex]);
	//SpriteAnimation Component
	princess.AddComponent<SpriteAnimation>();
	//Collider Component
	princess.AddComponent<Collider>();
	//Princess Component
	princess.AddComponent<Princess>();

	m_Princess = princess.GetComponent<Princess>();
}

void LevelManager::Spawn_TutorialUI() 
{
	bool isEndGame = CutsceneManager::GetInstance()->IsEndGame() || CutsceneManager::GetInstance()->IsEndGamex2();
	//declare iterator to find the tutorial UI property
	std::map<int, Vector2D>::iterator it = m_TutorialPos.find(levelIndex);

	//if there is a spawning property
	if (!isEndGame && it != m_TutorialPos.end())
	{
		//initialize Tutorial UI Entity
		Entity& tui(EntityManager::GetInstance()->AddEntity("TutorialUI"));

		//initialize components for Altrium Rune entity
		//Transform Component
		tui.AddComponent<Transform>((m_TutorialPos[levelIndex]).x, (m_TutorialPos[levelIndex]).y);
		//Tutorial UI Component
		tui.AddComponent<TutorialUI>(levelIndex);
	}
}

//====================INITIALIZE FUNCTION====================

void LevelManager::Clean() 
{
	DialogueManager::GetInstance()->Reset();
	//destroy all entities
	EntityManager::GetInstance()->ClearData();
	//immediately destroy all inactive entities
	EntityManager::GetInstance()->DestroyInActiveEntities();
	
	//clear level object vector
	m_LevelObjects.clear(); 

	//pointer to Hero Componetn is nullptr
	m_Player = nullptr; 
	//pointer to TimeCounter is nullptr
	m_TimeCounter = nullptr;
	//pointer to Princess is nullptr
	m_Princess = nullptr;
	//pointer to boss entity is nullptr
	m_BossEntity = nullptr;
	//pointer to Artrius is nullptr
	m_Artrius = nullptr;

}


//====================TIME COUNTER FUNCTION====================

void LevelManager::DecreaseTime(float amount)
{
	m_TimeCounter->DecreaseTimeCounter(amount);
}

void LevelManager::PauseTime()
{
	m_TimeCounter->Pause();
}

void LevelManager::ResumeTime()
{
	m_TimeCounter->Resume();
}

float LevelManager::GetTime() 
{
	return m_TimeCounter->GetTimeCount();
}

void LevelManager::SetStartingTime(float time)
{
	m_StaringTime = time;
}

void LevelManager::BlinkTime() 
{
	m_TimeCounter->Blink();
}

void LevelManager::StopBlinkTime() 
{
	m_TimeCounter->StopBlink();
}

//====================TIME COUNTER FUNCTION====================



//====================SET FUNCTION====================


void LevelManager::AssignPlayerToLevelObjects(Hero* player)
{
	//loop through the vector of Level Objects
	for (int i = 0; i < m_LevelObjects.size(); i++) 
	{
		//if it is Horizontal Monster
		if (m_LevelObjects[i]->HasComponent<HorizontalMonster>())
		{
			m_LevelObjects[i]->GetComponent<HorizontalMonster>()->SetPlayer(player);
		}
		//if it is Vertical Monster
		else if (m_LevelObjects[i]->HasComponent<VerticalMonster>())
		{
			m_LevelObjects[i]->GetComponent<VerticalMonster>()->SetPlayer(player);
		}
		//if it is Normal Monster
		else if (m_LevelObjects[i]->HasComponent<Monster>())
		{
			m_LevelObjects[i]->GetComponent<Monster>()->SetPlayer(player);
		}
		//if it is Altrium Rune
		else if (m_LevelObjects[i]->HasComponent<AltriumRune>())
		{
			m_LevelObjects[i]->GetComponent<AltriumRune>()->SetPlayer(player);
		}
		//if it is Section Changer
		else if (m_LevelObjects[i]->HasComponent<SectionChanger>())
		{
			m_LevelObjects[i]->GetComponent<SectionChanger>()->SetPlayer(player);
		}
		//if it is Horizontal Moving Tile
		else if (m_LevelObjects[i]->HasComponent<HorizontalMovingTile>()) 
		{
			m_LevelObjects[i]->GetComponent<HorizontalMovingTile>()->SetPlayer(player);
		}
		//if it is Artrius
		else if (m_LevelObjects[i]->HasComponent<Artrius>())
		{
			m_LevelObjects[i]->GetComponent<Artrius>()->SetPlayer(player);
		}
		//if it is Archfiend
		else if (m_LevelObjects[i]->HasComponent<Archfiend>())
		{
			m_LevelObjects[i]->GetComponent<Archfiend>()->SetPlayer(player);
		}
		//if it is Arzeros
		else if (m_LevelObjects[i]->HasComponent<Arzeros>())
		{
			m_LevelObjects[i]->GetComponent<Arzeros>()->SetPlayer(player);
		}
		//if it is FinalArzeros
		else if (m_LevelObjects[i]->HasComponent<FinalArzeros>())
		{
			m_LevelObjects[i]->GetComponent<FinalArzeros>()->SetPlayer(player);
		}
	}
}

void LevelManager::Set_RuneProp()
{
	//set Altrium Rune position

	int index;

	//level-0-1

	index = 8;

	m_RunePos[index] = Vector2D(1440.0f, 720.0f);
	m_RuneActive[index] = true;



	//level-1-1

	index = 9;

	m_RunePos[index] = Vector2D(0.0f, 1200.0f);
	m_RuneActive[index] = false;



	//level-1-3

	index = 11;

	m_RunePos[index] = Vector2D(1488.0f, 96.0f);
	m_RuneActive[index] = true;



	//level-2-1

	index = 12;

	m_RunePos[index] = Vector2D(0.0f, 720.0f);
	m_RuneActive[index] = false;



	//level-2-3

	index = 14;

	m_RunePos[index] = Vector2D(1536.0f, 144.0f);
	m_RuneActive[index] = true;



	//level-3-1

	index = 15;

	m_RunePos[index] = Vector2D(0.0f, 144.0f);
	m_RuneActive[index] = false;



	//level-3-2

	index = 16;

	m_RunePos[index] = Vector2D(1344.0f, 1344.0f);
	m_RuneActive[index] = true;



	//level-4-1

	index = 17;

	m_RunePos[index] = Vector2D(48.0f, 1440.0f);
	m_RuneActive[index] = false;



	//level-4-3

	index = 19;

	m_RunePos[index] = Vector2D(1488.0f, 96.0f);
	m_RuneActive[index] = true;



	//level-U-1

	index = 20;

	m_RunePos[index] = Vector2D(48.0f, 720.0f);
	m_RuneActive[index] = false;



	//level-U-7

	index = 26;

	m_RunePos[index] = Vector2D(0.0f, 96.0f);
	m_RuneActive[index] = true;



	//level-U-8

	index = 27;

	m_RunePos[index] = Vector2D(1440.0f, 720.0f);
	m_RuneActive[index] = false;



	//level-A-0

	index = 28;

	m_RunePos[index] = Vector2D(768.0f, 720.0f);
	m_RuneActive[index] = true;



	//level-A-1

	index = 29;

	m_RunePos[index] = Vector2D(1440.0f, 1440.0f);
	m_RuneActive[index] = false;



	//level-C-1

	index = 33;

	m_RunePos[index] = Vector2D(1440.0f, 1440.0f);
	m_RuneActive[index] = false;



	//Level-D-1

	index = 36;

	m_RunePos[index] = Vector2D(0.0f, 432.0f);
	m_RuneActive[index] = false;



	//Level-5-1

	index = 37;

	m_RunePos[index] = Vector2D(0.0f, 432.0f);
	m_RuneActive[index] = false;



	//Level-5-3

	index = 39;

	m_RunePos[index] = Vector2D(1344.0f, 1920.0f);
	m_RuneActive[index] = true;



	//Level-6-1

	index = 40;

	m_RunePos[index] = Vector2D(48.0f, 240.0f);
	m_RuneActive[index] = false;



	//Level-6-3

	index = 42;

	m_RunePos[index] = Vector2D(1488.0f, 336.0f);
	m_RuneActive[index] = true;



	//Level-E-1

	index = 43;

	m_RunePos[index] = Vector2D(0.0f, 672.0f);
	m_RuneActive[index] = false;



	//Level-E-3

	index = 45;

	m_RunePos[index] = Vector2D(0.0f, 1968.0f);
	m_RuneActive[index] = true;



	//Level-F-1

	index = 46;

	m_RunePos[index] = Vector2D(1488.0f, 720.0f);
	m_RuneActive[index] = false;



	//Level-F-3

	index = 48;

	m_RunePos[index] = Vector2D(0.0f, 528.0f);
	m_RuneActive[index] = true;

}

void LevelManager::Set_NormalMonsterProp()
{
	//set Normal Monster position
	//set Normal Monster flip

	int index;

	//level-1-1

	index = 9;

	m_NormalMonsterPos[index].push_back(Vector2D(1104.0f, 192.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-1-2

	index = 10;

	m_NormalMonsterPos[index].push_back(Vector2D(960.0f, 720.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(720.0f, 192.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);


	//level-1-3

	index = 11;

	m_NormalMonsterPos[index].push_back(Vector2D(768.0f, 480.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1056.0f, 336.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-2-1

	index = 12;

	m_NormalMonsterPos[index].push_back(Vector2D(1152.0f, 144.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);


	//level-2-2

	index = 13;

	m_NormalMonsterPos[index].push_back(Vector2D(240.0f, 144.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);


	//level-2-3

	index = 14;

	m_NormalMonsterPos[index].push_back(Vector2D(816.0f, 336.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-3-1

	index = 15;

	m_NormalMonsterPos[index].push_back(Vector2D(768.0f, 1152.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1344.0f, 1488.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1440.0f, 1632.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-3-2

	index = 16;

	m_NormalMonsterPos[index].push_back(Vector2D(240.0f, 1248.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);


	//level-4-2

	index = 18;

	m_NormalMonsterPos[index].push_back(Vector2D(672.0f, 720.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(864.0f, 720.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(528.0, 480.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1008.0f, 480.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-4-3

	index = 19;

	m_NormalMonsterPos[index].push_back(Vector2D(1152.0f, 48.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-A-2

	index = 30;

	m_NormalMonsterPos[index].push_back(Vector2D(1344.0f, 624.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1584.0f, 576.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-A-3

	index = 31;

	m_NormalMonsterPos[index].push_back(Vector2D(720.0f, 624.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1008.0f, 528.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-5-2

	index = 38;

	m_NormalMonsterPos[index].push_back(Vector2D(912.0f, 1008.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_NormalMonsterPos[index].push_back(Vector2D(1104.0f, 1104.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);


	//level-5-3

	index = 39;

	m_NormalMonsterPos[index].push_back(Vector2D(1248.0f, 336.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1392.0f, 528.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-6-1

	index = 40;

	m_NormalMonsterPos[index].push_back(Vector2D(624.0f, 336.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1008.0f, 384.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-6-2

	index = 41;

	m_NormalMonsterPos[index].push_back(Vector2D(1248.0f, 1776.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_NormalMonsterPos[index].push_back(Vector2D(864.0f, 1392.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_NormalMonsterPos[index].push_back(Vector2D(1104.0f, 1440.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_NormalMonsterPos[index].push_back(Vector2D(768.0f, 192.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_NormalMonsterPos[index].push_back(Vector2D(528.0f, 144.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);


	//level-6-3

	index = 42;

	m_NormalMonsterPos[index].push_back(Vector2D(480.0f, 1104.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_NormalMonsterPos[index].push_back(Vector2D(288.0f, 912.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_NormalMonsterPos[index].push_back(Vector2D(432.0f, 720.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-E-2

	index = 44;

	m_NormalMonsterPos[index].push_back(Vector2D(384.0f, 170.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-E-3

	index = 45;
	m_NormalMonsterPos[index].push_back(Vector2D(600.0f, 72.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
	m_NormalMonsterPos[index].push_back(Vector2D(1872.0f, 1248.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);


	//level-F-2

	index = 47;
	m_NormalMonsterPos[index].push_back(Vector2D(912.0f, 1296.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_NormalMonsterPos[index].push_back(Vector2D(672.0f, 1344.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);

}

void LevelManager::Set_HorizonalMonsterProp()
{
	//set Horizontal Monster position
	//set Horizontal Monster flip

	int index;


	//level-2-2

	index = 13;

	m_HorizontalMonsterPos[index].push_back(Vector2D(768.0f, 288.0f));


	//level-3-1

	index = 15;

	m_HorizontalMonsterPos[index].push_back(Vector2D(1104.0f, 1738.0f));


	//level-4-1

	index = 17;

	m_HorizontalMonsterPos[index].push_back(Vector2D(960.0f, 336.0f));


	//level-4-3

	index = 19;
	m_HorizontalMonsterPos[index].push_back(Vector2D(1008.0f, 1008.0f));
	m_HorizontalMonsterPos[index].push_back(Vector2D(1488.0f, 960.0f));


	//level-A-3

	index = 31;
	m_HorizontalMonsterPos[index].push_back(Vector2D(1536.0f, 912.0f));


	//level-5-3

	index = 39;
	m_HorizontalMonsterPos[index].push_back(Vector2D(1200.0f, 144.0f));
	m_HorizontalMonsterPos[index].push_back(Vector2D(1056.0f, 960.0f));
	m_HorizontalMonsterPos[index].push_back(Vector2D(864.0f, 1536.0f));


	//level-6-1

	index = 40;
	m_HorizontalMonsterPos[index].push_back(Vector2D(1872.0f, 480.0f));


	//level-6-2

	index = 41;
	m_HorizontalMonsterPos[index].push_back(Vector2D(1152.0f, 1872.0f));
	m_HorizontalMonsterPos[index].push_back(Vector2D(1296.0f, 720.0f));


	//level-6-3

	index = 42;
	m_HorizontalMonsterPos[index].push_back(Vector2D(624.0f, 432.0f));


	//level-E-3

	index = 45;
	m_HorizontalMonsterPos[index].push_back(Vector2D(1296.0f, 480.0f));
	m_HorizontalMonsterPos[index].push_back(Vector2D(1824.0f, 480.0f));


	//level-F-2

	index = 47;
	m_HorizontalMonsterPos[index].push_back(Vector2D(960.0f, 1872.0f));

}

void LevelManager::Set_VerticalMonsterProp()
{
	//set Vertical Monster position
	//set Vertical Monster flip

	int index;


	//level-2-3

	index = 14;

	m_VerticalMonsterPos[index].push_back(Vector2D(1152.0f, 144.0f));
	m_VerticalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-A-2

	index = 30;

	m_VerticalMonsterPos[index].push_back(Vector2D(1104.0f, 480.0f));
	m_VerticalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-C-1

	index = 33;

	m_VerticalMonsterPos[index].push_back(Vector2D(696.0f, 240.0f));
	m_VerticalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-6-1

	index = 40;

	m_VerticalMonsterPos[index].push_back(Vector2D(1632.0f, 48.0f));
	m_VerticalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-6-3

	index = 42;

	m_VerticalMonsterPos[index].push_back(Vector2D(912.0f, 1152.0f));
	m_VerticalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);
	m_VerticalMonsterPos[index].push_back(Vector2D(816.0f, 96.0f));
	m_VerticalMonsterFlip[index].push_back(SDL_FLIP_NONE);


	//level-F-2

	index = 47;

	m_VerticalMonsterPos[index].push_back(Vector2D(432.0f, 1152.0f));
	m_VerticalMonsterFlip[index].push_back(SDL_FLIP_HORIZONTAL);

}

void LevelManager::Set_HeroProp()
{
	//set Hero starting position
	//set Hero starting flip

	//level-T-1
	m_PlayerStartingPos.push_back(Vector2D(98.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(816.0f, 0.0f));

	//level-T-2
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-T-3
	m_PlayerStartingPos.push_back(Vector2D(1534.0f, 526.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-T-4
	m_PlayerStartingPos.push_back(Vector2D(1534.0f, 1538.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-T-5
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-T-6
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 1298.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-T-7
	m_PlayerStartingPos.push_back(Vector2D(1538.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-T-8
	m_PlayerStartingPos.push_back(Vector2D(1534.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-0-1
	m_PlayerStartingPos.push_back(Vector2D(530.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-1-1
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 1298.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-1-2
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 1058.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-1-3
	m_PlayerStartingPos.push_back(Vector2D(98.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-2-1
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(482.0f, 50.0f));

	//level-2-2
	m_PlayerStartingPos.push_back(Vector2D(1202.0f, 1058.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(1392.0f, 50.0f));

	//level-2-3
	m_PlayerStartingPos.push_back(Vector2D(98.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-3-1
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 242.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-3-2
	m_PlayerStartingPos.push_back(Vector2D(1442.0f, 50.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-4-1
	m_PlayerStartingPos.push_back(Vector2D(98.0f, 1538.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(530.0f, 0.0f));

	//level-4-2
	m_PlayerStartingPos.push_back(Vector2D(1538.0f, 1106.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-4-3
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 1298.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-U-1
	m_PlayerStartingPos.push_back(Vector2D(98.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(816.0f, 0.0f));

	//level-U-2
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-U-3
	m_PlayerStartingPos.push_back(Vector2D(1534.0f, 526.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-U-4
	m_PlayerStartingPos.push_back(Vector2D(1534.0f, 1538.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-U-5
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-U-6
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 1298.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-U-7
	m_PlayerStartingPos.push_back(Vector2D(1538.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-U-8
	m_PlayerStartingPos.push_back(Vector2D(1534.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-A-0
	m_PlayerStartingPos.push_back(Vector2D(1538.0f, 818.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-A-1
	m_PlayerStartingPos.push_back(Vector2D(1538.0f, 1538.0f)); //actual spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-A-2
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 914.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-A-3
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 914.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-A-4
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 530.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-C-1
	m_PlayerStartingPos.push_back(Vector2D(1538.0f, 1538.0f)); //actual spawning position
	//m_PlayerStartingPos.push_back(Vector2D(960.0f, 192.0f)); //testing spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-C-2
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 914.0f)); //actual spawning position
	//m_PlayerStartingPos.push_back(Vector2D(1776.0f, 960.0f)); //testing spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-C-3
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 914.0f)); //actual spawning position
	//m_PlayerStartingPos.push_back(Vector2D(1776.0f, 960.0f)); //testing spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-D-1
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 530.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-5-1
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 530.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-5-2
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 242.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-5-3
	m_PlayerStartingPos.push_back(Vector2D(98.0f, 50.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-6-1
	m_PlayerStartingPos.push_back(Vector2D(98.0f, 338.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(1394.0f, 0.0f));

	//level-6-2
	m_PlayerStartingPos.push_back(Vector2D(98.0f, 2162.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-6-3
	m_PlayerStartingPos.push_back(Vector2D(1774.0f, 1490.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-E-1
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 770.0f));
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-E-2
	m_PlayerStartingPos.push_back(Vector2D(144.0f, 1058.0f)); //actual spawning position
	//m_PlayerStartingPos.push_back(Vector2D(1392.0f, 864.0f)); //testing spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-E-3
	m_PlayerStartingPos.push_back(Vector2D(50.0f, 290.0f)); //actual spawning position
	//m_PlayerStartingPos.push_back(Vector2D(1056.0f, 2160.0f)); //testing spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_NONE);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-F-1
	m_PlayerStartingPos.push_back(Vector2D(1586.0f, 818.0f)); //actual spawning position
	//m_PlayerStartingPos.push_back(Vector2D(1056.0f, 2160.0f)); //testing spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-F-2
	m_PlayerStartingPos.push_back(Vector2D(1490.0f, 2258.0f)); //actual spawning position
	//m_PlayerStartingPos.push_back(Vector2D(1344.0f, 480.0f)); //testing spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-F-3
	m_PlayerStartingPos.push_back(Vector2D(2450.0f, 722.0f)); //actual spawning position
	//m_PlayerStartingPos.push_back(Vector2D(1152.0f, 720.0f)); //testing spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-G-1
	m_PlayerStartingPos.push_back(Vector2D(1394.0f, 722.0f)); //actual spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

	//level-H-1
	m_PlayerStartingPos.push_back(Vector2D(866.0f, 530.0f)); //actual spawning position
	m_PlayerStartingFlip.push_back(SDL_FLIP_HORIZONTAL);
	m_PlayerDeLevelPos.push_back(Vector2D(0.0f, 0.0f));

}

void LevelManager::Set_SectionChangerProp() 
{
	//set Section Changer changing index
	//set Section Changer image position

	int index;

	//level-T-1

	index = 0;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(1632.0f, 768.0f);


	//level-T-2

	index = 1;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 96.0f);


	//level-T-3

	index = 2;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = -2;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 528.0f);


	//level-T-4

	index = 3;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(1632.0f, 144.0f);


	//level-T-5

	index = 4;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 528.0f);


	//level-T-6

	index = 5;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 1;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][UP] = Vector2D(1440.0f, 0.0f);


	//level-T-7
	
	index = 6;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 144.0f);


	//level-0-1
	
	index = 8;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;


	//level-1-1

	index = 9;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 144.0f);


	//level-1-2

	index = 10;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 1;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][UP] = Vector2D(144.0f, 0.0f);


	//level-2-1

	index = 12;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 1;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][UP] = Vector2D(1104.0f, 0.0f);


	//level-2-2

	index = 13;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 1;
	m_SectionChangerIC[index][DOWN] = -1;

	m_SectionChangerPos[index][UP] = Vector2D(192.0f, 0.0f);


	//level-2-3

	index = 14;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = -1;


	//level-3-1

	index = 15;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 1;

	m_SectionChangerPos[index][DOWN] = Vector2D(816.0f, 0.0f);


	//level-4-1

	index = 17;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 336.0f);


	//level-4-2

	index = 18;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 1;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][UP] = Vector2D(816.0f, 0.0f);


	//level-4-3

	index = 19;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = -2;


	//level-U-1

	index = 20;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(1632.0f, 768.0f);


	//level-U-2

	index = 21;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 96.0f);


	//level-U-3

	index = 22;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = -2;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 528.0f);


	//level-U-4

	index = 23;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(1632.0f, 144.0f);


	//level-U-5

	index = 24;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 528.0f);


	//level-U-6

	index = 25;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 1;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][UP] = Vector2D(1440.0f, 0.0f);


	//level-U-8

	index = 27;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 384.0f);


	//level-A-1

	index = 29;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 144.0f);


	//level-A-2

	index = 30;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 576.0f);


	//level-A-3

	index = 31;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 672.0f);


	//level-C-1

	index = 33;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 144.0f);


	//level-C-2

	index = 34;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 576.0f);


	//level-C-3

	index = 35;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;


	//level-5-1

	index = 37;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 672.0f);


	//level-5-2

	index = 38;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 1;

	m_SectionChangerPos[index][DOWN] = Vector2D(144.0f, 0.0f);


	//level-6-1

	index = 40;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 768.0f);


	//level-6-2

	index = 41;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 144.0f);


	//level-6-3

	index = 42;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = -2;


	//level-E-1

	index = 43;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 1;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][UP] = Vector2D(912.0f, 0.0f);


	//level-E-2

	index = 44;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 1;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][RIGHT] = Vector2D(0.0f, 912.0f);


	//level-F-1

	index = 46;

	m_SectionChangerIC[index][LEFT] = 0;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 1;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][UP] = Vector2D(336.0f, 0.0f);


	//level-F-2

	index = 47;

	m_SectionChangerIC[index][LEFT] = 1;
	m_SectionChangerIC[index][RIGHT] = 0;
	m_SectionChangerIC[index][UP] = 0;
	m_SectionChangerIC[index][DOWN] = 0;

	m_SectionChangerPos[index][LEFT] = Vector2D(0.0f, 336.0f);

}

void LevelManager::Set_HorizontalMovingTile() 
{
	//set Horizontal Moving Tile position

	int index;

	////level-3-2

	index = 16;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(1200.0f, 192.0f));
	m_HorizontalMovingTilePos[index].push_back(Vector2D(1056.0f, 960.0f));
	m_HorizontalMovingTilePos[index].push_back(Vector2D(1104.0f, 1392.0f));


	////level-4-1

	index = 17;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(1248.0f, 912.0f));
	m_HorizontalMovingTilePos[index].push_back(Vector2D(960.0f, 432.0f));


	////level-4-2

	index = 18;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(1056.0f, 144.0f));


	////level-4-3

	index = 19;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(1008.0f, 480.0f));


	////level-A-3

	index = 31;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(816.0f, 720.0f));


	////level-5-3

	index = 39;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(960.0f, 1632.0f));


	////level-6-2

	index = 41;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(1152.0f, 2112.0f));
	m_HorizontalMovingTilePos[index].push_back(Vector2D(1200.0f, 960.0f));
	m_HorizontalMovingTilePos[index].push_back(Vector2D(1200.0f, 384.0f));


	////level-E-3

	index = 45;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(1584.0f, 576.0f));


	////level-F-2

	index = 47;

	m_HorizontalMovingTilePos[index].push_back(Vector2D(1104.0f, 912.0f));

}

void LevelManager::Set_PrincessProp() 
{
	//set Princess starting position
	//set Princess starting flip
	//[sprie height] = 104

	int index;


	//level-T-8

	index = 7;

	m_PrincessPos[index] = Vector2D(240.0f, 424.0f); //y-axis = [floor tile] - [sprite height]
	m_PrincessFlip[index] = SDL_FLIP_NONE;


	//level-A-4

	index = 32;

	m_PrincessPos[index] = Vector2D(816.0f, 568); //y-axis = [floor tile] - [sprite height]
	m_PrincessFlip[index] = SDL_FLIP_HORIZONTAL;


	//level-D-1

	index = 36;

	m_PrincessPos[index] = Vector2D(816.0f, 568); //y-axis = [floor tile] - [sprite height]
	m_PrincessFlip[index] = SDL_FLIP_HORIZONTAL;


	//level-5-1

	index = 37;

	m_PrincessPos[index] = Vector2D(816.0f, 568); //y-axis = [floor tile] - [sprite height]
	m_PrincessFlip[index] = SDL_FLIP_HORIZONTAL;


	//level-G-1

	index = 49;

	m_PrincessPos[index] = Vector2D(750.0f, 568.0f); //y-axis = [floor tile] - [sprite height]
	m_PrincessFlip[index] = SDL_FLIP_NONE;


	//level-H-1

	index = 50;

	m_PrincessPos[index] = Vector2D(750.0f, 568.0f); //y-axis = [floor tile] - [sprite height]
	m_PrincessFlip[index] = SDL_FLIP_NONE;
}

void LevelManager::Set_TutorialUIProp() 
{
	//set Tutorial UI starting position

	int index;


	//level-T-1

	index = 0;

	m_TutorialPos[index] = Vector2D(240.0f, 720.0f);


	//level-T-2

	index = 1;

	m_TutorialPos[index] = Vector2D(240.0f, 720.0f);


	//level-T-3

	index = 2;

	m_TutorialPos[index] = Vector2D(1104.0f, 672.0f);


	//level-T-4

	index = 3;

	m_TutorialPos[index] = Vector2D(1104.0f, 1344.0f);


	//level-T-5

	index = 4;

	m_TutorialPos[index] = Vector2D(864.0f, 720.0f);


	//level-T-6

	index = 5;

	m_TutorialPos[index] = Vector2D(240.0f, 960.0f);


	//level-1-1

	index = 9;

	m_TutorialPos[index] = Vector2D(384.0f, 1152.0f);


	//level-5-1

	index = 37;

	m_TutorialPos[index] = Vector2D(288.0f, 624.0f);


	//level-5-2

	index = 38;

	m_TutorialPos[index] = Vector2D(720.0f, 144.0f);
}

//====================SET FUNCTION====================


Hero* LevelManager::GetHero() 
{
	return m_Player;
}

Princess* LevelManager::GetPrincess() 
{
	return m_Princess;
}

std::string LevelManager::GetLevelName() 
{
	return m_AllLevels[levelIndex];
}

Entity* LevelManager::GetBossEntity() 
{
	return m_BossEntity;
}

Artrius* LevelManager::GetArtrius() 
{
	return m_Artrius;
}

void LevelManager::EasterEgg()
{
	//level-T-7

	int index = 6;

	m_NormalMonsterPos[index].push_back(Vector2D(672.0f, 288.0f));
	m_NormalMonsterFlip[index].push_back(SDL_FLIP_NONE);
}