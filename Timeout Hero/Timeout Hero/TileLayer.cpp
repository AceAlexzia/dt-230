#include "TileLayer.h"
#include "TextureManager.h"
#include "Vector2D.h"
#include "Camera.h"

TileLayer::TileLayer(int tileSize, int rowCount, int colCount, TileMap tileMap, TileSetList tilesets)
	:m_TileSize(tileSize), m_RowCount(rowCount), m_ColCount(colCount), m_TileMap(tileMap), m_TileSets(tilesets)
{
	for (unsigned int i = 0; i < m_TileSets.size(); i++) 
	{
		TextureManager::GetInstance()->Load(m_TileSets[i].Name, LEVEL_PATH + m_TileSets[i].Source);
	}
}


TileMap TileLayer::GetTileMap()
{ 
	return m_TileMap; 
}


void TileLayer::Render()
{
	//get camera rect
	SDL_Rect cam = Camera::GetInstance()->GetViewBox();

	TileSet ts = m_TileSets[0];

	//set rendering scope
	int top_tile = valueCap(cam.y / ts.TileSize, m_RowCount);
	int bottom_tile = valueCap((cam.y + cam.h) / ts.TileSize, m_RowCount);
	int left_tile = valueCap(cam.x / ts.TileSize, m_ColCount);
	int right_tile = valueCap((cam.x + cam.w) / ts.TileSize, m_ColCount);

	for (unsigned int i = top_tile; i <= bottom_tile; i++) {
		for (unsigned int j = left_tile; j <= right_tile; j++) {
			int tileID = m_TileMap[i][j];
			//TileSet ts = m_TileSets[0];
			if (tileID != 0)
			{
				int index = 0;
				if (m_TileSets.size() > 1) 
				{
					for (unsigned int k = 1; k < m_TileSets.size(); k++) {
						if (tileID > m_TileSets[k].FirstID && tileID < m_TileSets[k].LastID) 
						{
							tileID = tileID + m_TileSets[k].TileCount - m_TileSets[k].LastID;
							index = k;
							break;
						}
					}
				}

				ts = m_TileSets[index];

				int tileRow = tileID / ts.ColCount;
				int tileCol = tileID - tileRow * ts.ColCount - 1;
				// If this tile is on the last column
				if (tileID % ts.ColCount == 0) {
					tileRow--;
					tileCol = ts.ColCount - 1;
				}

				// Draw tile
				TextureManager::GetInstance()->DrawTile(ts.Name, ts.TileSize, j * ts.TileSize, i * ts.TileSize, tileRow, tileCol);
			}
		}
	}
}
