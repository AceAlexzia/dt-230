#include "SpriteAnimationComponent.h"

void SpriteAnimation::Initialize()
{
	m_Sprite = owner->GetComponent<Sprite>();
	m_IsEnded = false;
}

void SpriteAnimation::Update(float dt)
{
	if (m_AnimSpeed <= 0) 
	{
		m_Sprite->SetSpriteFrame(m_CurrentFrame);
		m_IsEnded = true;
		return;
	}

	m_TimeElapsed += dt;
	
	if (!m_IsEnded) //|| m_Repeat)
	{
		m_IsEnded = false;

		m_ChangingFrame = ((int)(m_TimeElapsed * 1000.0f) / m_AnimSpeed) % m_FrameCount;

		if (m_PrevFrame == -1)
		{
			m_PrevFrame = m_ChangingFrame;
		}
		else if (m_PrevFrame != m_ChangingFrame)
		{
			m_PrevFrame = m_ChangingFrame;
			m_CurrentFrame++;
			if (m_Repeat && m_CurrentFrame >= m_FrameCount)
			{
				m_CurrentFrame = 0;
			}
		}

	}

	if (!m_Repeat && m_CurrentFrame == m_FrameCount - 1)
	{
		m_IsEnded = true;
		m_CurrentFrame = m_FrameCount - 1;
	}

	m_Sprite->SetSpriteFrame(m_CurrentFrame);

}


bool SpriteAnimation::GetIsEnded()
{
	return m_IsEnded;
}

int SpriteAnimation::GetAnimationSpeed()
{
	return m_AnimSpeed;
}


void SpriteAnimation::SetProps(std::string assetTextureId, int frameCount, int animSpeed, bool repeat)
{
	m_TextureID = assetTextureId;
	m_FrameCount = frameCount;
	m_AnimSpeed = animSpeed;
	m_Repeat = repeat;

	//when changing animation id
	if (m_PrevTextureID != m_TextureID)
	{
		m_PrevTextureID = m_TextureID;
		m_CurrentFrame = 0;
		m_PrevFrame = -1;
		m_IsEnded = false;
		m_Sprite->SetTexture(m_TextureID);
		m_TimeElapsed = 0.0f;
	}
}