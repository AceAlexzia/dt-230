#include "Vector2D.h"

Vector2D::Vector2D(float x, float y) : x(x), y(y) {}


Vector2D Vector2D::operator+(const Vector2D& v2) const
{
	return Vector2D(x + v2.x, y + v2.y);
}

Vector2D Vector2D::operator-(const Vector2D& v2) const
{
	return Vector2D(x - v2.x, y - v2.y);
}

Vector2D Vector2D::operator*(const float scalar) const
{
	return Vector2D(x * scalar, y * scalar);
}


/*Vector2D Vector2D::GetNormalize() 
{
	float magnitude = sqrt((x * x) + (y * y));
	return Vector2D(x / magnitude, y / magnitude);
}*/


void Vector2D::Log(std::string msg)
{
	std::cout << msg << "(X, Y) = (" << x << ", " << y << ")" << std::endl;
}


float Vector2D::Magnitude()
{
	return sqrt((x * x) + (y * y));
}

Vector2D Vector2D::Normalize() 
{
	float magnitude = Magnitude();
	return *this = Vector2D(x / magnitude, y / magnitude);
}