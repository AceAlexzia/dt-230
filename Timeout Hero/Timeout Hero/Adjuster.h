#pragma once
#ifndef ADJUSTER_H
#define ADJUSTER_H

#include "Entity.h"
#include "UIComponent.h"
#include "TransformComponent.h"
#include "SpriteComponent.h"
#include "TextComponent.h"
#include "Button.h"
#include "Vector2D.h"
#include "AdjustArrow.h"
#include "SaveManager.h"
#include <sstream>
class Adjuster :public UIComponent
{
	public:
		Adjuster(int x, int y, std::vector<std::string> a, int now ,bool selecting = false);
		void Initialize() override;
		void Update(float deltaTime) override;
		void AdjustIndex();
		void SaveValue();
		Entity* SelectUI(int direction);

		Vector2D canvasPos;
		Entity* leftButton;
		Entity* rightButton;
		Entity* adjusterText;
		int currentIndex;
		bool isSet = false;
		std::vector<std::string> setIndex;

	private:
		SDL_Color color = { 255,255,255,255};
		Vector2D getPos;
};

#endif