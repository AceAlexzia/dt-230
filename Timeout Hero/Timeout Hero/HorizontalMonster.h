#pragma once
#ifndef HORIZONTAL_MONSTER_H
#define HORIZONTAL_MONSTER_H

#define H_MONSTER_SPEED 250
#define H_MONSTER_TIME_LENGTH 1.5f

#include "Monster.h"
#include "RigidbodyComponent.h"

class HorizontalMonster : public Monster
{
	public:
		HorizontalMonster() {}
		~HorizontalMonster() {}
		void Initialize() override;
		void Update(float deltaTime) override;

	private:
		Rigidbody* m_Rb;
		Vector2D m_MovingDirection = Vector2D(-1, 0);
		float m_TimeLength = 0.0f;
};

#endif