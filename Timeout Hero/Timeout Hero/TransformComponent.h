#ifndef TRANSFORMCOMPONENT_H
#define TRANSFORMCOMPONENT_H

#include <SDL.h>
#include "EntityManager.h"
#include "Game.h"
#include "Vector2D.h"

class Transform: public Component 
{
    public:
        Vector2D position;

        Transform(int posX, int posY);

        void Initialize() override {}
        void Update(float deltaTime) override {}

        void TranslateX(float x);
        void TranslateY(float y);
        void Translate(Vector2D v);

        void Log(std::string msg = "");
};

#endif
