#include "ControllerManager.h"

ControllerManager::ControllerManager() {}

ControllerManager::~ControllerManager() {}

void ControllerManager::Close()
{
	if (m_Controller != nullptr)
	{
		SDL_GameControllerClose(m_Controller);
	}
	m_Controller = nullptr;
}

void ControllerManager::Reset()
{
	for (int i = 0; i < SDL_CONTROLLER_AXIS_MAX; i++)
	{
		m_CurrentInput.axis[i] = 0;
		m_LastInput.axis[i] = 0;
	}

	for (int i = 0; i < SDL_CONTROLLER_BUTTON_MAX; i++)
	{
		m_CurrentInput.buttons[i] = false;
		m_LastInput.buttons[i] = false;
	}
}

void ControllerManager::Connect() 
{
	int numJoySticks = SDL_NumJoysticks();
	
	if (numJoySticks == 0) 
	{
		Close();
		return;
	}

	for (int i = 0; i < numJoySticks; i++)
	{
		if (SDL_IsGameController(i)) 
		{
			SDL_GameController* pad = SDL_GameControllerOpen(i);

			if (SDL_GameControllerGetAttached(pad)) 
			{
				if (m_Controller != pad) 
				{
					Close();
					m_Controller = pad;
					Reset();
				}
				return;
			}
			else 
			{
				std::cout << "SDL_GetError() = " << SDL_GetError() << std::endl;
			}
		}

		if (i == numJoySticks - 1) 
		{
			Close();
		}
	}
}

void ControllerManager::BeforeListen() 
{
	Connect();

	//if a controller is connected
	if (m_Controller != nullptr) 
	{
		for (int i = 0; i < SDL_CONTROLLER_AXIS_MAX; i++) {
			m_LastInput.axis[i] = m_CurrentInput.axis[i];
		}
		for (int i = 0; i < SDL_CONTROLLER_BUTTON_MAX; i++) {
			m_LastInput.buttons[i] = m_CurrentInput.buttons[i];
		}
	}
}

void ControllerManager::Listen(SDL_Event event) 
{
	//if the recieved events come from the the connecting controller
	if (m_Controller != nullptr && event.cbutton.which == SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(m_Controller)))
	{
		//if a controller button is pressed
		if (event.type == SDL_CONTROLLERBUTTONDOWN)
		{
			//the button is updated
			m_CurrentInput.buttons[event.cbutton.button] = true;
		}

		//if a controller button is just released
		if (event.type == SDL_CONTROLLERBUTTONUP)
		{
			//the button is updated
			m_CurrentInput.buttons[event.cbutton.button] = false;
		}

		//if a controller axis motion is updated
		if (event.type == SDL_CONTROLLERAXISMOTION)
		{
			//the button is updated
			m_CurrentInput.axis[event.caxis.axis] = event.caxis.value;
		}
	}

}

bool ControllerManager::GetKey(SDL_GameControllerButton button)
{
	if (m_Controller == nullptr) 
	{
		return false;
	}
	else 
	{
		return m_CurrentInput.buttons[button];
	}
}

bool ControllerManager::GetKeyUp(SDL_GameControllerButton button)
{
	if (m_Controller == nullptr)
	{
		return false;
	}
	else
	{
		return !m_CurrentInput.buttons[button] && m_LastInput.buttons[button];
	}
}

bool ControllerManager::GetKeyDown(SDL_GameControllerButton button)
{
	if (m_Controller == nullptr)
	{
		return false;
	}
	else
	{
		return m_CurrentInput.buttons[button] && !m_LastInput.buttons[button];
	}
}

float ControllerManager::GetAxisController(SDL_GameControllerAxis axis)
{
	if (m_Controller == nullptr)
	{
		return 0.0f;
	}
	else
	{
		float axisVal = m_CurrentInput.axis[axis];
		if (axisVal > DEAD_ZONE || axisVal < -DEAD_ZONE)
		{
			return axisVal / MAX_AXIS;
		}
		else 
		{
			return 0.0f;
		}
	}
}

bool ControllerManager::HasController()
{
	return m_Controller != nullptr;
}
