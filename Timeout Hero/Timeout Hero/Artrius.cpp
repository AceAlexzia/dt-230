#include "Artrius.h"

void Artrius::Initialize()
{
	m_Transform = owner->GetComponent<Transform>();
	m_Sprite = owner->GetComponent<Sprite>();
	m_Anim = owner->GetComponent<SpriteAnimation>();
	m_Anim->SetProps("Artrius_idle", 4, 200, true);
}

void Artrius::Update(float deltaTime)
{
	if (!m_IsTransforming) 
	{
		float offsetX, offsetY = ARTRIUS_OFFSET_Y;
		Vector2D targetPosition;
		Vector2D playerOrigin = GetOrigin(m_Player->owner->GetComponent<PlayerCollider>()->Get());

		SDL_RendererFlip heroFlip = m_Player->owner->GetComponent<Sprite>()->m_Flip;
		if (heroFlip == SDL_FLIP_NONE)
		{
			offsetX = ARTRIUS_OFFSET_X * -1;
		}
		else
		{
			offsetX = ARTRIUS_OFFSET_X;
		}

		targetPosition.x = Lerp(m_Transform->position.x, playerOrigin.x - m_Sprite->GetWidth() / 2 + offsetX, ARTRIUS_SPEED * deltaTime);
		targetPosition.y = Lerp(m_Transform->position.y, playerOrigin.y - m_Sprite->GetHeight() / 2 - offsetY, ARTRIUS_SPEED * deltaTime);

		if (targetPosition.x - last_position.x < ARTRIUS_DEAD_ZONE && targetPosition.x - last_position.x > -ARTRIUS_DEAD_ZONE)
		{
			m_Sprite->m_Flip = heroFlip;
		}
		else if (last_position.x > targetPosition.x && m_Sprite->m_Flip != SDL_FLIP_HORIZONTAL) {
			m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
		}
		else if (last_position.x < targetPosition.x && m_Sprite->m_Flip != SDL_FLIP_NONE) {
			m_Sprite->m_Flip = SDL_FLIP_NONE;
		}

		last_position.x = targetPosition.x;
		m_Transform->position = targetPosition;
	}
	else 
	{
		if (m_Transform->position.x != m_TransformingPos.x || m_Transform->position.y != m_TransformingPos.y) 
		{
			Vector2D targetPosition;
			targetPosition.x = Lerp(m_Transform->position.x, m_TransformingPos.x, (ARTRIUS_SPEED / 3.0f) * deltaTime);
			targetPosition.y = Lerp(m_Transform->position.y, m_TransformingPos.y, (ARTRIUS_SPEED / 3.0f) * deltaTime);

			m_Transform->position = targetPosition;

			m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
		}
	}
	
}

void Artrius::Transforming()
{
	m_IsTransforming = true;
}
