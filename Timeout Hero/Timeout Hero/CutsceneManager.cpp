#include "CutsceneManager.h"
#include "LevelManager.h"
#include "TransitionManager.h"

CutsceneManager* CutsceneManager::s_Instance = nullptr;

CutsceneManager* CutsceneManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new CutsceneManager();
}


void CutsceneManager::Initialize()
{
	ResetPlay();
}


void CutsceneManager::Update(float dt)
{
	if (PauseManager::GetInstance()->GetIsPause() || SceneManager::GetInstance()->scene->name != GAME_SCENE)
	{
		return;
	}

	Update_Triggering();
	Update_Event();
	Update_EndGamex2(dt);

	//update dialogue displaying
	DialogueManager::GetInstance()->Update(dt);
}


void CutsceneManager::Update_Triggering() 
{
	//cutscene is triggered for the first time
	if (IsTriggerred())
	{

		m_CurrentDialogueLine = 0;

		//displaying dialogue
		DialogueManager::GetInstance()->Speak(m_CutsceneName);

		//the cutscene is played
		m_IsPlayed[m_CutsceneName] = true;

	}
}

void CutsceneManager::Update_Event() 
{
	//get pointer to hero component
	Hero* hero = LevelManager::GetInstance()->GetHero();

	//is dialogue speaking
	bool isSpeaking = DialogueManager::GetInstance()->IsSpeaking();

	if (m_IsBossEvent) 
	{
		hero->m_IsEvent = true;
	}

	//if hero's event is not the same as speaking
	if (hero->m_IsEvent != isSpeaking && !TransitionManager::GetInstance()->IsTransition())
	{

		if (!m_IsBossEvent)
		{
			hero->m_IsEvent = isSpeaking;
		}

		//if event is set to false
		if (!isSpeaking)
		{
			if (m_CutsceneName == "Level-T-8") 
			{
				if (m_IsEndGame) 
				{
					LevelManager::GetInstance()->sectionChange = 1;
				}
				TransitionManager::GetInstance()->FadeTransition(0, 0, 0);
			}
			else if (m_CutsceneName == "Level-A-4")
			{
				TransitionManager::GetInstance()->FadeTransition(255, 255, 255);
				Camera::GetInstance()->CameraShake(1000.0f, 3.5f);
			}
			else if (m_CutsceneName == "Level-D-1") 
			{
				LevelManager::GetInstance()->sectionChange = 1;
				TransitionManager::GetInstance()->FadeTransition(255, 255, 255);
				Camera::GetInstance()->CameraShake(1000.0f, 3.5f);
			}
			else if (m_CutsceneName == "Level-G-1") 
			{
				Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();
				arzeros->GetComponent<FinalArzeros>()->StartAttacking();
			}
		}
	}

	//if dialogue is displaying
	if (isSpeaking) 
	{
		//get dialogue line
		int dialogueLine = DialogueManager::GetInstance()->GetDialogueLine();

		//if reaching new dialogue line
		if (m_CurrentDialogueLine != dialogueLine) 
		{
			//assign new dialogue line number
			m_CurrentDialogueLine = dialogueLine;

			//if cutscene name = specific cutscene -> triggered its event
			if (m_CutsceneName == "Level-T-7")
			{
				if (dialogueLine == 7)
				{
					
					if (hero->m_IsStop)
					{
						//stop updating hero
						hero->m_IsStop = false;

						//position to set
						Vector2D heroPos(384.0f, 194.0f);
						//set hero's warping position
						hero->owner->GetComponent<Transform>()->position = heroPos;
						//spawn warping particle
						ParticleManager::GetInstance()->Spawn("hero_warp_x", Vector2D(heroPos.x - 60, heroPos.y + 30));
						
						//if easter egg is not triggered
						if (!m_IsEasterEgg) 
						{
							//increase easter egg counter
							if (m_EasterEggCounter < 7)
							{
								m_EasterEggCounter++;
							}
							else
							{
								//trigger easter egg event
								LevelManager::GetInstance()->EasterEgg();
								//end the game
								m_IsEndGamex2 = true;
								m_WarpingCountx2 = 8;
								//set easter egg to true
								m_IsEasterEgg = true;
							}
						}
						
					}

				}
			}
			else if (m_CutsceneName == "Level-T-8")
			{
				if (dialogueLine == 13)
				{
					Entity* archfiend = LevelManager::GetInstance()->GetBossEntity();

					archfiend->GetComponent<Archfiend>()->Attack();

					DialogueManager::GetInstance()->Pause();
				}
			}
			else if (m_CutsceneName == "Level-C-3")
			{
				if (dialogueLine == 3) 
				{
					TransitionManager::GetInstance()->FadeTransition(255, 255, 255);
				}
			}
			else if (m_CutsceneName == "Level-D-1")
			{
				if (dialogueLine == 10)
				{
					Artrius* artrius = LevelManager::GetInstance()->GetArtrius();

					artrius->Transforming();
				}
			}
			else if (m_CutsceneName == "Level-5-1") 
			{
				if (dialogueLine == 2) 
				{
					Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();

					arzeros->GetComponent<Arzeros>()->FinishState();

					LevelManager::GetInstance()->GetPrincess()->MoveUp();
				}
			}
			else if (m_CutsceneName == "Level-E-1")
			{
				if (dialogueLine == 7)
				{
					Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();

					arzeros->GetComponent<Arzeros>()->FinishState();

				}
			}
			else if (m_CutsceneName == "Level-E-3")
			{
				if (dialogueLine == 15)
				{
					Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();

					arzeros->GetComponent<Arzeros>()->SetMoving(true);

				}
			}
			else if (m_CutsceneName == "Level-F-1")
			{
				if (dialogueLine == 29)
				{
					Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();

					arzeros->GetComponent<Arzeros>()->FinishState();

				}
			}
			else if (m_CutsceneName == "Level-G-1")
			{
				if (dialogueLine == 10)
				{
					Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();

					arzeros->GetComponent<FinalArzeros>()->SetMovingDir(DOWN);

				}
			}
			else if (m_CutsceneName == "Level-H-1")
			{
				if (dialogueLine == 1)
				{
					LevelManager::GetInstance()->GetPrincess()->UnCage();
				}
				else if (dialogueLine == 9) 
				{
					LevelManager::GetInstance()->sectionChange = 1;
					TransitionManager::GetInstance()->FadeTransition(255, 255, 255);
				}
			}
			
		}
		//if pausing dialogue
		//must end with => DialogueManager::GetInstance()->Resume();
		else if (DialogueManager::GetInstance()->IsPausingDialogue())
		{
			if (m_CutsceneName == "Level-T-8")
			{
				if (hero->Get_IsBlinking() && !hero->Get_IsKnockBack())
				{
					hero->owner->GetComponent<Sprite>()->m_IsEnable = false;
					hero->Spawn_DeadParticle();
					hero->m_IsStop = true;

					DialogueManager::GetInstance()->Resume();
				}
			}

		}
		
	}
	//if dialogue is not displaying
	else 
	{
		if (m_CutsceneName == "Level-T-7") 
		{
			if (!hero->Get_IsBlinking() && !hero->Get_IsKnockBack()) 
			{
				m_IsPlayed[m_CutsceneName] = false; //repeatable cutscene
			}
			
		}
		else if (m_CutsceneName == "Level-U-7")
		{
			if (hero->owner->GetComponent<Transform>()->position.x > 480.0f) 
			{
				//spawn warping particle
				ParticleManager::GetInstance()->Spawn("hero_warp_x", Vector2D(hero->owner->GetComponent<Transform>()->position.x - 60, hero->owner->GetComponent<Transform>()->position.y + 30));

				//position to set
				Vector2D heroPos(144.0f, 192.0f);

				//set hero's warping position
				hero->owner->GetComponent<Transform>()->position = heroPos;
				//set hero's flip
				hero->owner->GetComponent<Sprite>()->m_Flip = SDL_FLIP_HORIZONTAL;

				//spawn warping particle
				ParticleManager::GetInstance()->Spawn("hero_warp_x", Vector2D(heroPos.x - 60, heroPos.y + 30));

				//set artrius' position
				LevelManager::GetInstance()->GetArtrius()->owner->GetComponent<Transform>()->position = Vector2D(144.0f, 192.0f);
			}
			
		}
	}
}

void CutsceneManager::ResetPlay() 
{
	//reset dialogue property
	DialogueManager::GetInstance()->Reset();

	//reset event map
	m_IsPlayed["Level-T-1"] = false;
	m_IsPlayed["Level-T-4"] = false;
	m_IsPlayed["Level-T-6"] = false;
	m_IsPlayed["Level-T-7"] = false;
	m_IsPlayed["Level-T-8"] = false;
	m_IsPlayed["Level-0-1"] = false;
	m_IsPlayed["Level-1-1"] = false;
	m_IsPlayed["Level-1-2"] = false;
	m_IsPlayed["Level-2-1"] = false;
	m_IsPlayed["Level-4-1"] = false;
	m_IsPlayed["Level-U-4"] = false;
	m_IsPlayed["Level-U-7"] = false;
	m_IsPlayed["Level-U-8"] = false;
	m_IsPlayed["Level-A-4"] = false;
	m_IsPlayed["Level-C-3"] = false;
	m_IsPlayed["Level-D-1"] = false;
	m_IsPlayed["Level-5-1"] = false;
	m_IsPlayed["Level-E-1"] = false;
	m_IsPlayed["Level-E-3"] = false;
	m_IsPlayed["Level-F-1"] = false;
	m_IsPlayed["Level-G-1"] = false;
	m_IsPlayed["Level-H-1"] = false;

	m_IsBossEvent = false;

	m_IsEndGame = false;
	m_IsEndGamex2 = false;

	m_WarpingCountx2 = 0;
	m_WarpingTimeDelayx2 = 0.0f;
}


void CutsceneManager::SetCutsceneName(std::string levelname)
{
	//assign cutscnene's name
	m_CutsceneName = levelname;

	if (m_IsPlayed[m_CutsceneName])
	{
		if (m_CutsceneName == "Level-5-1" || m_CutsceneName == "Level-E-1" || m_CutsceneName == "Level-F-1")
		{
			Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();

			arzeros->GetComponent<Arzeros>()->FinishState();

			if (m_CutsceneName == "Level-5-1")
			{
				LevelManager::GetInstance()->GetPrincess()->MoveUp();
			}

		}
		else if (m_CutsceneName == "Level-G-1")
		{
			Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();

			arzeros->GetComponent<FinalArzeros>()->SetMovingDir(DOWN);
			arzeros->GetComponent<FinalArzeros>()->StartAttacking();
		}
	}
}

bool CutsceneManager::IsTriggerred()
{
	//if the cutscene is already played -> return false
	if (m_IsPlayed[m_CutsceneName] || TransitionManager::GetInstance()->IsTransition())
	{
		return false;
	}

	//cutscene triggering condition
	if (m_CutsceneName == "Level-T-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-T-4")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-T-6")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-T-7")
	{
		Hero* hero = LevelManager::GetInstance()->GetHero();

		if (hero->Get_IsBlinking() && !hero->Get_IsKnockBack()) 
		{
			hero->owner->GetComponent<Sprite>()->m_IsEnable = true;
			hero->owner->GetComponent<SpriteAnimation>()->SetProps("Hero-hurt", 2, 150);
			hero->m_IsStop = true;

			return true;
		}
		else 
		{
			return false;
		}
		
	}
	else if (m_CutsceneName == "Level-T-8")
	{
		Hero* hero = LevelManager::GetInstance()->GetHero();

		if (hero->owner->GetComponent<Transform>()->position.x < 1296.0f) 
		{
			return true;
		}
		else 
		{
			return false;
		}
		
	}
	else if (m_CutsceneName == "Level-0-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-1-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-1-2")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-2-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-4-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-U-4")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-U-7")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-U-8")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-A-4")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-C-3")
	{
		Entity* archfiend = LevelManager::GetInstance()->GetBossEntity();

		if (archfiend->GetComponent<Archfiend>()->IsDead()) 
		{
			return true;
		}
		else 
		{
			return false;
		}

	}
	else if (m_CutsceneName == "Level-D-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-5-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-E-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-E-3")
	{
		Entity* arzeros = LevelManager::GetInstance()->GetBossEntity();
		if (arzeros->GetComponent<Arzeros>()->IsFinish()) 
		{
			arzeros->GetComponent<Arzeros>()->SetMoving(false);
			return true;
		}
		else 
		{
			return false;
		}
	}
	else if (m_CutsceneName == "Level-F-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-G-1")
	{
		return true;
	}
	else if (m_CutsceneName == "Level-H-1")
	{
		return true;
	}
	else 
	{
		return false;
	}
}

void CutsceneManager::EndGame() 
{
	bool isAlreadyEnd = m_IsEndGame;

	if (isAlreadyEnd) 
	{
		for (std::map<std::string, bool>::iterator it = m_IsPlayed.begin(); it != m_IsPlayed.end(); it++) 
		{
			(*it).second = true;
		}
		m_IsPlayed["Level-T-1"] = false;
		m_IsEndGamex2 = true;
	}
	else 
	{
		ResetPlay();
		m_IsEndGame = true;
	}
}

bool CutsceneManager::IsEndGame()
{
	return m_IsEndGame;
}

bool CutsceneManager::IsEndGamex2()
{
	return m_IsEndGamex2;
}

void CutsceneManager::Update_EndGamex2(float deltatime) 
{
	if (!m_IsEndGamex2) 
	{
		return;
	}
	
	if (!LevelManager::GetInstance()->GetHero()->m_IsEvent) 
	{
		m_WarpingTimeDelayx2 += deltatime;
	}
	

	float maxDelay = GetWarpDelayx2();
	if (m_WarpingTimeDelayx2 > maxDelay) 
	{
		m_WarpingTimeDelayx2 = 0.0f;
		m_WarpingCountx2++;
		SetWarpLevelx2();
	}
}

void CutsceneManager::SetWarpLevelx2()
{
	LevelManager* levelManager = LevelManager::GetInstance();
	levelManager->sectionChange = 0;

	//set warp level
	if (m_WarpingCountx2 < 8) 
	{
		levelManager->levelIndex = m_WarpingLevelIndexx2[m_WarpingCountx2];
	}
	//if there's no level to continue warping
	//the game is fully ended
	else 
	{
		SceneManager::GetInstance()->LoadScene(SPLASH_SCENE);
	}

	levelManager->SetLoadNewLevel();
}

float CutsceneManager::GetWarpDelayx2() 
{
	if (m_WarpingCountx2 > 5) 
	{
		return 0.2f;
	}
	else if (m_WarpingCountx2 > 2) 
	{
		return 0.3f;
	}
	else if (m_WarpingCountx2 > 1) 
	{
		return 0.5f;
	}
	else if (m_WarpingCountx2 > 0) 
	{
		return 0.8f;
	}
	else
	{
		return 1.5f;
	}
}