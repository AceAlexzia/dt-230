#pragma once
#ifndef PARTICLE_MANAGER_H
#define PARTICLE_MANAGER_H

#include "Particle.h"
#include "Vector2D.h"
#include <iostream>

struct ParticleProperty
{
    int frameCount;
    int animSpeed;
    int width, height;
};

class ParticleManager
{
public:
    ParticleManager();
    void Initialize();
    void Spawn(std::string name, Vector2D position, float time = 0.0f);

    static ParticleManager* GetInstance();

private:
    std::map<std::string, ParticleProperty> particleList;

    static ParticleManager* s_Instance;
    void assignParticle();
};

#endif