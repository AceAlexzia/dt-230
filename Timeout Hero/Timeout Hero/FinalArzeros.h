#pragma once

#ifndef FINAL_ARZEROS
#define FINAL_ARZEROS

#include "ArchMonster.h"

#define FINAL_ARZEROS_ACCELERATION 500.0f
#define FINAL_ARZEROS_MAX_VELOCITY 500.0f
#define FINAL_ARZEROS_FLIP_VELOCITY 200.0f
#define FINAL_ARZEROS_HURT_TIME 0.15f
#define FINAL_ARZEROS_BLINK_LENGTH 0.10f
#define FINAL_ARZEROS_SHAKE_AMOUNT 500.0f
#define FINAL_ARZEROS_SWAPPING_TIME 0.5f
#define FINAL_ARZEROS_SWAPPING_LENGTH 0.2f

class FinalArzeros : public LevelObject
{
private:

	int m_HP = 4;

	bool m_FinishState = false;

	bool m_IsHurt = false;
	float m_HurtTime = 0.0f;

	float m_BlinkTime = 0.0f;

	bool m_IsDead = false;
	float m_DeadTimeCounter = 0.0f;

	float m_SwappingCounter = 0.0f;

	bool m_IsAttacking = false;

	//HP = 2 movement
	float m_SweepingTime = 2.0f;
	float m_SweepingCounter = m_SweepingTime / 2.5f;
	float m_SweepingForce = 300.0f;
	bool m_StartSweeping = false;
	//HP = 1 and HP = 3 movement
	Direction  m_MovingDir = UP;
	bool m_StartFinal = false;
	float m_DroppingPosX[3] = { 240.0f, 720.0f, 1152.0f };

	Vector2D m_ShakingPos = Vector2D(0.0f, 0.0f);

	Rigidbody* m_Rigidbody = nullptr;

	//Monster spawning
	SDL_Rect m_PrincessCollider;
	float m_MonsterSpawnPosY[4] = { 48.0f, 168.0f, 300.0f , 432.0f };
	float m_MonsterSpawningCounter = 2.5f;
	std::map<int, Vector2D> m_MonsterSpawningDelay; //health -> (min, max)

	//Gamemap variable
	int m_RowCount = 0;
	int m_ColCount = 0;
	int m_TileSize = 0;

	//private function
	//sub initialize function
	//initialize all components
	void Initialize_Component();
	//sub update function
	//update movemnt according to its health
	void Update_Movement(float deltatime);
	//update blinking and dead
	void Update_FinishState(float deltatime);
	//Update Hurt
	void Update_Hurt(float deltatime);
	//Update Moving
	void Update_Moving();
	//Update Collision
	void Update_Collision();
	//Update Animation
	void Update_Animation(float deltatime);
	//Update Monster Spawning
	void Update_MonsterSpawning(float deltatime);
	//setter
	void SetCollider();

	//archmonster spawner
	void SpawnArchMonster();

	//blinking
	void Blink(float deltatime);

public:

	FinalArzeros() {}
	virtual ~FinalArzeros() {}

	void Initialize() override;
	void Update(float deltaTime) override;

	//setter
	void FinishState();
	void Dead();
	void SetPrincessCollider(SDL_Rect princessCollider);

	void SetMovingDir(Direction dir);
	void StartAttacking();
};

#endif // !FINAL_ARZEROS
