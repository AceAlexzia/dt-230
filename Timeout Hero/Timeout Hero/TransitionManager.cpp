#include "TransitionManager.h"
#include "Game.h"

TransitionManager* TransitionManager::s_Instance = nullptr;

TransitionManager* TransitionManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new TransitionManager();
}


void TransitionManager::Initialize()
{
	
	m_FunctionDict[TRANSITION_FUNCTION_LEVEL_MANAGER] = &Transition::SetLoadNewLevel;
	m_FunctionDict[TRANSITION_FUNCTION_START_GAME] = &Transition::StartGame;

	//initialize transBox Entity
	Entity& transBox(EntityManager::GetInstance()->AddEntity("transBox"));

	transBox.AddComponent<TransitionBox>();

	m_TransBox = transBox.GetComponent<TransitionBox>();
}


void TransitionManager::SetFunction(std::string fn)
{
	m_TransBox->SetFunction(m_FunctionDict[fn]);
}


void TransitionManager::BoxTransition(std::string fn) 
{
	SetFunction(fn);
	m_TransBox->Transition();
}

void TransitionManager::FadeTransition(Uint8 r, Uint8 g, Uint8 b)
{
	m_IsFade = true;
	DrawRect(r, g, b, 0);
}

void TransitionManager::DrawRect(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	m_RectColor = { r, g, b, a };
}

void TransitionManager::Update(float deltatime) 
{
	if (m_IsFade) 
	{
		Uint8 alpha = 0;
		if (m_FadeTimeCounter < 7.5f)
		{
			//increase time counter
			m_FadeTimeCounter += deltatime;

			//set fade transitioning
			if (m_FadeTimeCounter >= 7.5f)
			{
				alpha = 0;

				//the transition is finished
				m_FadeTimeCounter = 0.0f;
				m_IsFade = false;
			}
			else if (m_FadeTimeCounter >= 7.0f)
			{
				alpha = 50;
			}
			else if (m_FadeTimeCounter >= 6.5f)
			{
				alpha = 100;
			}
			else if (m_FadeTimeCounter >= 6.0f)
			{
				alpha = 150;
			}
			else if (m_FadeTimeCounter >= 5.5f)
			{
				alpha = 200;
			}
			else if (m_FadeTimeCounter >= 3.5f)
			{
				alpha = 255;
				if (LevelManager::GetInstance()->sectionChange != 0) 
				{
					//go to next level
					LevelManager::GetInstance()->levelIndex++;
					//section changing is reset
					LevelManager::GetInstance()->sectionChange = 0;
					LevelManager::GetInstance()->SetLoadNewLevel();
				}
			}
			else if (m_FadeTimeCounter >= 3.0f)
			{
				alpha = 200;
			}
			else if (m_FadeTimeCounter >= 2.5f)
			{
				alpha = 150;
			}
			else if (m_FadeTimeCounter >= 2.0f)
			{
				alpha = 100;
			}
			else if (m_FadeTimeCounter >= 1.5f)
			{
				alpha = 50;
			}

			//set Hero is event
			LevelManager::GetInstance()->GetHero()->m_IsEvent = m_IsFade;
			//set rect color
			DrawRect(m_RectColor.r, m_RectColor.g, m_RectColor.b, alpha);
		}
	}
}

void TransitionManager::Render() 
{
	if (m_RectColor.a > 0) 
	{
		SDL_Renderer* renderer = Game::GetInstance()->m_Renderer;
		SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(renderer, m_RectColor.r, m_RectColor.g, m_RectColor.b, m_RectColor.a); // the rect color (solid red)
		SDL_Rect rect = { 0, 0, RENDER_LOGICAL_WIDTH, RENDER_LOGICAL_HEIGHT }; // the rectangle
		SDL_RenderFillRect(renderer, &rect);
		SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
	}
}


bool TransitionManager::IsFadeTransition() 
{
	return m_IsFade;
}

bool TransitionManager::IsBoxTransition()
{
	return m_TransBox->IsTransition();
}

bool TransitionManager::IsTransition() 
{
	return m_IsFade || m_TransBox->IsTransition();
}
