#pragma once
#ifndef PAUSE_MANAGER
#define PAUSE_MANAGER
#include "UIManager.h"
#include "InputManager.h"


class PauseManager
{
private :
	static PauseManager* s_Instance;

	bool m_IsPause = false;

	PauseManager() {}

public :

	static PauseManager* GetInstance();

	bool GetIsPause();

	void Update();

	//Pause the game
	void Pause();
	//Resume the game
	void Resume();
};

#endif // !PAUSE_MANAGER
