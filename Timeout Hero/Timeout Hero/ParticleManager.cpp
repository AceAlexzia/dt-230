#include "ParticleManager.h"

ParticleManager* ParticleManager::s_Instance = nullptr;

ParticleManager::ParticleManager()
{
    Initialize();
}

void ParticleManager::Initialize()
{
    assignParticle();
    /*for (int i = 0; i < 100; i++) {
        Spawn("hero_dash", Vector2D(120, 620));
    }*/
}

void ParticleManager::Spawn(std::string name, Vector2D position, float time)
{
    Entity& particle(EntityManager::GetInstance()->AddEntity(name + ":x:" + std::to_string((int)position.x) + ":y:" + std::to_string((int)position.y)));

    particle.AddComponent<Transform>(position.x, position.y);
    particle.AddComponent<Sprite>(name, particleList[name].width, particleList[name].height, RUNE_LAYER);
    particle.AddComponent<SpriteAnimation>();
    particle.AddComponent<Particle>();
    particle.GetComponent<Particle>()->despawn_time = time;

    if (time == 0.0f) {
        particle.GetComponent<Particle>()->finish_loop = true;
        particle.GetComponent<SpriteAnimation>()->SetProps(name, particleList[name].frameCount, particleList[name].animSpeed, false);
    }
    else {
        particle.GetComponent<SpriteAnimation>()->SetProps(name, particleList[name].frameCount, particleList[name].animSpeed);
    }
}

ParticleManager* ParticleManager::GetInstance()
{
    return s_Instance = (s_Instance != nullptr) ? s_Instance : new ParticleManager();
}

void ParticleManager::assignParticle()
{
    ParticleProperty particle;

    //monster explosion particle
    particle.frameCount = 18;
    particle.animSpeed = 45;
    particle.width = 128;
    particle.height = 128;
    particleList["monster_explosion"] = particle;

    //monster respawn particle
    particle.frameCount = 11;
    particle.animSpeed = 45;
    particle.width = 128;
    particle.height = 128;
    particleList["monster_respawn"] = particle;

    //archmonster explosion particle
    particle.frameCount = 18;
    particle.animSpeed = 45;
    particle.width = 128;
    particle.height = 128;
    particleList["archmonster_explosion"] = particle;

    //archmonster disappear particle
    particle.frameCount = 8;
    particle.animSpeed = 100;
    particle.width = 128;
    particle.height = 128;
    particleList["archmonster_disppear"] = particle;

    //spellmonster explosion particle
    particle.frameCount = 8;
    particle.animSpeed = 100;
    particle.width = 192;
    particle.height = 192;
    particleList["spellmonster_explosion"] = particle;

    //hero dead particle
    particle.frameCount = 14;
    particle.animSpeed = 55;
    particle.width = 96;
    particle.height = 96;
    particleList["hero_explosion"] = particle;

    //hero dash particle
    particle.frameCount = 1;
    particle.animSpeed = 55;
    particle.width = 66;
    particle.height = 66;
    particleList["hero_dash"] = particle;

    //hero dash left particle
    particle.frameCount = 1;
    particle.animSpeed = 55;
    particle.width = 66;
    particle.height = 66;
    particleList["hero_dash_left"] = particle;

    particle.frameCount = 14;
    particle.animSpeed = 55;
    particle.width = 64 * 1.5;
    particle.height = 128 * 1.5;
    particleList["hero_warp"] = particle;

    particle.frameCount = 14;
    particle.animSpeed = 55;
    particle.width = 128 * 1.5;
    particle.height = 64 * 1.5;
    particleList["hero_warp_x"] = particle;

    //hero getting rune particle
    particle.frameCount = 17;
    particle.animSpeed = 55;
    particle.width = 64 * 2.5;
    particle.height = 128 * 2.5;
    particleList["hero_getting_rune"] = particle;
}