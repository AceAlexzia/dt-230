#include "ColliderComponent.h"

Collider::Collider()
{
    SetBuffer(0, 0, 0, 0);
    Set(0, 0, 0, 0);
}

Collider::Collider(int x, int y, int w, int h)
{
    SetBuffer(0, 0, 0, 0);
    Set(x, y, w, h);
}

void Collider::Render()
{
    SDL_SetRenderDrawColor(Game::GetInstance()->m_Renderer, 255, 0, 0, 255);

    Vector2D cam = Camera::GetInstance()->GetPosition();
    SDL_Rect box = { (int)(m_Collider.x - cam.x), (int)(m_Collider.y - cam.y), m_Collider.w, m_Collider.h };
    SDL_RenderDrawRect(Game::GetInstance()->m_Renderer, &box);
}

SDL_Rect Collider::Get() 
{
    return m_Collider;
}

void Collider::SetBuffer(int x, int y, int w, int h)
{
    m_Buffer = { x, y, w, h };
}

void Collider::Set(int x, int y, int w, int h)
{
    m_Collider = { x - m_Buffer.x, y - m_Buffer.y, w - m_Buffer.w, h - m_Buffer.h };
}