#include "Princess.h"

void Princess::Initialize() 
{
	m_Transform = owner->GetComponent<Transform>();
	m_Sprite = owner->GetComponent<Sprite>();
	m_Animation = owner->GetComponent<SpriteAnimation>();
	m_Collider = owner->GetComponent<Collider>();

	m_Collider->SetBuffer(-55, -50, 110, 50);
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}

void Princess::Update(float deltatime)
{
	Update_AnimationState();
	Update_MovingUp();
}

void Princess::Update_AnimationState()
{
	if (m_IsCaged) 
	{
		m_Animation->SetProps("princess_idle_cage", 4, 250);
	}
	else 
	{
		if (m_IsUnCaged) 
		{
			m_Animation->SetProps("princess_idle_uncage", 4, 250);
		}
		else 
		{
			m_Animation->SetProps("princess_cage_disappear", 23, 180, false);

			//if the animation is ended -> change its state
			if (m_Animation->GetIsEnded()) 
			{
				m_IsUnCaged = true;
			}
		}
	}
}


void Princess::MoveUp() 
{
	m_IsMoveUp = true;

	if (m_Rigidbody == nullptr) 
	{
		owner->AddComponent<Rigidbody>();
		m_Rigidbody = owner->GetComponent<Rigidbody>();
		m_Rigidbody->setGravity(0.0f);
	}
}


void Princess::Update_MovingUp() 
{
	if (m_IsMoveUp) 
	{
		m_Rigidbody->ApplyForceY(-300.0f);
		m_Transform->Translate(m_Rigidbody->Position());
		if (m_Transform->position.y < m_Sprite->GetHeight() * -2) 
		{
			owner->Destroy();
		}

	}
}


void Princess::UnCage() 
{
	m_IsCaged = false;
}
