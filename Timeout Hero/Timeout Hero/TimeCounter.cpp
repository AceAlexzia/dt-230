#include "TimeCounter.h"

TimeCounter::TimeCounter(float time)
{
	SetTimeCount(time);
}

TimeCounter::~TimeCounter()
{
	AudioManager::GetInstance()->clockSfxPlayer.Stop();
}


void TimeCounter::Initialize()
{
	m_Transform = owner->GetComponent<Transform>();
	m_Text = owner->GetComponent<Text>();

	m_Text->font = TIME_COUNTER_FONT_NAME;
	m_Text->size = 75;
	m_Text->color = { 255, 255, 255, 255 };
	m_Text->outlineSize = 3;

	//change text
	Update_Text();
	//update text width and height
	m_Text->Update(0.0f);
	//update position
	Update_Position();

	AudioManager::GetInstance()->clockSfxPlayer.Play();
}


void TimeCounter::Update(float dt)
{

	if (m_IsPause || !m_IsEnable) 
	{
		return;
	}

	Update_Count(dt);
	Update_Text();
	Update_Position();
	Update_Blinking(dt);
}


void TimeCounter::Update_Count(float dt)
{
	if (m_TimeCount <= 0.0f)
	{
		m_TimeCount = 0.0f;
	}
	else
	{
		m_TimeCount -= dt;
	}
}

void TimeCounter::Update_Text()
{
	int timeCount = m_TimeCount;

	//value cap
	if (timeCount > 60)
	{
		timeCount = 60;
	}
	else if (timeCount < 0)
	{
		timeCount = 0;
	}

	//set to normal color
	if (timeCount > 30)
	{
		m_Text->color = { 255, 255, 255, 255 };
	}
	//change to yellow color when less than 30 secs
	else if (timeCount > 20) 
	{
		m_Text->color = { 255, 255, 50, 255 };
	}
	//change to orange color when less than 20 secs
	else if (timeCount > 10)
	{
		m_Text->color = { 255, 100, 50, 255 };
	}
	//change to red color when less than 10 secs
	else
	{
		m_Text->color = { 255, 0, 0, 255 };
	}

	m_Text->text = std::to_string(timeCount);

	AudioManager::GetInstance()->clockSfxPlayer.SetInteger("time", timeCount);

}

void TimeCounter::Update_Position() 
{
	m_Transform->position.x = (RENDER_LOGICAL_WIDTH / 2) - (m_Text->GetWidth() / 2);
	m_Transform->position.y = RENDER_LOGICAL_HEIGHT / 34;
}

void TimeCounter::Update_Blinking(float deltaTime)
{
	//Blinking
	if (m_IsBlinking)
	{
		m_Text->m_IsEnable = m_BlinkCount % 2 != 0;
		m_BlinkTime += deltaTime;

		if (m_BlinkTime >= TIME_COUNTER_BLINK_LENGTH)
		{
			m_BlinkTime = 0.0f;
			m_BlinkCount++;

			if (m_BlinkCount == TIME_COUNTER_BLINK_MAX_COUNT)
			{
				m_BlinkCount = 0;
				m_IsBlinking = false;
			}
		}
	}

	int timeCount = m_TimeCount;
	if (timeCount <= 0) 
	{
		StopBlink();
	}
}

void TimeCounter::DecreaseTimeCounter(float amount)
{
	if (!m_IsPause) 
	{
		m_TimeCount -= amount;
	}
	
}


void TimeCounter::SetTimeCount(float time) 
{
	m_TimeCount = time;
}

float TimeCounter::GetTimeCount() 
{ 
	return m_TimeCount; 
}


void TimeCounter::Pause() 
{
	if (!m_IsPause) 
	{
		if (AudioManager::GetInstance()->clockSfxPlayer.isPlaying()) 
		{
			AudioManager::GetInstance()->clockSfxPlayer.Stop();
			AudioManager::GetInstance()->clockSfxPlayer.SetInteger("time", 60);
		}
		
	}
	m_IsPause = true;
}

void TimeCounter::Resume()
{
	if (m_IsPause)
	{
		if (!AudioManager::GetInstance()->clockSfxPlayer.isPlaying())
		{
			AudioManager::GetInstance()->clockSfxPlayer.Play();
		}
	}
	m_IsPause = false;
}


void TimeCounter::Disable() 
{
	Pause();
	m_Text->m_IsEnable = false;
	m_IsEnable = false;
}

void TimeCounter::Enable()
{
	Resume();
	m_Text->m_IsEnable = true;
	m_IsEnable = true;
}

void TimeCounter::Blink() 
{
	if (m_IsEnable) 
	{
		//set to true
		m_IsBlinking = true;
		//reset time and count
		m_BlinkCount = 0;
		m_BlinkTime = 0.0f;
	}
}

void TimeCounter::StopBlink() 
{
	if (m_IsEnable) 
	{
		//set to false
		m_IsBlinking = false;
		//reset time and count
		m_BlinkCount = 0;
		m_BlinkTime = 0.0f;
		//enable text rendering
		m_Text->m_IsEnable = true;
	}
}
