#pragma once
#ifndef SCENE_H
#define SCENE_H
#include <iostream>
class Scene
{
	public:
		std::string name;
		virtual ~Scene() {}
		virtual void Start() = 0;
};

#endif // !SCENE_H