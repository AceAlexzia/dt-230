#pragma once

#ifndef CUTSCENE_MANAGER
#define CUTSCENE_MANAGER

#include "DialogueManager.h"

class CutsceneManager
{
	
private :

	static CutsceneManager* s_Instance;

	CutsceneManager() {}

	std::string m_CutsceneName = "";

	std::map<std::string, bool> m_IsPlayed;

	int m_CurrentDialogueLine = 0;

	bool m_IsEndGame = false;
	bool m_IsEndGamex2 = false;

	int m_WarpingCountx2 = 0;
	float m_WarpingTimeDelayx2 = 0.0f;
	int m_WarpingLevelIndexx2[8] = { 9, 13, 16, 17, 38, 40, 43 };

	int m_EasterEggCounter = 0;
	bool m_IsEasterEgg = false;

	//reset isPlayed
	void ResetPlay();
	//is cutscene triggered
	bool IsTriggerred();


	//sub update function
	//set cutscene triggering
	void Update_Triggering();
	//set is event 
	void Update_Event();
	//x2 function
	//update endgame event x2
	void Update_EndGamex2(float deltatime);
	float GetWarpDelayx2();
	void SetWarpLevelx2();


public:

	~CutsceneManager() {}

	//Getter
	static CutsceneManager* GetInstance();

	//init and update
	void Initialize();
	void Update(float dt);

	void SetCutsceneName(std::string levelname);

	bool m_IsBossEvent = false;

	void EndGame();
	bool IsEndGame();
	bool IsEndGamex2();
};

#endif // !CUTSCENE_MANAGER
