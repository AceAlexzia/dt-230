#include "HorizontalMonster.h"

void HorizontalMonster::Initialize()
{
	m_Collider = owner->GetComponent<Collider>();
	m_Transform = owner->GetComponent<Transform>();
	m_Sprite = owner->GetComponent<Sprite>();
	m_Anim = owner->GetComponent<SpriteAnimation>();
	m_Rb = owner->GetComponent<Rigidbody>();
	m_Rb->setGravity(0.0f);
	m_Anim->SetProps("horizontal_monster_idle", 4, 100, true);
	m_Collider->SetBuffer(-26, -34, 52, 52);
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}

void HorizontalMonster::Update(float deltaTime)
{
	if (m_IsAlive) 
	{
		m_Rb->m_velocity = m_MovingDirection * H_MONSTER_SPEED;
		m_Transform->Translate(m_Rb->Position());
		m_TimeLength += deltaTime;

		if (m_TimeLength >= H_MONSTER_TIME_LENGTH) 
		{
			m_TimeLength = 0.0f;
			m_MovingDirection = m_MovingDirection * -1;

			if (m_Sprite->m_Flip == SDL_FLIP_NONE) {
				m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
			}
			else {
				m_Sprite->m_Flip = SDL_FLIP_NONE;
			}

		}
		m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
	}
	else 
	{
		m_Rb->m_velocity = Vector2D(0, 0);
	}

	//update collision and respawning condition
	Monster::Update(deltaTime);
}