#include "Timer.h"

Timer* Timer::s_Instance = nullptr;

Timer* Timer::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new Timer();
}

float Timer::GetDeltaTime() 
{
	//time scale value capping
	if (timeScale < 0.0f) 
	{
		timeScale = 0.0f;
	}
	else if (timeScale > 1.0f) 
	{
		timeScale = 1.0f;
	}

	//return delta time value with time scaling
	return m_DeltaTime * timeScale;
}

float Timer::GetRawDeltaTime()
{
	//return delta time value WITHOUT time scaling
	return m_DeltaTime;
}


void Timer::Update(float dt)
{

	// Wait until 16ms has ellapsed since the last frame
	while (!SDL_TICKS_PASSED(SDL_GetTicks(), m_TicksLastFrame + FRAME_TARGET_TIME));

	// Delta time is the difference in ticks from last frame converted to seconds
	m_DeltaTime = (SDL_GetTicks() - m_TicksLastFrame) / 1000.0f;

	// Clamp deltaTime to a maximum value
	m_DeltaTime = (m_DeltaTime > 0.05f) ? 0.05f : m_DeltaTime;

	// Sets the new ticks for the current frame to be used in the next pass
	m_TicksLastFrame = SDL_GetTicks();

	if (m_TimeSpace > 0.0f) 
	{
		timeScale = SLOW_TIME_SCALE;
		SetTimeSpace(m_TimeSpace - m_DeltaTime);
	}

	if (timeScale == 1.0f) 
	{
		m_HandlingCounter = 0.0f;
	}
	else 
	{
		m_HandlingCounter += m_DeltaTime;

		if (m_HandlingCounter > 0.5f) 
		{
			m_HandlingCounter  = 0.0f;
			timeScale = 1.0f;
		}
	}
}

void Timer::SetTimeSpace(float timeSpace) 
{
	//assign time space local variable
	m_TimeSpace = timeSpace;

	//if time space < 0 -> reset time scaling
	if (m_TimeSpace < 0.0f) 
	{
		m_TimeSpace = 0.0f;
		timeScale = 1.0f;
	}
}

void Timer::SlowMotion() 
{
	//set time space
	float timeSpace = TIME_SPACE;
	SetTimeSpace(timeSpace);
	
}
