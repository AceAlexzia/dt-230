#pragma once
#ifndef ALTRIUM_RUNE_H
#define ALTRIUM_RUNE_H

#include "LevelObject.h"

class AltriumRune : public LevelObject
{
	public:
		AltriumRune(bool isActive = false);
		~AltriumRune();

		void Initialize() override;
		void Update(float deltaTime) override;

		//void Activate();

	private:
		//determine whether the rune is active
		bool m_IsActive = true;

		//checking the rune animation state in real time
		void Update_Active();
};

#endif