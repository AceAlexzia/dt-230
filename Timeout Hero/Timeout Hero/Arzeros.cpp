#include "Arzeros.h"
#include "LevelManager.h"
#include "TransitionManager.h"

Arzeros::Arzeros(int state)
{
	//set initialized state
	m_State = state;

	//check whether the state is valid
	if (m_State < 0 || m_State > 5)
	{
		std::cout << "Arzeros invalid state :  " << m_State << std::endl;
		//if not -> destory this object
		owner->Destroy();
	}
}

void Arzeros::Initialize()
{
	//add all neccessary components
	Initialize_Component();

	//initialize according to its state
	Initialize_State();

	//initialize archmonster property
	Initialize_AttackProperty();

	//set collider box position
	SetCollider();

	GameMap* gameMap = LevelManager::GetInstance()->GetMap();
	m_RowCount = gameMap->GetRowCount();
	m_ColCount = gameMap->GetColCount();
	m_TileSize = gameMap->GetTileSize();
}

void Arzeros::Update(float deltaTime)
{

	//finish state updating
	Update_FinishState(deltaTime);
	//update hurt state
	Update_Hurt(deltaTime);
	//update attack state
	Update_Attack(deltaTime);
	//update moving direction
	Update_Moving();
	//updating collision
	Update_Collision();
	//updating animation state
	Update_Animation();
}


void Arzeros::Initialize_Component()
{
	//initialize component
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(0.0f, 0.0f);
		m_Transform = owner->GetComponent<Transform>();
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("arzeros_idle", 232, 180, RUNE_LAYER);
		m_Sprite = owner->GetComponent<Sprite>();
	}
	if (!owner->HasComponent<SpriteAnimation>())
	{
		owner->AddComponent<SpriteAnimation>();
		m_Anim = owner->GetComponent<SpriteAnimation>();
	}
	if (!owner->HasComponent<Collider>())
	{
		owner->AddComponent<Collider>();
		m_Collider = owner->GetComponent<Collider>();
	}
	if (!owner->HasComponent<Rigidbody>())
	{
		owner->AddComponent<Rigidbody>();
		m_Rigidbody = owner->GetComponent<Rigidbody>();
	}

	m_Rigidbody->setGravity(0.0f);

	m_Anim->SetProps("arzeros_idle", 8, 200);
}

void Arzeros::Initialize_State()
{
	if (m_State == 0)
	{
		int levelindex = LevelManager::GetInstance()->levelIndex;

		if (levelindex == 37) 
		{
			m_Transform->position = Vector2D(768.0f, 420.0f);
		}
		else if (levelindex == 43) 
		{
			m_Transform->position = Vector2D(816.0f, 432.0f);
		}
		
		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;

		m_Collider->m_IsEnable = false;


	}
	else if (m_State == 1)
	{
		m_Transform->position = Vector2D(2064.0f, 816.0f);

		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;

		m_Collider->m_IsEnable = true;
	}
	else if (m_State == 2)
	{
		m_Transform->position = Vector2D(288.0f, 1968.0f);

		m_Sprite->m_Flip = SDL_FLIP_NONE;

		m_Collider->m_IsEnable = true;
	}
	else if (m_State == 3)
	{
		m_Transform->position = Vector2D(240.0f, 480.0f);

		m_Sprite->m_Flip = SDL_FLIP_NONE;

		m_Collider->m_IsEnable = false;
	}
	else if (m_State == 4)
	{
		m_Transform->position = Vector2D(0.0f, 288.0f);

		m_Sprite->m_Flip = SDL_FLIP_NONE;

		m_Collider->m_IsEnable = true;
	}
	else if (m_State == 5)
	{
		m_Transform->position = Vector2D(192.0f, 504.0f);

		m_Sprite->m_Flip = SDL_FLIP_NONE;

		m_Collider->m_IsEnable = true;
	}
}

void Arzeros::Initialize_AttackProperty()
{
	if (m_State == 1)
	{

		//temp variable
		int index = 0;

		//first attack
		m_ArchmonsterDirection[0].push_back(Vector2D(-1.0f, 0.0f));
		m_ArchmonsterDestroyTime[0].push_back(3.4f);

	}
	else if (m_State == 2)
	{

		//temp variable
		int index = 0;
		float degree = 0.0f;


		//first attack
		index = 0;

		degree = 7.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.5f);


		//second attack
		index = 1;

		degree = 4.5f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(6.1f);


		//third attack
		index = 2;

		degree = 2.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(6.1f);


		//fourth attack
		index = 3;

		degree = 1.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(6.1f);


		//fifth attack
		index = 4;

		degree = 6.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(4.7f);


		//sixth attack
		index = 5;

		degree = 9.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(3.7f);

	}
	else if (m_State == 4)
	{
		//temp variable
		int index = 0;
		float degree = 0.0f;


		//first attack
		index = 0;

		degree = 10.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.5f);

		degree = 10.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(3.7f);


		//second attack
		index = 1;

		degree = 6.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(4.8f);


		//third attack
		index = 2;

		degree = 0.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree * M_PI / 180.0f), sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(4.8f);


		//fourth attack
		index = 3;

		degree = 6.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(4.8f);


		//fifth attack
		index = 4;

		degree = 10.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.5f);

		degree = 10.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), sin(degree* M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(3.7f);

	}
	else if (m_State == 5) 
	{
		//temp variable
		int index = 0;
		float degree = 0.0f;


		//first attack
		index = 0;

		degree = 16.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(3.0f);

		degree = 16.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), sin(degree* M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.6f);


		//second attack
		index = 1;

		degree = 20.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.6f);

		degree = 20.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), sin(degree* M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.6f);

		degree = 0.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(3.3f);


		//third attack
		index = 2;

		degree = 22.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.7f);

		degree = 22.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), sin(degree* M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.7f);


		//fourth attack
		index = 3;

		degree = 24.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.7f);

		degree = 24.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), sin(degree* M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(2.7f);

		degree = 0.0f;
		m_ArchmonsterDirection[index].push_back(Vector2D(cos(degree* M_PI / 180.0f), -sin(degree * M_PI / 180.0f)));
		m_ArchmonsterDestroyTime[index].push_back(3.3f);


		//fifth attack
		index = 4;

		//same as third attack
		m_ArchmonsterDirection[index] = m_ArchmonsterDirection[2];
		m_ArchmonsterDestroyTime[index] = m_ArchmonsterDestroyTime[2];


		//sixth attack
		index = 5;

		//same as second attack
		m_ArchmonsterDirection[index] = m_ArchmonsterDirection[1];
		m_ArchmonsterDestroyTime[index] = m_ArchmonsterDestroyTime[1];


		//seven attack
		index = 6;

		//same as first attack
		m_ArchmonsterDirection[index] = m_ArchmonsterDirection[0];
		m_ArchmonsterDestroyTime[index] = m_ArchmonsterDestroyTime[0];


	}

	m_IsAttack = true;

}

void Arzeros::Update_FinishState(float deltatime)
{
	//if the state is finished
	if (m_FinishState)
	{
		if (m_State == 0)
		{
			m_Rigidbody->ApplyForceY(-ARZEROS_ACCELERATION);
		}
		else if (m_State == 1)
		{
			m_Rigidbody->ApplyForceX(ARZEROS_ACCELERATION);
			Blink(deltatime);
		}
		else if (m_State == 2)
		{
			if (m_CanMove) 
			{
				m_Rigidbody->ApplyForceX(-ARZEROS_ACCELERATION);
			}
			
			Blink(deltatime);
		}
		else if (m_State == 3)
		{
			m_Rigidbody->ApplyForceY(-ARZEROS_ACCELERATION);
		}
		else if (m_State == 4)
		{
			m_Rigidbody->ApplyForceX(-ARZEROS_ACCELERATION);
			Blink(deltatime);
		}
		else if (m_State == 5)
		{
			m_Rigidbody->ApplyForceX(-ARZEROS_ACCELERATION);
			Blink(deltatime);
		}
	}

}


void Arzeros::Update_Hurt(float deltatime)
{
	if (m_IsHurt)
	{
		m_HurtTime += deltatime;

		if (m_HurtTime >= ARZEROS_HURT_TIME)
		{
			m_HurtTime = 0.0f;
			m_IsHurt = false;

			//set finish state
			m_FinishState = true;
			

		}
	}
}


void Arzeros::Update_Attack(float deltatime)
{
	if (m_State < 1 || m_IsHurt || m_FinishState)
	{
		m_IsAttack = false;
		return;
	}

	if (!m_IsAttack)
	{
		m_AttackCooldown += deltatime;

		if ((m_State == 1 && m_AttackCooldown >= ARZEROS_ATTACK_COOLDOWN_STATE_1) || (m_State == 2 && m_AttackCooldown >= ARZEROS_ATTACK_COOLDOWN_STATE_2) || (m_State == 4 && m_AttackCooldown >= ARZEROS_ATTACK_COOLDOWN_STATE_4) || (m_State == 5 && m_AttackCooldown >= ARZEROS_ATTACK_COOLDOWN_STATE_5))
		{
			m_IsAttack = true;
		}
	}
	else
	{
		if (m_AttackCount < m_ArchmonsterDestroyTime.size())
		{
			m_AttackDelay += deltatime;

			if (m_AttackDelay >= ARZEROS_ATTACK_DELAY)
			{
				m_AttackDelay = 0.0f;
				m_AttackCount++;

				SpawnArchMonster();
			}
		}

		//end of attacking animation
		if (m_Anim->GetIsEnded())
		{
			if (m_AttackCount < m_ArchmonsterDestroyTime.size())
			{
				m_Anim->SetProps("arzeros_idle", 8, 100);
				m_IsAttack = true;
			}
			else
			{
				m_IsAttack = false;
				m_AttackCount = 0;
			}

			m_AttackDelay = 0.0f;
			m_AttackCooldown = 0.0f;
		}

	}
}


void Arzeros::Update_Moving()
{

	//limits the velocity
	//velocity X
	if (m_Rigidbody->m_velocity.x < -ARZEROS_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.x = -ARZEROS_MAX_VELOCITY;
	}
	else if (m_Rigidbody->m_velocity.x > ARZEROS_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.x = ARZEROS_MAX_VELOCITY;
	}
	//velocity Y
	if (m_Rigidbody->m_velocity.y < -ARZEROS_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.y = -ARZEROS_MAX_VELOCITY;
	}
	else if (m_Rigidbody->m_velocity.y > ARZEROS_MAX_VELOCITY)
	{
		m_Rigidbody->m_velocity.y = ARZEROS_MAX_VELOCITY;
	}

	//change sprite flipping direction
	if (m_Rigidbody->m_velocity.x < -ARZEROS_FLIP_VELOCITY && m_Sprite->m_Flip == SDL_FLIP_NONE)
	{
		m_Sprite->m_Flip = SDL_FLIP_HORIZONTAL;
	}
	else if (m_Rigidbody->m_velocity.x > ARZEROS_FLIP_VELOCITY && m_Sprite->m_Flip == SDL_FLIP_HORIZONTAL)
	{
		m_Sprite->m_Flip = SDL_FLIP_NONE;
	}

	//set new position
	m_Transform->Translate(m_Rigidbody->Position());
	SetCollider();

	//check whether the object's position is off the map
	if (m_Collider->Get().x + m_Collider->Get().w < 0 || m_Collider->Get().x > m_ColCount * m_TileSize || m_Collider->Get().y + m_Collider->Get().h < 0 || m_Collider->Get().y > m_RowCount * m_TileSize)
	{
		//set event
		CutsceneManager::GetInstance()->m_IsBossEvent = false;

		LevelManager::GetInstance()->ResumeTime();

		//destroy the object
		owner->Destroy();
	}

}


void Arzeros::Update_Collision()
{

	//if player collides with this
	if (CollisionTriggerred())
	{
		//if the player is dashing
		if (m_Player->Get_IsDashing())
		{
			//set IsDashThrough of Hero
			m_Player->m_IsDashThrough = true;
			m_IsHurt = true;

			//collision is disabled
			m_Collider->m_IsEnable = false;

			//set event
			CutsceneManager::GetInstance()->m_IsBossEvent = true;
		}
		//if the player is not blinking
		else if (!m_Player->Get_IsBlinking())
		{
			//the player hurts
			m_Player->Dead();
		}
	}
}


void Arzeros::Update_Animation()
{

	if (m_IsHurt)
	{
		m_Anim->SetProps("arzeros_hurt", 2, 0);
	}
	else if (m_IsAttack)
	{
		m_Anim->SetProps("arzeros_attack", 8, 100, false);
	}
	else
	{
		m_Anim->SetProps("arzeros_idle", 8, 200);
	}
}


void Arzeros::SetCollider()
{
	if (m_Sprite->m_Flip == SDL_FLIP_HORIZONTAL)
	{
		m_Collider->SetBuffer(-10, -22, 76, 40);
	}
	else
	{
		m_Collider->SetBuffer(-66, -22, 76, 40);
	}
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());
}


void Arzeros::SpawnArchMonster()
{

	float offsetX = -40.0f;

	//set all monster's spawning position
	Vector2D spawnPos(m_Transform->position.x + offsetX, m_Transform->position.y + 40);
	//check new offset when it is flipped
	if (m_Sprite->m_Flip == SDL_FLIP_NONE) 
	{
		spawnPos.x = m_Transform->position.x + (m_Sprite->GetWidth() / 2) - offsetX;
	}

	for (int i = 0; i < m_ArchmonsterDestroyTime[m_AttackCount - 1].size(); i++)
	{
		//get random number between 0 - 100
		float percent = GetRandom(0.0f, 100.0f);

		//compare it with the constant number comparator
		//if the percentage is higher -> spawn archmonster
		if (percent > ARZEROS_SPELL_PERCENTAGE) 
		{
			//initialize Arch Monster Entity
			Entity& archMonster(EntityManager::GetInstance()->AddEntity("ArchMonster" + i));

			//initialize components for Arch Monster entity
			//Transform Component
			archMonster.AddComponent<Transform>(spawnPos.x, spawnPos.y);
			//Sprite Component
			archMonster.AddComponent<Sprite>("arch-monster-idle", 96, 96, MONSTER_LAYER);
			//Collider Component
			archMonster.AddComponent<Collider>();
			//SpriteAnimation Component
			archMonster.AddComponent<SpriteAnimation>();
			//Rigidbody Component
			archMonster.AddComponent<Rigidbody>();
			//ArchMonster Component
			archMonster.AddComponent<ArchMonster>(m_ArchmonsterDestroyTime[m_AttackCount - 1][i], m_ArchmonsterDirection[m_AttackCount - 1][i]);

			archMonster.GetComponent<ArchMonster>()->SetPlayer(m_Player);
		}
		//if it is less than
		//if the percentage is less than -> spawn spellmonster
		else 
		{
			//initialize Spell Monster Entity
			Entity& spellMonster(EntityManager::GetInstance()->AddEntity("SpellMonster" + i));

			//initialize components for Spell Monster entity
			//Transform Component
			spellMonster.AddComponent<Transform>(spawnPos.x, spawnPos.y);
			//Sprite Component
			spellMonster.AddComponent<Sprite>("spell-monster-idle", 96, 96, MONSTER_LAYER);
			//Collider Component
			spellMonster.AddComponent<Collider>();
			//SpriteAnimation Component
			spellMonster.AddComponent<SpriteAnimation>();
			//Rigidbody Component
			spellMonster.AddComponent<Rigidbody>();
			//SpellMonster Component
			spellMonster.AddComponent<SpellMonster>(m_ArchmonsterDestroyTime[m_AttackCount - 1][i], m_ArchmonsterDirection[m_AttackCount - 1][i]);

			spellMonster.GetComponent<SpellMonster>()->SetPlayer(m_Player);
		}

		
	}

}


void Arzeros::Blink(float deltatime)
{
	m_BlinkTime += deltatime;

	if (m_BlinkTime >= ARZEROS_BLINK_LENGTH)
	{
		m_BlinkTime = 0.0f;

		m_Sprite->m_IsEnable = !m_Sprite->m_IsEnable;
	}
}


void Arzeros::FinishState()
{
	m_FinishState = true;
}


void Arzeros::Attack()
{
	m_IsAttack = true;
}

bool Arzeros::IsFinish() 
{
	return m_FinishState;
}

void Arzeros::SetMoving(bool set) 
{
	m_CanMove = set;
}
