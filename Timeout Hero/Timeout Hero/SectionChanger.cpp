﻿#include "SectionChanger.h"
#include "LevelManager.h"

SectionChanger::SectionChanger(int indexChanging, Direction dir, Vector2D imagePos)
{
	m_IndexChanging = indexChanging;
	m_Direction = dir;
	m_ImagePos = imagePos;
}

void SectionChanger::Initialize() 
{
	// Initialize Component
	m_Transform = owner->GetComponent<Transform>();
	m_Collider = owner->GetComponent<Collider>();
	

	int tileSize = LevelManager::GetInstance()->GetMap()->GetTileSize();
	int rowCount = LevelManager::GetInstance()->GetMap()->GetRowCount();
	int colCount = LevelManager::GetInstance()->GetMap()->GetColCount();

	Initialize_SC(tileSize, colCount, rowCount);

	if (m_IndexChanging > 0) 
	{
		Initialize_Image(tileSize, colCount, rowCount);
	}
	
}


void SectionChanger::Update(float deltaTime) 
{
	//if Hero collides with this entity
	if (CollisionTriggerred()) 
	{
		//collider is disable
		m_Collider->m_IsEnable = false;

		//level index is changed
		LevelManager::GetInstance()->levelIndex += m_IndexChanging;
		//section is changed
		LevelManager::GetInstance()->sectionChange += m_IndexChanging;
		//set starting time for next section
		LevelManager::GetInstance()->SetStartingTime(LevelManager::GetInstance()->GetTime());
		//pause the time counter
		LevelManager::GetInstance()->PauseTime();

		//Hero enters warp state
		m_Player->Warp(m_Direction);

		if (m_IndexChanging < 0) 
		{
			LevelManager::GetInstance()->SetIsDeLevel();
		}

	}

}


void SectionChanger::Initialize_SC(int tileSize, int colCount, int rowCount) 
{
	if (m_Direction == UP)
	{

		m_Transform->position.x = 0;
		m_Transform->position.y = 0;

		m_Collider->Set(m_Transform->position.x, m_Transform->position.y, tileSize * colCount, SC_LENGTH);

	}
	else if (m_Direction == LEFT)
	{

		m_Transform->position.x = 0;
		m_Transform->position.y = 0;

		m_Collider->Set(m_Transform->position.x, m_Transform->position.y, SC_LENGTH, tileSize * rowCount);

	}
	else if (m_Direction == RIGHT)
	{

		m_Transform->position.x = (tileSize * colCount) - SC_LENGTH;
		m_Transform->position.y = 0;

		m_Collider->Set(m_Transform->position.x, m_Transform->position.y, SC_LENGTH, tileSize * rowCount);
	}
	else if (m_Direction == DOWN)
	{

		m_Transform->position.x = 0;
		m_Transform->position.y = (tileSize * rowCount) - SC_LENGTH;

		m_Collider->Set(m_Transform->position.x, m_Transform->position.y, tileSize * colCount, SC_LENGTH);
	}
}

void SectionChanger::Initialize_Image(int tileSize, int colCount, int rowCount) 
{
	Entity& image(EntityManager::GetInstance()->AddEntity("SectionChangerImage" + std::to_string(m_Direction)));

	image.AddComponent<Transform>(m_ImagePos.x, m_ImagePos.y);
	image.AddComponent<Sprite>("section_changer_horizontal", 34, 34, RUNE_LAYER);
	image.AddComponent<SpriteAnimation>();

	if (m_Direction == UP)
	{
		image.GetComponent<Sprite>()->m_Flip = SDL_FLIP_VERTICAL;

		image.GetComponent<SpriteAnimation>()->SetProps("section_changer_vertical", 7, 100);

		image.GetComponent<Transform>()->position.y = SC_IMAGE_OFFSET;
	}
	else if (m_Direction == LEFT)
	{
		image.GetComponent<Sprite>()->m_Flip = SDL_FLIP_HORIZONTAL;

		image.GetComponent<SpriteAnimation>()->SetProps("section_changer_horizontal", 7, 100);

		image.GetComponent<Transform>()->position.x = SC_IMAGE_OFFSET;
	}
	else if (m_Direction == RIGHT)
	{
		image.GetComponent<SpriteAnimation>()->SetProps("section_changer_horizontal", 7, 100);

		image.GetComponent<Transform>()->position.x = (tileSize * colCount) - SC_IMAGE_OFFSET - image.GetComponent<Sprite>()->GetWidth();
	}
	else if (m_Direction == DOWN)
	{
		image.GetComponent<SpriteAnimation>()->SetProps("section_changer_vertical", 7, 100);

		image.GetComponent<Transform>()->position.y = (tileSize * rowCount) - SC_IMAGE_OFFSET - image.GetComponent<Sprite>()->GetHeight();
	}
}