#pragma once 
#ifndef ARTRIUS_H
#define ARTRIUS_H

#define ARTRIUS_SPEED 5.0f
#define ARTRIUS_OFFSET_X 40.0f
#define ARTRIUS_OFFSET_Y 20.0f
#define ARTRIUS_DEAD_ZONE 10.0f

#include "LevelObject.h"
#include "Constants.h"

class Artrius : public LevelObject
{
	public:
		Artrius(){}
		~Artrius(){}
		void Initialize() override;
		void Update(float deltaTime) override;

		void Transforming();

	private:
		Vector2D m_MovingDirection = Vector2D(1, 0);
		Vector2D last_position;

		bool m_IsTransforming = false;

		Vector2D m_TransformingPos = Vector2D(864.0f, 528.0f);
};

#endif