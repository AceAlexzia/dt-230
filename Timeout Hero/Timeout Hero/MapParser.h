#pragma once
#ifndef MAP_PARSER_H
#define MAP_PARSER_H

#include <map>
#include <string>
#include "GameMap.h"
#include "TileLayer.h"
#include "tinyxml-master/tinyxml.h"
#include "Constants.h"

class MapParser
{
	public:
		~MapParser() {}
		bool Load(std::string id, std::string source);
		void Clean();
		GameMap* GetMap(std::string id);
		static MapParser* GetInstance();

	private:
		MapParser() {}
		bool Parse(std::string id, std::string source);
		TileSet ParseTileSet(TiXmlElement* xmlTileset);
		TileLayer* ParseTileLayer(TiXmlElement* xmlLayer, TileSetList tilesets, int tileSize, int rowCount, int colCount);

		static MapParser* s_Instance;
		std::map<std::string, GameMap*> m_MapDict;
};

#endif