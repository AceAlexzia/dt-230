#include "AssetManager.h"
#include "TransformComponent.h"

AssetManager* AssetManager::s_Instance = nullptr;

AssetManager::AssetManager() {}

AssetManager* AssetManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new AssetManager();
}

AssetManager::~AssetManager()
{
    ClearData();
}

void AssetManager::ClearData() {
    textures.clear();
    fonts.clear();
}

void AssetManager::AddTexture(std::string textureId, std::string filePath) {
    textures.emplace(textureId, TextureManager::GetInstance()->LoadTexture(("../Assets/" + filePath).c_str()));
}

SDL_Texture* AssetManager::GetTexture(std::string textureId) {
    return textures[textureId];
}

TTF_Font* AssetManager::GetFont(std::string fontId) {
    return fonts[fontId];
}
