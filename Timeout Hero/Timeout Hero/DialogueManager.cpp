﻿#include "DialogueManager.h"

DialogueManager* DialogueManager::s_Instance = nullptr;

DialogueManager* DialogueManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new DialogueManager();
}

DialogueManager::DialogueManager() 
{
	m_SpeakerPanelID["Anonymous Guy"] = "Dialogue_AnonymousGuy";
	m_SpeakerPanelID["Archfiend"] = "Dialogue_Archfiend";
	m_SpeakerPanelID["Artrius"] = "Dialogue_Artrius";
	m_SpeakerPanelID["Arzeros"] = "Dialogue_Arzeros";
	m_SpeakerPanelID["Hero"] = "Dialogue_Hero";
	m_SpeakerPanelID["Princess"] = "Dialogue_Princess";
}

void DialogueManager::Update(float dt)
{
	//if dialogue is playing -> display it
	if (m_IsSpeaking) 
	{
		Display(dt);
	}
}

void DialogueManager::Speak(std::string cutsceneName)
{

	m_DataFile.open("../Assets/Dialogue/" + cutsceneName + ".txt");

	if (m_DataFile.is_open())
	{
		while (!m_DataFile.eof()) 
		{
			std::string word = "";
			getline(m_DataFile, word);
			m_DialogueVect.push_back(word);
		}

		m_DataFile.close();

		m_IsSpeaking = true;

		m_DialogueLine = 0;

		AddStaticticDialogue(cutsceneName);

		NextDialogue();
	}
}

void DialogueManager::Display(float dt)
{
	//get continue input
	bool isPress = InputManager::GetInstance()->GetKeyUp(SDL_SCANCODE_SPACE) || InputManager::GetInstance()->GetKeyUp(SDL_CONTROLLER_BUTTON_A);

	//increase dialogue counter
	m_DialogueCounter += dt;

	//if dialogue counter reaches its max value AND more letter will be displayed
	if (m_DialogueCounter > DIALOGUE_COUNTDOWN && !m_CurrentDialogue.empty()) 
	{
		std::string str(1, *m_CurrentDialogue.begin());
		//print out first letter
		AddDialogueText(str);
		//erase first letter
		m_CurrentDialogue.erase(m_CurrentDialogue.begin());
		//reset dialogue counter
		m_DialogueCounter = 0.0f;
	}

	//get skip dialogue boolean
	bool isSkipDialogue = SaveManager::GetInstance()->opt.skipDialogue;
	m_DialoguePanel->GetComponent<Sprite>()->m_IsEnable = !isSkipDialogue;
	m_DialgoueText->GetComponent<Text>()->m_IsEnable = !isSkipDialogue;
	m_DialgoueText2->GetComponent<Text>()->m_IsEnable = !isSkipDialogue;

	//if the player press continue
	if ((isPress || isSkipDialogue) && !m_IsPaused)
	{
		//if dialogue is ended
		if (m_CurrentDialogue.empty()) 
		{
			//go to next dialogue
			NextDialogue();
		}
		//if it is not ended
		else
		{
			while (!m_CurrentDialogue.empty()) 
			{
				std::string str(1, *m_CurrentDialogue.begin());
				//print out first letter
				AddDialogueText(str);
				//erase first letter
				m_CurrentDialogue.erase(m_CurrentDialogue.begin());
			}

			//reset dialogue counter
			m_DialogueCounter = 0.0f;
		}
	}

	if (m_DialoguePanel != nullptr) 
	{
		Vector2D campos = Camera::GetInstance()->GetPosition();
		m_DialoguePanel->GetComponent<Transform>()->position = Vector2D(232 + campos.x, 40 + campos.y);
	}
}


void DialogueManager::NextDialogue() 
{

	//if no dialogue will be displayed -> end the dialogue displaying
	if (m_DialogueVect.empty()) 
	{
		m_IsSpeaking = false;
		m_DialogueCounter = 0.0f;

		DestroyDialogue();

		return;
	}


	//convert first element of dialogue vector to stringstream
	std::stringstream word(m_DialogueVect.front());

	//erase first element of the dialogue vector
	m_DialogueVect.erase(m_DialogueVect.begin());

	//separate speaker and dialogue
	std::getline(word, m_Speaker, ':');
	std::getline(word, m_CurrentDialogue);

	if (m_DialoguePanel == nullptr) 
	{
		SpawnDialogue();
	}

	SetDialogueSpeaker();

	m_DialogueLine++;
}

bool DialogueManager::IsSpeaking() 
{
	return m_IsSpeaking;
}


int DialogueManager::GetDialogueLine() 
{
	return m_DialogueLine;
}

void DialogueManager::Pause() 
{
	m_IsPaused = true;
}

void DialogueManager::Resume() 
{
	m_IsPaused = false;
	NextDialogue();
}

bool DialogueManager::IsPausingDialogue() 
{
	return m_IsPaused;
}

void DialogueManager::SpawnDialogue(bool isTop) 
{
	Vector2D campos = Camera::GetInstance()->GetPosition();

	//dialogue panel
	Entity& panelEntity(EntityManager::GetInstance()->AddEntity("DialoguePanel"));
	panelEntity.AddComponent<Transform>(232 + campos.x, 40 + campos.y);
	panelEntity.AddComponent<Sprite>(m_SpeakerPanelID[m_Speaker], 1136, 288, TIME_COUNT_LAYER);

	m_DialoguePanel = &panelEntity;


	//dialogue text
	Entity& textEntity(EntityManager::GetInstance()->AddEntity("DialogueText"));
	textEntity.AddComponent<Transform>(317, 159);
	textEntity.AddComponent<Text>(TIME_COUNT_LAYER, "DialogueText");
	//set text property
	Text* text = textEntity.GetComponent<Text>();
	text->font = DIALOGUE_FONT_NAME;
	text->size = 48;
	text->color = { 255, 255, 255, 255 };
	text->outlineSize = 0;

	m_DialgoueText = &textEntity;


	//dialogue text
	Entity& textEntity2(EntityManager::GetInstance()->AddEntity("DialogueText2"));
	textEntity2.AddComponent<Transform>(317, 223);
	textEntity2.AddComponent<Text>(TIME_COUNT_LAYER, "DialogueText2");
	//set text property
	Text* text2 = textEntity2.GetComponent<Text>();
	text2->font = DIALOGUE_FONT_NAME;
	text2->size = 48;
	text2->color = { 255, 255, 255, 255 };
	text2->outlineSize = 0;

	m_DialgoueText2 = &textEntity2;

}

void DialogueManager::DestroyDialogue() 
{
	if (m_DialoguePanel != nullptr) 
	{
		m_DialoguePanel->Destroy();
		m_DialoguePanel = nullptr;
	}

	if (m_DialgoueText != nullptr)
	{
		m_DialgoueText->Destroy();
		m_DialgoueText = nullptr;
	}

	if (m_DialgoueText2 != nullptr)
	{
		m_DialgoueText2->Destroy();
		m_DialgoueText2 = nullptr;
	}
}

void DialogueManager::SetDialogueSpeaker() 
{
	m_DialoguePanel->GetComponent<Sprite>()->SetTexture(m_SpeakerPanelID[m_Speaker]);
	m_DialgoueText->GetComponent<Text>()->text = "";
	m_DialgoueText2->GetComponent<Text>()->text = "";
	m_CurrentText = m_DialgoueText->GetComponent<Text>();
}

void DialogueManager::AddDialogueText(std::string textToAdd)
{	
	if (m_CurrentText != m_DialgoueText2->GetComponent<Text>() && m_CurrentText->text.size() > 35 && textToAdd == " ")
	{
		m_CurrentText = m_DialgoueText2->GetComponent<Text>();
		return;
	}

	m_CurrentText->text = m_CurrentText->text + textToAdd;
}

void DialogueManager::Reset() 
{
	if (m_DataFile.is_open())
	{
		m_DataFile.close();
	}

	m_IsSpeaking = false;
	m_DialogueCounter = 0.0f;
	m_CurrentDialogue.erase();
	DestroyDialogue();
	m_IsPaused = false;
	m_DialogueLine = 0;
	m_DialogueVect.clear();
}


void DialogueManager::AddStaticticDialogue(std::string cutsceneName) 
{
	if (cutsceneName != "Level-H-1") 
	{
		return;
	}

	Saveslot saveslot = SaveManager::GetInstance()->save[SaveManager::GetInstance()->currentSlotPos];

	m_DialogueVect[5] = m_DialogueVect[5] + saveslot.mode + " mode";

	std::ostringstream out;
	out << std::fixed << std::setprecision(1) << saveslot.totalTime / 60.0f;
	m_DialogueVect[6] = m_DialogueVect[6] + out.str() + " min";

	m_DialogueVect[7] = m_DialogueVect[7] + std::to_string(saveslot.death) + " times";
}
