#include "InputManager.h"

InputManager* InputManager::s_Instance = nullptr;
ControllerManager InputManager::s_ControllerManager;

InputManager::InputManager() 
{
	m_KeyState = SDL_GetKeyboardState(nullptr);

	SDL_GameControllerEventState(SDL_ENABLE);
}

InputManager* InputManager::GetInstance() 
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new InputManager();
}

void InputManager::Listen() 
{
	s_ControllerManager.BeforeListen();

	SDL_Event event;
	while (SDL_PollEvent(&event)) 
	{
		if (event.type == SDL_QUIT || GetKeyDown(SDL_SCANCODE_Q) || GetKeyDown(SDL_CONTROLLER_BUTTON_BACK))
		{
			Game::GetInstance()->Quit();
		}

		//update controller
		s_ControllerManager.Listen(event);
	}

	ClearUnpressedKeys();

}


bool InputManager::GetKey(SDL_Scancode key) 
{
	if (IsPressingKey(key) == -1 && m_KeyState[key] == 1)
	{
		m_PressingKeys.push_back(key);
	}
	return m_KeyState[key] == 1;
}

bool InputManager::GetKeyDown(SDL_Scancode key) 
{
	if (IsPressingKey(key) == -1 && m_KeyState[key] == 1) 
	{
		m_PressingKeys.push_back(key);
		return true;
	}
	else 
	{
		return false;
	}
}

bool InputManager::GetKeyUp(SDL_Scancode key)
{
	if (GetKey(key)) 
	{
		return false;
	}
	else 
	{

		if (m_KeyUpKeys.size() == 0) 
		{
			return false;
		}

		for (int i = 0; i < m_KeyUpKeys.size(); i++) 
		{
			if (m_KeyUpKeys[i] == key) 
			{
				return true;
			}
		}

		return false;
	}
}

int InputManager::GetAxisKey(Axis axis) {
	if (axis == HORIZONTAL) 
	{
		if (GetKey(SDL_SCANCODE_D)) 
		{
			return 1;
		}
		else if (GetKey(SDL_SCANCODE_A)) 
		{
			return -1;
		}
	}
	else if (axis == VERTICAL) 
	{
		if (GetKey(SDL_SCANCODE_W)) 
		{
			return 1;
		}
		else if (GetKey(SDL_SCANCODE_S)) 
		{
			return -1;
		}
	}
	return 0;
}

int InputManager::IsPressingKey(SDL_Scancode key) 
{
	if (m_PressingKeys.size() == 0) 
	{
		return -1;
	}

	for (int i = 0; i < m_PressingKeys.size(); i++) 
	{
		if (key == m_PressingKeys[i]) {
			return i;
		}
	}
	return -1;
}

void InputManager::ClearUnpressedKeys() 
{
	if (m_KeyUpKeys.size() > 0) 
	{
		m_KeyUpKeys.clear();
	}

	for (int i = 0; i < m_PressingKeys.size(); i++)
	{
		if (m_KeyState[m_PressingKeys[i]] == 0)
		{
			m_KeyUpKeys.push_back(m_PressingKeys[i]);
			m_PressingKeys.erase(m_PressingKeys.begin() + i);
			i--;
		}
	}
}


bool InputManager::GetKey(SDL_GameControllerButton button)
{
	return s_ControllerManager.GetKey(button);
}

bool InputManager::GetKeyUp(SDL_GameControllerButton button)
{
	return s_ControllerManager.GetKeyUp(button);
}

bool InputManager::GetKeyDown(SDL_GameControllerButton button)
{
	return s_ControllerManager.GetKeyDown(button);
}

float InputManager::GetAxisController(SDL_GameControllerAxis axis)
{
	return s_ControllerManager.GetAxisController(axis);
}

bool InputManager::HasController()
{
	return s_ControllerManager.HasController();
}
