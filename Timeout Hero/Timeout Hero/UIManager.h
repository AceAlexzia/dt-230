#pragma once
#ifndef UIMANAGER_H
#define UIMANAGER_H
#include "Button.h"
#include "TextButton.h"
#include "CheckBox.h"
#include "UIComponent.h"
#include "SceneManager.h"
#include "PauseManager.h"
#include "TextComponent.h"
#include "TransformComponent.h"
#include "TextButtonSaveSlot.h"
#include "SaveSlotButton.h"
#include "SpriteComponent.h"
#include "ButtonInSaveSlot.h"
#include "Entity.h"
#include "Camera.h"
#include "Vector2D.h"
#include "Adjuster.h"
#include "SaveManager.h"
#include "LevelManager.h"
#include <iomanip>
#include <map>
#include <vector>
#define CONTROLCOOLDOWN 0.15f
#include <map>
class UIManager
{
	public:
		~UIManager();
		//void Initialize();
		void InitializeMenu();
		void SetSaveSlotPos();
		void InitializeSplash();
		void Update(float deltaTime);
		void Clean();
		void DisableAllUI();
		void PressMainMenu();
		void PressOption();
		void PressPauseMenu();
		void PressApplyButton();
		void SpawnSaveSlotUI();
		void SpawnMenuUI();
		void SpawnOptionUI();
		void SpawnPauseUI();
		void RemoveSaveslot(int pos);
		void PressEmpty(int pos);
		void DeleteTemporary();
		void RemoveMainUI();
		void RemovePauseUI();
		void GoToMainScene();
		void CreatSlot(int pos);
		void DeleteMainMenuUI();
		void DeleteOption();
		void ClearAllUI();
		void DeleteAllSaveSlot();
		void CancelSaveSlot(int pos);
		void DeleteSlot(int i);
		void NoHeroStandDeleteSave(int pos);
		void SpawnOptionPauseMenu();
		void PressUndoPauseOption();
		void PressOptionPause();
		void DeletePauseUI();
		bool checkPause;

		static UIManager* GetInstance();
	private:
		std::map<std::string, Entity*> m_AllUI;
		Entity* currentUI;
		static UIManager* s_Instance;

		Entity* GetSelectingButton();
		float cooldown = 0.0f;
		bool canMove;
		bool inCooldown;

		std::vector<std::string> resolution_text;
		std::vector<std::string> masterVolume_text;
		std::vector<std::string> sfxVolume_text;
		std::vector<std::string> musicVolume_text;
		std::vector<std::string> screenShake_text;
		std::vector<Vector2D> menuButton_Pos;
		std::vector<Entity*> pauseUI;
		std::vector<Entity*> startingUI;
		std::vector<Entity*> optionUI;
		std::vector<Entity*> saveSlotUI;

		std::vector<Entity*> temporarySlot;

		Entity* undo;
		Entity* startButton;
		Entity* resumeButton;
		Entity* firstSaveSlot;
		Entity* saveSlot[3];
		Entity* delButton[3];

		void Move(Direction direction);
		bool availableSave[3] = {false, false, false };


		Entity* allSaveSlot[3];

		Vector2D panelPos[3];
		Vector2D difficultyTextPos[3];
		Vector2D easyText[3];
		Vector2D normalText[3];
		Vector2D hardText[3];
		Vector2D timeoutText[3];
		Vector2D noheroText[3];
		Vector2D undoButton[3];

		int currentPos = 0;

		SDL_Color white = { 255,255,255,255 };
		SDL_Color red = { 255,0,0,255 };
		SDL_Color black = { 0,0,0,255 };
};

#endif UIMANAGER_H