#pragma once
#ifndef TIMER
#define TIMER


#define TIME_SPACE 0.17f;
#define SLOW_TIME_SCALE 0.18f;


#include "SDL.h"
#include "Constants.h"

class Timer
{
private :

	static Timer* s_Instance;

	//last frame delta time
	int m_TicksLastFrame = 0;
	//delta time value
	float m_DeltaTime = 0.0f;

	//time space while in slow motion period
	float m_TimeSpace = 0.0f;

	//handling var
	float m_HandlingCounter = 0.0f;

	Timer() {}

	//set m_TimeSpace
	void SetTimeSpace(float timeSpace);

public :
	//make delta time faster or slower
	float timeScale = 1.0f;

	static Timer* GetInstance();

	//get delta time with time scaling value
	float GetDeltaTime();
	//get delta time WITHOUT time scaling value
	float GetRawDeltaTime();

	void Update(float dt);

	//start slow motion
	void SlowMotion();

};

#endif // !TIMER
