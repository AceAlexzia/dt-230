#include <iostream>
#include "Entity.h"
#include "TransitionBox.h"

Entity::Entity(EntityManager& manager): manager(manager) 
{
    this->isActive = true;
}

Entity::~Entity()
{
    for (int i = 0; i < components.size(); i++)
    {
        Component* deletedComponent = components[i];
        delete deletedComponent;
        deletedComponent = nullptr;
    }

    components.clear();
}

Entity::Entity(EntityManager& manager, std::string name): manager(manager), name(name) 
{
    this->isActive = true;
}

void Entity::Update(float deltaTime) 
{

    //if the enity is about to be destroyed -> no need to update anything
    if (!isActive) 
    {
        return;
    }

    //loop through each component of the entity
    for (int i = 0 ; i < components.size(); i++) 
    {
        if (components[i] != nullptr)
        {
            //update the component
            components[i]->Update(deltaTime);
        }
    }
}

void Entity::Destroy() 
{
    //destroy everything unless it has the undestroyable component
    if (!this->HasComponent<TransitionBox>()) 
    {
        this->isActive = false;
    }
    
}

bool Entity::IsActive() const 
{
    return this->isActive;
}

void Entity::ListAllComponents() const 
{
    for (auto mapElement: componentTypeMap) 
    {
        std::cout << "    Component<" << mapElement.first->name() << ">" << std::endl;
    }
}
