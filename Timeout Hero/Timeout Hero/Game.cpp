
#include "Game.h"

#include <iostream>
#include "AssetManager.h"
#include "CollisionHandler.h"
#include "Camera.h"
#include "Timer.h"
#include "MapParser.h"
#include "FontManager.h"
#include "LevelManager.h"
#include "PauseManager.h"
#include "SceneManager.h"
#include "TransitionManager.h"
#include "UIManager.h"
#include "ParticleManager.h"
#include "SaveManager.h"

SDL_Renderer* Game::m_Renderer = nullptr;
Game* Game::s_Instance = nullptr;

Game::Game() 
{
	m_IsRunning = false;
}

Game* Game::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new Game();
}

Game::~Game() {}

bool Game::IsRunning() const 
{
    return m_IsRunning;
}

SDL_Window* Game::GetWindow()
{
    return m_Window;
}


//loadign asset funciton
void LoadAsset() 
{
    //Load all Assets as SDL_Texture and store them as a std::map
    //Hero
    AssetManager::GetInstance()->AddTexture("Hero-idle", "Character/Hero/Hero-idle.png");
    AssetManager::GetInstance()->AddTexture("Hero-dash", "Character/Hero/Hero-dash.png");
    AssetManager::GetInstance()->AddTexture("Hero-fall-grey", "Character/Hero/Hero-fall-grey.png");
    AssetManager::GetInstance()->AddTexture("Hero-fall-yellow", "Character/Hero/Hero-fall-yellow.png");
    AssetManager::GetInstance()->AddTexture("Hero-hurt", "Character/Hero/Hero-hurt.png");
    AssetManager::GetInstance()->AddTexture("Hero-jump-idle", "Character/Hero/Hero-jump-idle.png");
    AssetManager::GetInstance()->AddTexture("Hero-jump-grey", "Character/Hero/Hero-jump-grey.png");
    AssetManager::GetInstance()->AddTexture("Hero-walk", "Character/Hero/Hero-walk.png");
    AssetManager::GetInstance()->AddTexture("Hero-wall-jump", "Character/Hero/Hero-wall-jump.png");
    //Altrium Rune
    AssetManager::GetInstance()->AddTexture("altrium_rune_off", "LevelObject/AltriumRune/altrium-rune-off.png");
    AssetManager::GetInstance()->AddTexture("altrium_rune_on", "LevelObject/AltriumRune/altrium-rune-on.png");
    //Monster
    AssetManager::GetInstance()->AddTexture("normal_monster_idle", "LevelObject/LittleMonster/normal-monster-idle.png");
    AssetManager::GetInstance()->AddTexture("vertical_monster_idle", "LevelObject/LittleMonster/vertical-monster-idle.png");
    AssetManager::GetInstance()->AddTexture("horizontal_monster_idle", "LevelObject/LittleMonster/horizontal-monster-idle.png");
    AssetManager::GetInstance()->AddTexture("arch-monster-idle", "LevelObject/LittleMonster/arch-monster-idle.png");
    AssetManager::GetInstance()->AddTexture("spell-monster-idle", "LevelObject/LittleMonster/spell-monster-idle.png");
    //Transition Box
    AssetManager::GetInstance()->AddTexture("blackscreen", "TransitionBox/blackscreen.png");
    //Section Changer
    AssetManager::GetInstance()->AddTexture("section_changer_horizontal", "LevelObject/SectionChanger/SectionChanger-right.png");
    AssetManager::GetInstance()->AddTexture("section_changer_vertical", "LevelObject/SectionChanger/SectionChanger-down.png");
    //Moving Tile
    AssetManager::GetInstance()->AddTexture("Horizontal_Moving_Tile", "LevelObject/MovingTiles/horizontal-moving-tiles.png");
    //Artrius
    AssetManager::GetInstance()->AddTexture("Artrius_idle", "LevelObject/Artrius/Artrius.png");
    //Archfiend
    AssetManager::GetInstance()->AddTexture("archfiend_attack", "LevelObject/Archfiend/archfiend_attack.png");
    AssetManager::GetInstance()->AddTexture("archfiend_fly", "LevelObject/Archfiend/archfiend_fly.png");
    AssetManager::GetInstance()->AddTexture("archfiend_hurt", "LevelObject/Archfiend/archfiend_hurt.png");
    AssetManager::GetInstance()->AddTexture("archfiend_dead", "LevelObject/Archfiend/archfiend_dead.png");
    //Arzeros
    AssetManager::GetInstance()->AddTexture("arzeros_attack", "LevelObject/Arzeros/arzeros_attack.png");
    AssetManager::GetInstance()->AddTexture("arzeros_idle", "LevelObject/Arzeros/arzeros_idle.png");
    AssetManager::GetInstance()->AddTexture("arzeros_hurt", "LevelObject/Arzeros/arzeros_hurt.png");
    AssetManager::GetInstance()->AddTexture("arzeros_hurt_final", "LevelObject/Arzeros/arzeros_hurt_final.png");
    AssetManager::GetInstance()->AddTexture("arzeros_dead", "LevelObject/Arzeros/arzeros_dead.png");
    //Princess
    AssetManager::GetInstance()->AddTexture("princess_idle_cage", "Character/Princess/princess_idle_cage.png");
    AssetManager::GetInstance()->AddTexture("princess_idle_uncage", "Character/Princess/princess_idle_uncage.png");
    AssetManager::GetInstance()->AddTexture("princess_cage_disappear", "Character/Princess/princess_cage_disappear.png");
    //Particle
    AssetManager::GetInstance()->AddTexture("hero_explosion", "Character/Hero/Hero_Explosion2.png");
    AssetManager::GetInstance()->AddTexture("hero_dash", "Character/Hero/Hero-dash2.png");
    AssetManager::GetInstance()->AddTexture("hero_dash_left", "Character/Hero/Hero-dash-left2.png");
    AssetManager::GetInstance()->AddTexture("hero_warp", "Character/Hero/Hero_Warp.png");
    AssetManager::GetInstance()->AddTexture("hero_warp_x", "Character/Hero/Hero_Warp_X.png");
    AssetManager::GetInstance()->AddTexture("hero_getting_rune", "Character/Hero/Hero_Getting_Rune.png");
    AssetManager::GetInstance()->AddTexture("monster_explosion", "LevelObject/LittleMonster/monster_explosion.png");
    AssetManager::GetInstance()->AddTexture("monster_respawn", "LevelObject/LittleMonster/monster_respawn2.png");
    AssetManager::GetInstance()->AddTexture("archmonster_explosion", "LevelObject/LittleMonster/archmonster_explosion.png");
    AssetManager::GetInstance()->AddTexture("archmonster_disppear", "LevelObject/LittleMonster/archmonster_disppear.png");
    AssetManager::GetInstance()->AddTexture("spellmonster_explosion", "LevelObject/LittleMonster/spellmonster_explosion.png");

    //Adjuster Asset
    AssetManager::GetInstance()->AddTexture("AdjusterBox", "UI/Adjuster/Adjuster.png");
    AssetManager::GetInstance()->AddTexture("AdjusterButton", "UI/Adjuster/Adjuster_button.png");
    AssetManager::GetInstance()->AddTexture("AdjusterButton2", "UI/Adjuster/Adjuster_button(2).png");
    //Apply Button Asset
    AssetManager::GetInstance()->AddTexture("ApplyButton", "UI/Apply/Apply.png");
    AssetManager::GetInstance()->AddTexture("ApplyButton2", "UI/Apply/Apply(2).png");
    //CheckBox Asset
    AssetManager::GetInstance()->AddTexture("CheckBox", "UI/Checkbox/Checkbox_white.png");
    AssetManager::GetInstance()->AddTexture("CheckBox2", "UI/Checkbox/Checkbox_white(2).png");
    AssetManager::GetInstance()->AddTexture("CheckBox3", "UI/Checkbox/Checkbox_yellow.png");
    AssetManager::GetInstance()->AddTexture("CheckBox4", "UI/Checkbox/Checkbox_yellow(2).png");
    //Panel Asset
    AssetManager::GetInstance()->AddTexture("SettingPanel", "UI/SettingBG/Panel.png");
    //Back Button Asset
    AssetManager::GetInstance()->AddTexture("Undo", "UI/Undo/Undo.png");
    AssetManager::GetInstance()->AddTexture("Undo2", "UI/Undo/Undo2.png");
    //Saveslot Asset
    AssetManager::GetInstance()->AddTexture("Cancel", "UI/SaveSlot/SaveSlot_Cancel.png");
    AssetManager::GetInstance()->AddTexture("Cancel2", "UI/SaveSlot/SaveSlot_Cancel(2).png");
    AssetManager::GetInstance()->AddTexture("Delete", "UI/SaveSlot/SaveSlot_Delete.png");
    AssetManager::GetInstance()->AddTexture("Delete2", "UI/SaveSlot/SaveSlot_Delete(2).png");
    AssetManager::GetInstance()->AddTexture("SaveEmpty", "UI/SaveSlot/SaveSlot_Empty.png");
    AssetManager::GetInstance()->AddTexture("SaveEmpty2", "UI/SaveSlot/SaveSlot_Empty(2).png");
    AssetManager::GetInstance()->AddTexture("Save", "UI/SaveSlot/SaveSlot_Unempty.png");
    AssetManager::GetInstance()->AddTexture("Save2", "UI/SaveSlot/SaveSlot_Unempty(2).png");
    AssetManager::GetInstance()->AddTexture("BlackBG", "UI/BlackBG.png");
    //Dialgoue Asset
    AssetManager::GetInstance()->AddTexture("Dialogue_AnonymousGuy", "UI/Dialogue/Dialogue_AnonymousGuy.png");
    AssetManager::GetInstance()->AddTexture("Dialogue_Archfiend", "UI/Dialogue/Dialogue_Archfiend.png");
    AssetManager::GetInstance()->AddTexture("Dialogue_Artrius", "UI/Dialogue/Dialogue_Artrius.png");
    AssetManager::GetInstance()->AddTexture("Dialogue_Arzeros", "UI/Dialogue/Dialogue_Arzeros.png");
    AssetManager::GetInstance()->AddTexture("Dialogue_Hero", "UI/Dialogue/Dialogue_Hero.png");
    AssetManager::GetInstance()->AddTexture("Dialogue_Princess", "UI/Dialogue/Dialogue_Princess.png");
    //Tutorial Asset
    AssetManager::GetInstance()->AddTexture("ctrl_canceljump", "UI/Tutorial/ctrl_canceljump.png");
    AssetManager::GetInstance()->AddTexture("ctrl_climb", "UI/Tutorial/ctrl_climb.png");
    AssetManager::GetInstance()->AddTexture("ctrl_dash", "UI/Tutorial/ctrl_dash.png");
    AssetManager::GetInstance()->AddTexture("ctrl_die", "UI/Tutorial/ctrl_die.png");
    AssetManager::GetInstance()->AddTexture("ctrl_glide", "UI/Tutorial/ctrl_glide.png");
    AssetManager::GetInstance()->AddTexture("ctrl_jump", "UI/Tutorial/ctrl_jump.png");
    AssetManager::GetInstance()->AddTexture("ctrl_move", "UI/Tutorial/ctrl_move.png");
    AssetManager::GetInstance()->AddTexture("ctrl_multidash", "UI/Tutorial/ctrl_multidash.png");
    AssetManager::GetInstance()->AddTexture("kb_canceljump", "UI/Tutorial/kb_canceljump.png");
    AssetManager::GetInstance()->AddTexture("kb_climb", "UI/Tutorial/kb_climb.png");
    AssetManager::GetInstance()->AddTexture("kb_dash", "UI/Tutorial/kb_dash.png");
    AssetManager::GetInstance()->AddTexture("kb_die", "UI/Tutorial/kb_die.png");
    AssetManager::GetInstance()->AddTexture("kb_glide", "UI/Tutorial/kb_glide.png");
    AssetManager::GetInstance()->AddTexture("kb_jump", "UI/Tutorial/kb_jump.png");
    AssetManager::GetInstance()->AddTexture("kb_move", "UI/Tutorial/kb_move.png");
    AssetManager::GetInstance()->AddTexture("kb_multidash", "UI/Tutorial/kb_multidash.png");
    AssetManager::GetInstance()->AddTexture("jumphere", "UI/Tutorial/jumphere.png");
    
    //Game Icon Asset
    AssetManager::GetInstance()->AddTexture("MainIcon", "UI/GameIcon/MainIcon.png");

    AssetManager::GetInstance()->AddTexture("Credit", "UI/Credit/Credit.png");
    AssetManager::GetInstance()->AddTexture("KMUTT", "UI/Credit/KMUTT_Logo.png");
    AssetManager::GetInstance()->AddTexture("DD-DT", "UI/Credit/dd dt.png");

}

void Game::Initialize() 
{
    //if SDL cannot initialize
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) 
    {
        std::cerr << "Error initializing SDL." << std::endl;
        return;
    }
    //if SDL_Image cannot initialize
	if (SDL_Init(SDL_INIT_VIDEO) != 0 && IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) != 0) 
    {
		std::cerr << "Error initializing SDL_Image" << std::endl;
		return;
	}
    //if SDL_Ttf cannot initialize
    if (TTF_Init() != 0) 
    {
        std::cerr << "Error initializing SDL_TTF" << std::endl;
        return;
    }
    //------------ Load Option -------------
    SaveManager::GetInstance()->LoadOption();
    //create SDL Window
    if (!SaveManager::GetInstance()->opt.full)
    {
        m_Window = SDL_CreateWindow(NULL, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SaveManager::GetInstance()->opt.resolution_width, SaveManager::GetInstance()->opt.resolution_height, SDL_WINDOW_SHOWN);
    }
    else
    {
        m_Window = SDL_CreateWindow(NULL, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SaveManager::GetInstance()->opt.resolution_width, SaveManager::GetInstance()->opt.resolution_height, SDL_WINDOW_FULLSCREEN);
    }
    //if window is not created properly
    if (!m_Window)
    {
        std::cerr << "Error creating SDL window." << std::endl;
        return;
    }

    //create SDL Renderer
	m_Renderer = SDL_CreateRenderer(m_Window, -1, 0);

    //if renderer is not created
    if (!m_Renderer)
    {
        std::cerr << "Error creating SDL renderer." << std::endl;
        return;
    }

    //disable cursor
    SDL_ShowCursor(SDL_DISABLE);

    //set logical resoulution for renderer scaling
    SDL_RenderSetLogicalSize(m_Renderer, RENDER_LOGICAL_WIDTH, RENDER_LOGICAL_HEIGHT);

    //loading all assets
    LoadAsset();

    //initialize scene manager
    SceneManager::GetInstance()->Initialize();

    //initialize audio manager
    AudioManager::GetInstance()->Initialize();
    
    //set m_IsRunning after complete initalizing everything
    m_IsRunning = true;
}

void Game::ProcessInput() 
{
	InputManager::GetInstance()->Listen();
}

void Game::Update() 
{
    float dt = Timer::GetInstance()->GetDeltaTime();

    //update timer
    Timer::GetInstance()->Update(dt);
    
    //update level manager (only in gameplay scene)
    SceneManager::GetInstance()->Update(dt);
    //update UI in the game
    UIManager::GetInstance()->Update(dt);

    //update all entities 
    EntityManager::GetInstance()->Update(dt);

    //update audio playing
    AudioManager::GetInstance()->Update();

    //framerate checking
    //std::cout << 1.0f / dt << std::endl;
    
    

}

void Game::Render() 
{

    SDL_SetRenderDrawColor(m_Renderer, 0, 0, 0, 255);
    SDL_RenderClear(m_Renderer);

    SceneManager::GetInstance()->Render();
	EntityManager::GetInstance()->Render();
    

    SDL_RenderPresent(m_Renderer);
}

void Game::Loop() 
{
    ProcessInput();
    Update();
    Render();
    EntityManager::GetInstance()->DestroyInActiveEntities();
}


void Game::Clean()
{
    MapParser::GetInstance()->Clean();
    FontManager::GetInstance()->Clean();
    EntityManager::GetInstance()->ClearData();
}


void Game::Quit()
{
	m_IsRunning = false;
}


void Game::Destroy() 
{
    Clean();
    SDL_DestroyRenderer(m_Renderer);
    SDL_DestroyWindow(m_Window);

	//delete all static instance
    Delete(AssetManager::GetInstance());
	Delete(EntityManager::GetInstance());
	Delete(FontManager::GetInstance());
    Delete(TextureManager::GetInstance());
	Delete(InputManager::GetInstance());
	Delete(Timer::GetInstance());
    Delete(Camera::GetInstance());
    Delete(MapParser::GetInstance());
    Delete(CollisionHandler::GetInstance());
    Delete(LevelManager::GetInstance());
    Delete(ParticleManager::GetInstance());
    Delete(PauseManager::GetInstance());
    Delete(SceneManager::GetInstance());
    Delete(TransitionManager::GetInstance());
    Delete(UIManager::GetInstance());
    Delete(SaveManager::GetInstance());
    Delete(DialogueManager::GetInstance());
    Delete(CutsceneManager::GetInstance());
    Delete(AudioManager::GetInstance());
    SDL_Quit();
}
