#include "Credit.h"

Credit::Credit()
{

}

Credit::~Credit()
{

}

void Credit::Initialize()
{
	Camera::GetInstance()->Reset();
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(0, 0);
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("Credit", 1600, 11600, TIME_COUNT_LAYER);
	}
}

void Credit::Update(float dt)
{
	if (first <= 2.0f)
	{
		first += dt;

	}
	if (owner->HasComponent<Transform>())
	{
		//std::cout << "Position.y = " << owner->GetComponent<Transform>()->position.y << std::endl;
		if (owner->GetComponent<Transform>()->position.y <= -10700)
		{
			//std::cout << "124567" << std::endl;
			end = true;
		}
		else if (first >= 2.0f && owner->GetComponent<Transform>()->position.y >= -10700)
		{
			owner->GetComponent<Transform>()->position.y -= ROLLSPEED * dt;

		}
	}
	if (InputManager::GetInstance()->GetKeyUp(SDL_SCANCODE_SPACE) || InputManager::GetInstance()->GetKeyUp(SDL_CONTROLLER_BUTTON_A))
	{
		SceneManager::GetInstance()->LoadScene(MENU_SCENE);
		if (remove(SaveManager::GetInstance()->savefile[SaveManager::GetInstance()->currentSlotPos].c_str()) != 0)
		{
			//perror("Error Deleting File");
		}
		else
		{
			//puts("File Successfully Deleted");
		}
		SaveManager::GetInstance()->hasSave[SaveManager::GetInstance()->currentSlotPos] = false;
		SaveManager::GetInstance()->LoadSave(SaveManager::GetInstance()->currentSlotPos);
	}
	
	
}
