#pragma once
#ifndef TILE_LAYER_H
#define TILE_LAYER_H

#include <string>
#include <vector>

#define LEVEL_PATH "../Assets/GameMap/"

struct TileSet 
{
	int FirstID, LastID;
	int RowCount, ColCount;
	int TileCount, TileSize;
	std::string Name, Source;
};

using TileSetList = std::vector<TileSet>;
using TileMap = std::vector<std::vector<int> >;

class TileLayer
{
	public:
		TileLayer(int tileSize, int rowCount, int colCount, TileMap tileMap, TileSetList tileSets);
		~TileLayer() {}

		TileMap GetTileMap();

		void Render();

	private:
		friend class CollisionHandler;
		int m_TileSize;
		int m_RowCount, m_ColCount;
		TileMap m_TileMap;
		TileSetList m_TileSets;
};


inline int valueCap(int val, int maxCap)
{
	if (val < 0)
	{
		val = 0;
	}
	else if (val >= maxCap)
	{
		val = maxCap - 1;
	}

	return val;
}
#endif