#include "SaveSlotButton.h"

SaveSlotButton::SaveSlotButton(int x, int y, bool canClick, std::string text, std::string sTexture, std::string selectTexture, int textSize, int pos, bool selecting)
{
	buttonPos.x = x;
	buttonPos.y = y;
	available = canClick;
	if (selecting)
	{
		isSelecting = true;
	}
	startingTexture = sTexture;
	selectingTexture = selectTexture;
	buttonText = text;
	sizeText = textSize;
	ID = pos;
}

void SaveSlotButton::Initialize()
{
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(buttonPos.x, buttonPos.y);
	}
	if (!owner->HasComponent<Sprite>())
	{
		owner->AddComponent<Sprite>("ApplyButton", 146, 38, TIME_COUNT_LAYER);
	}

	Entity& text(EntityManager::GetInstance()->AddEntity("SaveSlotText" + ID));
	text.AddComponent<Transform>(owner->GetComponent<Transform>()->position.x, owner->GetComponent<Transform>()->position.y);
	text.AddComponent<Text>(TIME_COUNT_LAYER, "SaveSlotText" + owner->name + std::to_string(ID), "DisposableDroidBB_bld", sizeText, textColor, buttonText);
	textButton = &text;
	isSet = true;
}

void SaveSlotButton::Update(float deltaTime)
{
	textButton->GetComponent<Transform>()->position.x = owner->GetComponent<Sprite>()->GetWidth() / 2 - (textButton->GetComponent<Text>()->GetWidth() / 2) + owner->GetComponent<Transform>()->position.x;
	textButton->GetComponent<Transform>()->position.y = owner->GetComponent<Sprite>()->GetHeight() / 2 - (textButton->GetComponent<Text>()->GetHeight() / 2) + owner->GetComponent<Transform>()->position.y;


	//--------------------------------- Will Opti in the future ---------------------------------
	if (isSet)
	{
		if (!isSelecting)
		{
			owner->GetComponent<Sprite>()->SetTexture(startingTexture);
			textButton->GetComponent<Text>()->color = textColor;
		}
		else
		{
			owner->GetComponent<Sprite>()->SetTexture(selectingTexture);
			textButton->GetComponent<Text>()->color = { 251,242,54,255 };
		}
		isSet = false;
	}
}

void SaveSlotButton::SetFunction(void(*fn)())
{
	m_FnPtr = fn;
}

void SaveSlotButton::SetEmptySaveSlot(void(*fn)(int i))
{
	m_FnPtr2 = fn;
}

void SaveSlotButton::OnClick()
{
	m_FnPtr();
}

void SaveSlotButton::OnClick(int i)
{
	m_FnPtr2(i);
}

Entity* SaveSlotButton::SelectUI(int direction)
{
	return nextUI[direction];
}
