#include "MapParser.h"

MapParser* MapParser::s_Instance = nullptr;

MapParser* MapParser::GetInstance() 
{ 
    return s_Instance = (s_Instance != nullptr) ? s_Instance : new MapParser(); 
}

GameMap* MapParser::GetMap(std::string id)
{
    return m_MapDict[id];
}


bool MapParser::Load(std::string id, std::string source)
{
    return Parse(id, source);
}


bool MapParser::Parse(std::string id, std::string source)
{
    TiXmlDocument xml;
    xml.LoadFile(source);

    if (xml.Error()) {
        std::cout << "Failed to load: " << source << std::endl;
        return false;
    }

    TiXmlElement* root = xml.RootElement();
    int rowCount, colCount, tileSize = 0;

    root->Attribute("width", &colCount);
    root->Attribute("height", &rowCount);
    root->Attribute("tilewidth", &tileSize);

    //Parse tilesets
    TileSetList tilesets;
    for (TiXmlElement* e = root->FirstChildElement(); e != nullptr; e = e->NextSiblingElement()) {
        if (e->Value() == std::string("tileset")) {
            tilesets.push_back(ParseTileSet(e));
        }
    }

    //Parse Layers
    GameMap* gamemap = new GameMap();
    for (TiXmlElement* e = root->FirstChildElement(); e != nullptr; e = e->NextSiblingElement()) {
        if (e->Value() == std::string("layer")) {
            TileLayer* tileLayer = ParseTileLayer(e, tilesets, tileSize, rowCount, colCount);
            gamemap->m_MapLayer.push_back(tileLayer);
        }
    }

    m_MapDict[id] = gamemap;

    //set other tilemap's attibute
    m_MapDict[id]->m_ColCount = colCount;
    m_MapDict[id]->m_RowCount = rowCount;
    m_MapDict[id]->m_TileSize = tileSize;

    return true;
}

TileSet MapParser::ParseTileSet(TiXmlElement* xmlTileset)
{
    TileSet tileset;

    tileset.Name = xmlTileset->Attribute("name");
    xmlTileset->Attribute("firstgid", &tileset.FirstID);

    xmlTileset->Attribute("tilecount", &tileset.TileCount);
    tileset.LastID = (tileset.FirstID + tileset.TileCount) - 1;

    xmlTileset->Attribute("columns", &tileset.ColCount);
    tileset.RowCount = tileset.TileCount / tileset.ColCount;

    xmlTileset->Attribute("tilewidth", &tileset.TileSize);

    TiXmlElement* image = xmlTileset->FirstChildElement();
    tileset.Source = image->Attribute("source");
    return tileset;
}

TileLayer* MapParser::ParseTileLayer(TiXmlElement* xmlLayer, TileSetList tilesets, int tileSize, int rowCount, int colCount)
{
    TiXmlElement* data = nullptr;
    for (TiXmlElement* e = xmlLayer->FirstChildElement(); e != nullptr; e->NextSiblingElement()) {
        if (e->Value() == std::string("data")) {
            data = e;
            break;
        }
    }

    //Parse Layer tilemap
    std::string matrix(data->GetText());
    std::istringstream iss(matrix);
    std::string id;

    TileMap tilemap(rowCount, std::vector<int>(colCount, 0));

    for (int row = 0; row < rowCount; row++) {
        for (int col = 0; col < colCount; col++) {

            std::getline(iss, id, ',');
            std::stringstream converter(id);
            converter >> tilemap[row][col];
            if (!iss.good()) {
                break;
            }
        }
    }

    return new TileLayer(tileSize, rowCount, colCount, tilemap, tilesets);
}


void MapParser::Clean()
{

    std::map<std::string, GameMap*>::iterator it;
    for (it = m_MapDict.begin(); it != m_MapDict.end(); it++) 
    {
        Delete(it->second);
    }
    m_MapDict.clear();
}