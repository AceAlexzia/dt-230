#pragma once
#ifndef CREDIT_H
#define CREDIT_H
#define ROLLSPEED 200
#include "Component.h"
#include "TransformComponent.h"
#include "SpriteComponent.h"
#include "SceneManager.h"
#include "SaveManager.h"
class Credit :public Component
{
	public:
		Credit();
		~Credit();
		void Initialize() override;
		void Update(float dt) override;
	private:
		float first = 0.0f;
		bool end = false;
		
};

#endif // !CREDIT_H