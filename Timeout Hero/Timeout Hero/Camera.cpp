#include "Camera.h"

//include for GetOrigin
#include "PlayerColliderComponent.h"
#include "SpriteComponent.h"

Camera* Camera::s_Instance = nullptr;

Camera::Camera()
{
	Reset();
}


Camera* Camera::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new Camera;
}


SDL_Rect Camera::GetViewBox()
{
	return m_ViewBox;
}

Vector2D Camera::GetPosition()
{
	return Vector2D(m_ViewBox.x, m_ViewBox.y);

}

void Camera::SetHeroOrigin(Vector2D pos)
{
	m_HeroOrigin = pos;
}

void Camera::SetPosition(Vector2D pos)
{
	m_ViewBox = { (int)pos.x, (int)pos.y, RENDER_LOGICAL_WIDTH, RENDER_LOGICAL_HEIGHT };
}

void Camera::SetBoundary(Vector2D pos)
{
	m_Boundary = pos;
}

void Camera::CameraShake(float amount, float time)
{
	m_ShakeAmount = amount * (SaveManager::GetInstance()->opt.screenshake / 10.0f);
	m_ShakeTime = time;
}


void Camera::Update_Positon(float deltatime)
{

	//get camera origin
	Vector2D cameraOrigin = GetOrigin(m_ViewBox);

	//initialize target position
	Vector2D targetPos(0.0f, 0.0f);

	//get lerping position between CAMERA origin and HERO origin
	targetPos.x = Lerp(cameraOrigin.x, m_HeroOrigin.x, deltatime * CAMERA_LERPING_SPEED) - (RENDER_LOGICAL_WIDTH / 2);
	targetPos.y = Lerp(cameraOrigin.y, m_HeroOrigin.y - CAMERA_OFFSET_Y, deltatime * CAMERA_LERPING_SPEED) - (RENDER_LOGICAL_HEIGHT / 2);

	//set canera position according to the target
	SetPosition(targetPos);

}

void Camera::Update_Boundary()
{
	if (m_ViewBox.x < 0.0f)
	{
		SetPosition(Vector2D(0.0f, m_ViewBox.y));
	}
	else if (m_ViewBox.x + m_ViewBox.w > m_Boundary.x)
	{
		SetPosition(Vector2D(m_Boundary.x - m_ViewBox.w, m_ViewBox.y));
	}

	if (m_ViewBox.y < 0.0f)
	{
		SetPosition(Vector2D(m_ViewBox.x, 0.0f));
	}
	else if (m_ViewBox.y + m_ViewBox.h > m_Boundary.y)
	{
		SetPosition(Vector2D(m_ViewBox.x, m_Boundary.y - m_ViewBox.h));
	}
}

void Camera::Update_CameraShake(float deltatime)
{
	//enable camera shaking if shake time > 0.0
	if (m_ShakeTime > 0.0f)
	{
		//set offset of x and y
		float offsetX = GetRandom(-1.0f, 1.0f) * m_ShakeAmount * deltatime;
		float offsetY = GetRandom(-1.0f, 1.0f) * m_ShakeAmount * deltatime;

		//set position = currentPosition + offset
		SetPosition(Vector2D(GetPosition().x + offsetX, GetPosition().y + offsetY));

		//decrease shake time until it reaches 0.0 or less than
		m_ShakeTime -= deltatime;

		//checking whether the camera postion exits its boundary and set it back
		Update_Boundary();
	}
}


void Camera::Initialize()
{
	Vector2D targetPos(0.0f, 0.0f);

	//initialize position by setting Hero at the center of the screen
	//get the center of Hero in x, y axis
	targetPos.x = m_HeroOrigin.x - (RENDER_LOGICAL_WIDTH / 2);
	targetPos.y = m_HeroOrigin.y - CAMERA_OFFSET_Y - (RENDER_LOGICAL_HEIGHT / 2);

	//assign hero's origin starting postiion
	SetPosition(targetPos);

	//update boundary
	Update_Boundary();
}

void Camera::Update(float deltatime)
{
	//translate camera position
	Update_Positon(deltatime);
	//checking whether the camera postion exits its boundary and set it back
	Update_Boundary();
	//update camera shaking
	Update_CameraShake(deltatime);
}

void Camera::Reset()
{
	SetHeroOrigin(Vector2D(0.0f, 0.0f));
	SetPosition(m_HeroOrigin);
	SetBoundary(Vector2D(0.0f, 0.0f));
	CameraShake(0.0f, 0.0f);
}
