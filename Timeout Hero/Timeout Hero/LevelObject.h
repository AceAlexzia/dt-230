#pragma once
#ifndef LEVEL_OBJECT
#define LEVEL_OBJECT

#include "Hero.h"
#include "ColliderComponent.h"
#include "CollisionHandler.h"
#include "Component.h"

class LevelObject : public Component
{
	public:
		LevelObject() {}
		void Initialize() override {}
		void Update(float deltaTime) override {}
		void SetPlayer(Hero* player);
		bool CollisionTriggerred();

	protected:
		Collider* m_Collider;
		SpriteAnimation* m_Anim;
		Transform* m_Transform;
		Sprite* m_Sprite;
		Hero* m_Player = nullptr;
};

#endif