#pragma once
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <cstdlib>
#include "SDL.h"
#include "Vector2D.h"


//window width and height
const unsigned int RESOLUTION_WIDTH = 1920;
const unsigned int RESOLUTION_HEIGHT = 1080;


//field of view setiing
const unsigned int FIELD_OF_VIEW = 100; // Set to 120 to gain  1920 x 1080 resolution
                                        // Set to 100 to gain  1600 x 900  resolution
                                        // Set to 80  to gain  1280 x 720  resolution


//logical rendering can be scaled accordingly to resolution size
const unsigned int RENDER_LOGICAL_WIDTH = 16 * FIELD_OF_VIEW;
const unsigned int RENDER_LOGICAL_HEIGHT = 9 * FIELD_OF_VIEW;


//max FPS setting
const unsigned int FPS = 200;
const unsigned int FRAME_TARGET_TIME = 1000 / FPS;


enum LayerType {
    MONSTER_LAYER,
    RUNE_LAYER,
    HERO_LAYER,
    ARTRIUS_LAYER,
    TIME_COUNT_LAYER,
    TRANSITION_LAYER,
    PANEL_LAYER,
    UI_LAYER,
    LAYER_COUNT //DO NOT REMOVE THIS ONE
};


enum Direction {
    UP,
    LEFT,
    RIGHT,
    DOWN
};


static float Lerp(float a, float b, float f)
{
    return a + f * (b - a);
}


//returns random floating point between a to b
static float GetRandom(float a, float b) 
{
    float random = ((float)rand()) / (float)RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

static Vector2D GetOrigin(SDL_Rect rect) 
{
    return Vector2D(rect.x + (rect.w / 2), rect.y + (rect.h / 2));
}

template<class T>
static void Delete(const T& data)
{
    if (data != nullptr)
    {
        delete data;
    }
}

#endif
