#include "UIManager.h"
#include "UIClickButtonFunction.h"

UIManager* UIManager::s_Instance = nullptr;
UIManager::~UIManager()
{
	Clean();
}
void UIManager::Clean()
{
	//std::cout << "clear" << std::endl;
	menuButton_Pos.clear();
	resolution_text.clear();
	startingUI.clear();
	optionUI.clear();
	pauseUI.clear();
	masterVolume_text.clear();
	sfxVolume_text.clear();
	musicVolume_text.clear();
	screenShake_text.clear();
	m_AllUI.clear();
	temporarySlot.clear();
}

void UIManager::DisableAllUI()
{
	for (std::map<std::string, Entity*>::iterator it = m_AllUI.begin(); it != m_AllUI.end(); it++)
	{
		if (it->second->HasComponent<Sprite>())
		{
			it->second->GetComponent<Sprite>()->m_IsEnable = false;
		}
	}
}
void UIManager::PressPauseMenu()
{
	currentUI = resumeButton;
	currentUI->GetComponent<TextButton>()->isSelecting = true;
}

void UIManager::PressApplyButton()
{

	SaveManager* saveManager = SaveManager::GetInstance();

	//std::cout << "Press Apply Button" << std::endl;

	//--------- SaveOption -------
	saveManager->SetOption(saveManager->opt.resolution_width, saveManager->opt.resolution_height, saveManager->opt.full, saveManager->opt.master, saveManager->opt.sfx, saveManager->opt.music, saveManager->opt.screenshake, saveManager->opt.skipDialogue);

	//--------- LoadOption -------
	saveManager->LoadOption();

}

void UIManager::SpawnSaveSlotUI()
{
	DeleteMainMenuUI();
	ClearAllUI();
	for (int i = 0; i < 3; i++)
	{
		SaveManager::GetInstance()->LoadSave(i);

	}

	currentPos = 0;
	Entity& slot1(EntityManager::GetInstance()->AddEntity("SaveSlot1"));
	Entity& slot2(EntityManager::GetInstance()->AddEntity("SaveSlot2"));
	Entity& slot3(EntityManager::GetInstance()->AddEntity("SaveSlot3"));
	Entity& del1(EntityManager::GetInstance()->AddEntity("DeleteSave1"));
	Entity& del2(EntityManager::GetInstance()->AddEntity("DeleteSave2"));
	Entity& del3(EntityManager::GetInstance()->AddEntity("DeleteSave3"));
	//Check Each slot
	if (!SaveManager::GetInstance()->hasSave[0])
	{
		slot1.AddComponent<Transform>(278, 62);
		slot1.AddComponent<Sprite>("SaveEmpty", 1043, 242, TIME_COUNT_LAYER);
		slot1.AddComponent<SaveSlotButton>(278, 62, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 0);
		slot1.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot1);
		slot1.GetComponent<SaveSlotButton>()->isSelecting = true;
		m_AllUI[slot1.name] = &slot1;
		currentUI = &slot1;
		saveSlot[0] = &slot1;
	}
	else
	{
		slot1.AddComponent<Transform>(278, 62);
		slot1.AddComponent<Sprite>("SaveEmpty", 920, 240, TIME_COUNT_LAYER);
		slot1.AddComponent<SaveSlotButton>(278, 595, true, "", "Save", "Save2", 0, 0);
		slot1.GetComponent<SaveSlotButton>()->hasSave = true;
		slot1.GetComponent<SaveSlotButton>()->isSelecting = true;
		slot1.GetComponent<SaveSlotButton>()->SetFunction(&ClickButton::ClickPlayButton);

		Entity& percentText(EntityManager::GetInstance()->AddEntity("% Text"));
		if (SaveManager::GetInstance()->save[0].playStage < 10)
		{
			percentText.AddComponent<Transform>(336, 134);
		}
		else
		{
			percentText.AddComponent<Transform>(316, 134);
		}
		//percentText.AddComponent<Transform>(316 /*+ 20*/, 134);
		int level = SaveManager::GetInstance()->save[0].playStage * 100 / LevelManager::GetInstance()->GetLevelAmount();
		std::string showText = std::to_string(level);
		percentText.AddComponent<Text>(TIME_COUNT_LAYER, "PercentText " + slot1.name, "DisposableDroidBB_bld", 100, white, showText + "%");

		int deadNumber = SaveManager::GetInstance()->save[0].death;
		std::string showDead = std::to_string(deadNumber);
		Entity& deathText(EntityManager::GetInstance()->AddEntity("Death Text"));
		deathText.AddComponent<Transform>(545, 75);
		deathText.AddComponent<Text>(TIME_COUNT_LAYER, "DeathText " + slot1.name, "DisposableDroidBB_bld", 67, white, "Death: " + showDead);

		Entity& modeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		modeText.AddComponent<Transform>(545, 145);
		modeText.AddComponent<Text>(TIME_COUNT_LAYER, "ModeText " + slot1.name, "DisposableDroidBB_bld", 67, white, "Mode: " + SaveManager::GetInstance()->save[0].mode);

		float t = SaveManager::GetInstance()->save[0].totalTime / 60.0f;
		std::ostringstream stm;
		stm << std::fixed << std::setprecision(1) << t;


		Entity& timeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		timeText.AddComponent<Transform>(545, 215);
		timeText.AddComponent<Text>(TIME_COUNT_LAYER, "TimeText " + slot1.name, "DisposableDroidBB_bld", 67, white, "Time: " + stm.str() + " min");

		del1.AddComponent<Transform>(1210, 62);
		del1.AddComponent<Sprite>("Delete", 110, 240, TIME_COUNT_LAYER);
		del1.AddComponent<ButtonInSaveSlot>(1210, 62, true, "Delete", "Delete2", 0);
		del1.GetComponent<ButtonInSaveSlot>()->SetFunction(&ClickButton::DeleteEachSlot);
		slot1.GetComponent<SaveSlotButton>()->nextUI[RIGHT] = &del1;
		del1.GetComponent<ButtonInSaveSlot>()->nextUI[LEFT] = &slot1;

		saveSlot[0] = &slot1;
	}
	if (!SaveManager::GetInstance()->hasSave[1])
	{
		slot2.AddComponent<Transform>(278, 330);
		slot2.AddComponent<Sprite>("ApplyButton", 1043, 242, TIME_COUNT_LAYER);
		slot2.AddComponent<SaveSlotButton>(278, 330, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 1);
		slot2.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot2);
		//currentUI = &button;
		m_AllUI[slot2.name] = &slot2;
		/*emptyFirstSlotUI.push_back(&slot2);
		emptyFirstSlotUI.push_back(slot2.GetComponent<Button>()->textButton);*/
		saveSlot[1] = &slot2;
	}
	else
	{
		slot2.AddComponent<Transform>(278, 330);
		slot2.AddComponent<Sprite>("SaveEmpty", 920, 240, TIME_COUNT_LAYER);
		slot2.AddComponent<SaveSlotButton>(278, 595, true, "", "Save", "Save2", 0, 1);
		slot2.GetComponent<SaveSlotButton>()->hasSave = true;

		slot2.GetComponent<SaveSlotButton>()->SetFunction(&ClickButton::ClickPlayButton);

		Entity& percentText(EntityManager::GetInstance()->AddEntity("% Text"));
		if (SaveManager::GetInstance()->save[1].playStage < 10)
		{
			percentText.AddComponent<Transform>(336, 403);
		}
		else
		{
			percentText.AddComponent<Transform>(316, 403);
		}
		//percentText.AddComponent<Transform>(316 /*+ 20*/, 403);
		int level = SaveManager::GetInstance()->save[1].playStage * 100 / LevelManager::GetInstance()->GetLevelAmount();
		std::string showText = std::to_string(level);
		percentText.AddComponent<Text>(TIME_COUNT_LAYER, "PercentText " + slot2.name, "DisposableDroidBB_bld", 100, white, showText + "%");

		int deadNumber = SaveManager::GetInstance()->save[1].death;
		std::string showDead = std::to_string(deadNumber);
		Entity& deathText(EntityManager::GetInstance()->AddEntity("Death Text"));
		deathText.AddComponent<Transform>(545, 344);
		deathText.AddComponent<Text>(TIME_COUNT_LAYER, "DeathText " + slot2.name, "DisposableDroidBB_bld", 67, white, "Death: " + showDead);

		Entity& modeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		modeText.AddComponent<Transform>(545, 414);
		modeText.AddComponent<Text>(TIME_COUNT_LAYER, "ModeText " + slot2.name, "DisposableDroidBB_bld", 67, white, "Mode: " + SaveManager::GetInstance()->save[1].mode);

		float t = SaveManager::GetInstance()->save[1].totalTime / 60.0f;
		std::ostringstream stm;
		stm << std::fixed << std::setprecision(1) << t;

		Entity& timeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		timeText.AddComponent<Transform>(545, 483);
		timeText.AddComponent<Text>(TIME_COUNT_LAYER, "TimeText " + slot2.name, "DisposableDroidBB_bld", 67, white, "Time: " + stm.str() + " min");

		del2.AddComponent<Transform>(1210, 330);
		del2.AddComponent<Sprite>("Delete", 110, 240, TIME_COUNT_LAYER);
		del2.AddComponent<ButtonInSaveSlot>(1210, 328, true, "Delete", "Delete2", 1);
		del2.GetComponent<ButtonInSaveSlot>()->SetFunction(&ClickButton::DeleteEachSlot);
		slot2.GetComponent<SaveSlotButton>()->nextUI[RIGHT] = &del2;
		del2.GetComponent<ButtonInSaveSlot>()->nextUI[LEFT] = &slot2;
		saveSlot[1] = &slot2;
	}
	if (!SaveManager::GetInstance()->hasSave[2])
	{
		slot3.AddComponent<Transform>(278, 595);
		slot3.AddComponent<Sprite>("ApplyButton", 1043, 242, TIME_COUNT_LAYER);
		slot3.AddComponent<SaveSlotButton>(278, 595, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 2);
		slot3.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot3);
		m_AllUI[slot3.name] = &slot3;
		saveSlot[2] = &slot3;
	}
	else
	{
		slot3.AddComponent<Transform>(278, 595);
		slot3.AddComponent<Sprite>("ApplyButton", 922, 242, TIME_COUNT_LAYER);
		slot3.AddComponent<SaveSlotButton>(278, 595, true, "", "Save", "Save2", 0, 2);
		slot3.GetComponent<SaveSlotButton>()->hasSave = true;
		slot3.GetComponent<SaveSlotButton>()->SetFunction(&ClickButton::ClickPlayButton);

		Entity& percentText(EntityManager::GetInstance()->AddEntity("% Text"));
		if (SaveManager::GetInstance()->save[2].playStage < 10)
		{
			percentText.AddComponent<Transform>(336, 673);
		}
		else
		{
			percentText.AddComponent<Transform>(316, 673);
		}
		//percentText.AddComponent<Transform>(316 /*+ 20*/, 673);
		int level = SaveManager::GetInstance()->save[2].playStage * 100 / LevelManager::GetInstance()->GetLevelAmount();
		std::string showText = std::to_string(level);
		percentText.AddComponent<Text>(TIME_COUNT_LAYER, "PercentText " + slot3.name, "DisposableDroidBB_bld", 100, white, showText + "%");

		int deadNumber = SaveManager::GetInstance()->save[2].death;
		std::string showDead = std::to_string(deadNumber);
		Entity& deathText(EntityManager::GetInstance()->AddEntity("Death Text"));
		deathText.AddComponent<Transform>(545, 614);
		deathText.AddComponent<Text>(TIME_COUNT_LAYER, "DeathText " + slot3.name, "DisposableDroidBB_bld", 67, white, "Death: " + showDead);

		Entity& modeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		modeText.AddComponent<Transform>(545, 684);
		modeText.AddComponent<Text>(TIME_COUNT_LAYER, "ModeText " + slot3.name, "DisposableDroidBB_bld", 67, white, "Mode: " + SaveManager::GetInstance()->save[2].mode);

		float t = SaveManager::GetInstance()->save[2].totalTime / 60.0f;
		std::ostringstream stm;
		stm << std::fixed << std::setprecision(1) << t;

		Entity& timeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		timeText.AddComponent<Transform>(545, 753);
		timeText.AddComponent<Text>(TIME_COUNT_LAYER, "TimeText " + slot3.name, "DisposableDroidBB_bld", 67, white, "Time: " + stm.str() + " min");

		del3.AddComponent<Transform>(1210, 595);
		del3.AddComponent<Sprite>("Delete", 110, 240, TIME_COUNT_LAYER);
		del3.AddComponent<ButtonInSaveSlot>(1210, 595, true, "Delete", "Delete2", 2);
		del3.GetComponent<ButtonInSaveSlot>()->SetFunction(&ClickButton::DeleteEachSlot);

		slot3.GetComponent<SaveSlotButton>()->nextUI[RIGHT] = &del3;
		del3.GetComponent<ButtonInSaveSlot>()->nextUI[LEFT] = &slot3;

		saveSlot[2] = &slot3;
		/*Entity& percentText3(EntityManager::GetInstance()->AddEntity("% Text3"));
		percentText3.AddComponent<Text>(TIME_COUNT_LAYER, "AdjusterText" + slot3.name, "DisposableDroidBB", 100, white, "0%");*/
	}
	Entity& back(EntityManager::GetInstance()->AddEntity("Back"));
	back.AddComponent<Transform>(65, 65);
	back.AddComponent<Sprite>("Undo", 80, 80, TIME_COUNT_LAYER);
	back.AddComponent<Button>(65, 65, true, "", "Undo", "Undo2", 50);
	back.GetComponent<Button>()->SetFunction(&ClickButton::ClickMenuButton);
	undo = &back;

	back.GetComponent<Button>()->nextUI[DOWN] = &slot1;
	back.GetComponent<Button>()->nextUI[RIGHT] = &slot1;
	slot1.GetComponent<SaveSlotButton>()->nextUI[UP] = &back;
	slot1.GetComponent<SaveSlotButton>()->nextUI[LEFT] = &back;
	slot1.GetComponent<SaveSlotButton>()->nextUI[DOWN] = &slot2;

	slot2.GetComponent<SaveSlotButton>()->nextUI[DOWN] = &slot3;
	slot2.GetComponent<SaveSlotButton>()->nextUI[UP] = &slot1;

	slot3.GetComponent<SaveSlotButton>()->nextUI[UP] = &slot2;
	//slot3.GetComponent<SaveSlotButton>()->nextUI[DOWN] = &slot2;
	currentUI = &slot1;
}

void UIManager::PressMainMenu()
{
	//DeleteAllSaveSlot();
	DeleteOption();
	ClearAllUI();
	SpawnMenuUI();
}

void UIManager::PressOption()
{
	DeleteMainMenuUI();
	ClearAllUI();
	SpawnOptionUI();
}

void UIManager::SpawnMenuUI()
{
	Entity& button(EntityManager::GetInstance()->AddEntity("StartMenu"));
	button.AddComponent<TextButton>(698, 581, true, 83, "Start", "editundo", white, true);
	button.GetComponent<TextButton>()->SetFunction(&ClickButton::CreateAllSaveSlot);
	startingUI.push_back(&button);
	currentUI = &button;
	m_AllUI[button.name] = &button;
	startButton = &button;

	Entity& icon(EntityManager::GetInstance()->AddEntity("MainMenuIcon"));
	icon.AddComponent<Transform>(403, 43);
	icon.AddComponent<Sprite>("MainIcon", 793, 459, TIME_COUNT_LAYER);
	startingUI.push_back(&icon);
	m_AllUI[icon.name] = &icon;

	Entity& button2(EntityManager::GetInstance()->AddEntity("OptionMenu"));
	button2.AddComponent<TextButton>(688, 678, true, 83, "Option", "editundo", white, false);
	button2.GetComponent<TextButton>()->SetFunction(&ClickButton::PressOptionManMenu);
	startingUI.push_back(&button2);
	m_AllUI[button2.name] = &button2;

	Entity& button3(EntityManager::GetInstance()->AddEntity("Exit"));
	button3.AddComponent<TextButton>(730, 774, true, 83, "Exit", "editundo", white, false);
	button3.GetComponent<TextButton>()->SetFunction(&ClickButton::ExitGame);
	startingUI.push_back(&button3);
	m_AllUI[button3.name] = &button3;

	button.GetComponent<TextButton>()->nextUI[UP] = &button3;
	button.GetComponent<TextButton>()->nextUI[DOWN] = &button2;

	button2.GetComponent<TextButton>()->nextUI[UP] = &button;
	button2.GetComponent<TextButton>()->nextUI[DOWN] = &button3;

	button3.GetComponent<TextButton>()->nextUI[UP] = &button2;
	button3.GetComponent<TextButton>()->nextUI[DOWN] = &button;

	Entity& logo1(EntityManager::GetInstance()->AddEntity("KMUTT Logo"));
	logo1.AddComponent<Transform>(1213, 766);
	logo1.AddComponent<Sprite>("KMUTT", 111, 111, TIME_COUNT_LAYER);
	startingUI.push_back(&logo1);
	m_AllUI[logo1.name] = &logo1;

	Entity& logo2(EntityManager::GetInstance()->AddEntity("DD-DT Logo"));
	logo2.AddComponent<Transform>(1359, 779);
	logo2.AddComponent<Sprite>("DD-DT", 204, 83, TIME_COUNT_LAYER);
	startingUI.push_back(&logo2);
	m_AllUI[logo1.name] = &logo2;


	Entity& copyrightText(EntityManager::GetInstance()->AddEntity("Copyright Text"));
	copyrightText.AddComponent<Transform>(28, 865);
	copyrightText.AddComponent<Text>(TIME_COUNT_LAYER, "Copyright Text", "DisposableDroidBB_bld", 30, white, "� 2020 Digital Design and Technology");
	startingUI.push_back(&copyrightText);
	m_AllUI[copyrightText.name] = &copyrightText;

}

void UIManager::SpawnOptionUI()
{
	//SaveManager::GetInstance()->LoadOption();
	Entity& undoButton(EntityManager::GetInstance()->AddEntity("UndoButton"));
	undoButton.AddComponent<Transform>(63, 49);
	undoButton.AddComponent<Sprite>("Undo", 80, 80, TIME_COUNT_LAYER);
	undoButton.AddComponent<Button>(63, 49, true, "", "Undo", "Undo2", true);
	undoButton.GetComponent<Button>()->SetFunction(&ClickButton::PressMainMenu);
	undoButton.GetComponent<Button>()->isSelecting = true;
	optionUI.push_back(&undoButton);
	m_AllUI[undoButton.name] = &undoButton;
	currentUI = &undoButton;

	//Prepare for OptionUI
	Entity& panel(EntityManager::GetInstance()->AddEntity("Panel"));
	panel.AddComponent<Transform>(426, 139);
	panel.AddComponent<Sprite>("SettingPanel", 756, 751, TIME_COUNT_LAYER);
	optionUI.push_back(&panel);
	m_AllUI[panel.name] = &panel;

	//Header Part
	Entity& optionText(EntityManager::GetInstance()->AddEntity("optionText"));
	optionText.AddComponent<Transform>(632, 23);
	optionText.AddComponent<Text>(TIME_COUNT_LAYER, "OptionMenu", "editundo", 125, white, "OPTION");
	optionUI.push_back(&optionText);
	m_AllUI[optionText.name] = &optionText;


	//Graphic Part
	Entity& graphicText(EntityManager::GetInstance()->AddEntity("MenuGraphic"));
	graphicText.AddComponent<Transform>(563, 218);
	graphicText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuGraphic", "DisposableDroidBB_bld", 40, white, "Graphic");
	optionUI.push_back(&graphicText);
	m_AllUI[graphicText.name] = &graphicText;

	Entity& resolutionText(EntityManager::GetInstance()->AddEntity("MenuResolution"));
	resolutionText.AddComponent<Transform>(549, 278);
	resolutionText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuResolution", "DisposableDroidBB", 30, white, "Resolution");
	optionUI.push_back(&resolutionText);
	m_AllUI[resolutionText.name] = &resolutionText;

	Entity& resAdjuster(EntityManager::GetInstance()->AddEntity("ResolutionAdjuster"));
	if (SaveManager::GetInstance()->opt.resolution_width == 1280 && SaveManager::GetInstance()->opt.resolution_height == 720)
	{
		resAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 272, resolution_text, 0);
	}
	else if (SaveManager::GetInstance()->opt.resolution_width == 1920 && SaveManager::GetInstance()->opt.resolution_height == 1080)
	{
		resAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 272, resolution_text, 2);
	}
	else if (SaveManager::GetInstance()->opt.resolution_width == 1600 && SaveManager::GetInstance()->opt.resolution_height == 900)
	{
		resAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 272, resolution_text, 1);
	}
	optionUI.push_back(&resAdjuster);
	m_AllUI[resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->name] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->name] = resAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[resAdjuster.name] = &resAdjuster;
	optionUI.push_back(resAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(resAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(resAdjuster.GetComponent<Adjuster>()->adjusterText);

	//resAdjuster.GetComponent<Adjuster>()->leftButton
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[RIGHT];

	resAdjuster.GetComponent<Adjuster>()->nextUI[RIGHT]->GetComponent<AdjustArrow>()->nextUI[LEFT] = &resAdjuster;
	resAdjuster.GetComponent<Adjuster>()->nextUI[DOWN] = &resAdjuster;
	resAdjuster.GetComponent<Adjuster>()->nextUI[UP] = &resAdjuster;

	Entity& fullscreenText(EntityManager::GetInstance()->AddEntity("MenuFullscreen"));
	fullscreenText.AddComponent<Transform>(549, 327);
	fullscreenText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuFullscreen", "DisposableDroidBB", 30, white, "Fullscreen");
	optionUI.push_back(&fullscreenText);
	m_AllUI[fullscreenText.name] = &fullscreenText;

	Entity& fullscreenCheckBox(EntityManager::GetInstance()->AddEntity("FullscreenCheckBox"));
	fullscreenCheckBox.AddComponent<CheckBox>(923, 317, SaveManager::GetInstance()->opt.full);

	//fullscreenCheckBox.AddComponent<CheckBox>(menuButton_Pos[3].x, menuButton_Pos[3].y, false);
	//checkBox1.GetComponent<CheckBox>()->SetFunction(&ClickButton::Check1);
	m_AllUI[fullscreenCheckBox.name] = &fullscreenCheckBox;
	optionUI.push_back(&fullscreenCheckBox);

	//Audio Part
	Entity& audioText(EntityManager::GetInstance()->AddEntity("MenuGraphic"));
	audioText.AddComponent<Transform>(561, 380);
	audioText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuAudio", "DisposableDroidBB_bld", 40, white, "Audio");
	optionUI.push_back(&audioText);
	m_AllUI[audioText.name] = &audioText;

	Entity& masterVolume(EntityManager::GetInstance()->AddEntity("MenuMasterVolume"));
	masterVolume.AddComponent<Transform>(549, 437);
	masterVolume.AddComponent<Text>(TIME_COUNT_LAYER, "MenuMasterVolume", "DisposableDroidBB", 30, white, "Master Volume");
	optionUI.push_back(&masterVolume);
	m_AllUI[masterVolume.name] = &masterVolume;

	Entity& masterAdjuster(EntityManager::GetInstance()->AddEntity("MasterAdjuster"));
	masterAdjuster.AddComponent<Adjuster>(859, 434, masterVolume_text, SaveManager::GetInstance()->opt.master);
	optionUI.push_back(&masterAdjuster);
	m_AllUI[masterAdjuster.GetComponent<Adjuster>()->leftButton->name] = masterAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[masterAdjuster.GetComponent<Adjuster>()->rightButton->name] = masterAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[masterAdjuster.name] = &masterAdjuster;
	optionUI.push_back(masterAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(masterAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(masterAdjuster.GetComponent<Adjuster>()->adjusterText);


	Entity& sfxVolume(EntityManager::GetInstance()->AddEntity("MenuSFXVolume"));
	sfxVolume.AddComponent<Transform>(549, 486);
	sfxVolume.AddComponent<Text>(TIME_COUNT_LAYER, "MenuSFXVolume", "DisposableDroidBB", 30, white, "SFX Volume");
	optionUI.push_back(&sfxVolume);
	m_AllUI[sfxVolume.name] = &sfxVolume;

	Entity& sfxAdjuster(EntityManager::GetInstance()->AddEntity("SFXAdjuster"));
	sfxAdjuster.AddComponent<Adjuster>(859, 483, sfxVolume_text, SaveManager::GetInstance()->opt.sfx);
	optionUI.push_back(&sfxAdjuster);
	m_AllUI[sfxAdjuster.GetComponent<Adjuster>()->leftButton->name] = sfxAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[sfxAdjuster.GetComponent<Adjuster>()->rightButton->name] = sfxAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[sfxAdjuster.name] = &sfxAdjuster;
	optionUI.push_back(sfxAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(sfxAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(sfxAdjuster.GetComponent<Adjuster>()->adjusterText);


	Entity& musicVolume(EntityManager::GetInstance()->AddEntity("MenuSFXVolume"));
	musicVolume.AddComponent<Transform>(549, 535);
	musicVolume.AddComponent<Text>(TIME_COUNT_LAYER, "MenuMusicVolume", "DisposableDroidBB", 30, white, "Music Volume");
	optionUI.push_back(&musicVolume);
	m_AllUI[musicVolume.name] = &musicVolume;

	Entity& musicAdjuster(EntityManager::GetInstance()->AddEntity("MusicAdjuster"));
	musicAdjuster.AddComponent<Adjuster>(859, 532, musicVolume_text, SaveManager::GetInstance()->opt.music);
	optionUI.push_back(&musicAdjuster);
	m_AllUI[musicAdjuster.GetComponent<Adjuster>()->leftButton->name] = musicAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[musicAdjuster.GetComponent<Adjuster>()->rightButton->name] = musicAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[musicAdjuster.name] = &musicAdjuster;
	optionUI.push_back(musicAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(musicAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(musicAdjuster.GetComponent<Adjuster>()->adjusterText);

	//Gameplay Part
	Entity& gameplayText(EntityManager::GetInstance()->AddEntity("MenuGraphic"));
	gameplayText.AddComponent<Transform>(562, 588);
	gameplayText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuGameplay", "DisposableDroidBB_bld", 40, white, "Gameplay");
	optionUI.push_back(&gameplayText);
	m_AllUI[gameplayText.name] = &gameplayText;

	Entity& screenShake(EntityManager::GetInstance()->AddEntity("MenuScreenShake"));
	screenShake.AddComponent<Transform>(551, 644);
	screenShake.AddComponent<Text>(TIME_COUNT_LAYER, "MenuScreenShake", "DisposableDroidBB", 30, white, "Screen Shake");
	optionUI.push_back(&screenShake);
	m_AllUI[screenShake.name] = &screenShake;


	Entity& screenShakeAdjuster(EntityManager::GetInstance()->AddEntity("screenShakeAdjuster"));
	screenShakeAdjuster.AddComponent<Adjuster>(859, 639, screenShake_text, SaveManager::GetInstance()->opt.screenshake);
	optionUI.push_back(&screenShakeAdjuster);
	m_AllUI[screenShakeAdjuster.GetComponent<Adjuster>()->leftButton->name] = screenShakeAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[screenShakeAdjuster.GetComponent<Adjuster>()->rightButton->name] = screenShakeAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[screenShakeAdjuster.name] = &screenShakeAdjuster;
	optionUI.push_back(screenShakeAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(screenShakeAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(screenShakeAdjuster.GetComponent<Adjuster>()->adjusterText);

	Entity& speedrunClockText(EntityManager::GetInstance()->AddEntity("MenuSpeedrunClock"));
	speedrunClockText.AddComponent<Transform>(549, 690);
	speedrunClockText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuSpeedrunClock", "DisposableDroidBB", 30, white, "Skip Dialogue");
	optionUI.push_back(&speedrunClockText);
	m_AllUI[speedrunClockText.name] = &speedrunClockText;

	Entity& speedRunClockCheckBox(EntityManager::GetInstance()->AddEntity("speedRunClockCheckBox"));
	speedRunClockCheckBox.AddComponent<CheckBox>(923, 684, SaveManager::GetInstance()->opt.skipDialogue);
	optionUI.push_back(&speedRunClockCheckBox);
	m_AllUI[speedRunClockCheckBox.name] = &speedRunClockCheckBox;

	Entity& applyButton(EntityManager::GetInstance()->AddEntity("ApplyButton"));
	applyButton.AddComponent<Button>(934, 765, true, "Apply", "ApplyButton", "ApplyButton2", 30);
	applyButton.GetComponent<Button>()->SetFunction(&ClickButton::ClickApplyButton);
	optionUI.push_back(&applyButton);
	m_AllUI[applyButton.name] = &applyButton;
	optionUI.push_back(applyButton.GetComponent<Button>()->textButton);



	undoButton.GetComponent<Button>()->nextUI[UP] = &applyButton;
	undoButton.GetComponent<Button>()->nextUI[DOWN] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	undoButton.GetComponent<Button>()->nextUI[RIGHT] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	//Left 1
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = &undoButton;
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = &undoButton;
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = &fullscreenCheckBox;
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = resAdjuster.GetComponent<Adjuster>()->rightButton;

	//FullCheckBox
	fullscreenCheckBox.GetComponent<CheckBox>()->nextUI[UP] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	fullscreenCheckBox.GetComponent<CheckBox>()->nextUI[DOWN] = masterAdjuster.GetComponent<Adjuster>()->leftButton;
	//Left 2
	masterAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = &fullscreenCheckBox;
	masterAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = sfxAdjuster.GetComponent<Adjuster>()->leftButton;
	masterAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = masterAdjuster.GetComponent<Adjuster>()->rightButton;
	//Left 3
	sfxAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = masterAdjuster.GetComponent<Adjuster>()->leftButton;
	sfxAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = musicAdjuster.GetComponent<Adjuster>()->leftButton;
	sfxAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = sfxAdjuster.GetComponent<Adjuster>()->rightButton;
	//Left 4
	musicAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = sfxAdjuster.GetComponent<Adjuster>()->leftButton;
	musicAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = screenShakeAdjuster.GetComponent<Adjuster>()->leftButton;
	musicAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = musicAdjuster.GetComponent<Adjuster>()->rightButton;
	//Left 5
	screenShakeAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = musicAdjuster.GetComponent<Adjuster>()->leftButton;
	screenShakeAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = &speedRunClockCheckBox;
	screenShakeAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = screenShakeAdjuster.GetComponent<Adjuster>()->rightButton;

	//SpeedRunClock
	speedRunClockCheckBox.GetComponent<CheckBox>()->nextUI[UP] = screenShakeAdjuster.GetComponent<Adjuster>()->leftButton;
	speedRunClockCheckBox.GetComponent<CheckBox>()->nextUI[DOWN] = &applyButton;
	//Right 1
	resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = &undoButton;
	resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = &fullscreenCheckBox;
	resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	//Right 2
	masterAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = &fullscreenCheckBox;
	masterAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = sfxAdjuster.GetComponent<Adjuster>()->rightButton;
	masterAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = masterAdjuster.GetComponent<Adjuster>()->leftButton;
	//Right 3
	sfxAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = masterAdjuster.GetComponent<Adjuster>()->rightButton;
	sfxAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = musicAdjuster.GetComponent<Adjuster>()->rightButton;
	sfxAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = sfxAdjuster.GetComponent<Adjuster>()->leftButton;
	//Right 4
	musicAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = sfxAdjuster.GetComponent<Adjuster>()->rightButton;
	musicAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = screenShakeAdjuster.GetComponent<Adjuster>()->rightButton;
	musicAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = musicAdjuster.GetComponent<Adjuster>()->leftButton;
	//Right 5
	screenShakeAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = musicAdjuster.GetComponent<Adjuster>()->rightButton;
	screenShakeAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = &speedRunClockCheckBox;
	screenShakeAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = screenShakeAdjuster.GetComponent<Adjuster>()->leftButton;

	applyButton.GetComponent<Button>()->nextUI[UP] = &speedRunClockCheckBox;
	applyButton.GetComponent<Button>()->nextUI[DOWN] = &undoButton;
}

void UIManager::SpawnPauseUI()
{
	Entity& blackBG(EntityManager::GetInstance()->AddEntity("BlackBG"));
	blackBG.AddComponent<Transform>(Camera::GetInstance()->GetPosition().x, Camera::GetInstance()->GetPosition().y);
	blackBG.AddComponent<Sprite>("BlackBG", 1920, 1080, TIME_COUNT_LAYER);
	pauseUI.push_back(&blackBG);

	/*SDL_SetRenderDrawColor(Game::GetInstance()->m_Renderer, 0, 0, 0, 0);*/
	Entity& resume(EntityManager::GetInstance()->AddEntity("ResumeText"));
	resume.AddComponent<TextButton>(715, 379, true, 60, "RESUME", "editundo", white);
	resume.GetComponent<TextButton>()->SetFunction(&ClickButton::PressResume);
	pauseUI.push_back(&resume);
	//currentUI = &resumeButton;
	m_AllUI[resume.name] = &resume;
	//resume.GetComponent<TextButton>()
	resumeButton = &resume;

	Entity& pauseText(EntityManager::GetInstance()->AddEntity("pauseText"));
	pauseText.AddComponent<Transform>(550, 101);
	pauseText.AddComponent<Text>(TIME_COUNT_LAYER, "PauseMenu", "editundo", 208, white, "PAUSE");
	pauseUI.push_back(&pauseText);
	m_AllUI[pauseText.name] = &pauseText;



	Entity& optionButton2(EntityManager::GetInstance()->AddEntity("Option2"));
	optionButton2.AddComponent<TextButton>(714, 523, true, 60, "OPTION", "editundo", white, false);
	optionButton2.GetComponent<TextButton>()->SetFunction(&ClickButton::PressOptionPause);
	pauseUI.push_back(&optionButton2);
	m_AllUI[optionButton2.name] = &optionButton2;

	Entity& mainMenuButton(EntityManager::GetInstance()->AddEntity("MainMenu"));
	mainMenuButton.AddComponent<TextButton>(661, 666, true, 60, "MAIN MENU", "editundo", white, false);
	mainMenuButton.GetComponent<TextButton>()->SetFunction(&ClickButton::ClickMenuButton);
	pauseUI.push_back(&mainMenuButton);
	m_AllUI[mainMenuButton.name] = &mainMenuButton;

	resume.GetComponent<TextButton>()->nextUI[UP] = &mainMenuButton;
	resume.GetComponent<TextButton>()->nextUI[DOWN] = &optionButton2;

	optionButton2.GetComponent<TextButton>()->nextUI[UP] = &resume;
	optionButton2.GetComponent<TextButton>()->nextUI[DOWN] = &mainMenuButton;

	mainMenuButton.GetComponent<TextButton>()->nextUI[UP] = &optionButton2;
	mainMenuButton.GetComponent<TextButton>()->nextUI[DOWN] = &resume;
	//EntityManager::GetInstance()->ListAllEntities();


}

void UIManager::RemoveSaveslot(int pos)
{
	saveSlot[pos]->Destroy();
}

void UIManager::PressEmpty(int pos)
{
	currentPos = pos;
	//(int x, int y, bool canClick, int size, std::string text, std::string font, SDL_Color color, bool selecting)
	RemoveSaveslot(pos);
	Entity& panel(EntityManager::GetInstance()->AddEntity("Panel" + pos));
	panel.AddComponent<Transform>(panelPos[pos].x, panelPos[pos].y);
	panel.AddComponent<Sprite>("SaveEmpty", 1043, 242, TIME_COUNT_LAYER);
	temporarySlot.push_back(&panel);

	Entity& difText(EntityManager::GetInstance()->AddEntity("difText" + pos));
	difText.AddComponent<Transform>(difficultyTextPos[pos].x, difficultyTextPos[pos].y);
	difText.AddComponent<Text>(TIME_COUNT_LAYER, "difText" + pos, "DisposableDroidBB_bld", 83, white, "Select Difficulty");
	temporarySlot.push_back(&difText);

	Entity& easy(EntityManager::GetInstance()->AddEntity("easy" + pos));
	easy.AddComponent<TextButtonSaveSlot>(easyText[pos].x, easyText[pos].y, true, 50, "Easy", "DisposableDroidBB_bld", white, true);
	easy.GetComponent<TextButtonSaveSlot>()->SetFunction(&ClickButton::CreateSave);
	easy.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineSize = 2;
	easy.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineColor = black;
	m_AllUI[easy.name] = &easy;
	temporarySlot.push_back(&easy);

	Entity& normal(EntityManager::GetInstance()->AddEntity("normal" + pos));
	normal.AddComponent<TextButtonSaveSlot>(normalText[pos].x, normalText[pos].y, true, 50, "Normal", "DisposableDroidBB_bld", white, false);
	normal.GetComponent<TextButtonSaveSlot>()->SetFunction(&ClickButton::CreateSave);
	normal.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineSize = 2;
	normal.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineColor = black;
	m_AllUI[normal.name] = &normal;
	temporarySlot.push_back(&normal);

	Entity& hard(EntityManager::GetInstance()->AddEntity("hard" + pos));
	hard.AddComponent<TextButtonSaveSlot>(hardText[pos].x, hardText[pos].y, true, 50, "Hard", "DisposableDroidBB_bld", white, false);
	hard.GetComponent<TextButtonSaveSlot>()->SetFunction(&ClickButton::CreateSave);
	hard.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineSize = 2;
	hard.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineColor = black;
	m_AllUI[hard.name] = &hard;
	temporarySlot.push_back(&hard);

	Entity& timeout(EntityManager::GetInstance()->AddEntity("timeout" + pos));
	timeout.AddComponent<TextButtonSaveSlot>(timeoutText[pos].x, timeoutText[pos].y, true, 50, "Timeout", "DisposableDroidBB_bld", red, false);
	timeout.GetComponent<TextButtonSaveSlot>()->SetFunction(&ClickButton::CreateSave);
	timeout.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineSize = 2;
	timeout.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineColor = black;
	m_AllUI[timeout.name] = &timeout;
	temporarySlot.push_back(&timeout);

	Entity& nohero(EntityManager::GetInstance()->AddEntity("nohero" + pos));
	nohero.AddComponent<TextButtonSaveSlot>(noheroText[pos].x, noheroText[pos].y, true, 50, "No-Hero-Stands", "DisposableDroidBB_bld", red, false);
	nohero.GetComponent<TextButtonSaveSlot>()->SetFunction(&ClickButton::CreateSave);
	nohero.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineSize = 2;
	nohero.GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->outlineColor = black;
	m_AllUI[timeout.name] = &nohero;
	temporarySlot.push_back(&nohero);

	Entity& undo3(EntityManager::GetInstance()->AddEntity("Undo3" + pos));
	undo3.AddComponent<Transform>(undoButton[pos].x, undoButton[pos].y);
	undo3.AddComponent<Sprite>("Cancel", 44, 44, TIME_COUNT_LAYER);
	undo3.AddComponent<ButtonInSaveSlot>(undoButton[pos].x, undoButton[pos].y, true, "Cancel", "Cancel2", pos);
	undo3.GetComponent<ButtonInSaveSlot>()->SetFunction(&ClickButton::CancelSaveSlot);
	m_AllUI[undo3.name] = &undo3;
	temporarySlot.push_back(&undo3);

	easy.GetComponent<TextButtonSaveSlot>()->nextUI[RIGHT] = &normal;
	easy.GetComponent<TextButtonSaveSlot>()->nextUI[LEFT] = &nohero;
	easy.GetComponent<TextButtonSaveSlot>()->nextUI[UP] = &undo3;

	normal.GetComponent<TextButtonSaveSlot>()->nextUI[LEFT] = &easy;
	normal.GetComponent<TextButtonSaveSlot>()->nextUI[RIGHT] = &hard;
	normal.GetComponent<TextButtonSaveSlot>()->nextUI[UP] = &undo3;

	hard.GetComponent<TextButtonSaveSlot>()->nextUI[LEFT] = &normal;
	hard.GetComponent<TextButtonSaveSlot>()->nextUI[RIGHT] = &timeout;
	hard.GetComponent<TextButtonSaveSlot>()->nextUI[UP] = &undo3;

	timeout.GetComponent<TextButtonSaveSlot>()->nextUI[LEFT] = &hard;
	timeout.GetComponent<TextButtonSaveSlot>()->nextUI[RIGHT] = &nohero;
	timeout.GetComponent<TextButtonSaveSlot>()->nextUI[UP] = &undo3;

	nohero.GetComponent<TextButtonSaveSlot>()->nextUI[LEFT] = &timeout;
	nohero.GetComponent<TextButtonSaveSlot>()->nextUI[RIGHT] = &easy;
	nohero.GetComponent<TextButtonSaveSlot>()->nextUI[UP] = &undo3;

	undo3.GetComponent<ButtonInSaveSlot>()->nextUI[DOWN] = &easy;
	currentUI = &easy;
}

void UIManager::DeleteTemporary()
{
	for (auto& UI : temporarySlot)
	{
		UI->Destroy();
	}
	temporarySlot.clear();
}

void UIManager::RemoveMainUI()
{
	for (auto& UI : startingUI)
	{
		UI->Destroy();
	}
	for (auto& UI : optionUI)
	{
		UI->Destroy();
	}
	startingUI.clear();
	optionUI.clear();
}

void UIManager::RemovePauseUI()
{
	if (pauseUI.size() != 0)
	{
		for (auto& UI : pauseUI)
		{
			UI->Destroy();
		}
		pauseUI.clear();
	}

}

void UIManager::GoToMainScene()
{
	SceneManager::GetInstance()->LoadScene(MENU_SCENE);
	//CutsceneManager::GetInstance()->ResetPlay();
}


void UIManager::InitializeMenu()
{
	Camera::GetInstance()->Reset();

	resolution_text.push_back("1280x720");
	resolution_text.push_back("1600x900");
	resolution_text.push_back("1920x1080");

	for (int i = 0; i <= 10; i++)
	{
		masterVolume_text.push_back(std::to_string(i));
		sfxVolume_text.push_back(std::to_string(i));
		musicVolume_text.push_back(std::to_string(i));
		screenShake_text.push_back(std::to_string(i));
	}
	SpawnMenuUI();
	SetSaveSlotPos();
}

void UIManager::SetSaveSlotPos()
{
	difficultyTextPos[0].x = difficultyTextPos[1].x = difficultyTextPos[2].x = 515;
	difficultyTextPos[0].y = 105;
	difficultyTextPos[1].y = 371;
	difficultyTextPos[2].y = 638;

	easyText[0].x = easyText[1].x = easyText[2].x = 318;
	easyText[0].y = 196;
	easyText[1].y = 462;
	easyText[2].y = 729;

	normalText[0].x = normalText[1].x = normalText[2].x = 458;
	normalText[0].y = 196;
	normalText[1].y = 462;
	normalText[2].y = 729;

	hardText[0].x = hardText[1].x = hardText[2].x = 630;
	hardText[0].y = 196;
	hardText[1].y = 462;
	hardText[2].y = 729;

	timeoutText[0].x = timeoutText[1].x = timeoutText[2].x = 767;
	timeoutText[0].y = 196;
	timeoutText[1].y = 462;
	timeoutText[2].y = 729;

	noheroText[0].x = noheroText[1].x = noheroText[2].x = 950;
	noheroText[0].y = 196;
	noheroText[1].y = 462;
	noheroText[2].y = 729;

	undoButton[0].x = undoButton[1].x = undoButton[2].x = 1220;
	undoButton[0].y = 82;
	undoButton[1].y = 348;
	undoButton[2].y = 615;

	panelPos[0].x = panelPos[1].x = panelPos[2].x = 278;
	panelPos[0].y = 62;
	panelPos[1].y = 330;
	panelPos[2].y = 595;
}

void UIManager::InitializeSplash()
{
	/*Entity& text(EntityManager::GetInstance()->AddEntity("AdjustArrowText"));
	text.AddComponent<Transform>(Camera::GetInstance()->GetPosition().x, Camera::GetInstance()->GetPosition().y);
	SDL_Color white = { 255,255,255,255 };
	text.AddComponent<Text>(TIME_COUNT_LAYER, "ToBeContinue", "ArcadeClassic", 100, white, "To     be     continued");
	text.GetComponent<Transform>()->position.x = 400;
	text.GetComponent<Transform>()->position.y = 380;*/
}

void UIManager::Update(float deltaTime)
{
	if (currentUI != nullptr)
	{
		//std::cout << currentUI->name << std::endl;
	}
	bool isPause = PauseManager::GetInstance()->GetIsPause();
	if (SceneManager::GetInstance()->scene->name == MENU_SCENE || (SceneManager::GetInstance()->scene->name == GAME_SCENE && isPause))
	{
		if (!canMove)
		{
			cooldown += deltaTime;
			if (cooldown >= CONTROLCOOLDOWN)
			{
				canMove = true;
				inCooldown = false;
			}
		}

	}
	if (SceneManager::GetInstance()->scene->name == MENU_SCENE || (SceneManager::GetInstance()->scene->name == GAME_SCENE && isPause))
	{
		if (canMove)
		{
			bool inputArray[4];
			inputArray[UP] = InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_W) || InputManager::GetInstance()->GetAxisController(SDL_CONTROLLER_AXIS_LEFTY) < 0.0f || InputManager::GetInstance()->GetKeyDown(SDL_CONTROLLER_BUTTON_DPAD_UP);
			inputArray[DOWN] = InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_S) || InputManager::GetInstance()->GetAxisController(SDL_CONTROLLER_AXIS_LEFTY) > 0.0f || InputManager::GetInstance()->GetKeyDown(SDL_CONTROLLER_BUTTON_DPAD_DOWN);
			inputArray[LEFT] = InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_A) || InputManager::GetInstance()->GetAxisController(SDL_CONTROLLER_AXIS_LEFTX) < 0.0f || InputManager::GetInstance()->GetKeyDown(SDL_CONTROLLER_BUTTON_DPAD_LEFT);
			inputArray[RIGHT] = InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_D) || InputManager::GetInstance()->GetAxisController(SDL_CONTROLLER_AXIS_LEFTX) > 0.0f || InputManager::GetInstance()->GetKeyDown(SDL_CONTROLLER_BUTTON_DPAD_RIGHT);

			for (int i = 0; i < 4; i++)
			{
				if (inputArray[i])
				{
					Move(static_cast<Direction>(i));
					return;
				}
			}

			if (InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_SPACE) || InputManager::GetInstance()->GetKeyUp(SDL_CONTROLLER_BUTTON_A))
			{
				if (currentUI->HasComponent<CheckBox>())
				{
					if (currentUI->GetComponent<CheckBox>()->isSelecting)
					{
						/*if (currentUI->GetComponent<CheckBox>()->isCheck == true)
						{
							currentUI->GetComponent<CheckBox>()->isCheck = false;
						}
						else
						{
							currentUI->GetComponent<CheckBox>()->isCheck = true;
						}*/
						GetSelectingButton()->GetComponent<CheckBox>()->OnClick();
					}
				}
				else if (currentUI->HasComponent<Button>())
				{
					if (currentUI->GetComponent<Button>()->isSelecting)
					{
						currentUI->GetComponent<Button>()->OnClick();
					}
					cooldown = CONTROLCOOLDOWN;
				}
				else if (currentUI->HasComponent<SaveSlotButton>())
				{
					if (currentUI->GetComponent<SaveSlotButton>()->isSelecting && !currentUI->GetComponent<SaveSlotButton>()->hasSave)
					{
						currentUI->GetComponent<SaveSlotButton>()->OnClick(currentUI->GetComponent<SaveSlotButton>()->ID);
					}
					else if (currentUI->GetComponent<SaveSlotButton>()->isSelecting && currentUI->GetComponent<SaveSlotButton>()->hasSave)
					{
						SaveManager::GetInstance()->currentSlotPos = currentUI->GetComponent<SaveSlotButton>()->ID;
						currentUI->GetComponent<SaveSlotButton>()->OnClick();
					}
				}
				else if (currentUI->HasComponent<TextButtonSaveSlot>())
				{
					if (currentUI->GetComponent<TextButtonSaveSlot>()->isSelecting)
					{
						SaveManager::GetInstance()->SetSave(currentPos, 0, 0, currentUI->GetComponent<TextButtonSaveSlot>()->textButton->GetComponent<Text>()->text, 0);
						SaveManager::GetInstance()->LoadSave(currentPos);
						currentUI->GetComponent<TextButtonSaveSlot>()->OnClick(currentPos);
					}
				}
				else if (currentUI->HasComponent<AdjustArrow>())
				{
					if (currentUI->GetComponent<AdjustArrow>()->isSelecting && currentUI->GetComponent<AdjustArrow>()->ownAdjuster->HasComponent<Adjuster>())
					{
						//GetSelectingButton()->GetComponent<Adjuster>()->OnClick();
						currentUI->GetComponent<AdjustArrow>()->isPressed = true;
						currentUI->GetComponent<AdjustArrow>()->ownAdjuster->GetComponent<Adjuster>()->AdjustIndex();
					}
					cooldown = CONTROLCOOLDOWN;
				}
				else if (currentUI->HasComponent<TextButton>())
				{
					if (currentUI->GetComponent<TextButton>()->isSelecting)
					{
						currentUI->GetComponent<TextButton>()->OnClick();
					}
					cooldown = CONTROLCOOLDOWN;
				}
				else if (currentUI->HasComponent<ButtonInSaveSlot>())
				{
					if (currentUI->GetComponent<ButtonInSaveSlot>()->isSelecting)
					{
						currentUI->GetComponent<ButtonInSaveSlot>()->OnClick(currentUI->GetComponent<ButtonInSaveSlot>()->ID);
					}
					cooldown = CONTROLCOOLDOWN;
				}
			}
		}
	}
}

Entity* UIManager::GetSelectingButton()
{
	for (std::map<std::string, Entity*>::iterator it = m_AllUI.begin(); it != m_AllUI.end(); it++)
	{
		if (it->second->HasComponent<Button>())
		{
			if (it->second->GetComponent<Button>()->isSelecting)
			{
				return it->second;
			}
		}
		else if (it->second->HasComponent<CheckBox>())
		{
			if (it->second->GetComponent<CheckBox>()->isSelecting)
			{
				return it->second;
			}
		}
		else if (it->second->HasComponent<Adjuster>())
		{
			if (it->second->GetComponent<Adjuster>()->isSelecting)
			{
				return it->second;
			}
		}
		else if (it->second->HasComponent<AdjustArrow>())
		{
			if (it->second->GetComponent<AdjustArrow>()->isSelecting)
			{
				return it->second;
			}
		}
		else if (it->second->HasComponent<TextButton>())
		{
			if (it->second->GetComponent<TextButton>()->isSelecting)
			{
				return it->second;
			}
		}
		else if (it->second->HasComponent<SaveSlotButton>())
		{
			if (it->second->GetComponent<SaveSlotButton>()->isSelecting)
			{
				return it->second;
			}
		}
		else if (it->second->HasComponent<TextButtonSaveSlot>())
		{
			if (it->second->GetComponent<TextButtonSaveSlot>()->isSelecting)
			{
				return it->second;
			}
		}
		else if (it->second->HasComponent<ButtonInSaveSlot>())
		{
			if (it->second->GetComponent<ButtonInSaveSlot>()->isSelecting)
			{
				return it->second;
			}
		}
	}
}

UIManager* UIManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new UIManager();
}


void UIManager::Move(Direction direction)
{
	if (currentUI->HasComponent<Button>())
	{
		if (currentUI->GetComponent<Button>()->SelectUI(direction) != nullptr)
		{
			currentUI->GetComponent<Button>()->isSelecting = false;
			currentUI->GetComponent<Button>()->isSet = true;

			if (currentUI->GetComponent<Button>()->SelectUI(direction)->HasComponent<Button>())
			{
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<Button>()->isSelecting = true;
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<Button>()->isSet = true;
			}
			else if (currentUI->GetComponent<Button>()->SelectUI(direction)->HasComponent<CheckBox>())
			{
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<CheckBox>()->isSelecting = true;
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<Button>()->SelectUI(direction)->HasComponent<Adjuster>())
			{
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<Adjuster>()->isSelecting = true;
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<Adjuster>()->isSet = true;
			}
			else if (currentUI->GetComponent<Button>()->SelectUI(direction)->HasComponent<AdjustArrow>())
			{
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSelecting = true;
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSet = true;
			}
			else if (currentUI->GetComponent<Button>()->SelectUI(direction)->HasComponent<TextButton>())
			{
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<TextButton>()->isSelecting = true;
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<TextButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<Button>()->SelectUI(direction)->HasComponent<SaveSlotButton>())
			{
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSelecting = true;
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<Button>()->SelectUI(direction)->HasComponent<TextButtonSaveSlot>())
			{
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			else if (currentUI->GetComponent<Button>()->SelectUI(direction)->HasComponent<ButtonInSaveSlot>())
			{
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<Button>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSet = true;
			}
			currentUI = currentUI->GetComponent<Button>()->SelectUI(direction);
		}
		cooldown = 0.0f;
		canMove = false;
		//cooldown = CONTROLCOOLDOWN;
	}
	else if (currentUI->HasComponent<CheckBox>())
	{
		if (currentUI->GetComponent<CheckBox>()->SelectUI(direction) != nullptr)
		{
			currentUI->GetComponent<CheckBox>()->isSelecting = false;
			currentUI->GetComponent<CheckBox>()->isSet = true;

			if (currentUI->GetComponent<CheckBox>()->SelectUI(direction)->HasComponent<Button>())
			{
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<Button>()->isSelecting = true;
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<Button>()->isSet = true;
			}
			else if (currentUI->GetComponent<CheckBox>()->SelectUI(direction)->HasComponent<CheckBox>())
			{
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<CheckBox>()->isSelecting = true;
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<CheckBox>()->SelectUI(direction)->HasComponent<Adjuster>())
			{
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<Adjuster>()->isSelecting = true;
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<Adjuster>()->isSet = true;
			}
			else if (currentUI->GetComponent<CheckBox>()->SelectUI(direction)->HasComponent<AdjustArrow>())
			{
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSelecting = true;
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSet = true;
			}
			else if (currentUI->GetComponent<CheckBox>()->SelectUI(direction)->HasComponent<TextButton>())
			{
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<TextButton>()->isSelecting = true;
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<TextButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<CheckBox>()->SelectUI(direction)->HasComponent<SaveSlotButton>())
			{
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSelecting = true;
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<CheckBox>()->SelectUI(direction)->HasComponent<TextButtonSaveSlot>())
			{
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			else if (currentUI->GetComponent<CheckBox>()->SelectUI(direction)->HasComponent<ButtonInSaveSlot>())
			{
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<CheckBox>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSet = true;
			}
			currentUI = currentUI->GetComponent<CheckBox>()->SelectUI(direction);
		}
		canMove = false;
		cooldown = 0.0f;

		//cooldown = CONTROLCOOLDOWN;
	}
	//
	else if (currentUI->HasComponent<Adjuster>())
	{
		if (currentUI->GetComponent<Adjuster>()->SelectUI(direction) != nullptr)
		{
			currentUI->GetComponent<Adjuster>()->isSelecting = false;
			currentUI->GetComponent<Adjuster>()->isSet = true;

			if (currentUI->GetComponent<Adjuster>()->SelectUI(direction)->HasComponent<Button>())
			{
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<Button>()->isSelecting = true;
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<Button>()->isSet = true;
			}
			else if (currentUI->GetComponent<Adjuster>()->SelectUI(direction)->HasComponent<CheckBox>())
			{
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<CheckBox>()->isSelecting = true;
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<Adjuster>()->SelectUI(direction)->HasComponent<Adjuster>())
			{
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<Adjuster>()->isSelecting = true;
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<Adjuster>()->SelectUI(direction)->HasComponent<AdjustArrow>())
			{
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSelecting = true;
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<Adjuster>()->SelectUI(direction)->HasComponent<TextButton>())
			{
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<TextButton>()->isSelecting = true;
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<Adjuster>()->SelectUI(direction)->HasComponent<SaveSlotButton>())
			{
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSelecting = true;
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<Adjuster>()->SelectUI(direction)->HasComponent<TextButtonSaveSlot>())
			{
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			else if (currentUI->GetComponent<Adjuster>()->SelectUI(direction)->HasComponent<ButtonInSaveSlot>())
			{
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<Adjuster>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			currentUI = currentUI->GetComponent<Adjuster>()->SelectUI(direction);
		}
		canMove = false;
		cooldown = 0.0f;

		//cooldown = CONTROLCOOLDOWN;
	}
	else if (currentUI->HasComponent<AdjustArrow>())
	{
		if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction) != nullptr)
		{
			currentUI->GetComponent<AdjustArrow>()->isSelecting = false;
			currentUI->GetComponent<AdjustArrow>()->isSet = true;

			if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->HasComponent<Button>())
			{
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<Button>()->isSelecting = true;
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<Button>()->isSet = true;
			}
			else if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->HasComponent<CheckBox>())
			{
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<CheckBox>()->isSelecting = true;
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->HasComponent<Adjuster>())
			{
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<Adjuster>()->isSelecting = true;
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<Adjuster>()->isSet = true;
			}
			else if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->HasComponent<AdjustArrow>())
			{
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSelecting = true;
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSet = true;
			}
			else if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->HasComponent<TextButton>())
			{
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<TextButton>()->isSelecting = true;
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<TextButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->HasComponent<SaveSlotButton>())
			{
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSelecting = true;
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->HasComponent<TextButtonSaveSlot>())
			{
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			else if (currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->HasComponent<ButtonInSaveSlot>())
			{
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<AdjustArrow>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSet = true;
			}
			currentUI = currentUI->GetComponent<AdjustArrow>()->SelectUI(direction);
		}
		canMove = false;
		cooldown = 0.0f;
		//cooldown = CONTROLCOOLDOWN;
	}
	else if (currentUI->HasComponent<TextButton>())
	{
		if (currentUI->GetComponent<TextButton>()->SelectUI(direction) != nullptr)
		{
			currentUI->GetComponent<TextButton>()->isSelecting = false;
			currentUI->GetComponent<TextButton>()->isSet = true;

			if (currentUI->GetComponent<TextButton>()->SelectUI(direction)->HasComponent<Button>())
			{
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<Button>()->isSelecting = true;
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<Button>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButton>()->SelectUI(direction)->HasComponent<CheckBox>())
			{
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<CheckBox>()->isSelecting = true;
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButton>()->SelectUI(direction)->HasComponent<Adjuster>())
			{
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<Adjuster>()->isSelecting = true;
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<Adjuster>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButton>()->SelectUI(direction)->HasComponent<AdjustArrow>())
			{
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSelecting = true;
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButton>()->SelectUI(direction)->HasComponent<TextButton>())
			{
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<TextButton>()->isSelecting = true;
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<TextButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButton>()->SelectUI(direction)->HasComponent<SaveSlotButton>())
			{
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSelecting = true;
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButton>()->SelectUI(direction)->HasComponent<TextButtonSaveSlot>())
			{
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButton>()->SelectUI(direction)->HasComponent<ButtonInSaveSlot>())
			{
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<TextButton>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSet = true;
			}
			currentUI = currentUI->GetComponent<TextButton>()->SelectUI(direction);
		}
		canMove = false;
		cooldown = 0.0f;

		//cooldown = CONTROLCOOLDOWN;
	}
	else if (currentUI->HasComponent<SaveSlotButton>())
	{
		if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction) != nullptr)
		{
			currentUI->GetComponent<SaveSlotButton>()->isSelecting = false;
			currentUI->GetComponent<SaveSlotButton>()->isSet = true;


			if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->HasComponent<Button>())
			{
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<Button>()->isSelecting = true;
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<Button>()->isSet = true;
			}
			else if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->HasComponent<CheckBox>())
			{
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<CheckBox>()->isSelecting = true;
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->HasComponent<Adjuster>())
			{
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<Adjuster>()->isSelecting = true;
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<Adjuster>()->isSet = true;
			}
			else if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->HasComponent<AdjustArrow>())
			{
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSelecting = true;
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSet = true;
			}
			else if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->HasComponent<TextButton>())
			{
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<TextButton>()->isSelecting = true;
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<TextButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->HasComponent<SaveSlotButton>())
			{
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSelecting = true;
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->HasComponent<TextButtonSaveSlot>())
			{
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			else if (currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->HasComponent<ButtonInSaveSlot>())
			{
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSet = true;
			}
			currentUI = currentUI->GetComponent<SaveSlotButton>()->SelectUI(direction);
		}
		canMove = false;
		cooldown = 0.0f;

		//cooldown = CONTROLCOOLDOWN;
	}
	else if (currentUI->HasComponent<TextButtonSaveSlot>())
	{
		if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction) != nullptr)
		{
			currentUI->GetComponent<TextButtonSaveSlot>()->isSelecting = false;
			currentUI->GetComponent<TextButtonSaveSlot>()->isSet = true;

			if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->HasComponent<Button>())
			{
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<Button>()->isSelecting = true;
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<Button>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->HasComponent<CheckBox>())
			{
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<CheckBox>()->isSelecting = true;
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->HasComponent<Adjuster>())
			{
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<Adjuster>()->isSelecting = true;
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<Adjuster>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->HasComponent<AdjustArrow>())
			{
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSelecting = true;
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->HasComponent<TextButton>())
			{
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<TextButton>()->isSelecting = true;
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<TextButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->HasComponent<SaveSlotButton>())
			{
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSelecting = true;
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->HasComponent<TextButtonSaveSlot>())
			{
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			else if (currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->HasComponent<ButtonInSaveSlot>())
			{
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSet = true;
			}
			currentUI = currentUI->GetComponent<TextButtonSaveSlot>()->SelectUI(direction);
		}
		canMove = false;
		cooldown = 0.0f;
	}
	else if (currentUI->HasComponent<ButtonInSaveSlot>())
	{
		if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction) != nullptr)
		{
			currentUI->GetComponent<ButtonInSaveSlot>()->isSelecting = false;
			currentUI->GetComponent<ButtonInSaveSlot>()->isSet = true;
			if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->HasComponent<Button>())
			{
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<Button>()->isSelecting = true;
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<Button>()->isSet = true;
			}
			else if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->HasComponent<CheckBox>())
			{
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<CheckBox>()->isSelecting = true;
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<CheckBox>()->isSet = true;
			}
			else if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->HasComponent<Adjuster>())
			{
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<Adjuster>()->isSelecting = true;
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<Adjuster>()->isSet = true;
			}
			else if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->HasComponent<AdjustArrow>())
			{
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSelecting = true;
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<AdjustArrow>()->isSet = true;
			}
			else if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->HasComponent<TextButton>())
			{
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<TextButton>()->isSelecting = true;
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<TextButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->HasComponent<SaveSlotButton>())
			{
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSelecting = true;
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<SaveSlotButton>()->isSet = true;
			}
			else if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->HasComponent<TextButtonSaveSlot>())
			{
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<TextButtonSaveSlot>()->isSet = true;
			}
			else if (currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->HasComponent<ButtonInSaveSlot>())
			{
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSelecting = true;
				currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction)->GetComponent<ButtonInSaveSlot>()->isSet = true;
			}
			currentUI = currentUI->GetComponent<ButtonInSaveSlot>()->SelectUI(direction);
		}
		canMove = false;
		cooldown = 0.0f;
	}
}

void UIManager::CreatSlot(int pos)
{
	//std::cout << "CreateSlot: " << pos << std::endl;
	if (pos == 0)
	{
		Entity& Slot1(EntityManager::GetInstance()->AddEntity("Slot1"));
		Slot1.AddComponent<Transform>(278, 62);
		Slot1.AddComponent<Sprite>("Save", 920, 240, TIME_COUNT_LAYER);
		Slot1.AddComponent<SaveSlotButton>(278, 62, true, "", "Save", "Save2", 0, 0);
		Slot1.GetComponent<SaveSlotButton>()->SetFunction(&ClickButton::ClickPlayButton);
		Slot1.GetComponent<SaveSlotButton>()->hasSave = true;
		saveSlotUI.push_back(&Slot1);
		Slot1.GetComponent<SaveSlotButton>()->isSelecting = true;
		m_AllUI[Slot1.name] = &Slot1;
		currentUI = &Slot1;
		saveSlot[0] = &Slot1;


		Entity& percentText(EntityManager::GetInstance()->AddEntity("% Text"));
		if (SaveManager::GetInstance()->save[pos].playStage < 10)
		{
			percentText.AddComponent<Transform>(336, 134);
		}
		else
		{
			percentText.AddComponent<Transform>(316, 134);
		}
		int level = SaveManager::GetInstance()->save[pos].playStage * 100 / LevelManager::GetInstance()->GetLevelAmount();
		std::string showText = std::to_string(level);
		percentText.AddComponent<Text>(TIME_COUNT_LAYER, "PercentText " + Slot1.name, "DisposableDroidBB_bld", 100, white, showText + "%");

		int deadNumber = SaveManager::GetInstance()->save[pos].death;
		std::string showDead = std::to_string(deadNumber);
		Entity& deathText(EntityManager::GetInstance()->AddEntity("Death Text"));
		deathText.AddComponent<Transform>(545, 75);
		deathText.AddComponent<Text>(TIME_COUNT_LAYER, "DeathText " + Slot1.name, "DisposableDroidBB_bld", 67, white, "Death: " + showDead);

		Entity& modeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		modeText.AddComponent<Transform>(545, 145);
		modeText.AddComponent<Text>(TIME_COUNT_LAYER, "ModeText " + Slot1.name, "DisposableDroidBB_bld", 67, white, "Mode: " + SaveManager::GetInstance()->save[pos].mode);

		Entity& timeText(EntityManager::GetInstance()->AddEntity("Time Text"));
		timeText.AddComponent<Transform>(545, 215);
		timeText.AddComponent<Text>(TIME_COUNT_LAYER, "TimeText " + Slot1.name, "DisposableDroidBB_bld", 67, white, "Time: 0.0 min");

		Entity& del1(EntityManager::GetInstance()->AddEntity("Del1"));
		del1.AddComponent<Transform>(1210, 62);
		del1.AddComponent<Sprite>("Delete", 110, 240, TIME_COUNT_LAYER);
		del1.AddComponent<ButtonInSaveSlot>(1210, 62, true, "Delete", "Delete2", 0);
		del1.GetComponent<ButtonInSaveSlot>()->SetFunction(&ClickButton::DeleteEachSlot);
		delButton[0] = &del1;
		currentUI = &Slot1;
		availableSave[0] = true;
		//allSaveSlot[0] = &slot1;
	}
	else if (pos == 1)
	{
		Entity& Slot2(EntityManager::GetInstance()->AddEntity("Slot2"));
		Slot2.AddComponent<Transform>(278, 330);
		Slot2.AddComponent<Sprite>("SaveEmpty", 920, 240, TIME_COUNT_LAYER);
		Slot2.AddComponent<SaveSlotButton>(278, 595, true, "", "Save", "Save2", 0, 1);
		Slot2.GetComponent<SaveSlotButton>()->SetFunction(&ClickButton::ClickPlayButton);
		Slot2.GetComponent<SaveSlotButton>()->hasSave = true;
		saveSlotUI.push_back(&Slot2);
		Slot2.GetComponent<SaveSlotButton>()->isSelecting = true;
		m_AllUI[Slot2.name] = &Slot2;
		currentUI = &Slot2;
		saveSlot[1] = &Slot2;

		Entity& percentText(EntityManager::GetInstance()->AddEntity("% Text"));
		if (SaveManager::GetInstance()->save[pos].playStage < 10)
		{
			percentText.AddComponent<Transform>(336, 403);
		}
		else
		{
			percentText.AddComponent<Transform>(316, 403);
		}
		//percentText.AddComponent<Transform>(316 /*+ 20*/, 403);
		int level = SaveManager::GetInstance()->save[pos].playStage * 100 / LevelManager::GetInstance()->GetLevelAmount();
		std::string showText = std::to_string(level);
		percentText.AddComponent<Text>(TIME_COUNT_LAYER, "PercentText " + Slot2.name, "DisposableDroidBB_bld", 100, white, showText + "%");

		int deadNumber = SaveManager::GetInstance()->save[pos].death;
		std::string showDead = std::to_string(deadNumber);
		Entity& deathText(EntityManager::GetInstance()->AddEntity("Death Text"));
		deathText.AddComponent<Transform>(545, 344);
		deathText.AddComponent<Text>(TIME_COUNT_LAYER, "DeathText " + Slot2.name, "DisposableDroidBB_bld", 67, white, "Death: " + showDead);

		Entity& modeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		modeText.AddComponent<Transform>(545, 414);
		modeText.AddComponent<Text>(TIME_COUNT_LAYER, "ModeText " + Slot2.name, "DisposableDroidBB_bld", 67, white, "Mode: " + SaveManager::GetInstance()->save[pos].mode);

		Entity& timeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		timeText.AddComponent<Transform>(545, 483);
		timeText.AddComponent<Text>(TIME_COUNT_LAYER, "TimeText " + Slot2.name, "DisposableDroidBB_bld", 67, white, "Time: 0.0 min");

		Entity& del2(EntityManager::GetInstance()->AddEntity("Del2"));
		del2.AddComponent<Transform>(1210, 330);
		del2.AddComponent<Sprite>("Delete", 110, 240, TIME_COUNT_LAYER);
		del2.AddComponent<ButtonInSaveSlot>(1210, 328, true, "Delete", "Delete2", 1);
		del2.GetComponent<ButtonInSaveSlot>()->SetFunction(&ClickButton::DeleteEachSlot);
		delButton[1] = &del2;
		availableSave[1] = true;
	}
	else if (pos == 2)
	{
		Entity& Slot3(EntityManager::GetInstance()->AddEntity("Slot3"));
		Slot3.AddComponent<Transform>(278, 595);
		Slot3.AddComponent<Sprite>("ApplyButton", 922, 242, TIME_COUNT_LAYER);
		Slot3.AddComponent<SaveSlotButton>(278, 595, true, "", "Save", "Save2", 0, 2);
		Slot3.GetComponent<SaveSlotButton>()->SetFunction(&ClickButton::ClickPlayButton);
		Slot3.GetComponent<SaveSlotButton>()->hasSave = true;
		saveSlotUI.push_back(&Slot3);
		Slot3.GetComponent<SaveSlotButton>()->isSelecting = true;
		m_AllUI[Slot3.name] = &Slot3;
		currentUI = &Slot3;
		saveSlot[2] = &Slot3;

		Entity& percentText(EntityManager::GetInstance()->AddEntity("% Text"));
		if (SaveManager::GetInstance()->save[pos].playStage < 10)
		{
			percentText.AddComponent<Transform>(336, 673);
		}
		else
		{
			percentText.AddComponent<Transform>(316, 673);
		}
		int level = SaveManager::GetInstance()->save[pos].playStage * 100 / LevelManager::GetInstance()->GetLevelAmount();
		std::string showText = std::to_string(level);
		//percentText.AddComponent<Transform>(316 /*+ 20*/, 673);
		percentText.AddComponent<Text>(TIME_COUNT_LAYER, "PercentText " + Slot3.name, "DisposableDroidBB_bld", 100, white, showText + "%");

		int deadNumber = SaveManager::GetInstance()->save[pos].death;
		std::string showDead = std::to_string(deadNumber);
		Entity& deathText(EntityManager::GetInstance()->AddEntity("Death Text"));
		deathText.AddComponent<Transform>(545, 614);
		deathText.AddComponent<Text>(TIME_COUNT_LAYER, "DeathText " + Slot3.name, "DisposableDroidBB_bld", 67, white, "Death: 0"/*+ DeathTime*/);

		Entity& modeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		modeText.AddComponent<Transform>(545, 684);
		modeText.AddComponent<Text>(TIME_COUNT_LAYER, "ModeText " + Slot3.name, "DisposableDroidBB_bld", 67, white, "Mode: " + SaveManager::GetInstance()->save[pos].mode);

		Entity& timeText(EntityManager::GetInstance()->AddEntity("Mode Text"));
		timeText.AddComponent<Transform>(545, 753);
		timeText.AddComponent<Text>(TIME_COUNT_LAYER, "TimeText " + Slot3.name, "DisposableDroidBB_bld", 67, white, "Time: 0.0 min");


		Entity& del3(EntityManager::GetInstance()->AddEntity("DeleteSave3"));
		del3.AddComponent<Transform>(1210, 595);
		del3.AddComponent<Sprite>("Delete", 110, 240, TIME_COUNT_LAYER);
		del3.AddComponent<ButtonInSaveSlot>(1210, 595, true, "Delete", "Delete2", 2);
		del3.GetComponent<ButtonInSaveSlot>()->SetFunction(&ClickButton::DeleteEachSlot);
		delButton[2] = &del3;
		availableSave[2] = true;

	}

	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[DOWN] = saveSlot[1];
	saveSlot[1]->GetComponent<SaveSlotButton>()->nextUI[DOWN] = saveSlot[2];

	saveSlot[2]->GetComponent<SaveSlotButton>()->nextUI[UP] = saveSlot[1];
	saveSlot[1]->GetComponent<SaveSlotButton>()->nextUI[UP] = saveSlot[0];
	if (delButton[0] != nullptr && pos == 0)
	{
		saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[RIGHT] = delButton[0];
		delButton[0]->GetComponent<ButtonInSaveSlot>()->nextUI[LEFT] = saveSlot[0];
	}
	if (delButton[1] != nullptr && pos == 1)
	{
		saveSlot[1]->GetComponent<SaveSlotButton>()->nextUI[RIGHT] = delButton[1];
		delButton[1]->GetComponent<ButtonInSaveSlot>()->nextUI[LEFT] = saveSlot[1];
	}
	if (delButton[2] != nullptr && pos == 2)
	{
		saveSlot[2]->GetComponent<SaveSlotButton>()->nextUI[RIGHT] = delButton[2];
		delButton[2]->GetComponent<ButtonInSaveSlot>()->nextUI[LEFT] = saveSlot[2];
	}
	undo->GetComponent<Button>()->nextUI[RIGHT] = saveSlot[0];
	undo->GetComponent<Button>()->nextUI[DOWN] = saveSlot[0];

	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[UP] = undo;
	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[LEFT] = undo;

	DeleteTemporary();
}

void UIManager::DeleteMainMenuUI()
{
	for (auto& UI : startingUI)
	{
		UI->Destroy();
	}
	startingUI.clear();
}

void UIManager::DeleteOption()
{
	for (auto& UI : optionUI)
	{
		UI->Destroy();
	}
	optionUI.clear();
}


void UIManager::ClearAllUI()
{
	m_AllUI.clear();
}

void UIManager::DeleteAllSaveSlot()
{
	for (int i = 0; i < 3; i++)
	{
		saveSlot[i]->Destroy();
		if (delButton[i] != nullptr)
		{
			delButton[i]->Destroy();
		}
	}
}

void UIManager::CancelSaveSlot(int pos)
{
	DeleteTemporary();
	if (pos == 0)
	{
		Entity& slot1(EntityManager::GetInstance()->AddEntity("SaveSlot1"));
		slot1.AddComponent<Transform>(278, 62);
		slot1.AddComponent<Sprite>("SaveEmpty", 1043, 242, TIME_COUNT_LAYER);
		slot1.AddComponent<SaveSlotButton>(278, 62, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 0, true);
		slot1.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot1);
		slot1.GetComponent<SaveSlotButton>()->isSelecting = true;
		m_AllUI[slot1.name] = &slot1;
		currentUI = &slot1;
		saveSlot[0] = &slot1;
	}
	else if (pos == 1)
	{
		Entity& slot2(EntityManager::GetInstance()->AddEntity("SaveSlot2"));
		slot2.AddComponent<Transform>(278, 330);
		slot2.AddComponent<Sprite>("ApplyButton", 1043, 242, TIME_COUNT_LAYER);
		slot2.AddComponent<SaveSlotButton>(278, 330, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 1, true);
		slot2.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot2);
		currentUI = &slot2;
		m_AllUI[slot2.name] = &slot2;
		saveSlot[1] = &slot2;
	}
	else if (pos == 2)
	{
		Entity& slot3(EntityManager::GetInstance()->AddEntity("SaveSlot3"));

		slot3.AddComponent<Transform>(278, 595);
		slot3.AddComponent<Sprite>("ApplyButton", 1043, 242, TIME_COUNT_LAYER);
		slot3.AddComponent<SaveSlotButton>(278, 595, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 2, true);
		slot3.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot3);
		currentUI = &slot3;
		m_AllUI[slot3.name] = &slot3;
		saveSlot[2] = &slot3;
	}
	/*if (saveSlot[0] == nullptr)
	{
		Entity& slot1(EntityManager::GetInstance()->AddEntity("SaveSlot1"));
		slot1.AddComponent<Transform>(278, 62);
		slot1.AddComponent<Sprite>("SaveEmpty", 1043, 242, TIME_COUNT_LAYER);
		slot1.AddComponent<SaveSlotButton>(278, 62, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 0, true);
		slot1.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot1);
		slot1.GetComponent<SaveSlotButton>()->isSelecting = true;
		m_AllUI[slot1.name] = &slot1;
		currentUI = &slot1;
		saveSlot[0] = &slot1;
	}
	else if (saveSlot[1] == nullptr)
	{
		Entity& slot2(EntityManager::GetInstance()->AddEntity("SaveSlot2"));
		slot2.AddComponent<Transform>(278, 330);
		slot2.AddComponent<Sprite>("ApplyButton", 1043, 242, TIME_COUNT_LAYER);
		slot2.AddComponent<SaveSlotButton>(278, 330, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 1, true);
		slot2.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot2);
		currentUI = &slot2;
		m_AllUI[slot2.name] = &slot2;
		saveSlot[1] = &slot2;
	}
	else if (saveSlot[2] == nullptr)
	{
		Entity& slot3(EntityManager::GetInstance()->AddEntity("SaveSlot3"));

		slot3.AddComponent<Transform>(278, 595);
		slot3.AddComponent<Sprite>("ApplyButton", 1043, 242, TIME_COUNT_LAYER);
		slot3.AddComponent<SaveSlotButton>(278, 595, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 2, true);
		slot3.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot3);
		currentUI = &slot3;
		m_AllUI[slot3.name] = &slot3;
		saveSlot[2] = &slot3;
	}*/
	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[DOWN] = saveSlot[1];
	saveSlot[1]->GetComponent<SaveSlotButton>()->nextUI[DOWN] = saveSlot[2];

	saveSlot[2]->GetComponent<SaveSlotButton>()->nextUI[UP] = saveSlot[1];
	saveSlot[1]->GetComponent<SaveSlotButton>()->nextUI[UP] = saveSlot[0];

	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[UP] = undo;
	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[LEFT] = undo;
	undo->GetComponent<Button>()->nextUI[RIGHT] = saveSlot[0];
	undo->GetComponent<Button>()->nextUI[DOWN] = saveSlot[0];
}

void UIManager::DeleteSlot(int pos)
{
	saveSlot[pos]->Destroy();
	if (pos == 0)
	{
		Entity& slot1(EntityManager::GetInstance()->AddEntity("SaveSlot1"));
		slot1.AddComponent<Transform>(278, 62);
		slot1.AddComponent<Sprite>("SaveEmpty", 1043, 242, TIME_COUNT_LAYER);
		slot1.AddComponent<SaveSlotButton>(278, 62, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 0, true);
		slot1.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot1);
		//availableSave[0] = false;
		slot1.GetComponent<SaveSlotButton>()->isSelecting = true;
		m_AllUI[slot1.name] = &slot1;
		currentUI = &slot1;
		saveSlot[0] = &slot1;
	}
	else if (pos == 1)
	{
		Entity& slot2(EntityManager::GetInstance()->AddEntity("SaveSlot2"));
		slot2.AddComponent<Transform>(278, 330);
		slot2.AddComponent<Sprite>("ApplyButton", 1043, 242, TIME_COUNT_LAYER);
		slot2.AddComponent<SaveSlotButton>(278, 330, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 1, true);
		slot2.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot2);
		currentUI = &slot2;
		m_AllUI[slot2.name] = &slot2;
		saveSlot[1] = &slot2;
		//availableSave[1] = false;
	}
	else if (pos == 2)
	{
		Entity& slot3(EntityManager::GetInstance()->AddEntity("SaveSlot3"));

		slot3.AddComponent<Transform>(278, 595);
		slot3.AddComponent<Sprite>("ApplyButton", 1043, 242, TIME_COUNT_LAYER);
		slot3.AddComponent<SaveSlotButton>(278, 595, true, "Empty Slot", "SaveEmpty", "SaveEmpty2", 100, 2, true);
		slot3.GetComponent<SaveSlotButton>()->SetEmptySaveSlot(&ClickButton::ClickEmpty);
		saveSlotUI.push_back(&slot3);
		currentUI = &slot3;
		m_AllUI[slot3.name] = &slot3;
		saveSlot[2] = &slot3;
		//availableSave[2] = false;
	}
	if (remove(SaveManager::GetInstance()->savefile[pos].c_str()) != 0)
	{
		//perror("Error Deleting File");
	}
	else
	{
		//puts("File Successfully Deleted");
	}
	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[DOWN] = saveSlot[1];
	saveSlot[1]->GetComponent<SaveSlotButton>()->nextUI[DOWN] = saveSlot[2];

	saveSlot[2]->GetComponent<SaveSlotButton>()->nextUI[UP] = saveSlot[1];
	saveSlot[1]->GetComponent<SaveSlotButton>()->nextUI[UP] = saveSlot[0];

	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[UP] = undo;
	saveSlot[0]->GetComponent<SaveSlotButton>()->nextUI[LEFT] = undo;
	undo->GetComponent<Button>()->nextUI[RIGHT] = saveSlot[0];
	undo->GetComponent<Button>()->nextUI[DOWN] = saveSlot[0];
}

void UIManager::NoHeroStandDeleteSave(int pos)
{
	GoToMainScene();
	if (remove(SaveManager::GetInstance()->savefile[pos].c_str()) != 0)
	{
		//perror("Error Deleting File");
	}
	else
	{
		//puts("File Successfully Deleted");
	}
	SaveManager::GetInstance()->hasSave[pos] = false;
}

void UIManager::SpawnOptionPauseMenu()
{

	resolution_text.push_back("1280x720");
	resolution_text.push_back("1600x900");
	resolution_text.push_back("1920x1080");

	for (int i = 0; i <= 10; i++)
	{
		masterVolume_text.push_back(std::to_string(i));
		sfxVolume_text.push_back(std::to_string(i));
		musicVolume_text.push_back(std::to_string(i));
		screenShake_text.push_back(std::to_string(i));
	}

	Entity& blackBG(EntityManager::GetInstance()->AddEntity("BlackBG"));
	blackBG.AddComponent<Transform>(Camera::GetInstance()->GetPosition().x, Camera::GetInstance()->GetPosition().y);
	blackBG.AddComponent<Sprite>("BlackBG", 1920, 1080, TIME_COUNT_LAYER);
	optionUI.push_back(&blackBG);

	//SaveManager::GetInstance()->LoadOption();
	Entity& undoButton(EntityManager::GetInstance()->AddEntity("UndoButton"));
	undoButton.AddComponent<Transform>(Camera::GetInstance()->GetPosition().x + 63, Camera::GetInstance()->GetPosition().y + 49);
	undoButton.AddComponent<Sprite>("Undo", 80, 80, TIME_COUNT_LAYER);
	undoButton.AddComponent<Button>(63, 49, true, "", "Undo", "Undo2", true);
	undoButton.GetComponent<Button>()->SetFunction(&ClickButton::PressUndoPauseOption);
	undoButton.GetComponent<Button>()->isSelecting = true;
	optionUI.push_back(&undoButton);
	m_AllUI[undoButton.name] = &undoButton;
	currentUI = &undoButton;

	//Prepare for OptionUI
	Entity& panel(EntityManager::GetInstance()->AddEntity("Panel"));
	panel.AddComponent<Transform>(Camera::GetInstance()->GetPosition().x + 426, Camera::GetInstance()->GetPosition().y + 139);
	panel.AddComponent<Sprite>("SettingPanel", 756, 751, TIME_COUNT_LAYER);
	optionUI.push_back(&panel);
	m_AllUI[panel.name] = &panel;

	//Header Part
	Entity& optionText(EntityManager::GetInstance()->AddEntity("optionText"));
	optionText.AddComponent<Transform>(632, 23);
	optionText.AddComponent<Text>(TIME_COUNT_LAYER, "OptionMenu", "editundo", 125, white, "OPTION");
	optionUI.push_back(&optionText);
	m_AllUI[optionText.name] = &optionText;


	//Graphic Part
	Entity& graphicText(EntityManager::GetInstance()->AddEntity("MenuGraphic"));
	graphicText.AddComponent<Transform>(563, 218);
	graphicText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuGraphic", "DisposableDroidBB_bld", 40, white, "Graphic");
	optionUI.push_back(&graphicText);
	m_AllUI[graphicText.name] = &graphicText;

	Entity& resolutionText(EntityManager::GetInstance()->AddEntity("MenuResolution"));
	resolutionText.AddComponent<Transform>(549, 278);
	resolutionText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuResolution", "DisposableDroidBB", 30, white, "Resolution");
	optionUI.push_back(&resolutionText);
	m_AllUI[resolutionText.name] = &resolutionText;

	Entity& resAdjuster(EntityManager::GetInstance()->AddEntity("ResolutionAdjuster"));
	if (SaveManager::GetInstance()->opt.resolution_width == 1280 && SaveManager::GetInstance()->opt.resolution_height == 720)
	{
		resAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 272, resolution_text, 0);
	}
	else if (SaveManager::GetInstance()->opt.resolution_width == 1920 && SaveManager::GetInstance()->opt.resolution_height == 1080)
	{
		resAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 272, resolution_text, 2);
	}
	else if (SaveManager::GetInstance()->opt.resolution_width == 1600 && SaveManager::GetInstance()->opt.resolution_height == 900)
	{
		resAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 272, resolution_text, 1);
	}
	
	optionUI.push_back(&resAdjuster);
	m_AllUI[resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->name] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->name] = resAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[resAdjuster.name] = &resAdjuster;
	optionUI.push_back(resAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(resAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(resAdjuster.GetComponent<Adjuster>()->adjusterText);

	//resAdjuster.GetComponent<Adjuster>()->leftButton
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[RIGHT];

	resAdjuster.GetComponent<Adjuster>()->nextUI[RIGHT]->GetComponent<AdjustArrow>()->nextUI[LEFT] = &resAdjuster;
	resAdjuster.GetComponent<Adjuster>()->nextUI[DOWN] = &resAdjuster;
	resAdjuster.GetComponent<Adjuster>()->nextUI[UP] = &resAdjuster;

	Entity& fullscreenText(EntityManager::GetInstance()->AddEntity("MenuFullscreen"));
	fullscreenText.AddComponent<Transform>(549, 327);
	fullscreenText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuFullscreen", "DisposableDroidBB", 30, white, "Fullscreen");
	optionUI.push_back(&fullscreenText);
	m_AllUI[fullscreenText.name] = &fullscreenText;

	Entity& fullscreenCheckBox(EntityManager::GetInstance()->AddEntity("FullscreenCheckBox"));
	fullscreenCheckBox.AddComponent<CheckBox>(Camera::GetInstance()->GetPosition().x + 923, Camera::GetInstance()->GetPosition().y + 317, SaveManager::GetInstance()->opt.full);

	//fullscreenCheckBox.AddComponent<CheckBox>(menuButton_Pos[3].x, menuButton_Pos[3].y, false);
	//checkBox1.GetComponent<CheckBox>()->SetFunction(&ClickButton::Check1);
	m_AllUI[fullscreenCheckBox.name] = &fullscreenCheckBox;
	optionUI.push_back(&fullscreenCheckBox);

	//Audio Part
	Entity& audioText(EntityManager::GetInstance()->AddEntity("MenuGraphic"));
	audioText.AddComponent<Transform>(561, 380);
	audioText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuAudio", "DisposableDroidBB_bld", 40, white, "Audio");
	optionUI.push_back(&audioText);
	m_AllUI[audioText.name] = &audioText;

	Entity& masterVolume(EntityManager::GetInstance()->AddEntity("MenuMasterVolume"));
	masterVolume.AddComponent<Transform>(549, 437);
	masterVolume.AddComponent<Text>(TIME_COUNT_LAYER, "MenuMasterVolume", "DisposableDroidBB", 30, white, "Master Volume");
	optionUI.push_back(&masterVolume);
	m_AllUI[masterVolume.name] = &masterVolume;

	Entity& masterAdjuster(EntityManager::GetInstance()->AddEntity("MasterAdjuster"));
	masterAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 434, masterVolume_text, SaveManager::GetInstance()->opt.master);
	optionUI.push_back(&masterAdjuster);
	m_AllUI[masterAdjuster.GetComponent<Adjuster>()->leftButton->name] = masterAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[masterAdjuster.GetComponent<Adjuster>()->rightButton->name] = masterAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[masterAdjuster.name] = &masterAdjuster;
	optionUI.push_back(masterAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(masterAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(masterAdjuster.GetComponent<Adjuster>()->adjusterText);


	Entity& sfxVolume(EntityManager::GetInstance()->AddEntity("MenuSFXVolume"));
	sfxVolume.AddComponent<Transform>(549, 486);
	sfxVolume.AddComponent<Text>(TIME_COUNT_LAYER, "MenuSFXVolume", "DisposableDroidBB", 30, white, "SFX Volume");
	optionUI.push_back(&sfxVolume);
	m_AllUI[sfxVolume.name] = &sfxVolume;

	Entity& sfxAdjuster(EntityManager::GetInstance()->AddEntity("SFXAdjuster"));
	sfxAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 483, sfxVolume_text, SaveManager::GetInstance()->opt.sfx);
	optionUI.push_back(&sfxAdjuster);
	m_AllUI[sfxAdjuster.GetComponent<Adjuster>()->leftButton->name] = sfxAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[sfxAdjuster.GetComponent<Adjuster>()->rightButton->name] = sfxAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[sfxAdjuster.name] = &sfxAdjuster;
	optionUI.push_back(sfxAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(sfxAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(sfxAdjuster.GetComponent<Adjuster>()->adjusterText);


	Entity& musicVolume(EntityManager::GetInstance()->AddEntity("MenuSFXVolume"));
	musicVolume.AddComponent<Transform>(549, 535);
	musicVolume.AddComponent<Text>(TIME_COUNT_LAYER, "MenuMusicVolume", "DisposableDroidBB", 30, white, "Music Volume");
	optionUI.push_back(&musicVolume);
	m_AllUI[musicVolume.name] = &musicVolume;

	Entity& musicAdjuster(EntityManager::GetInstance()->AddEntity("MusicAdjuster"));
	musicAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 532, musicVolume_text, SaveManager::GetInstance()->opt.music);
	optionUI.push_back(&musicAdjuster);
	m_AllUI[musicAdjuster.GetComponent<Adjuster>()->leftButton->name] = musicAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[musicAdjuster.GetComponent<Adjuster>()->rightButton->name] = musicAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[musicAdjuster.name] = &musicAdjuster;
	optionUI.push_back(musicAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(musicAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(musicAdjuster.GetComponent<Adjuster>()->adjusterText);

	//Gameplay Part
	Entity& gameplayText(EntityManager::GetInstance()->AddEntity("MenuGraphic"));
	gameplayText.AddComponent<Transform>(562, 588);
	gameplayText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuGameplay", "DisposableDroidBB_bld", 40, white, "Gameplay");
	optionUI.push_back(&gameplayText);
	m_AllUI[gameplayText.name] = &gameplayText;

	Entity& screenShake(EntityManager::GetInstance()->AddEntity("MenuScreenShake"));
	screenShake.AddComponent<Transform>(551, 644);
	screenShake.AddComponent<Text>(TIME_COUNT_LAYER, "MenuScreenShake", "DisposableDroidBB", 30, white, "Screen Shake");
	optionUI.push_back(&screenShake);
	m_AllUI[screenShake.name] = &screenShake;


	Entity& screenShakeAdjuster(EntityManager::GetInstance()->AddEntity("screenShakeAdjuster"));
	screenShakeAdjuster.AddComponent<Adjuster>(Camera::GetInstance()->GetPosition().x + 859, Camera::GetInstance()->GetPosition().y + 639, screenShake_text, SaveManager::GetInstance()->opt.screenshake);
	optionUI.push_back(&screenShakeAdjuster);
	m_AllUI[screenShakeAdjuster.GetComponent<Adjuster>()->leftButton->name] = screenShakeAdjuster.GetComponent<Adjuster>()->leftButton;
	m_AllUI[screenShakeAdjuster.GetComponent<Adjuster>()->rightButton->name] = screenShakeAdjuster.GetComponent<Adjuster>()->rightButton;
	m_AllUI[screenShakeAdjuster.name] = &screenShakeAdjuster;
	optionUI.push_back(screenShakeAdjuster.GetComponent<Adjuster>()->leftButton);
	optionUI.push_back(screenShakeAdjuster.GetComponent<Adjuster>()->rightButton);
	optionUI.push_back(screenShakeAdjuster.GetComponent<Adjuster>()->adjusterText);

	Entity& speedrunClockText(EntityManager::GetInstance()->AddEntity("MenuSpeedrunClock"));
	speedrunClockText.AddComponent<Transform>(549, 690);
	speedrunClockText.AddComponent<Text>(TIME_COUNT_LAYER, "MenuSpeedrunClock", "DisposableDroidBB", 30, white, "Skip Dialogue");
	optionUI.push_back(&speedrunClockText);
	m_AllUI[speedrunClockText.name] = &speedrunClockText;

	Entity& speedRunClockCheckBox(EntityManager::GetInstance()->AddEntity("speedRunClockCheckBox"));
	speedRunClockCheckBox.AddComponent<CheckBox>(Camera::GetInstance()->GetPosition().x + 923, Camera::GetInstance()->GetPosition().y + 684, SaveManager::GetInstance()->opt.skipDialogue);
	optionUI.push_back(&speedRunClockCheckBox);
	m_AllUI[speedRunClockCheckBox.name] = &speedRunClockCheckBox;

	Entity& applyButton(EntityManager::GetInstance()->AddEntity("ApplyButton"));
	applyButton.AddComponent<Button>(Camera::GetInstance()->GetPosition().x + 934, Camera::GetInstance()->GetPosition().y + 765, true, "Apply", "ApplyButton", "ApplyButton2", 30);
	applyButton.GetComponent<Button>()->SetFunction(&ClickButton::ClickApplyButton);
	optionUI.push_back(&applyButton);
	m_AllUI[applyButton.name] = &applyButton;
	optionUI.push_back(applyButton.GetComponent<Button>()->textButton);



	undoButton.GetComponent<Button>()->nextUI[UP] = &applyButton;
	undoButton.GetComponent<Button>()->nextUI[DOWN] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	undoButton.GetComponent<Button>()->nextUI[RIGHT] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	//Left 1
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = &undoButton;
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = &undoButton;
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = &fullscreenCheckBox;
	resAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = resAdjuster.GetComponent<Adjuster>()->rightButton;

	//FullCheckBox
	fullscreenCheckBox.GetComponent<CheckBox>()->nextUI[UP] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	fullscreenCheckBox.GetComponent<CheckBox>()->nextUI[DOWN] = masterAdjuster.GetComponent<Adjuster>()->leftButton;
	//Left 2
	masterAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = &fullscreenCheckBox;
	masterAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = sfxAdjuster.GetComponent<Adjuster>()->leftButton;
	masterAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = masterAdjuster.GetComponent<Adjuster>()->rightButton;
	//Left 3
	sfxAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = masterAdjuster.GetComponent<Adjuster>()->leftButton;
	sfxAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = musicAdjuster.GetComponent<Adjuster>()->leftButton;
	sfxAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = sfxAdjuster.GetComponent<Adjuster>()->rightButton;
	//Left 4
	musicAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = sfxAdjuster.GetComponent<Adjuster>()->leftButton;
	musicAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = screenShakeAdjuster.GetComponent<Adjuster>()->leftButton;
	musicAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = musicAdjuster.GetComponent<Adjuster>()->rightButton;
	//Left 5
	screenShakeAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[UP] = musicAdjuster.GetComponent<Adjuster>()->leftButton;
	screenShakeAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = &speedRunClockCheckBox;
	screenShakeAdjuster.GetComponent<Adjuster>()->leftButton->GetComponent<AdjustArrow>()->nextUI[RIGHT] = screenShakeAdjuster.GetComponent<Adjuster>()->rightButton;

	//SpeedRunClock
	speedRunClockCheckBox.GetComponent<CheckBox>()->nextUI[UP] = screenShakeAdjuster.GetComponent<Adjuster>()->leftButton;
	speedRunClockCheckBox.GetComponent<CheckBox>()->nextUI[DOWN] = &applyButton;
	//Right 1
	resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = &undoButton;
	resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = &fullscreenCheckBox;
	resAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = resAdjuster.GetComponent<Adjuster>()->leftButton;
	//Right 2
	masterAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = &fullscreenCheckBox;
	masterAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = sfxAdjuster.GetComponent<Adjuster>()->rightButton;
	masterAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = masterAdjuster.GetComponent<Adjuster>()->leftButton;
	//Right 3
	sfxAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = masterAdjuster.GetComponent<Adjuster>()->rightButton;
	sfxAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = musicAdjuster.GetComponent<Adjuster>()->rightButton;
	sfxAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = sfxAdjuster.GetComponent<Adjuster>()->leftButton;
	//Right 4
	musicAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = sfxAdjuster.GetComponent<Adjuster>()->rightButton;
	musicAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = screenShakeAdjuster.GetComponent<Adjuster>()->rightButton;
	musicAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = musicAdjuster.GetComponent<Adjuster>()->leftButton;
	//Right 5
	screenShakeAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[UP] = musicAdjuster.GetComponent<Adjuster>()->rightButton;
	screenShakeAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[DOWN] = &speedRunClockCheckBox;
	screenShakeAdjuster.GetComponent<Adjuster>()->rightButton->GetComponent<AdjustArrow>()->nextUI[LEFT] = screenShakeAdjuster.GetComponent<Adjuster>()->leftButton;

	applyButton.GetComponent<Button>()->nextUI[UP] = &speedRunClockCheckBox;
	applyButton.GetComponent<Button>()->nextUI[DOWN] = &undoButton;
}

void UIManager::PressUndoPauseOption()
{
	DeleteOption();
	resolution_text.clear();
	optionUI.clear();
	pauseUI.clear();
	masterVolume_text.clear();
	sfxVolume_text.clear();
	musicVolume_text.clear();
	screenShake_text.clear();
	SpawnPauseUI();
	PressPauseMenu();
}

void UIManager::PressOptionPause()
{
	m_AllUI.clear();
	SpawnOptionPauseMenu();
	DeletePauseUI();
}

void UIManager::DeletePauseUI()
{
	for (auto& UI : pauseUI)
	{
		UI->Destroy();
	}
	pauseUI.clear();
}
