#include "SceneManager.h"
#include "LevelManager.h"
#include "EntityManager.h"
#include "InputManager.h"
#include "TransitionManager.h"

SceneManager* SceneManager::s_Instance = nullptr;

SceneManager::~SceneManager()
{
	delete scene;
}

void SceneManager::Initialize()
{
	//EntityManager::GetInstance()->ClearData();
	//initialize TransitionManager
	TransitionManager::GetInstance()->Initialize();
	//initialize LevelManager (this should be done only once)
	LevelManager::GetInstance()->Initialize();
	//initialize scene as a splash screen scene
	LoadScene(STARTING_SCENE);
}

void SceneManager::LoadScene(std::string sceneName)
{
	EntityManager::GetInstance()->ClearData();
	EntityManager::GetInstance()->DestroyInActiveEntities();
	//if scene is allocated
	if (scene != nullptr)
	{
		//check whether the previous scene is GameScene
		if (scene->name == GAME_SCENE)
		{
			//clean LevelManager tilemap
			LevelManager::GetInstance()->Clean();
			//set audio controller
			AudioManager::GetInstance()->musicPlayer.Stop();
			AudioManager::GetInstance()->musicPlayer.SetBool("isGameplay", false);
			AudioManager::GetInstance()->musicPlayer.SetInteger("LevelIndex", 0);
			AudioManager::GetInstance()->musicPlayer.Play();
		}
		else
		{
			if (scene->name == MENU_SCENE)
			{
				UIManager::GetInstance()->Clean();
			}
		}
		//delete scene memory allocated
		delete scene;
	}

	//Checking new scene name
	if (sceneName == MENU_SCENE)
	{
		//Change to MenuScene
		scene = new MenuScene();
		//-------------------************Load Game************--------------
		SaveManager::GetInstance()->Initialize();
	}
	else if (sceneName == GAME_SCENE)
	{
		//Change to GameScene
		scene = new GameScene();
		//Start the gameplay
		LevelManager::GetInstance()->StartGame();
	}
	else if (sceneName == SPLASH_SCENE)
	{
		//Change to SplashScene
		scene = new SplashScene();
	}
}

void SceneManager::Update(float deltaTime)
{
	//ONLY FOR TESTING
	if (InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_1))
	{
		LoadScene(MENU_SCENE);
	}
	else if (InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_2))
	{
		LoadScene(GAME_SCENE);
	}
	else if (InputManager::GetInstance()->GetKeyDown(SDL_SCANCODE_3))
	{
		LoadScene(SPLASH_SCENE);
	}

	//if current scene is GameScene
	if (scene->name == GAME_SCENE)
	{
		//Update Camera, PauseManager, LevelManager
		LevelManager::GetInstance()->Update(deltaTime);
	}
}

void SceneManager::Render()
{
	//if current scene is GameScene
	if (scene->name == GAME_SCENE)
	{
		//Render tilemap
		LevelManager::GetInstance()->Render();
	}
}

SceneManager* SceneManager::GetInstance()
{
	return s_Instance = (s_Instance != nullptr) ? s_Instance : new SceneManager();
}
