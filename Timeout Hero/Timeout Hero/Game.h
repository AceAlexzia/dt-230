#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "Entity.h"
#include "Component.h"
#include "EntityManager.h"
#include "InputManager.h"
#include "GameMap.h"
#include "AudioManager.h"

class AssetManager;

class Game 
{
    private:
        bool m_IsRunning;
        SDL_Window *m_Window;
		static Game* s_Instance;

		Game();

        //Game Loop
        void ProcessInput();
        void Update();
        void Render();

        //After exit game loop
        void Clean();

    public:
        static SDL_Renderer* m_Renderer;

        ~Game();
		static Game* GetInstance();

        void Initialize();
        void Loop();


        void Quit();
        void Destroy();

        bool IsRunning() const;
        SDL_Window* GetWindow();
};

#endif
