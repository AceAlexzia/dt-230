#include "TextButtonSaveSlot.h"

TextButtonSaveSlot::TextButtonSaveSlot(int x, int y, bool canClick, int size, std::string text, std::string font, SDL_Color color, bool selecting)
{
	buttonPos.x = x;
	buttonPos.y = y;
	available = canClick;
	textSize = size;
	if (selecting)
	{
		isSelecting = true;
	}
	buttonText = text;
	fontName = font;
	textColor = color;
}

TextButtonSaveSlot::~TextButtonSaveSlot()
{
	textButton->Destroy();
}

void TextButtonSaveSlot::Initialize()
{
	if (!owner->HasComponent<Transform>())
	{
		owner->AddComponent<Transform>(buttonPos.x, buttonPos.y);
	}

	Entity& text(EntityManager::GetInstance()->AddEntity("TextButton"));
	text.AddComponent<Transform>(owner->GetComponent<Transform>()->position.x, owner->GetComponent<Transform>()->position.y);
	text.AddComponent<Text>(TIME_COUNT_LAYER, "ButtonText" + owner->name, fontName, textSize, textColor, buttonText);

	textButton = &text;
	isSet = true;
}

void TextButtonSaveSlot::Update(float deltaTime)
{
	//--------------------------------- Will Opti in the future ---------------------------------
	if (isSet)
	{
		if (isSelecting)
		{
			textButton->GetComponent<Text>()->color = { 251,242,54,255 };
		}
		else
		{
			textButton->GetComponent<Text>()->color = textColor;
		}
		isSet = false;
	}
	
}

void TextButtonSaveSlot::SetFunction(void(*fn)(int i))
{
	m_FnPtr = fn;
}

void TextButtonSaveSlot::OnClick(int i)
{
	m_FnPtr(i);
}

Entity* TextButtonSaveSlot::SelectUI(int direction)
{
	return nextUI[direction];
}
