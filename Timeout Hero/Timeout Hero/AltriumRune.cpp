#include "AltriumRune.h"

AltriumRune::AltriumRune(bool isActive)
{
	m_IsActive = isActive;
}

AltriumRune::~AltriumRune() {}

void AltriumRune::Initialize()
{
	//assign all components as pointers
	m_Collider = owner->GetComponent<Collider>();
	m_Transform = owner->GetComponent<Transform>();
	m_Sprite = owner->GetComponent<Sprite>();
	m_Anim = owner->GetComponent<SpriteAnimation>();

	//set collider offset and postion
	m_Collider->SetBuffer(-80, -48, 160, 80);
	m_Collider->Set(m_Transform->position.x, m_Transform->position.y, m_Sprite->GetWidth(), m_Sprite->GetHeight());

	//set rune's animation state
	Update_Active();
}

void AltriumRune::Update(float deltaTime)
{
	//if rune is not active -> nothing to update
	if (!m_IsActive) 
	{
		return;
	}

	//if rune collides with Hero -> Hero enters getting rune state
	if (CollisionTriggerred()) 
	{
		m_Player->ReachRune();
	}

}


void AltriumRune::Update_Active()
{
	//if it's active -> use active animation
	if (m_IsActive) 
	{
		m_Anim->SetProps("altrium_rune_on", 6, 400);
	}
	//if it's not active -> use inactive animation
	else
	{
		m_Anim->SetProps("altrium_rune_off", 4, 200);
	}
}

