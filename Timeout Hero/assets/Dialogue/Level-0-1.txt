Artrius:Hi
Hero:hey?
Artrius:Welcome to the Castle of Darkness
Artrius:I am Artrius
Artrius:The one who revived you
Hero:Really?
Hero:Thanks a lot! That was so close
Artrius:don't have to thank me
Artrius:can't you see that you are cursed
Hero:what are you talking about?
Artrius:the time on your head
Hero:yes..? it's 60
Hero:What does that mean?
Artrius:you are cursed by Archfiend
Hero:Ummm...Archfiend?
Artrius:the big bad guy that captures the Princess
Hero:Ahhh... gotta defeat him somehow
Artrius:I've seen this curse before
Artrius:after you move...
Artrius:you will only have 60 seconds left to be alive
Artrius:don't let the time reaches 0 or you are dead
Hero:What??
Hero:My life only has 60 seconds left!??
Artrius:calm down...
Artrius:I have granted you an eternal life
Artrius:you will just warp back to the last Altrium Rune
Hero:What is it? What should I do now?
Artrius:Stop asking!
Artrius:it's pretty simple
Artrius:reach the next Altrium Rune within 60 seconds
Artrius:get the blue glowing orb for me and you will regain your lifetime back
Artrius:now reach the first Rune on your right hand side